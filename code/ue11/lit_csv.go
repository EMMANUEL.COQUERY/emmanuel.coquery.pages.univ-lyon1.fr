package main

import (
	"encoding/csv"
	"fmt"
	"io"
	"os"
	"strconv"
)

func valeurOk(v float64) bool {
	return 0.0 <= v && v <= 1.0
}

func main() {
	f, err := os.Open("monfichier.csv")
	if err != nil {
		fmt.Errorf("erreur de lecture")
	}
	r := csv.NewReader(f)
	for {
		record, err := r.Read()
		if err == io.EOF {
			break
		}
		val, _ := strconv.ParseFloat(record[2], 64)
		if !valeurOk(val) {
			fmt.Printf("Valeur erronée: %f", val)
		}
	}
}
