from sqlalchemy import Column, Integer, String, create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import scoped_session, sessionmaker

Base = declarative_base()


class Etudiant(Base):
    __tablename__ = "etudiant"

    numero = Column(Integer, primary_key=true)
    nom = Column(String)
    formation = Column(String)


session_factory = scoped_session(sessionmaker())
engine = create_engine("postgresql://orm:orm@localhost:5432/orm")
session_factory.configure(bind=engine)
