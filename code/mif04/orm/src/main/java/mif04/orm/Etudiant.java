package mif04.orm;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Etudiant {
  @Id
  private int numero;
  private String nom;
  private String formation;

  public int getNumero() {
    return numero;
  }

  public void setNumero(int numero) {
    this.numero = numero;
  }

  public String getNom() {
    return nom;
  }

  public void setNom(String nom) {
    this.nom = nom;
  }

  public String getFormation() {
    return formation;
  }

  public void setFormation(String formation) {
    this.formation = formation;
  }
}
