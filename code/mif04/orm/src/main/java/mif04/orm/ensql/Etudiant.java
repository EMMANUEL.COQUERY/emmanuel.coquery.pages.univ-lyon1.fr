package mif04.orm.ensql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

// create table etudiant(numero integer primary key, nom text, formation text);

public class Etudiant {

    private int numero;
    private String nom;
    private String formation;

    public Etudiant(int numero, String nom, String formation) {
        this.numero = numero;
        this.nom = nom;
        this.formation = formation;
    }

    public void save(Connection cnx) throws SQLException {
        PreparedStatement pstat = cnx.prepareStatement("INSERT INTO etudiant(numero, nom, formation) VALUES (?,?,?)");
        pstat.setInt(1, numero);
        pstat.setString(2, nom);
        pstat.setString(3, formation);
        pstat.executeUpdate();
    }

    public void update(Connection cnx) throws SQLException {
        PreparedStatement pstat = cnx.prepareStatement("UPDATE etudiant SET nom=?, formation=? WHERE numero=?");
        pstat.setString(1, nom);
        pstat.setString(2, formation);
        pstat.setInt(3, numero);
        pstat.executeUpdate();
    }

    public static Etudiant getByNumero(int numero, Connection cnx) throws SQLException {
        PreparedStatement pstat = cnx.prepareStatement("SELECT numero,nom,formation FROM etudiant WHERE numero=?");
        pstat.setInt(1, numero);
        ResultSet rs = pstat.executeQuery();
        if (rs.next()) {
            return new Etudiant(rs.getInt("numero"), rs.getString("nom"), rs.getString("formation"));
        } else {
            return null;
        }
    }
}