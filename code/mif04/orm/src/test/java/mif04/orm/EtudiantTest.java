package mif04.orm;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;

public class EtudiantTest {

  private static EntityManager em;

  @BeforeClass
  public static void setupEM() {
    em = Persistence.createEntityManagerFactory("pu-orm").createEntityManager();
  }

  @Test
  public void testInsert() {
    // EntityManager em =
    // Persistence.createEntityManagerFactory("pu-orm").createEntityManager();
    Etudiant e = new Etudiant();
    e.setNom("Toto");
    e.setFormation("m1info");
    e.setNumero(12345678);
    em.getTransaction().begin();
    em.persist(e);
    em.getTransaction().commit();
  }

  @After
  public void cleanup() {
    em.getTransaction().begin();
    for (Etudiant e : em.createQuery("SELECT e FROM Etudiant e", Etudiant.class).getResultList()) {
      em.remove(e);
    }
    em.getTransaction().commit();
  }
}
