import psycopg2

# create table etudiant(numero integer primary key, nom text, formation text);

conn = psycopg2.connect("dbname=ecoquery")
cur = conn.cursor()
cur.execute("SELECT * FROM etudiant WHERE formation='m1info'")
for row in cur.fetchall():
    nom = row[1]  # nom
    id = row[0]  # numero
    print("Etudiant: " + nom + " (" + str(id) + ")")
conn.close()


class Etudiant:
    def __init__(self, numero, nom, formation):
        self.numero = numero
        self.nom = nom
        self.formation = formation

    def save(self, cnx):
        cur = cnx.cursor()
        cur.execute(
            """
            INSERT INTO etudiant(numero,nom,formation)
            VALUES (%s,%s,%s)
            """,
            (self.numero, self.nom, self.formation),
        )

    def update(self, cnx):
        cur = cnx.cursor()
        cur.execute(
            """
            UPDATE etudiant
            SET nom=%s, formation=%s
            WHERE numero=%s
            """,
            (self.nom, self.formation, self.numero),
        )


def getEtudiantByNum(num, cnx):
    cur = cnx.cursor()
    cur.execute(
        """
        SELECT numero, nom, formation 
        FROM etudiant 
        WHERE numero=%s
        """,
        (num,))
    row = cur.fetchone()
    if row is not None:
        return Etudiant(row[0], row[1], row[2])
    else:
        return None


conn = psycopg2.connect("dbname=ecoquery")
e1 = Etudiant(45678901, "Machine", "m1info")
e1.save(conn)
e1.nom = "Machinette"
e1.update(conn)
e2 = getEtudiantByNum(e1.numero, conn)
print(e2.nom)
cur = conn.cursor()
cur.execute("""DELETE FROM etudiant WHERE numero=%s""", (e1.numero,))
conn.close()
