-- Création de la table de travail
CREATE TABLE s2016 (
	"Programme 2016" DECIMAL NOT NULL,
	"SIREN" VARCHAR NOT NULL,
	"NIC" VARCHAR,
	"Dénomination" VARCHAR NOT NULL,
	"Montant" DECIMAL NOT NULL,
	"Objet" VARCHAR NOT NULL,
	"Parlementaire" VARCHAR,
	"Réserve 2016" VARCHAR,
	"Convention 2016" VARCHAR,
	"COG : code département" VARCHAR,
	"COG : code commune" DECIMAL,
	"COG : ville ou pays" VARCHAR NOT NULL,
	"Nomenclature juridique" DECIMAL NOT NULL,
	"Code NAF" VARCHAR NOT NULL,
	"Situation SIRENE" VARCHAR,
	"RNA" VARCHAR
);

-- Remplissage de la table
\copy s2016 from projet-de-loi-de-finances-pour-2018-plf-2018-donnees-de-lannexe-jaune-effort-fin.csv csv delimiter ';' encoding 'utf-8' header;

-- Requête pour tester les valeurs non décimales d'un attribut
SELECT att 
from s2016v
WHERE att !~ '^[0-9]+(\.[0-9]+)?$'
LIMIT 5;

--- Siren
SELECT distinct "SIREN"
from s2016
WHERE siren !~ '^[0-9]+(\.[0-9]+)?$';
-- LIMIT 1;
-- Valeurs "N/C"
-- On met à jour avec un NULL
ALTER TABLE s2016 ALTER COLUMN "SIREN" DROP NOT NULL;
UPDATE s2016
SET "SIREN" = NULL
WHERE "SIREN" !~ '^[0-9]+(\.[0-9]+)?$';
-- On change le type
ALTER TABLE s2016 ALTER COLUMN "SIREN" TYPE NUMERIC USING "SIREN"::numeric;
-- Recréer la vue s2016v

--- NIC
SELECT distinct "NIC" 
from s2016
WHERE "NIC" !~ '^[0-9]+(\.[0-9]+)?$';

UPDATE s2016
SET "NIC" = NULL
WHERE "NIC" !~ '^[0-9]+(\.[0-9]+)?$';

ALTER TABLE s2016 ALTER COLUMN "NIC" TYPE NUMERIC USING "NIC"::numeric;

--- "Réserve 2016"
SELECT DISTINCT "Réserve 2016"
FROM s2016
WHERE "Réserve 2016" IS NOT NULL;
-- Expliciter les valeurs booléennes à venir
UPDATE s2016
SET "Réserve 2016" = 'TRUE' 
WHERE "Réserve 2016" IS NOT NULL;
UPDATE s2016
SET "Réserve 2016" = 'FALSE' 
WHERE "Réserve 2016" IS NULL;
-- Changement de type
ALTER TABLE s2016 ALTER COLUMN "Réserve 2016" TYPE boolean USING "Réserve 2016"::boolean;


-- Création d'une vue pour simplifier les requêtes
CREATE OR REPLACE VIEW s2016v AS
SELECT 	"Programme 2016" as programme,
	"SIREN" as siren,
	"NIC" as nic,
	"Dénomination" as denomination,
	"Montant" as montant,
	"Objet" as objet,
	"Parlementaire" as parlementaire,
	"Réserve 2016" as reserve,
	"Convention 2016" as convention,
	"COG : code département" as dpt,
	"COG : code commune" as com,
	"COG : ville ou pays" as villepays,
	"Nomenclature juridique" nomenclature,
	"Code NAF" as naf,
	"Situation SIRENE" as situation,
	"RNA" as rna
FROM s2016;

---- Schéma des données

-- Lignes identiques sauf pour le montant
SELECT *
FROM s2016v as a
JOIN s2016v as b
ON  (a.siren = b.siren or a.siren is null and b.siren is null)
AND a.programme = b.programme
AND (a.nic = b.nic or a.nic is null and b.nic is null)
AND a.denomination = b.denomination
AND a.objet = b.objet
AND (a.parlementaire = b.parlementaire or a.parlementaire is null and b.parlementaire is null)
AND a.reserve = b.reserve
AND (a.convention = b.convention or a.convention is null and b.convention is null)
AND (a.dpt = b.dpt or a.dpt is null and b.dpt is null)
AND (a.com = b.com or a.com is null and b.com is null)
AND a.villepays = b.villepays
AND a.nomenclature = b.nomenclature
AND a.naf = b.naf
AND (a.situation = b.situation or a.situation is null and b.situation is null)
AND (a.rna = b.rna or a.rna is null and b.rna is null)
WHERE a.montant < b.montant;


--- 

-- DF Attendues
--  siren, nic -> dpt 
SELECT a.siren, a.nic, a.dpt, b.dpt
FROM s2016v a
JOIN s2016v b ON a.siren = b.siren AND a.nic = b.nic
WHERE a.dpt < b.dpt;
-- fausse à priori pas une erreur

--  dpt, com -> villepays
SELECT a.dpt, a.com, a.villepays, b.villepays
FROM s2016v a
JOIN s2016v b ON a.dpt = b.dpt AND a.com = b.com
WHERE a.villepays < b.villepays;
-- satisfaite

--  siren, nic -> denomination
SELECT a.siren, a.nic, a.denomination, b.denomination
FROM s2016v a
JOIN s2016v b ON a.siren = b.siren AND a.nic = b.nic
WHERE a.denomination < b.denomination;


--  programme, siren, nic -> objet ???
--  siren, nic -> parlementaire ???
--  siren, nic -> situation
--  siren, nic -> nomenclature
--  siren, nic -> rna
--  rna -> siren, nic
