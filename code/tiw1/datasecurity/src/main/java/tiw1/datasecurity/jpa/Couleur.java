package tiw1.datasecurity.jpa;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Objects;

@Entity
public class Couleur {
  @Id
  @GeneratedValue
  private Long id;

  private String nom;
  private String description;
  private String colorCode;

  public Couleur() {
  }

  public Couleur(String nom, String description, String colorCode) {
    this.nom = nom;
    this.description = description;
    this.colorCode = colorCode;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getNom() {
    return nom;
  }

  public void setNom(String nom) {
    this.nom = nom;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getColorCode() {
    return colorCode;
  }

  public void setColorCode(String colorCode) {
    this.colorCode = colorCode;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Couleur couleur = (Couleur) o;
    return Objects.equals(id, couleur.id);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id);
  }
}
