package tiw1.datasecurity;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DatasecurityApplication {

	public static void main(String[] args) {
		SpringApplication.run(DatasecurityApplication.class, args);
	}

}
