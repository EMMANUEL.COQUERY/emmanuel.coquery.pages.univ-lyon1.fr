package tiw1.datasecurity.jpa;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface CouleurRepository extends JpaRepository<Couleur, Long> {

  Optional<Couleur> findByNom(String nom);

  @Query("select c from Couleur c where c.colorCode = ?1")
  Optional<Couleur> getByColorCode(String colorCode);

}
