package tiw1.datasecurity.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tiw1.datasecurity.jpa.Couleur;
import tiw1.datasecurity.jpa.CouleurRepository;

import java.util.Collection;
import java.util.Optional;

@RestController
@RequestMapping("/couleur")
public class CouleurController {

  @Autowired
  private CouleurRepository cR;

  @GetMapping
  public Collection<Couleur> lesCouleurs() {
    return cR.findAll();
  }

  @GetMapping(path = "/{id}")
  public ResponseEntity<Couleur> uneCouleur(@PathVariable Long id) {
    Optional<Couleur> c = cR.findById(id);
    if (c.isEmpty()) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    } else {
      return new ResponseEntity<>(c.get(), HttpStatus.OK);
    }
  }

  @PostMapping
  public ResponseEntity<Couleur> ajouteCouleur(@RequestBody Couleur c) {
    cR.save(c);
    return new ResponseEntity<>(cR.findById(c.getId()).get(), HttpStatus.OK);
  }
}
