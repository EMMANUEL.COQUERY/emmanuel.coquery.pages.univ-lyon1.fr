package tiw1.datasecurity;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import tiw1.datasecurity.jpa.Couleur;
import tiw1.datasecurity.jpa.CouleurRepository;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class DatasecurityApplicationTests {

    private static Logger LOG = LoggerFactory.getLogger(DatasecurityApplicationTests.class);

    @Autowired
    private CouleurRepository couleurRepository;

    @Test
    void contextLoads() {
        assertNotNull(couleurRepository);
        LOG.info("Classe d'implementation de CouleurRepository: {}", couleurRepository.getClass());
    }

    @Test
    void testCouleurRepository() {
        Couleur c = new Couleur("vert", "basique", "00FF00");
        couleurRepository.save(c);
        Optional<Couleur> c2 = couleurRepository.findById(c.getId());
        assertTrue(c2.isPresent());
        assertEquals(c, c2.get());
    }

    @Test
    void testMethodeGenereeParNom() {
        Couleur c = new Couleur("bleu", "basique", "0000FF");
        couleurRepository.save(c);
        Optional<Couleur> c2 = couleurRepository.findByNom(c.getNom());
        assertTrue(c2.isPresent());
        assertEquals(c, c2.get());
    }

    @Test
    void testRequeteJPQL() {
        Couleur c = new Couleur("rouge", "basique", "FF0000");
        couleurRepository.save(c);
        Optional<Couleur> c2 = couleurRepository.getByColorCode(c.getColorCode());
        assertTrue(c2.isPresent());
        assertEquals(c, c2.get());
    }
}
