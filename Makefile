#h
#h Makefile script for managing hugo builds **in the right version**
#h

HUGO_VERSION=0.125.6
ARCHI=darwin-universal
REGISTRY=harbor.pagoda.os.univ-lyon1.fr
TAG_BASE=${REGISTRY}/ecoquery-hugo/hugo
TAG_VERSION=${TAG_BASE}:${HUGO_VERSION}
TAG_LATEST=${TAG_BASE}:latest

default: view

#h make view (default target)
#h    runs a local server
#h
view: hugo
	./hugo server
# check these options: --disableFastRender --i18n-warnings

hugo:
	curl -L https://github.com/gohugoio/hugo/releases/download/v${HUGO_VERSION}/hugo_extended_${HUGO_VERSION}_${ARCHI}.tar.gz -o hugo.tgz
	tar xzf hugo.tgz hugo
	rm hugo.tgz

#h make docker-image
#h    builds docker image for running hugo
#h
docker-image:
	docker build --build-arg HUGO_VERSION=${HUGO_VERSION} -t ${TAG_VERSION} -t ${TAG_LATEST} .

#h make docker-image
#h    builds docker and push docker image for running hugo
#h
docker-push: docker-image
	docker login $REGISTRY
	docker push $TAG_VERSION
	docker push $TAG_LATEST

#h make help
#h    prints this help
#h
help:
	@sed -ne 's/^[#]h\(.*\)$$/\1/p' Makefile

.PHONY: view docker-image docker-push help default
