FROM --platform=linux/amd64 ubuntu:20.04

RUN apt update && DEBIAN_FRONTEND=noninteractive apt install -y curl golang git
ARG HTTP_PROXY
ARG HTTPS_PROXY=${HTTP_PROXY}
ARG HUGO_VERSION=0.88.1
ARG HUGO_BINARY=hugo_extended_${HUGO_VERSION}_linux-amd64.tar.gz
RUN curl -L -o /tmp/hugo.tgz https://github.com/gohugoio/hugo/releases/download/v$HUGO_VERSION/$HUGO_BINARY
RUN tar xzf /tmp/hugo.tgz hugo && mv hugo /usr/local/bin
RUN mkdir /work
WORKDIR /work
