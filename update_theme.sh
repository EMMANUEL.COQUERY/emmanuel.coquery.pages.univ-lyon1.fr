#!/usr/bin/env bash

cd $(dirname $0)
hugo mod get -u ./...
hugo mod tidy
