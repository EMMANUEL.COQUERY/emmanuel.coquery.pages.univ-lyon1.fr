---
title: Introduction à l'analyse des données - séance 3
summary: "Séance 3 - requêtes avancées"
authors: [admin]
tags: [code]
categories: [cours]
slides:
  theme: "white"
  highlight_style: "github"
---

## Introduction à l'analyse de données

### Séance 3 - requêtes avancées

[Emmanuel Coquery](https://perso.liris.cnrs.fr/emmanuel.coquery/home)

<https://perso.liris.cnrs.fr/emmanuel.coquery/home/enseignement/ue8/>

---

## Vu précédement

- Données tabulaires
- Pandas pour charger des données
- Dataframes, Series, index
- Filtrer les données (colonnes, lignes)

---

## Exercices de la séance 2

- Donner le nombre de sujets.
- Donner le nombre d'essais pour lesquels le deuxième prénom est Justin.
- Donner le nombre d'essais ayant un temps de réponse supérieur à 20000 ms.
- Donner la moyenne des temps de réponse lorsque le prénom joué en premier est celui du sujet.
- Les sujets ayant au moins une fois répondu en plus de 50000 ms.

---

## Calculer

Expressions où certaines valeurs sont des Series

- les Series doivent utiliser le même index
- produit une Series
  - avec le même index
- chaque ligne résultat est obtenue
  - en évaluant l'expression
  - les Series de l'expression sont remplacées
    par leur valeur pour cette ligne

---

## Exemples

```python
  df['RT'] / 1000
```

```python
  df['Dist_A'] - df['Dist_B']
```

---

## Ajouter une colonne

Faire une affectation sur la colonne à créer

- notation `['macolonne']` pour indiquer
  le nom de la nouvelle colonne
- la valeur rangée est une Series

Exemple

```
df['diff_dist'] = df['Dist_A'] - df['Dist_B']
```

Attention: modifie la Dataframe

---

## Combiner des Dataframes

### Concaténation

- ```
  .concat([df1,df2,df3])
  ```
  - concaténation des lignes
  - indexes avec valeurs différentes
- ```
  .concat([df1,df2,df3],
          axis=1, join="inner")
  ```
  - concaténation des colonnes
  - même index

---

## Exercice

- Découper les données en 2:
  - une Dataframe pour les essais où le temps de réponse est plus petit ou égal à 2 s
  - une Dataframe pour les essais où le temps de réponse est plus grand que 2 s
- Réassembler les Dataframes via concat
- Que dire de l'ordre de l'index ?
