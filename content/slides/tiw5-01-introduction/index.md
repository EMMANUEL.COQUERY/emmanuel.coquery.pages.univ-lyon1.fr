---
title: Projet TIW5
summary: "Démarrage du projet TIW5"
authors: [admin]
tags: [code]
categories: [cours]
date: "2020-09-21T00:00:00Z"
slides:
  theme: "white"
  highlight_style: "github"
---

# Projet TIW5

## Kickoff

[Emmanuel Coquery](mailto:emmanuel.coquery@univ-lyon1.fr)

---

## Principes

- Similaire à MIF10, mais modalités différentes
- 3 équipes à déterminer parmi les FI
- 3 sujets proposés, un sujet par équipe

---

## Equipes: contraintes de formation

- Intégrer un nombre équilibré de
  - personnes provenant du M1 Info UCBL
  - personnes venant de l'extérieur
- Les étudiants suceptibles de basculer en ALT se répartissent dans les différents groupes

---

## Sujets

1. Sympozer (L. Medini)
2. Qualité du catalogage à la BU (F. Duchateau)
3. Intégrations d'application dans le SI du département (E.Coquery)

---

## Sympozer

https://sympozer.liris.cnrs.fr/

---

## Qualité du catalogage à la BU

Chaque nouvelle œuvre (livre, DVD, thèse, etc.) est cataloguée par la BU selon des normes nationales : besoin d'un **contrôle qualité**

---

## Qualité du catalogage à la BU

### Objectifs

- vérification automatique de règles (ex, présence de champs obligatoires, cohérence des informations renseignées)
- utilisation de services distants (ex, ABES)
- détection des mentions d'entités non référencées

---

<img src="CheckSUDOC2.JPG" width="70%">

---

## Intégrations d'applications

- Connecter des applications au SI du département
- Exemples:
  - lier SonarQube à gitlab
  - lier Harbor à gitlab

---

## Intégrations d'applications

### Comment ?

- utilisation des webhooks, des API REST et de LDAP
- application REST (pour les webhooks)
- interface d'administration

---

## Planning

- semaine du 21 sept. : définission des équipes et choix des sujets
- mercredi 30 sept. : présentation des propositions commerciales
- mercredi 28 oct. : comité de pilotage
- mercredi 25 nov. : comité de pilotage
- mercredi 16 déc. ?? : comité de pilotage
- mercredi 27 jan. : comité de pilotage
- mardi 23 ou mercredi 24 fév. : soutenances

---

## Exemples de supports

[Proposition commerciale](https://perso.liris.cnrs.fr/rthion/files/Enseignement/TIW5/Daniela_TSAMALAX_Cours%20gestion%20projet_CM1%20-%20Exemple%20proposition_V2.0.pdf)

[Comité de pilotage](https://perso.liris.cnrs.fr/rthion/files/Enseignement/TIW5/Daniela_TSAMALAX_Cours%20gestion%20projet_CM3%20-%20Exemple%20support%20copil.pdf)

---

## Budget alloué

1.  compter les demies-journées pour chaque période, ~6 périodes avec ~5 1/2 journées
2.  multiplier par 3.5h par demie-journée de travail,
3.  appliquer un ratio (par exemple) 1.33,
4.  on multiplie par le nombre de personnes dans l'équipe.

À répartir dans les différentes tâches du projet.
