---
title: Introduction à l'analyse des données - séance 1
summary: "Séance 1 - introduction"
authors: [admin]
tags: [code]
categories: [cours]
date: "2023-09-12T00:00:00Z"
slides:
  theme: "white"
  highlight_style: "github"
---

# Introduction à l'analyse de données

## Séance 1 - introduction

[Emmanuel Coquery](https://perso.liris.cnrs.fr/emmanuel.coquery/home)

https://perso.liris.cnrs.fr/emmanuel.coquery/home/enseignement/ue8/

---

## Données

- Représentation informatique d'informations
- Même information $\leftrightarrow$ plusieurs représentations
- Information peut être explicite ou implicite dans des données

---

### Exemple de données

| Nom     | Age |
| ------- | --- |
| Alice   | 8   |
| Bernard | 64  |

Quelques informations:

- Alice a 8 ans
- Alice est plus jeune que Bernard
- La moyenne d'âge est de 36 ans

---

### Structuration des données

- connaître la forme des données
  - aide à les comprendre
  - nécessaire pour les exploiter
- données complexes = assemblage de données plus simples

---

### Données atomiques

- les plus simples
- une seule valeur
- ont un type qui permet de les classifier et de les interpréter

---

### Exemples de données atomiques

- `8` : int ou integer représente entier
- `8.0` : float représente un nombre à virgule
- `"Alice"` ou `'Alice'`: str ou string, représente le texte entre les guillemets

---

### Données tabulaires

- Organisation de données en tableau
- Les cases contiennent des données atomiques
- Souvent les lignes représentent des "objets" et les colonnes des caractéristiques

---

### Exemple de données tabulaires

| Nom     | Age |
| ------- | --- |
| Alice   | 8   |
| Bernard | 64  |

---

## (Rappels de) Python

- Langage de programmation
- Très utilisé en analyse de données

Exemple de notebook

---

### Expressions

- calcul
  - valeurs
  - opérateurs
  - fonctions
- simples: expressions arithmétiques
- complexes: utilisation de fonctions et de valeurs complexes

---

### Types

- "Classification" des valeurs
- Pour commencer:
  - `int` (nombres sans virgule, "entiers")\
    `1`, `2358`, etc
  - `float` (nombres à virgule ≠ réels)\
    `0.2`, `56.32`, `12.3E10`, etc
  - `str` (texte, chaînes de caractères, string)\
    `"Bonjour"`, `"Emmanuel"`, etc
  - `bool` (booléens, i.e. vrai ou faux)\
    `True`, `False`

---

### Quelques opérateurs prédéfinis

$int\times int\rightarrow int$

- `+`, `-`, `*`,
- `//` (division entière)
- `**` (puissance)

$\Rightarrow$ faire 2/3 calculs d'`int` dans l'interpéteur Python

---

### Quelques opérateurs prédéfinis

$float\times float\rightarrow float$

- `*`, `-`, `*`, `/`, `**`

$\Rightarrow$ faire 2/3 calculs de `float` dans l'interpéteur Python

---

### Quelques opérateurs prédéfinis

$str\times str\rightarrow str$

- `+`
- "opérateurs" postfixés: `.upper()`, `.lower()`

$\Rightarrow$ essayer de prédire l'effet de ces opérateurs sur des `str`, puis vérifier dans l'interpréteur

---

### Quelques opérateurs prédéfinis

$bool\times bool\rightarrow bool$

| x     | y     | x `and` y | x `or` y | `not ` x |
| ----- | ----- | --------- | -------- | -------- |
| True  | True  | True      | True     | False    |
| True  | False | False     | True     | False    |
| False | True  | False     | True     | True     |
| False | False | False     | False    | True     |

---

### Expressions booléennes

Prédire puis vérifier le résultats des expressions suivantes:

- True or (True and False)
- (False and False) or not (True and False)
- False and not False
- True and not True

---

### Mélanges de types

En utilisant l'interpréteur, deviner le type résultat des opérateur suivants en fonction du type de leurs arguments

- `+` et `*` avec $int\times float$
- `/` avec $int\times int$
- `+` avec $int\times str$
- `*` avec $int\times str$

---

### Comparaisons

Opérateurs de comparaison

`==`, `<`, `>`, `<=`, `>=`

- pour le str: ordre alphabétique basé sur l'ordre des caractères en UTF-8
  - regarder ce qui se passe dans l'interpréteur avec des accents et/ou des majuscules
- regarder `<` entre booléens, essayer de le recoder avec les autres opérateurs

---

### Conversion de type

Convertir vers un type:

`int(...)`, `float(...)`, `str(...)`, `bool(...)`

Typer chaque sous-expression, évaluer à la main, puis vérifier le résultat de

`int(3*"3") // 111 == 3`

---

### Fonctions additionnelles: module `math`

- commande\
  `import math`
- donne accès à `math.sin(...)`, `math.floor(...)`, `math.pi`

---

### Bibliothèque standard Python

https://docs.python.org/3.9/library/index.html

---

## Instructions

Instruction = action à réaliser

Modifications de l'environnement

---

### Environnement ?

- physique
- système
- **mémoire du programme**

---

### Variables

- Emplacement ($\approx$ case) mémoire
- Contient une valeur
  - on peut placer une valeur dans une variable
  - on peut aller chercher la valeur d'une variable

Brique de base de la programmation

---

### Affectation

Range une valeur dans une variable

Syntaxe

`ma_variable = une expression`

Exemple

```
sous_total = 5 + 3 * 12
```

---

### Récupérer la valeur d'une variable

On utilise simplement le nom de la variable dans une expression

Exemple

```
total = sous_total + 7
```

---

### Afficher

`print(...)`

- instruction qui affiche son argument
- c'est en fait une fonction au sens informatique

Exemple

```
print("Le carré de 3 est " + str(3**2))
```

- Remarque: en mode interactif, l'interpréteur exécute les instructions normalement. Si on tape une expression, l'interpréteur exécute en fait un print de cette expression

---

### Script ou programme Python

- fichier contenant une suite d'instructions
- le nom du fichier fini par `.py`\
  (attention dans l'explorateur de fichiers)
- syntaxe, pour le moment:
  - faire attention à commencer les instructions en début de ligne
  - les instruction sont écrites sur une seule ligne

---

### Commentaires

- texte qui n'est pas interprété
- utile pour documenter le programme
- syntaxe: tout ce qui suit le caractère `#`

exemple

```
# un commentaire seul sur une ligne
print("Salut") # cette instruction affiche Salut
```

---

### Écrire et exécuter un premier programme

```
import math
a = 1
b = -12
c = 35
delta = b ** 2 - 4 * a * c
# il faudrait tester delta ...
x1 = (-b + math.sqrt(delta)) / (2 * a)
x2 = (-b - math.sqrt(delta)) / (2 * a)
poly = str(a)+" x^2 + "+str(b)+" x + "+str(c)
print("Les solutions de "+poly+" sont "+str(x1)+" et "+str(x2))
```

---

### Exécuter le programme précédent

- à la main en essayant de prévoir l'effet de chaque instruction
- en copiant/collant directement dans l'interpréteur
- en enregistrant comme un script, puis en exécutant ce script
  - depuis l'éditeur
  - éventuellement directement depuis la ligne de commande

---

### Affectations, suite

- une variable est créée à sa première affectation
  - lire une variable inexistante provoque une erreur:
    ```
    print(toto+2) # si toto n'a jamais été affecté
    ```
- _**changer**_ la valeur d'une variable: refaire une affectation dessus
  - il est possible d'utiliser la valeur (ancienne) d'une variable dans le calcul de la (nouvelle) valeur qui lui est affectée

---

### Exemple d'affectation successives

script `boulangerie.py`

```
prix_bombon = 0.10
prix_croissant = 1.0
prix_pain = 1.2
total = prix_bombon * 8
total = total + prix_croissant * 5
total = total + prix_pain * 2
print("Je dois "+str(total)+" euros")
```

- Exécuter ce script, puis une version modifiée qui affiche la valeur de total après les deux premières affectations.
