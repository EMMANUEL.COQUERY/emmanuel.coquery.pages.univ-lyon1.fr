---
title: Introduction à l'analyse des données - séance 2
summary: "Séance 2 - jupyter & pandas"
authors: [admin]
tags: [code]
categories: [cours]
slides:
  theme: "white"
  highlight_style: "github"
---

## Introduction à l'analyse de données

### Séance 2 - jupyter & pandas

[Emmanuel Coquery](https://perso.liris.cnrs.fr/emmanuel.coquery/home)

<https://perso.liris.cnrs.fr/emmanuel.coquery/home/enseignement/ue8/>

---

## Accès à la plateforme

Serveur Jupyter de l'UCBL1

<https://jupyter.univ-lyon1.fr>

- choisir un serveur (e.g. Serveur M2)
- se connecter avec les identifiants de l'université
- type de serveur: choisir Jupyter Lab puis Start
- choisir un fichier ou créer un notebook Python 3

---

## Prise en main

Reprendre et exécuter dans Jupyter les petits exemples
Python de la [séance 1]({{< ref "/slides/ue8-seance1-introduction" >}}).

Durant la séance utiliser les cellules "Markdown" pour prendre des notes.

---

## Données de travail

[Télécharger](https://filesender.renater.fr/?s=download&token=9841f77e-45f0-451c-826a-f56b93584e14)

avant le 05/10/2023

(mot de passe fourni en cours)

Déposer le fichier excel dans l'espace jupyter

- Renommer le fichier pour supprimer l'accent et remplacer les espaces par `_`

---

## Pandas

Bibliothèque de manipulation de données tabulaires

2 structures principales

- Dataframe: tableau à plusieurs colonnes
- Series: tableau à 1 dimension

[Documentation](https://pandas.pydata.org/docs/user_guide/index.html)

---

## En pratique

- Une cellule pour charger pandas

  ```
  import pandas
  ```

- Une cellule pour charger dans un Dataframe le contenu du fichier excel:

  ```
  df = pandas.read_excel("Donnees_M2_RD.xlsx")
  df
  ```

- Faire le lien entre les colonnes et la notice en pdf

---

## Index

- Valeur permettant d'identifier une ligne
- exemples:
  - numéro (pas forcément consécutif)
  - date
  - string

Quelles valeurs sont utilisées comme index dans le Dataframe précédent ?

---

## Colonnes d'un Dataframe (projection)

```
madataframe['macolonne']
madataframe[['macolonne','monautrecolonne']]
```

On obtient une `Series`

- Afficher (indépendamment) chacune des colonnes du Dataframe exemple.
- Que peut-on dire sur les index des séries obtenues?

---

## Valeurs uniques

Valeurs uniques dans une série:

```
maserie.drop_duplicates()
```

- Vérifier les différentes valeurs des différentes colonnes du jeu de données.
- Que dire des index ?

---

## Quelques statistiques simples

Sur une Series / Dataframe, on peut extraire des statistiques via:

- `.min()`, `.max()`, `.count()`
- `.mean()`, `.std()` (écart type), `.quantile(q)`

---

## Quelques statistiques simples (suite)

- Comparer l'effet de `.min()` sur la colonne `RT` et sur toute la Dataframe.
- Utiliser `quantile` avec comme argument la valeur `0.25`.
- Utiliser `quantile` avec comme argument le tableau `[0.25,0.5,0.75]`.

---

## Filtrer les données (sélection)

Ne garder que les lignes _intéressantes_

- principe: exprimer qu'une line est intéressante via une _condition_
  - porte sur la/les valeur(s) de la ligne
- si la condition s'évalue à vrai: on garde la ligne

---

## Series booléennes dans Pandas

Exemple

```
maserie >= 12000
```

- tester avec la Series de la colonne `RT`
- expliquer l'effet de la comparaison sur la série

On peut utiliser `==`, `>`, `<`, `>=`, `<=`

---

## Sélection dans une Dataframe

- Series boolenne avec index commun à une Dataframe
- On conserve dans la Dataframe les index qui correspondent à True dans la Series

syntaxe

```
madataframe[seriesbool]
```

---

## Exemple

- Placer la Series booléenne précédente dans une variable
- Utiliser cette série pour filtrer la Dataframe de départ
- Obtenir le même résultat sans passer par une variable intermédiaire

---

## Conditions plus complexes

- On peut combiner deux Series booléennes via\
  `&` (et), `|` (ou) et `~` (non)
- Attention: mettre des parenthèses autour des comparaisons, par exemple

  ```
  (df['colonne1'] > 12) | (df['colonne2'] == 5)
  ```

---

## Exemple

- Créer une Series booléenne vraie pour les index correspondant à un `Name_A` qui vaut 0 et un RT supérieur à 14000
- Utiliser la Series pour filtrer la Dataframe et vérifier le résultat

---

## Combiner les calculs

- Une sélection sur une Dataframe produit une Dataframe
- Une projection produit une Dataframe ou une Series
- On peut utiliser les opérations vues jusqu'ici sur la nouvelle Dataframe/Series

---

## Exercices

- Donner le nombre de sujets.
- Donner le nombre d'essais pour lesquels le deuxième prénom est Justin.
- Donner le nombre d'essais ayant un temps de réponse supérieur à 20000 ms.
- Donner la moyenne des temps de réponse lorsque le prénom joué en premier est celui du sujet.
- Les sujets ayant au moins une fois répondu en plus de 50000 ms.
