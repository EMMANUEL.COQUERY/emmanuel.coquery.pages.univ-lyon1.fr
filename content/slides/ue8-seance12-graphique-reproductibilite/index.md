---
title: Introduction à l'analyse des données - séance 12
summary: "Séance 12 - Figure - Reproductibilité"
authors: [admin]
tags: [code]
categories: [cours]
slides:
  theme: "white"
  highlight_style: "github"
---

## Introduction à l'analyse de données

### Séance 12 - Figures - Reproductibilité

[Emmanuel Coquery](https://perso.liris.cnrs.fr/emmanuel.coquery/home)

<https://perso.liris.cnrs.fr/emmanuel.coquery/home/enseignement/ue8/>

---

## Vu précédement

- Données tabulaires
- Pandas, Dataframes, Series, indexes
- Filtrer les données (colonnes, lignes)
- Combiner des données, (concat, jointure, aggrégation)
- Figures (2D, 3D)
- Indexation à plusieurs niveaux, cubes
- SQL

---

## Personnalisation de figures

---

## Rappel Line plot en Matplotlib

```python
import matplotlib.pyplot as plt
import matplotlib as mpl
```

```python
ma_serie = (ma_dataframe
            .set_index("colonne_x")
            .sort_index()
            ["colonne_y"])
```

```python
fig, ax = plt.subplots()
ax.plot(ma_serie)
```

---

## Légendes abcisse / ordonnée

```python
ax.set_xlabel("ma légende sur les abscisses")
ax.set_ylabel("ma légende sur les ordonnées")
ax.plot(...)
```

---

## Échelle abcisse / ordonnée

```python
ax.axis([x_min, x_max, y_min, y_max])
ax.plot(...)
```

---

## Styles de lignes

```python
ax.plot(ma_serie, color="green", linestyle=":")
```

- couleurs:
  - par nom ([réf](https://en.wikipedia.org/wiki/X11_color_names))
  - par valeurs RVB dans [0, 1], e.g. `(0.1, 0.2, 0.5)`
  - par valeurs RVB en hexadécimal, e.g. `#c2a8f3`
- linestyles: `'-'`, `'--'`, `'-.'`, `':'`, ...

---

## Lignes multiples et légendes

```python
ax.plot(ma_serie1, label="Série 1")
ax.plot(ma_serie2, label="Série 2")
ax.legend()
```

---

## Exercice

Tracer une figure du temps de réaction moyen de selon la différence de distance symbolique
avec deux courbes: une pour les essais réussis et une pour pour les échecs.

---

## Science ouverte & reproductibilité

---

## Reproduire pour vérifier

- erreurs
  - de manipulation
  - de calcul
  - d'interprétation
  - de méthode

---

## Pour reproduire

- données (ouvertes)
- codes de calcul (ouverts)
  - de l'acquisition
  - à la production des éléments interpétés:
    - mesures statistiques
    - tableaux
    - figures

---

## Données ouvertes (FAIR)

- Findable (trouvables, donc bien référencées)
- Accessible (suffisement faciles à récupérer)
- Interoperable (dans un format exploitable, avec une interprétation claire)
- Reusable (licence d'utilisation, description des données, de leur provenance)

---

## Codes de calcul pour la reproductibilité

- Code accessible (e.g. référencé sur [Software Heritage](https://www.softwareheritage.org/?lang=fr)), avec une licence ouverte
- Code compréhensible (noms clair, commentaires, simplicité de lecture)
- Environnement logiciel maîtrisé et bien décrit (versions de langage, de bibliothèque, système)
