---
title: Introduction à la programmation - séance 4
summary: "Séance 4 - structures de contrôle (suite)"
authors: [admin]
tags: [code]
categories: [cours]
date: "2022-11-15T00:00:00Z"
slides:
  theme: "white"
  highlight_style: "github"
---

## Introduction à la programmation d'expériences

### Séance 4 - boucles - chaînes de caractères

[Emmanuel Coquery](https://perso.liris.cnrs.fr/emmanuel.coquery/home)

https://perso.liris.cnrs.fr/emmanuel.coquery/home/enseignement/ue11/

---

## Récapitulatif

- expressions
- types
- instructions (affectation, print)
- script
- conditionnelles

---

### Boucles

Répétition d'un bloc

- `for`: boucle pour "parcourir" un ensemble de valeurs
- `while`: boucle qui est répétée tant qu'une condition est vraie

---

### Boucle for

Syntaxe

```
for ma_variable in range(debut,fin):
  instruction_1
  instruction_2
  ...
```

- rmq: la vraie syntaxe est plus générale

---

### Boucle for

Sémantique

- range représente tous les int compris entre
  - debut inclus
  - fin excluse
- le bloc est exécuté pour chaque valeur représentée par range
  - la valeur concernée est rangée dans ma_variable pour chaque "tour" de boucle

---

### Exemple: boucle for

`somme.py`

```
total = 0
for n in range(1,10):
  total = total + n
print("La somme des entiers de 1 à 9 est "+str(total))
```

- Exercice: modifier ce programme pour
  - saisir un nombre
  - calculer la factorielle de ce nombre

---

### Boucle while

Syntaxe

```
while expr_cond:
  instruction_1
  instruction_2
  ...
```

---

### Boucle while

Sémantique

- La boucle est répétée tant que cond_expr s'évalue à True
- Seule la modification de l'environnement peut arrêter la boucle
  - souvent une modification des variables

---

### Exemple boucle while

racine.py

```
x = 27
r = 0

while r * r <= x:
  r = r + 1

r = r - 1 # pourquoi ?

print("La racine carrée entière de "+str(x)+" est "+str(r))
```

---

### Exercice

Pliage

- Écrire un programme qui détermine combien de fois il faut plier une feuille de 1 mm d'épaisseur pour obtenir une épaisseur d'1 m (en imaginant qu'on peut plier la feuille autant de fois que nécessaire)

---

### Structures imbriquées

- Il est possible d'imbriquer des structures de contrôle les unes dans les autres
- On a déjà vu des imbrication avec des `if`
- Il est possible d'imbriquer
  - des `if` dans des boucles
  - des boucles dans d'autres boucles
  - des boucles dans des `if`

---

### Exercices

Écrire des programmes pour

- Afficher les carrés d'entiers pairs entre 0 et 10.
- Donner l'entier entre 0 et 20 dont le sinus a la plus grande valeur.
- Calculer les $k$ premiers nombres premiers

---

## Chaînes de caractères

Caractères; sous-chaînes et positions

---

### Positions et sous-chaînes

Accéder à un morceau de chaîne

Exemple: `"Gâteau au chocolat"`

`"Gâteau"` est la sous-chaîne contenant les 6 premières lettres (on dit caractère)

---

### Positions dans une chaîne

- La première position est 0
- La taille de la chaîne est donnée par la fonction `len`
- La dernière position dans `s` est donc `len(s)-1`

---

### Sous-chaînes

Opérateur `[]` extrait une sous-chaîne

- `s[d:f]` est la sous-chaîne de s qui commence à la position `d` et fini à la position `f-1`\
  (i.e. la position `f` exclue)

Exemple

```
In [1]: "abcdef"[2:4]
Out[1]: 'cd'
```

- Essayer avec des positions négatives ou plus grandes que la longueur

---

### Caractère à la position i

Opérateur `[]` sans `:`

- `s[i]` est le caractère à la position `i`
- valeur renvoyée comme une chaîne de taille 1

Exemple

```
In [2]: "abcdef"[2]
Out[2]: 'c'
```
