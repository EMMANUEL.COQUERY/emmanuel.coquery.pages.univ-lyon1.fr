---
title: Introduction à la programmation - séance 5
summary: "Séance 5 - tableaux"
authors: [admin]
tags: [code]
categories: [cours]
date: "2023-11-29T00:00:00Z"
slides:
  theme: "white"
  highlight_style: "github"
---

## Introduction à la programmation d'expériences

### Séance 5 - tableaux

[Emmanuel Coquery](https://perso.liris.cnrs.fr/emmanuel.coquery/home)

<https://perso.liris.cnrs.fr/emmanuel.coquery/home/enseignement/ue11/>

---

## Récapitulatif

- expressions
- types
- instructions (affectation, print)
- script
- conditionnelles, boucles
- chaînes de caractères (str)

---

## Structures de données

### Tableaux

---

## Structure de **données** ?

- Donnée complexe contenant des données plus simples
  - Exemple: une `string` contient des caractères
- ⚠️ structure de donnée ≠ structure de contrôle ⚠️

---

## Tableaux

- Fonctionnement par position similaire aux `string`
- Peut contenir tout type de données:
  - `int`, `float`, `bool`, `string`
  - d'autres tableaux
- Opérateur `[]` similaire aux `string`
- Type: `list`
- Une case de tableau est ce qui contient la valeur à une position du tableau
  - Une case peut être vue comme une variable

---

## Syntaxe des tableaux

- Délimités par `[` et `]`
- Valeurs contenues listées dans l'ordre des positions et séparées par `,`

---

## Exemples de tableaux

```python
[] # tableau de taille 0
[1, 2, 3]
["a", "bcd", "", "fgh"]
[3.2, 3.14]
[True, False]

[ [1, 2], [7, 5], [] ]
```

---

## Parcours de tableaux

On peut parcourir un tableau avec des boucles

```python
for v in mon_tableau:
    print(v)
```

En utilisant les positions:

```python
for pos in range(0,len(mon_tableau)):
    print(mon_tableau[pos])
```

---

## append

Ajoute une case à la fin du tableau

```python
t = ["ab", "cd"]
t.append("ef")
print(t)
```

---

## Affectation de case

```python
t = []
for i in range(0,3):
    t.append(i)
print(t)
for i in range(0,3):
    t[i] = i*i
print(t)
for i in range(0,3):
    t[i] = t[i]+i
print(t)
```

---

## Live coding

- Récupérer une série de nombres depuis un fichier dont le nom est saisi au clavier et les placer dans un tableau
- Saisir un nombre k
- Donner les k nombres les plus grands du tableau

---

## Exercice

Saisir un nombre au clavier et afficher le [triangle de Pascal](https://fr.wikipedia.org/wiki/Triangle_de_Pascal) avec ce nombre de lignes:

```txt
1
1 1
1 2 1
1 3 3 1
1 4 6 4 1
1 5 10 10 5 1
```
