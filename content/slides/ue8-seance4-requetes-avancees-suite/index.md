---
title: Introduction à l'analyse des données - séance 4
summary: "Séance 4 - requêtes avancées (suite) & affichage"
authors: [admin]
tags: [code]
categories: [cours]
slides:
  theme: "white"
  highlight_style: "github"
---

## Introduction à l'analyse de données

### Séance 4 - requêtes avancées (suite)

[Emmanuel Coquery](https://perso.liris.cnrs.fr/emmanuel.coquery/home)

<https://perso.liris.cnrs.fr/emmanuel.coquery/home/enseignement/ue8/>

---

## Vu précédement

- Données tabulaires
- Pandas pour charger des données
- Dataframes, Series, index
- Filtrer les données (colonnes, lignes)
- Ajouter des colonnes calculées
- Combiner des données

---

## Jointure (merge)

Combiner deux Dataframes

- Les lignes sont combinées quand elles ont même valeur sur les colonnes/index spécifiés
- Les colonnes viennent des deux Dataframes d'origine

---

## Exemple

Au tableau

---

## Digression

### Dataframe dans le code

```python
dist_i_m = pandas.DataFrame(
    { "Dist_I_m": [ 0.2, 0.3, 0.4, 0.6, 0.8 ] },
    index = [1,2,3,4,5]
)
```

- Que représente la Dataframe obtenue ?
- En faire une deuxième pour `Dist` mais avec une colonne pour la valeur correspondante à un Space externe

---

## Jointure via merge

Exemple

```python
pandas.merge(df, dist_i_m, left_on='Dist_A', right_index=True)
```

Expliquer le rôle de chaque argument de merge

---

## Renommage de colonne

Changer le nom d'une ou plusieurs colonnes

```python
df_r = df.rename(columns={
    'col1': 'col1_renommee',
    'col2': 'col2_renommee'})
```

---

### Exercice

- Créer une Dataframe où `Dist_A` et `Dist_B` sont remplacées par des colonnes `Dist_A_m` et `Dist_B_m` avec les distances en mètres
  - Attention aux changement d'échelle selon valeur de `Space`.

---

## Aggrégations de lignes

### (groupby)

Calculer un résultat en regroupant des lignes

_e.g._ calculer des moyennes par participant

Critère de regroupement: on regroupe les lignes ayant la même valeur pour une colonne particulière

---

## Syntaxe du groupby

```python
ma_dataframe.groupby(by="ma_colonne").ma_fct_aggregation()
```

Exemple

```python
df.groupby(by='Subject').mean()
```

- Quel est l'index de la Dataframe résultat ?

---

### Exercices

- Donner le temps de réaction maximal par sujet.
- Donner le temps de réaction moyen par premier nom (`Name_a`).
- Donner le temps de réaction moyen selon la distance en mètres du premier prénom prononcé.
- Donner le sujet ayant le plus faible temps de réaction moyen (utiliser `.idxmin()`).
