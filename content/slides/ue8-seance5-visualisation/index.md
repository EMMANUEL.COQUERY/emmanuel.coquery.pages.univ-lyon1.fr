---
title: Introduction à l'analyse des données - séance 5
summary: "Séance 5 - visualisation"
authors: [admin]
tags: [code]
categories: [cours]
slides:
  theme: "white"
  highlight_style: "github"
---

## Introduction à l'analyse de données

### Séance 5 - visualisation

[Emmanuel Coquery](https://perso.liris.cnrs.fr/emmanuel.coquery/home)

<https://perso.liris.cnrs.fr/emmanuel.coquery/home/enseignement/ue8/>

---

## Vu précédement

- Données tabulaires
- Pandas, Dataframes, Series, index
- Filtrer les données (colonnes, lignes)
- Ajouter des colonnes calculées
- Combiner des données, jointure
- Aggrégation (group by)

---

## Tracer des figures

Bibliothèque `matplotlib`

```python
import matplotlib.pyplot as plt
import matplotlib as mpl
```

Pandas fourni des fonctionnalités pour faciliter l'usage de `matplotlib`

[Documentation](https://matplotlib.org/stable/users/index.html)

---

## Une première courbe

```python
# Créer une figure
fig, ax = plt.subplots()
# Tracer la courbe pour une série
ma_serie.plot()
```

---

### Exercice

- Créer une Series contenant le temps de réaction moyen en fonction de la distance A en mètres
- Tracer la courbe correspondant à la série
- Créer la série et tracer la courbe correspondante pour la distance B.

---

## D'autres types de graphiques

```python
ma_df.plot.scatter('Col1', 'Col2')
ma_df.plot.bar('Col1', 'Col2')
ma_df.plot.box(by='Col1', column='Col2')
ma_df.plot.hist(by='Col',bins=mon_nombre_de_barres)
```

---

## Quelques arguments additionnels pour les graphiques

- `ax=mon_ax` pour réutiliser une figure définie par:

  ```python
  fig, mon_ax = plt.subplots()
  ```

- `c='#a98d19'` pour spécifier une couleur

---

### Exercice box

- Ajouter au besoin une colonne pour la différence entre Dist_A et Dist_B
- Faire un box plot du temps de réaction selon la différence de distance
- Faire un deuxième box plot en se limitant aux essais réussis et un troisième pour les échecs

---

### Exercice histogramme

- Faire un histogramme de la quantité d'essais selon le temps de réaction

---

### Exercice bar

- Créer une dataframe contenant les essais en mode Dic(hotique) à gauche et enfaire une copie via

  ```python
  df_copie = pandas.DataFrame(df_a_copier)
  ```

- Ajouter un attribut mode_side contenant la valeur `Dic_G`
- En créer deux autres versions: une pour Dic à droite et une pour Dio(tique) (sans différencier les côtés)

---

### Exercice bar (suite)

- Assembler les 3 dataframes obtenues
- Faire un affichage de barres avec le nombre d'essais réussis.
