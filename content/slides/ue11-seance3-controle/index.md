---
title: Introduction à la programmation - séance 3
summary: "Séance 3 - structures de contrôle"
authors: [admin]
tags: [code]
categories: [cours]
date: "2022-11-15T00:00:00Z"
slides:
  theme: "white"
  highlight_style: "github"
---

## Introduction à la programmation d'expériences

### Séance 3 - structures de contrôle

[Emmanuel Coquery](https://perso.liris.cnrs.fr/emmanuel.coquery/home)

https://perso.liris.cnrs.fr/emmanuel.coquery/home/enseignement/ue11/

---

## Récapitulatif

- expressions
- types
- instructions (affectation, print)
- script

---

## Structures de contrôle

- Objectif: pouvoir changer le comportement du programme selon les valeurs calculées à certains moments
- outils: conditionnelles et boucles

---

### Conditionnelles

- Choix des instructions / sous-expressions à utiliser en fonction d'une valeur
- Valeur: expression de type `bool`

---

### Conditionnelle d'expression

Syntaxe

```
(expr_1 if expr_cond else expr_2)
```

Sémantique

- Si `expr_cond` s'évalue à `True`
- alors le résultat est donné par le calcul de `expr_1`
- sinon (_i.e._ si `expr_cond` s'évalue à `False`)\
  le résultat est donné par le calcul de `expr_2`

---

### Exemple de conditionnelle d'expression

y est la valeur absolue de x

```
x = -12
y = x if x >= 0 else -x
```

---

### Conditionnelle pour des suites d'instructions

Syntaxe

```
if expr_cond :
  instruction_a1
  instruction_a2
  ...
else :
  instruction_b1
  instruction_b2
  ...
```

- Les `:` sont importants
- le décalage des instructions est important
- les "paquets" d'instructions sont des **blocs**
- le `else` est optionel

---

### Conditionnelle pour des suites d'instructions

Sémantique

- Si `expr_cond` s'évalue à `True`
- alors l'interpréteur exécute les instructions "a"
- sinon (_i.e._ si `expr_cond` s'évalue à `False`)\
  l'interpréteur exécute les instructions "b"

---

### Exemple de conditionnelle pour des suites d'instructions

`jeune.py`

```
age = int(input("Quel âge ? "))
if age <= 20 :
  print("Je suis jeune")
else:
  print("Je suis jeune aussi, car c'est dans la tête")
```

- `input` pose une question et renvoie la valeur saisie au clavier

---

### Imbrication de if

- Il est possible de placer une conditionnelle `if` "à l'intérieur" d'une autre
- Les blocs de la conditionnelle "interne" sont alors décalés d'un espacement supplémentaire

Exemple

```
age = int(input("Quel âge ? "))
if age > 20 :
  if age <= 80 :
    print("Je suis jeune aussi, car c'est dans la tête")
  else:
    print("Je ne suis plus aussi jeune qu'avant")
else:
  print("Je suis jeune")
```

---

### Exercice

- Reprendre le programme de calcul des racines de trinôme du second degré
- Ajouter des input pour saisir les valeurs de a, b et c
- Utiliser des if pour distinguer les cas à 0, 1 et 2 solutions

---

### Boucles

Répétition d'un bloc

- `for`: boucle pour "parcourir" un ensemble de valeurs
- `while`: boucle qui est répétée tant qu'une condition est vraie

---

### Boucle for

Syntaxe

```
for ma_variable in range(debut,fin):
  instruction_1
  instruction_2
  ...
```

- rmq: la vraie syntaxe est plus générale

---

### Boucle for

Sémantique

- range représente tous les int compris entre
  - debut inclus
  - fin excluse
- le bloc est exécuté pour chaque valeur représentée par range
  - la valeur concernée est rangée dans ma_variable pour chaque "tour" de boucle

---

### Exemple: boucle for

`somme.py`

```
total = 0
for n in range(1,10):
  total = total + n
print("La somme des entiers de 1 à 9 est "+str(total))
```

- Exercice: modifier ce programme pour
  - saisir un nombre
  - calculer la factorielle de ce nombre

---

### Boucle while

Syntaxe

```
while expr_cond:
  instruction_1
  instruction_2
  ...
```

---

### Boucle while

Sémantique

- La boucle est répétée tant que cond_expr s'évalue à True
- Seule la modification de l'environnement peut arrêter la boucle
  - souvent une modification des variables

---

### Exemple boucle while

racine.py

```
x = 27
r = 0

while r * r <= x:
  r = r + 1

r = r - 1 # pourquoi ?

print("La racine carrée entière de "+str(x)+" est "+str(r))
```

---

### Exercice

Pliage

- Écrire un programme qui détermine combien de fois il faut plier une feuille de 1 mm d'épaisseur pour obtenir une épaisseur d'1 m (en imaginant qu'on peut plier la feuille autant de fois que nécessaire)

---

### Structures imbriquées

- Il est possible d'imbriquer des structures de contrôle les unes dans les autres
- On a déjà vu des imbrication avec des `if`
- Il est possible d'imbriquer
  - des `if` dans des boucles
  - des boucles dans d'autres boucles
  - des boucles dans des `if`

---

### Exercices

Écrire des programmes pour

- Afficher les carrés d'entiers pairs entre 0 et 10.
- Donner l'entier entre 0 et 20 dont le sinus a la plus grande valeur.
- Calculer les $k$ premiers nombres premiers
