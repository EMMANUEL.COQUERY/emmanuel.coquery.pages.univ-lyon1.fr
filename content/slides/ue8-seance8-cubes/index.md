---
title: Introduction à l'analyse des données - séance 8
summary: "Séance 8 - cubes"
authors: [admin]
tags: [code]
categories: [cours]
slides:
  theme: "white"
  highlight_style: "github"
---

## Introduction à l'analyse de données

### Séance 8 - cubes de données

[Emmanuel Coquery](https://perso.liris.cnrs.fr/emmanuel.coquery/home)

<https://perso.liris.cnrs.fr/emmanuel.coquery/home/enseignement/ue8/>

---

## Vu précédement

- Données tabulaires
- Pandas, Dataframes, Series, indexes
- Filtrer les données (colonnes, lignes)
- Combiner des données, (concat, jointure, aggrégation)
- Figures (2D, 3D)
- Indexation à plusieurs niveaux

---

## Générer un index

Index comprenant toutes les combinaisons de valeurs possibles issues de chaque série d'une liste de séries

```python
pandas.MultiIndex.from_product(
    liste_de_series,
    names=liste_de_noms)
```

---

## Réindexer

Changer l'index en gardant les lignes présentes dans l'ancien index et en ajoutant des lignes pour les valeurs présentes uniquement dans le nouvel index

```python
ma_dataframe.reindex(
    index=nouvel_index,
    fill_value=valeur_si_manquant)
```

---

## Digression: Créer une liste à partir d'une liste existante

```python
liste_transformee = [ calcul(elt) for elt in liste_depart ]
```

exemple

```python
valeurs = [ 3, 4, 5, 6 , 7, 8 ]
valeurs_plus_3 = [ v+3 for v in valeurs ]
```

---

## Exercice: configurations explorées

Quelle exhaustivité dans les combinaisons de paramètres de l'expérience ?

- Générer un index de toutes les combinaisons de valeurs possibles des colonnes de paramètres expérimentaux
- En utilisant groupby sur la liste des colonnes de paramètres expérimentaux (dans le même ordre), faire le compte du nombre d'essai pour chaque combinaison de paramètres

---

## Exercice: configurations explorées (suite)

- Reindexer la dataframe avec l'index précédent, en utilisant 0 pour le fill
- Essayer de déterminer si certains paramètres ont été moins bien explorés que d'autres

---

## Cube de données

(version simplifiée)

- Catégorisation des colonnes en
  - mesures
    - _e.g._ prises pendant une expérience
  - dimensions
    - _e.g._ paramètres d'un essai
- Une ligne = un fait
  - _e.g._ un essai

---

## Dimensions

- valeurs utilisées pour découper l'espace
- fixer une valeur revient à couper une tranche d'espace

Dessin

---

## Mesures

- s'agrège dans une case de l'espace
- les valeurs de plusieurs cases s'agrègent ensemble
- fonctionne bien pour: min, max, sum
- attention à
  - count (recoder avec sum)
  - mean, std, etc

---

## Cube de l'expérience de discrimination

- mesures:
  RT, nombre d'essais, nombre d'essais réussis, autre ?
- dimensions:
  Subject, Name_A, Name_B, Dist_A, Dist_B, Mode, Space, Side, autre ?

---

## Préparer les faits

- Ajouter les colonnes pertinentes (_e.g._ nombre d'essais (réussis)).
- Comprendre comment on peut/va aggréger les valeurs.

---

## Explorer le cube

- Calculer les valeurs associées à une tranche via des groupby
- Le cas échéant, faire les calculs séparément puis assembler (via `concat` ou `merge`) les dataframes.
