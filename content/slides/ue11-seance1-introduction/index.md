---
title: Introduction à la programmation - séance 1
summary: "Séance 1 - introduction à la programmation"
authors: [admin]
tags: [code]
categories: [cours]
date: "2022-11-29T00:00:00Z"
slides:
  theme: "white"
  highlight_style: "github"
---

## Introduction à la programmation d'expériences

[Emmanuel Coquery](https://perso.liris.cnrs.fr/emmanuel.coquery/home)

<https://perso.liris.cnrs.fr/emmanuel.coquery/home/enseignement/ue11/>

{{< speaker_note >}}

- Only the speaker can read these notes
- Press `S` key to view
- Press `F` key to enter fullscreen mode

{{< /speaker_note >}}

---

## Objectifs

- Apprendre les notions de base de la programmation
- Les mettre en pratique à travers 2 langages
  - Python (langage générique)
  - OpenSesame (logiciel d'expérimentation)

Démo opensesame

---

### Organisation

#### Séances

Cours - TD (sur feuille) - TPs (sur machine)

#### Évaluation

- contrôles sur papier et sur machine
- un projet
- une épreuve finale

---

## Programmation

Exprimer un **traitement** dans un **langage**

Souvent: un calcul

En pratique: on a en plus des interactions avec l'environnement

---

### Langages (en informatique)

Syntaxe: **comment** on (d)écrit les **traitements**

Sémantique: **signification** de ce qui est écrit

---

### Quelques catégories de langages

- Documents: HTML, XML, JSON, RDF ... CSV
- Shells: Cmd MS-DOS, PowerShell, Zsh, ...
- Requêtes: SQL, SPARQL, JSONPath, ...
- Programmation: Python, C, C++, Java, Javascript, OCaml, Go, Rust, ...

---

### Exemple: HTML

```html
<html>
  <head>
    <title>Bonjour</title>
  </head>
  <body>
    <h1>Une belle page</h1>
    <p>Avec un paragraphe</p>
  </body>
</html>
```

---

### Exemple: JSON

```json
{
  "enseignant": {
    "nom": "Coquery",
    "prenom": "Emmanuel"
  },
  "cours": {
    "titre": "Introduction à la programmation",
    "code": "STR1034M"
  }
}
```

---

### Exemple: CSV

```csv
Nom,Prenom,Num,Note1,Note2
Spears,Alice,123456789,12.0,9.0
Booker,Bob,234567890,5.0,16.5
Barr,Sigourney,345678901,12.0,18.0
```

---

### Exemple: Commandes DOS

```dos
C:\Users\ecoquery>dir
 Le volume dans le lecteur C n’a pas de nom.
 Le numéro de série du volume est 2039-E686

 Répertoire de C:\Users\ecoquery

10/11/2022  08:27    <DIR>          .conda
10/11/2022  08:24                25 .condarc
10/11/2022  08:23    <DIR>          .continuum

...

               1 fichier(s)               25 octets
              15 Rép(s)  240 001 818 624 octets libres
```

```doc
C:\Users\ecoquery>cd Music

C:\Users\ecoquery\Music>
```

---

### Exemple: Zsh

```shell
☸ docker-desktop in ~
❯ ls
Applications      Parallels
Applications (Parallels)  Pictures
Desktop        Public

...

☸ docker-desktop in ~
❯ cd Music

☸ docker-desktop in ~/Music
❯
```

---

### Exemple: SQL

```sql
SELECT nomEns, prenomEns
FROM cours
WHERE formation = 'M1-Sante'
ORDER BY nomEns
```

---

### Exemple: Python

```
import csv

def valeurOk(v):
    return v >= 0.0 and v <= 1.0

with open('monfichier.csv') as f:
    for row in csv.reader(f):
        if not valeurOk(float(row[2])):
            print("Valeur erronnée:", row[2])
```

---

### Exemple: Go

```go
func valeurOk(v float64) bool {
  return 0.0 <= v && v <= 1.0
}

func main() {
  f, err := os.Open("monfichier.csv")
  if err != nil {
    fmt.Errorf("erreur de lecture")
  }
  r := csv.NewReader(f)
  for {
    record, err := r.Read()
    if err == io.EOF {
      break
    }
    val, _ := strconv.ParseFloat(record[2], 64)
    if !valeurOk(val) {
      fmt.Printf("Valeur erronée: %f", val)
    }
  }
}
```

---

## Python

Langage de **programmation**

- interprété
- typage dynamique
- style plutôt impératif
- écosystème riche pour la gestion et l'analyse des données

---

### Environnement de travail

On travaillera sur

<https://jupyter.univ-lyon1.fr/>

---

### Expressions

- calcul
  - valeurs
  - opérateurs
  - fonctions
- simples: expressions arithmétiques
- complexes: utilisation de fonctions et de valeurs complexes

---

### Types

- "Classification" des valeurs
- Pour commencer:
  - `int` (nombres sans virgule, "entiers")\
    `1`, `2358`, etc
  - `float` (nombres à virgule ≠ réels)\
    `0.2`, `56.32`, `12.3E10`, etc
  - `str` (texte, chaînes de caractères, string)\
    `"Bonjour"`, `"Emmanuel"`, etc
  - `bool` (booléens, i.e. vrai ou faux)\
    `True`, `False`

---

### Quelques opérateurs prédéfinis

$int\times int\rightarrow int$

- `+`, `-`, `*`,
- `//` (division entière)
- `**` (puissance)

$\Rightarrow$ faire 2/3 calculs d'`int` dans l'interpéteur Python

---

### Quelques opérateurs prédéfinis

$float\times float\rightarrow float$

`+`, `-`, `*`, `/`, `**`

$\Rightarrow$ faire 2/3 calculs de `float` dans l'interpéteur Python

---

### Quelques opérateurs prédéfinis

$str\times str\rightarrow str$

- `+`
- "opérateurs" postfixés: `.upper()`, `.lower()`

$\Rightarrow$ essayer de prédire l'effet de ces opérateurs sur des `str`, puis vérifier dans l'interpréteur

---

### Quelques opérateurs prédéfinis

$bool\times bool\rightarrow bool$

| x     | y     | x `and` y | x `or` y | `not ` x |
| ----- | ----- | --------- | -------- | -------- |
| True  | True  | True      | True     | False    |
| True  | False | False     | True     | False    |
| False | True  | False     | True     | True     |
| False | False | False     | False    | True     |

---

### Expressions booléennes

Prédire puis vérifier le résultats des expressions suivantes:

- True or (True and False)
- (False and False) or not (True and False)
- False and not False
- True and not True

---

### Mélanges de types

En utilisant l'interpréteur, deviner le type résultat des opérateur suivants en fonction du type de leurs arguments

- `+` et `*` avec $int\times float$
- `/` avec $int\times int$
- `+` avec $int\times str$
- `*` avec $int\times str$

---

### Comparaisons

Opérateurs de comparaison

`==`, `<`, `>`, `<=`, `>=`

- pour le str: ordre alphabétique basé sur l'ordre des caractères en UTF-8
  - regarder ce qui se passe dans l'interpréteur avec des accents et/ou des majuscules
- regarder `<` entre booléens, essayer de le recoder avec les autres opérateurs

---

### Conversion de type

Convertir vers un type:

`int(...)`, `float(...)`, `str(...)`, `bool(...)`

Typer chaque sous-expression, évaluer à la main, puis vérifier le résultat de

`int(3*"3") // 111 == 3`

---

### Fonctions additionnelles: module `math`

- commande\
  `import math`
- donne accès à `math.sin(...)`, `math.floor(...)`, `math.pi`

---

### Bibliothèque standard Python

https://docs.python.org/3.9/library/index.html

---

## Instructions

Instruction = action à réaliser

Modifications de l'environnement

---

### Environnement ?

- physique
- système
- **mémoire du programme**

---

### Variables

- Emplacement ($\approx$ case) mémoire
- Contient une valeur
  - on peut placer une valeur dans une variable
  - on peut aller chercher la valeur d'une variable

Brique de base de la programmation

---

### Affectation

Range une valeur dans une variable

Syntaxe

`ma_variable = une expression`

Exemple

```
sous_total = 5 + 3 * 12
```

---

### Récupérer la valeur d'une variable

On utilise simplement le nom de la variable dans une expression

Exemple

```
total = sous_total + 7
```

---

### Afficher

`print(...)`

- instruction qui affiche son argument
- c'est en fait une fonction au sens informatique

Exemple

```
print("Le carré de 3 est " + str(3**2))
```

- Remarque: en mode interactif, l'interpréteur exécute les instructions normalement. Si on tape une expression, l'interpréteur exécute en fait un print de cette expression

---

### Script ou programme Python

- fichier contenant une suite d'instructions
- le nom du fichier fini par `.py`\
  (attention dans l'explorateur de fichiers)
- syntaxe, pour le moment:
  - faire attention à commencer les instructions en début de ligne
  - les instruction sont écrites sur une seule ligne

---

### Commentaires

- texte qui n'est pas interprété
- utile pour documenter le programme
- syntaxe: tout ce qui suit le caractère `#`

exemple

```python
# un commentaire seul sur une ligne
print("Salut") # cette instruction affiche Salut
```

---

### Un premier programme

```python
import math
a = 1
b = -12
c = 35
delta = b ** 2 - 4 * a * c
# il faudrait tester delta ...
x1 = (-b + math.sqrt(delta)) / (2 * a)
x2 = (-b - math.sqrt(delta)) / (2 * a)
poly = str(a)+" x^2 + "+str(b)+" x + "+str(c)
print("Les solutions de "+poly+" sont "+str(x1)+" et "+str(x2))
```

---

### Affectations, suite

- une variable est créée à sa première affectation

  - lire une variable inexistante provoque une erreur:

    ```python
    print(toto+2) # si toto n'a jamais été affecté
    ```

- _**changer**_ la valeur d'une variable: refaire une affectation dessus
  - il est possible d'utiliser la valeur (ancienne) d'une variable dans le calcul de la (nouvelle) valeur qui lui est affectée

---

### Exemple d'affectation successives

script `boulangerie.py`

```python
prix_bombon = 0.10
prix_croissant = 1.0
prix_pain = 1.2
total = prix_bombon * 8
total = total + prix_croissant * 5
total = total + prix_pain * 2
print("Je dois "+str(total)+" euros")
```

- Exécuter ce script, puis une version modifiée qui affiche la valeur de total après les deux premières affectations.

---

## Point d'étape

- expressions
- types
- instructions (affectation, print)
- script
