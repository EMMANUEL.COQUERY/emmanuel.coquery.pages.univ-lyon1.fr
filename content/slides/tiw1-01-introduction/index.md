---
title: Intergiciels et services - Introduction
summary: "Introduction du cours intergiciels et services"
authors: [admin]
tags: [code]
categories: [cours]
date: "2019-09-10T00:00:00Z"
slides:
  theme: "white"
  highlight_style: "github"
---

# Intergiciels et services

## Introduction

[M2TIW](http://master-info.univ-lyon1.fr/TIW) - [TIW-IS](https://forge.univ-lyon1.fr/tiw-is/tiw-is-2022-2023) - [Emmanuel Coquery](https://perso.liris.cnrs.fr/emmanuel.coquery/home)

{{< speaker_note >}}

- Only the speaker can read these notes
- Press `S` key to view
- Press `F` key to enter fullscreen mode

{{< /speaker_note >}}

---

## Organisation

### 2 intervenants

[Emmanuel Coquery](https://perso.liris.cnrs.fr/emmanuel.coquery/home)

[Lionel Médini](http://liris.cnrs.fr/lionel.medini/)

### Page de l'UE

https://forge.univ-lyon1.fr/tiw-is/tiw-is-2022-2023

---

## Au menu

- Révision Java
- Introduction aux composants, design patterns: _IoC_, contexte, annuaire, ...
  - à la main
  - dans Spring
- Services Web, utilisation & gestion des messages

---

# Bonnes pratiques

---

## Environnement de développement intégré

### Connaître son IDE

- Raccourcis clavier
- Structuration des projets
- Affichage/intégration de la documentation
- Intégration outils de build, serveurs
- Affichage des logs
- Génération de code
- ...

---

## Environnement de développement intégré

### Connaître son IDE

Il est souvent rentable de passer 5 / 10 min à comprendre comment bien faire une chose dans son IDE.

---

## Environnement de développement intégré

### Savoir se passer de son IDE

- Pour compiler, exécuter les tests, ...
- Pour déployer
- Savoir se débrouiller en ligne de commande

---

## Bibliothèques & _frameworks_

### Log

- L'utilisation de `System.out`/`.err` est **interdite**
- Utiliser [`logging`](https://docs.oracle.com/en/java/javase/11/docs/api/java.logging/java/util/logging/package-summary.html), [SLF4J/Logback](https://logback.qos.ch/), [Log4J](https://logging.apache.org/log4j/2.x/), ...

---

## Bibliothèques & _frameworks_

### Test

- Tester son code
- via un framework de test (JUnit, TestNG, etc)
- regarder ce qui existe pour tester dans les cas plus complexes (e.g. test d'intégration):
  - Spring Test, Arquilian
  - SOAPUI, Selenium, jmeter

---

## Qualité de code

- Faire attention à la qualité du code:
  - formattage
  - commentaire
  - simplicité de compréhension
- Auditer le code
  - Automatiquement (e.g. sonarqube)
  - Entre vous

---

# Point technique: intégration continue sous gitlab

---

## Fichier `.gitlab-ci.yml`

- À la racine du dépôt git.
- jobs (scripts à exécuter)
- variables, logiciels, etc

---

## Exemple

```
image: "ubuntu:18.04"

stages:
  - construction
  - test
```

---

## Exemple - suite 1

```
compilation:
  stage: construction
  variables:
    FLAGS: "-o 2"
  artifacts:
    paths:
      - toto
    expire_in: 1 day
  script:
    - gcc $FLAGS toto.c -o toto
```

---

## Exemple - suite 2

```
test-basique:
  stage: test
  script:
    - ./toto
```

---

# Introduction rapide à Docker

---

## Baremetal, VM, conteneur

- Baremetal: machine physique
- Virtual Machine: ordinateur virtuel simulé par un logiciel (hyperviseur)
- Container: processus isolé de la machine dans laquelle il tourne

---

## VM et conteneur: similitudes

- système de fichier (et donc pile logicielle) dédié
- réseau isolé
- pas d'accès direct à la machine hôte

---

## VM et conteneurs: différences

- VM > tout un système d'exploitation
- conteneur > un processus (qui peut éventuellement en démarrer d'autres)
- VM > noyau propre
- conteneur > noyau de la machine hôte

---

## Docker

- système de conteneurisation
- démon: lance et gère les conteneurs
- cli: interface utilisateur
- sous Windows/MacOSX: le démon tourne dans une VM
- image: système de fichier du conteneur
- volume: montage d'un répertoire externe dans un conteneur

---

## Exemple

```
docker run -e PG_PASSWORD=tototiti -p 5433:5432 postgres:12
```

- lancement d'une image `postgres` en version `12`
- redirection du port `5433` de l'hôte sur le port `5432` du conteneur
- fixe la variable d'environment `PG_PASSWORD` à `tototiti`

---

## Commandes utiles

- `docker ps [-a]`: infos sur les conteneurs
- `docker rm [--force] hash_ou_nom`: \
  supprime un conteneur
- `docker [start|stop] hash_ou_nom`
- `docker [-it] exec hash_ou_nom cmd arg1 arg2 ...`: \
  lance une commande "dans le conteneur" indiqué

---

## Docker Compose

- Lance un ou plusieurs conteneurs
- Configurés dans un fichier `docker-compose.yml`
- Préconfiguration réseau: ces conteneurs se voient mutuellement

---

## Exemple

```
version: "3.7"

services:
  postgres:
    image: postgres:12
    volumes:
      - ./pg_data:/var/lib/postgresql/data
    environment:
      POSTGRES_USER: sonar
      POSTGRES_PASSWORD: sonar
      POSTGRES_DB: sonarqube
      PGDATA: /var/lib/postgresql/data/pgdata
```

---

## Exemple - suite

### (Sonarqube - 1)

```
  sonarqube:
    image: sonarqube:lts
    ports:
      - "9000:9000"
    volumes:
      - "./conf:/opt/sonarqube/conf"
      - "./extensions:/opt/sonarqube/extensions"
      - "./logs:/opt/sonarqube/logs"
      - "./data:/opt/sonarqube/data"
```

---

## Exemple - suite

### (Sonarqube - 2)

```
    depends_on:
      - postgres
    environment:
      - "sonar.jdbc.username=sonar"
      - "sonar.jdbc.password=sonar"
      - "sonar.jdbc.url=jdbc:postgresql://postgres/sonarqube"
```

---

## Commandes utiles

- `docker compose up [-d]`: créée et démarre les ressources
  (conteneurs, réseaux, etc)
- `docker compose down`: arrête et détruit les ressources
- `docker compose [start|stop]`
- `docker compose logs [-f]`
- `docker compose exec service commande`
