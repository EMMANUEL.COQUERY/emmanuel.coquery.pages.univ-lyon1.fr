---
title: Introduction à la programmation - séance 4
summary: "Séance 4 - structures de données"
authors: [admin]
tags: [code]
categories: [cours]
date: "2023-01-03T00:00:00Z"
slides:
  theme: "white"
  highlight_style: "github"
---

# Introduction à la programmation

## Séance 4 - structures de données

[Emmanuel Coquery](https://perso.liris.cnrs.fr/emmanuel.coquery/home)

https://perso.liris.cnrs.fr/emmanuel.coquery/home/enseignement/ue11/

---

# Chaînes de caractères

Caractères; sous-chaînes et positions

---

## Positions et sous-chaînes

Accéder à un morceau de chaîne

Exemple: `"Gâteau au chocolat"`

`"Gâteau"` est la sous-chaîne contenant les 6 premières lettres (on dit caractère)

---

### Positions dans une chaîne

- La première position est 0
- La taille de la chaîne est donnée par la fonction `len`
- La dernière position dans `s` est donc `len(s)-1`

---

### Sous-chaînes

Opérateur `[]` extrait une sous-chaîne

- `s[d:f]` est la sous-chaîne de s qui commence à la position `d` et fini à la position `f-1`\
  (i.e. la position `f` exclue)

Exemple

```
In [1]: "abcdef"[2:4]
Out[1]: 'cd'
```

- Essayer avec des positions négatives ou plus grandes que la longueur

---

### Caractère à la position i

Opérateur `[]` sans `:`

- `s[i]` est le caractère à la position `i`
- valeur renvoyée comme une chaîne de taille 1

Exemple

```
In [2]: "abcdef"[2]
Out[2]: 'c'
```

---

### Exercices

- Écrire un programme qui lit un fichier dont le nom a été saisi au clavier et qui affiche le nombre de fois où le caractère `e` apparait dans le fichier.

- Faire une variante où on demande en plus à l'utilisateur de saisir tous les caractères à compter (la saisie est une chaîne et on compte dans le fichier chacun des caractères qui apparaissent dans la chaîne)

---

### Exercice

https://adventofcode.com/2022/day/2

---

# Structures de données

## Tableaux

---

## Structure de **données** ?

- Donnée complexe contenant des données plus simples
  - Exemple: une `string` contient des caractères
- ⚠️ structure de donnée ≠ structure de contrôle ⚠️

---

## Tableaux

- Fonctionnement par position similaire aux `string`
- Peut contenir tout type de données:
  - `int`, `float`, `bool`, `string`
  - d'autres tableaux
- Opérateur `[]` similaire aux `string`
- Type: `list`
- Une case de tableau est ce qui contient la valeur à une position du tableau
  - Une case peut être vue comme une variable

---

## Syntaxe des tableaux

- Délimités par `[` et `]`
- Valeurs contenues listées dans l'ordre des positions et séparées par `,`

---

## Exemples de tableaux

```
[] # tableau de taille 0
[1, 2, 3]
["a", "bcd", "", "fgh"]
[3.2, 3.14]
[True, False]

[ [1, 2], [7, 5], [] ]
```

---

## Parcours de tableaux

On peut parcourir un tableau avec des boucles

```
for v in mon_tableau:
    print(v)
```

En utilisant les positions:

```
for pos in range(0,len(mon_tableau)):
    print(mon_tableau[pos])
```

---

## append

Ajoute une case à la fin du tableau

```
t = ["ab", "cd"]
t.append("ef")
print(t)
```

---

## Affectation de case

```
t = []
for i in range(0,3):
    t.append(i)
print(t)
for i in range(0,3):
    t[i] = i*i
print(t)
for i in range(0,3):
    t[i] = t[i]+i
print(t)
```

---

## Exercice

- Récupérer une série de nombres depuis un fichier dont le nom est saisi au clavier et les placer dans un tableau
- Saisir un nombre k
- Donner les k nombres les plus grands du tableau

---

## Exercice

Reprendre

https://adventofcode.com/2022/day/1

En stockant chaque nombre de calories dans un tableau de sac à dos, un sac à dos étant lui même un tableau de nombre de calories.

---

## Exercice

Saisir un nombre au clavier et afficher le [triangle de Pascal](https://fr.wikipedia.org/wiki/Triangle_de_Pascal) avec ce nombre de lignes:

```
1
1 1
1 2 1
1 3 3 1
1 4 6 4 1
1 5 10 10 5 1
```
