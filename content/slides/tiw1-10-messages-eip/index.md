---
title: "Messages: files de messages et EIP - Intergiciels et services"
summary: "Slides on messaging."
date: "2019-12-06T00:00:00Z"
draft: false
authors: [admin]
tags: []
categories: []
slides:
  theme: "white" # Reveal JS theme name
  highlight_style: "github" # Highlight JS theme name
---

# Bus de messages et Patrons d’intégration

[M2TIW](http://master-info.univ-lyon1.fr/TIW) - [TIW1](https://forge.univ-lyon1.fr/tiw-is/tiw1-is-2019) - [Emmanuel Coquery](https://perso.liris.cnrs.fr/emmanuel.coquery/home)

{{< speaker_note >}}

- Only the speaker can read these notes
- Press `S` key to view
- Press `F` key to enter fullscreen mode

{{< /speaker_note >}}

---

## Composition de services

---

### Un service peut en cacher d’autres (A=B+C)

Un service peut être vu comme une interface sur:

- un / des processus métier
- utilisant à leur tour des services
  - qui réalisent des actions à plus petite échelle / moins complexes
- "analyse descendante" vers SOA
  - attention aux limites de l’analogie !

---

### Pourquoi composer des services ?

Dans le cadre de gros systèmes d’information :

- Logique applicative complexe
- Processus décomposables
- Décentralisation des traitements

---

### Quelques problématiques autour de la composition

- Liées aux processus métiers complexes

  - Transactions longues, instances multiples, concurrence, distribution
  - Point de vue: Orchestration vs chorégraphies

- Gestion des messages

  - Comment organiser la transmission
  - Quelle efficacité ?

---

### Transactions de longue durée

Persistance de l’état du processus

- Stocker l’état (au sens automate) + données de chaque instance
- Penser le process comme une gestion de ressources (type REST)

⚠️ Impact sur la conception ⚠️

---

### Transactions métier

Attention à la répartition (commit à 2 ou 3 phases)

Pas systématiquement de rollback

- Impacts d’une action informatique\\
  dans le monde réel\
  - _e.g._ livraison effectuée
- **Compensation** au lieu du rollback
  - _e.g._ renvoi des produits, remboursement, _etc_.
- une compensation peut être une transaction

---

### Parallélisme et concurrence

Tâche à exécuter de manière concurrente / parallèle

- Dépendances avec les tâches
- Synchronisation

---

### Gestion des dépendances entre tâches

- Moteur de workflow\
  ⚠️ Exceptions dans le flot "normal" ⚠️
- Système de règles ECA (Event-Condition-Action)\
  ⚠️ Comportement parfois complexe ⚠️

---

### Synchronisation des tâches

les tâches dont on dépend sont terminées ?

- une
- toutes
- une combinaison

---

### Orchestration _vs_ chorégraphies

Points de vue différents sur une composition

Complémentaires

---

### Orchestration

- Vue centralisée
- Un service gère
  - le processus
  - les appels aux autres services

_e.g._ gestion d’un ticket dans un gestionnaire de bugs

---

### Chorégraphie

Processus géré de manière distribuée

_e.g._ authentification type Kerberos, \
paiement via une tierce partie

⇨ Représentation via diagramme de séquence

---

### Instances de processus métier

Suite d'actions correspondant au traitement d'une demande particulière

_e.g._ déroulé de la commande n°123456

---

### Instances et corrélations

Comment savoir si deux messages sont liés ?

- (méta)données,
  _e.g._ identifiant de session
- plus complexe:
  - acteurs non connus au démarrage du processus
    _.eg._ un nouveau participant arrive en cours d’exécution
  - identifiants non partagés entre acteurs
  - processus / service partagé entre plusieurs autres processus

---

### Limites des services simples dans un cadre SOA

- Garder l’aspect **couplage faible**
- Adresses / points d’accès
  - fixes / utilisation d’un annuaire\
    → complexe si beaucoup de services
- Interactions complexes codées dans chaque client

---

## Patrons d'intégration

---

### Design patterns spécifiques à l'intégration dans les systèmes d'information

Basé sur la manipulation et la transmission de messages métier

≠ catégories:
canaux, routage, transformation, _etc_

---

### Quelques exemples de patrons d'intégration

---

### [Queues](https://www.enterpriseintegrationpatterns.com/patterns/messaging/PointToPointChannel.html)

![](PointToPointSolution.gif)

Chaque message transmis 1 fois

---

### [Topics](https://www.enterpriseintegrationpatterns.com/patterns/messaging/PublishSubscribeChannel.html)

![](PublishSubscribeSolution.gif)

≈ broadcast

---

### [Canaux persistants](https://www.enterpriseintegrationpatterns.com/patterns/messaging/GuaranteedMessaging.html)

![](GuaranteedMessagingSolution.gif)

Pour ne pas perdre de message

---

### Consommateur

- Synchrone\
  Le consommateur récupère explicitement le message avec un appel bloquant

- Asynchrone\
  Le consommateur enregistre un callback

Similaire à synchrone vs asynchrone en programmation client/serveur

---

### Frameworks de (transmission de) messages

Aller au delà de REST / SOAP+HTTP

- Performance (binaire, protocole dédié, etc)
- Intégration de
  - capacités de routage
  - garanties sur les messages\
    (exactly once, persistance)
- Rôle de tampon en cas de charge temporaire

---

### Sérialisation: quelques implémentations

texte (JSON / XML + compression)

ou binaire:

- native au langage (e.g. `Serializable` en Java)
- bibliothèques ou standards (multi-langages)
  - IDL / Common Data Representation (CORBA)
  - [Thrift](https://thrift.apache.org/), [Avro](http://avro.apache.org/), [Protocol Buffers](https://developers.google.com/protocol-buffers/)
    → [Comparatif](https://fr.slideshare.net/IgorAnishchenko/pb-vs-thrift-vs-avro)

---

### Transmission: quelques standards

- [AMQP](http://www.amqp.org/): multi-langage, fiabilité, queues, topics
- [MQTT](http://mqtt.org/): multi-langage, léger
- [JMS](https://fr.wikipedia.org/wiki/Java_Message_Service): standard Java, fiabilité, queues, topics

---

### Transmission: quelques implémentations

- [RabbitMQ](https://www.rabbitmq.com/): queues, deliver exactly once, clusters, routing, AMQP, MQTT, langages variés
- [ActiveMQ](http://activemq.apache.org/): topics, queues, apache, EIP via intégration avec Apache Camel, AMQP, JMS, langages variés
- [Kafka](https://kafka.apache.org/): topics, clusters, traitement de flux, langages variés, bonne capacité d’absorption des pics de messages
- [ZeroMQ](https://zeromq.org/): basique, léger, efficace, langages variés

---

### [Pipeline de services](https://www.enterpriseintegrationpatterns.com/patterns/messaging/PipesAndFilters.html)

![](PipesAndFilters.gif)

Traitement composite de messages

- 1 étape = 1 appel à un service
- réponse d’un service = entrée du service suivant

---

### Filtres: [messages](https://www.enterpriseintegrationpatterns.com/patterns/messaging/Filter.html) / [contenu](https://www.enterpriseintegrationpatterns.com/patterns/messaging/ContentFilter.html)

![](MessageFilter.gif)

![](ContentFilter.gif)

- Peuvent supprimer (des parties d’) un message
- Se combinent bien avec les pipelines

---

### Transformations: [restructuration](https://www.enterpriseintegrationpatterns.com/patterns/messaging/MessageTranslator.html)

![](MessageTranslator.gif)

Traduction de schéma

---

### Transformations: [Encapsulation](https://www.enterpriseintegrationpatterns.com/patterns/messaging/EnvelopeWrapper.html)

![](Wrapper.gif)

_e.g._ transmission via SOAP

---

### Transformations: [Enrichissement](https://www.enterpriseintegrationpatterns.com/patterns/messaging/DataEnricher.html)

![](DataEnricher.gif)

---

### [Routage basés sur les messages](https://www.enterpriseintegrationpatterns.com/patterns/messaging/ContentBasedRouter.html)

![](ContentBasedRouter.gif)

- Seul le contenu du message (header / payload) est utilisé pour décider de la destination
- Possibilité de transformation préalable pour gérer les cas complexes

---

### [Routage dynamique](https://www.enterpriseintegrationpatterns.com/patterns/messaging/DynamicRouter.html)

![](DynamicRouter.gif)

Choix de la destination selon l'état du système

_e.g._ équilibrage de charge

---

### [Pipeline dynamique](https://www.enterpriseintegrationpatterns.com/patterns/messaging/RoutingTable.html)

![](RoutingTableSimple.gif)

Similaire au [pipeline de services](https://www.enterpriseintegrationpatterns.com/patterns/messaging/PipesAndFilters.html), mais avec une séquence qui dépend du contenu

---

### [Division / Aggrégation](https://www.enterpriseintegrationpatterns.com/patterns/messaging/DistributionAggregate.html)

![](DistributionAggregate.gif)

[Diviseur](https://www.enterpriseintegrationpatterns.com/patterns/messaging/Sequencer.html) et [aggrégateur](https://www.enterpriseintegrationpatterns.com/patterns/messaging/Aggregator.html) peuvent être utilisés séparément.

---

### [Réordonnancement de messages](https://www.enterpriseintegrationpatterns.com/patterns/messaging/Resequencer.html)

![](Resequencer.gif)

Batch (attente, tri, traitement):\
déclenchement sur quantité ou timeout

_vs_

Stream: file de priorité

---

## Bus de services

Serveur / cluster

- Hébergeant des services / composants légers (filtres, etc)
- Fournissant / s’intégrant avec des frameworks de messages
- Capacités de routage
- Intègre de nombreux connecteurs
- Système de gestion de services

---

## Point Techno

### Apache Camel

---

### [Apache Camel](https://camel.apache.org/)

DSL à travers une API Java\
pour assembler des EIPs

```
CamelContext context = new DefaultCamelContext();

context.addRoutes(new RouteBuilder() {
        public void configure() {
            from("test-jms:queue:test.queue")
            .to("bean:validateOrder")
            .to("file://test");
        }
    });
```

---

### [Apache Camel](https://camel.apache.org/)

DSL XML\
pour assembler des EIPs

```
<route>
   <from uri="jms:queue:order"/>
   <to uri="bean:validateOrder"/>
   <to uri="mina:tcp://mainframeip:4444?textline=true"/>
   <to uri="bean:sendConfirmEmail"/>
</route>
```

---

### [Apache Camel](https://camel.apache.org/)

#### Composants et schémas d'URL

Composants/EIP en CAMEL\
= \
préfixe de l'URL

⇒ 280 composants listés sur le site Web

---

### [Apache Camel](https://camel.apache.org/)

#### Déploiement / intégration

- Intégrable dans de nombreux systèmes\
  _e.g._ Spring (Boot), OSGi (Karaf), etc
- Déployable dans des infras cloud\
  _e.g._ Kubernetes (Camel K), Quarkus

---

### [Spring Integration](https://spring.io/projects/spring-integration)

Similaire à Apache Camel

DSL XML & Java

---

## Point Techno

### RabbitMQ & Spring AMQP

---

### [RabbitMQ](https://www.rabbitmq.com/)

Serveur de transmission de messages

Queues & Topics

Distribution (sharding)

Persistence des messages

---

### RabbitMQ: serveur

Implémenté en Erlang (lng fonctionnel)

Facilement déployable via Docker

Interface de monitoring (`management`)

---

### RabbitMQ: Clients

Nombreux langages supportés via AMQP

Intégration dans de nombreux frameworks:\
_e.g._ Camel, Spring, etc

---

### RabbitMQ: Clients - API

- `Connection`: abstraction sur la communication réseau (socket, authentification, _etc_)
- `Channel`: composant de gestion des messages
- `Queue`: re-déclarable
- `Channel.basicPublish`: envoyer un message

---

### RabbitMQ: Clients - connect

```
ConnectionFactory factory = new ConnectionFactory();
factory.setHost("localhost");
try (Connection connection = factory.newConnection();
     Channel channel = connection.createChannel()) {
    channel.queueDeclare(QUEUE_NAME,
             false, false, false, null);
    // do some work
}
```

_c.f._ [tutoriel](https://www.rabbitmq.com/tutorials/tutorial-one-java.html)

---

### RabbitMQ: Clients - send

```
String message = "Hello World!";
channel.basicPublish("", QUEUE_NAME, null,
         message.getBytes(StandardCharsets.UTF_8));
System.out.println(" [x] Sent '" + message + "'");
```

_c.f._ [tutoriel](https://www.rabbitmq.com/tutorials/tutorial-one-java.html)

---

### RabbitMQ: Clients - receive

```
DeliverCallback deliverCallback = (consumerTag, delivery) -> {
    String message = new String(delivery.getBody(), "UTF-8");
    System.out.println(" [x] Received '" + message + "'");
};
channel.basicConsume(QUEUE_NAME, true, deliverCallback, consumerTag -> { });
```

_c.f._ [tutoriel](https://www.rabbitmq.com/tutorials/tutorial-one-java.html)

---

### Avec [Spring AMQP](https://spring.io/projects/spring-amqp)

- `RabbitTemplate` pour gérer les messages
  - `convertAndSend(queueName, msg)` pour envoyer
- `@RabbitListener(queues)` sur une classe pour recevoir
  - `@RabbitHandler` sur la méthode de réception

---

### Avec [Spring AMQP](https://spring.io/projects/spring-amqp) - send

```
public class Tut1Sender {

    @Autowired
    private RabbitTemplate template;

    @Scheduled(fixedDelay = 1000, initialDelay = 500)
    public void send() {
        String message = "Hello World!";
        this.template.convertAndSend("hello", message);
        System.out.println(" [x] Sent '" + message + "'");
    }
}
```

_c.f._ [tutoriel](https://www.rabbitmq.com/tutorials/tutorial-one-spring-amqp.html)

---

### Avec [Spring AMQP](https://spring.io/projects/spring-amqp) - receive

```
@RabbitListener(queues = "hello")
public class Tut1Receiver {

    @RabbitHandler
    public void receive(String in) {
        System.out.println(" [x] Received '" + in + "'");
    }
}
```

_c.f._ [tutoriel](https://www.rabbitmq.com/tutorials/tutorial-one-spring-amqp.html)

---

## References

- http://www.enterpriseintegrationpatterns.com/
  Images [![](https://www.enterpriseintegrationpatterns.com/img/cc_by.png)](http://creativecommons.org/licenses/by/4.0/) Gregor Hohpe and Bobby Woolf
