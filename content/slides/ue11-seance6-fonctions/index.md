---
title: Introduction à la programmation - séance 6
summary: "Séance 6 - fonctions"
authors: [admin]
tags: [code]
categories: [cours]
date: "2023-11-29T00:00:00Z"
slides:
  theme: "white"
  highlight_style: "github"
---

## Introduction à la programmation d'expériences

### Séance 6 - fonctions

[Emmanuel Coquery](https://perso.liris.cnrs.fr/emmanuel.coquery/home)

<https://perso.liris.cnrs.fr/emmanuel.coquery/home/enseignement/ue11/>

---

## Récapitulatif

- expressions
- types
- instructions (affectation, print)
- script
- conditionnelles, boucles
- chaînes de caractères (str)
- tableaux

---

## Fonctions

- En mathématiques: associe une valeur résultat à chaque (combinaison) de valeur passée en argument
- En informatique: effectue un calcul sur la base de la valeur des arguments
  - en verra plus tard: peut interagir avec l'environnement

---

### Définition de fonction

Syntaxe

```python
def ma_fonction(x,y,z,...):
  instruction_1
  instruction_2
  ...
  return expr_resultat
```

---

### Définition de fonction (2)

#### Paramètres formels

- les arguments x,y,z,... donnés à la définition de la fonction sont les paramètres _**formels**_
  - $\approx$ variables initialisées avec les valeurs passée en argument lors de l'utilisation de la fonction

---

### Définition de fonction (3)

#### Corps de la fonction

- ce sont les instructions du bloc de la fonction
- définit comment le résultat est calculé

---

### Définition de fonction (4)

#### Instruction return

- l'instruction `return` indique le résultat renvoyé par la fonction
- cette instruction arrête l'exécution du corps de la fonction

---

### Utilisation de fonction

Syntaxe

```python
ma_fonction(expr_x, expr_y, expr_z, ...)
```

---

### Appel (utilisation) de fonction

#### Paramètres effectifs

- C'est la valeur des expressions expr_x, expr_y, expr_z
- Cette valeur est utilisée pour initialiser les paramètres formels

---

### Déroulement d'un appel de fonction

1. Les paramètres effectifs sont évalués
2. Les paramètres formels sont créés et prennent la valeur des paramètres effectifs correspondant
3. Le corps de la fonction est exécuté
4. Quand l'instruction return est exécutée:
   - la valeur de l'expression résultat est calculée
   - toutes les variables (et les paramètres formels) créées dans la fonction sont détruites
   - la valeur de _**retour**_ est celle du return

---

### Exemple de fonction

```python
def abs(x):
  if x < 0:
    return -x
  else:
    return x

print("La valeur absolue de -3 est "+str(abs(-3)))
print("La valeur absolue de 5 est "+str(abs(5)))
```

---

### Fonctions: bonnes pratiques

- Écrire un commentaire décrivant ce que calcule la fonction _**avant**_ d'écrire le corps de la fonction
  - On peut remplacer le commentaire par une string spéciale au tout début du corps de fonction
- Faire des petites fonctions
  - maximum 20 lignes

---

### Analyse descendante

- Remarque: on peut appeler une fonction dans le corps d'une fonction
- Pour écrire une fonction qui réalise un calcul compliqué:
  - décomposer le calcul/traitement en sous-problèmes plus simples
  - écrire une fonction pour chaque sous-problème
  - la fonction consiste alors à assembler les résultats des sous-problèmes

---

### Analyse descendante - suite

- chaque fonction correspondant à un sous-problème peut être, au besoin, réalisée avec la même méthodologie
