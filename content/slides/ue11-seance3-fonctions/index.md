---
title: Introduction à la programmation - séance 3
summary: "Séance 3 - fonctions"
authors: [admin]
tags: [code]
categories: [cours]
date: "2022-12-12T00:00:00Z"
slides:
  theme: "white"
  highlight_style: "github"
---

# Introduction à la programmation

## Séance 3 - fonctions

[Emmanuel Coquery](https://perso.liris.cnrs.fr/emmanuel.coquery/home)

https://perso.liris.cnrs.fr/emmanuel.coquery/home/enseignement/ue11/

---

## Récapitulatif

- expressions, instructions
- types
- contrôle: if, for, while

---

### Rappel évaluation

1h en milieu de matinée

Si vous avez des questions c'est la dernière chance

---

### Corrigé exercice

Calculer les $k$ premiers nombres premiers

"Live-coding"

https://forge.univ-lyon1.fr/-/snippets/63

---

## Fonctions

- En mathématiques: associe une valeur résultat à chaque (combinaison) de valeur passée en argument
- En informatique: effectue un calcul sur la base de la valeur des arguments
  - en verra plus tard: peut interagir avec l'environnement

---

### Définition de fonction

Syntaxe

```
def ma_fonction(x,y,z,...):
  instruction_1
  instruction_2
  ...
  return expr_resultat
```

---

### Définition de fonction

#### Paramètres formels

- les arguments x,y,z,... donnés à la définition de la fonction sont les paramètres _**formels**_
  - $\approx$ variables initialisées avec les valeurs passée en argument lors de l'utilisation de la fonction

---

### Définition de fonction

#### Corps de la fonction

- ce sont les instructions du bloc de la fonction
- définit comment le résultat est calculé

---

### Définition de fonction

#### Instruction return

- l'instruction `return` indique le résultat renvoyé par la fonction
- cette instruction arrête l'exécution du corps de la fonction

---

### Utilisation de fonction

Syntaxe

```
ma_fonction(expr_x, expr_y, expr_z, ...)
```

---

### Appel (utilisation) de fonction

#### Paramètres effectifs

- C'est la valeur des expressions expr_x, expr_y, expr_z
- Cette valeur est utilisée pour initialiser les paramètres formels

---

### Déroulement d'un appel de fonction

1. Les paramètres effectifs sont évalués
2. Les paramètres formels sont créés et prennent la valeur des paramètres effectifs correspondant
3. Le corps de la fonction est exécuté
4. Quand l'instruction return est exécutée:
   - la valeur de l'expression résultat est calculée
   - toutes les variables (et les paramètres formels) créées dans la fonction sont détruites
   - la valeur de _**retour**_ est celle du return

---

### Exemple de fonction

```raw
def abs(x):
  if x < 0:
    return -x
  else:
    return x

print("La valeur absolue de -3 est "+str(abs(-3)))
print("La valeur absolue de 5 est "+str(abs(5)))
```

---

### Fonctions: bonnes pratiques

- Écrire un commentaire décrivant ce que calcule la fonction _**avant**_ d'écrire le corps de la fonction
  - On peut remplacer le commentaire par une string spéciale au tout début du corps de fonction
- Faire des petites fonctions
  - maximum 20 lignes

---

### Analyse descendante

- Remarque: on peut appeler une fonction dans le corps d'une fonction
- Pour écrire une fonction qui réalise un calcul compliqué:
  - décomposer le calcul/traitement en sous-problèmes plus simples
  - écrire une fonction pour chaque sous-problème
  - la fonction consiste alors à assembler les résultats des sous-problèmes

---

### Analyse descendante - suite

- chaque fonction correspondant à un sous-problème peut être, au besoin, réalisée avec la même méthodologie

---

## Point d'étape

- structures de controle
  - conditionnelles, boucles
- fonctions

---

## Sapins

<img src="./IMG_20221214_133644.jpg" alt="sapin" height="500"/>

---

### Exercice - sapin 1

- Écrire un programme qui affiche un sapin en texte, par exemple un sapin de hauteur 5:

```raw
    #
   ###
  #####
 #######
    |
```

- on souhaite pouvoir paramétrer le dessin par la hauteur du sapin

---

### Exercice - forêt de sapins 1

- Écrire un programme qui affiche $k$ sapins sur le modèle de l'exercice précédent, mais alignés horizontalement
- ici 4 sapins de hauteur 5:

```raw
    #       #       #       #
   ###     ###     ###     ###
  #####   #####   #####   #####
 ####### ####### ####### #######
    |       |       |       |
```

---

### Exercice - sapin 2

On change à présent la forme du sapin:

```raw
      #
     ###
    #####
     ###
    #####
   #######
    #####
   #######
  #########
      |
```

- Faire un programme pour le sapin seul, puis pour une forêt
- Ajouter un paramètre pour la hauteur des "étages"

---

### Exercice - sapin de Noël

On ajoute des décorations

```raw
      #
     ###
    o###o
     ###
    #####
   o#####o
    #####
   #######
  o#######o
      |
```

---

## Bibliothèques et modules

Modules: groupe de fonctions (et plus)

Bibliothèques: code rassemblant ensemble de fonctionnalités réutilisables

Une bibliothèque contient un ou plusieurs modules

---

## Installer une bibliothèque avec conda

en shell

```raw
conda install la_bibliotheque
```

---

## Utiliser un module

dans du code python

```
import le_module

...
le_module.la_fonction(...)
...
```

---

## Tests unitaires

Automatiser ses tests

---

### Installation

- activer l'environment:

  ```
  conda create -n ue11 python=3.9
  conda activate ue11
  ```

- pytest (dans anaconda prompt)

  ```
  conda install pytest
  ```

  <!-- conda install -c spyder-ide spyder-unittest
  -->

---

### Exemple de test

`test_exemple.py`

```
# Un test qui réussi
def test_ex1():
    assert 3 == 2+1

# un test qui échoue
def test_ex2():
    assert 4 == 1+1
```

---

### Lancer les tests

- En shell, aller dans le répertoire des fichiers source
- Lancer la commande `pytest`

---

## Lire un fichier texte ligne par ligne

```
with open("monfichier.txt") as f:
  for une_ligne in f:
    # faire quelque chose avec la ligne
```

- `open`: "ouvre" et lit le fichier
- `f` représente l'ensemble des lignes du fichier
  - on peut boucler sur les lignes avec `for`
  - le type de f est un nouveau type
- le `with` permet de s'assurer que le fichier est correctement "fermé" une fois la lecture du contenu terminée

---

### Exemple

Afficher les lignes d'un fichier contenant un certain texte

```
nom_fichier = input("Entrez le nom du fichier: ")
a_chercher = input("Entrer la chaîne à chercher: ")
with open(nom_fichier) as f:
  for ligne in f:
    if a_chercher in ligne:
      print(ligne)
```

- `chaine1 in chaine2` vaut vrai et `chaine1` est un morceau (on dit une sous-chaîne) de `chaine2`

---

### Avent of code J1

https://adventofcode.com/2022/day/1
