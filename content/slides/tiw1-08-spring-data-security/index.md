---
title: "Transactions, Spring Data & Spring Security - Intergiciels et services"
date: 2019-10-29T09:11:00+01:00
draft: false
authors: [admin]
slides:
  theme: "white" # Reveal JS theme name
  highlight_style: "github" # Highlight JS theme name
---

# Intergiciels et services

## Transactions, Spring Data & Spring Security

[M2TIW](http://master-info.univ-lyon1.fr/TIW) - [TIW1](https://forge.univ-lyon1.fr/tiw-is/tiw-is-2022-2023) - [Emmanuel Coquery](https://perso.liris.cnrs.fr/emmanuel.coquery/home)

{{< speaker_note >}}

- Only the speaker can read these notes
- Press `S` key to view
- Press `F` key to enter fullscreen mode

{{< /speaker_note >}}

---

## Transactions

---

### ACID

- Atomicité
- Cohérence
- Isolation
- Persistence (_Durability_)

---

### Bases de données et serveurs d'application

- Fournissent des transactions ACID
- Relayées par les APIs / pilotes:
  - JDBC
  - JPA
- Pas les seules ressources concernées, _e.g._
  - Fichiers
  - Messages (services, files de messages)

⇒ Transactions multiparties complexes

---

### Two-phase commit

![](two-phase-commit.gif)

[Source IBM](http://www.ibm.com/developerworks/data/library/techarticle/dm-0611chang/)

---

### Java Transactional API (JTA)

- API de coordination des transactions
- Méthodes & annotations pour gérer les transactions
  - Classes `UserTransaction` (EJB), `EntityTransaction` (JPA)
  - Méthode `setRollbackOnly`
- Annotation pour spécifier les transactions

---

### Modes transactionnels possibles

- `@TransactionAttribute`:
  - `MANDATORY`, `REQUIRED`, `REQUIRES_NEW`, ...
- Spring: `@Transactional`

---

### Aspects pour les transactions

![](tx.png)

[Source Doc Spring](https://docs.spring.io/spring-framework/docs/current/spring-framework-reference/data-access.html#transaction-declarative)

---

## [Spring Data](https://spring.io/projects/spring-data)

---

## Objectifs

- Faciliter le développement de DAO/repositories
- Générer du code pour les parties "faciles"
- JDBC, JPA, MongoDB, Cassandra, Redis, _etc_
- Spring Boot: facilité de configuration

---

### Principe

- Point de départ: entité mappée, _e.g._ en JPA
- Spring fourni une interface générique de _Repository_
  - qu'il est possible d'étendre pour ajouter des méthodes supplémentaires
- Spring va générer une classe implémentant l'interface voulue
- Une instance de la classe générée sera placée dans le contexte Spring

---

### Exemple: entité

```
@Entity
public class Couleur {
  @Id
  @GeneratedValue
  private Long id;

  private String nom;
  private String description;
  private String colorCode;

  // ...  accesseurs, constructeurs, equals, etc
}
```

---

### Exemple: interface

```
public interface CouleurRepository
  extends CrudRepository<Couleur, Long> {
}
```

définit `save`, `findById`, `findAll`, `count`, `delete`, `existsById`, _etc_\
(_c.f._ code de [`CrudRepository`](https://github.com/spring-projects/spring-data-commons/blob/master/src/main/java/org/springframework/data/repository/CrudRepository.java))

---

### Exemple: utilisation

```
@Autowired
private CouleurRepository couleurRepository;

@Test
void testCouleurRepository() {
  Couleur c = new Couleur("vert","basique","00FF00");
  couleurRepository.save(c);
  Optional<Couleur> c2 = couleurRepository.findById(c.getId());
  assertTrue(c2.isPresent());
  assertEquals(c,c2.get());
}
```

---

### En coulisses: classe générée

```
LOG.info("Classe d'implementation de CouleurRepository: {}",
         couleurRepository.getClass());
```

donne

```
Classe d'implementation de CouleurRepository:
class com.sun.proxy.$Proxy95
```

- Spring a généré une implémentation de CouleurRepository
- Cette implémentation a été ajoutée au contexte

---

### Ajouter d'autres méthodes au Repositories

```
public interface CouleurRepository extends CrudRepository<Couleur, Long> {

  Optional<Couleur> findByNom(String nom);

}
```

```
@Test
void testMethodeGenereeParNom() {
  Couleur c = new Couleur("bleu","basique","00FF00");
  couleurRepository.save(c);
  Optional<Couleur> c2 = couleurRepository.findByNom(c.getNom());
  assertTrue(c2.isPresent());
  assertEquals(c,c2.get());
}
```

---

### Code généré via noms de méthodes

- Langage de requête basé sur le nom des méthodes
  - `find` | `By` | `Nom` | `And` | `ColorCode`
- Traduction en JPQL:\
  `SELECT c`\
  `FROM Couleur c`\
  `WHERE c.nom = :nom`\
  `AND c.colorCode = :colorCode`

[Documentation](https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#jpa.query-methods.query-creation),
[Référence](https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#repository-query-keywords)

---

### Requêtes ad-hoc

Pour les cas complexes: spécifier une requête JPQL

```
public interface CouleurRepository
  extends JpaRepository<Couleur, Long> { //!\\

  @Query("select c from Couleur c where c.colorCode = ?1")
  Optional<Couleur> getByColorCode(String colorCode);

}
```

---

## [Spring Security](https://spring.io/projects/spring-security)

---

### Objectifs et caractéristiques

- faciliter la gestion de la sécurité:
  - authentification & autorisation
- intégration Spring Web MVC
- modularité

---

### Security Context

- obtenu via le `SecurityContextHolder`
- fourni une `Authentication` permettant de récupérer
  - un _principal_
  - des _granted authorities_

---

### Principal & User Details

le _principal_ contient des informations sur l'utilisateur

- souvent une instance implémentant [`UserDetails`](https://docs.spring.io/spring-security/site/docs/5.2.1.RELEASE/api/index.html?org/springframework/security/core/userdetails/UserDetails.html)
- obtenu grâce à une instance d'un [`UserDetailsService`](https://docs.spring.io/spring-security/site/docs/5.2.1.RELEASE/api/index.html?org/springframework/security/core/userdetails/UserDetailsService.html)

---

### Granted Authorities

- une _authority_ permet d'accorder un droit \
  _e.g._ un rôle
- les _granted authorities_ sont celles attribuées à l'utilisateur (_principal_) courant.

---

### Authentification

- processus de validation et complétion d'une `Authentication`
- normalement déclenchée par le framework
  - typiquement dans un `ServletFilter`
- fait via un [`AuthenticationManager`](https://docs.spring.io/spring-security/site/docs/5.2.1.RELEASE/api/index.html?org/springframework/security/authentication/AuthenticationManager.html)
  - décliné selon le type d'authentification\
    ([DAO](https://docs.spring.io/spring-security/site/docs/5.2.1.RELEASE/api/index.html?org/springframework/security/authentication/dao/DaoAuthenticationProvider.html), [LDAP](https://docs.spring.io/spring-security/site/docs/5.2.1.RELEASE/api/index.html?org/springframework/security/ldap/authentication/AbstractLdapAuthenticationProvider.html), etc)

---

### Autorisation

- en général à travers des `ServletFilter`s \
  et/ou des aspects
- décision prise par un [`AccessDecisionManager`](https://docs.spring.io/spring-security/site/docs/5.2.1.RELEASE/api/index.html?org/springframework/security/access/AccessDecisionManager.html)

---

### Access Decision Manager

- `decide(authentication, targetObject, configAttributes)`
- `AccessDeniedException`, `InsufficientAuthenticationException`
- appelé par un `SecurityInterceptor`

---

### Cas particulier: votes

utilise des [`AccessDecisionVoter`](https://docs.spring.io/spring-security/site/docs/5.2.1.RELEASE/api/index.html?org/springframework/security/access/AccessDecisionVoter.html)s \
(avec une méthode `vote` similaire à `decide`)

![](access-decision-voting.png)

Source [Spring](https://docs.spring.io/spring-security/site/docs/5.2.1.RELEASE/reference/htmlsingle/#authz-access-voting)

---

### Intégration avec Spring Web MVC

Activation via `@EnableWebSecurity`

Classes qui étendent [`WebSecurityConfigurerAdapter`](https://docs.spring.io/spring-security/site/docs/5.2.1.RELEASE/api/index.html?org/springframework/security/config/annotation/web/configuration/WebSecurityConfigurerAdapter.html)

[Documentation](https://docs.spring.io/spring-security/site/docs/5.2.1.RELEASE/reference/htmlsingle/#mvc), [CORS](https://docs.spring.io/spring-security/site/docs/5.2.1.RELEASE/reference/htmlsingle/#cors)

[exemple login](https://docs.spring.io/spring-security/site/docs/5.2.1.RELEASE/reference/htmlsingle/#form-login-java-configuration), [exemple plus complexe](https://docs.spring.io/spring-security/site/docs/5.2.1.RELEASE/reference/htmlsingle/#multiple-httpsecurity)
