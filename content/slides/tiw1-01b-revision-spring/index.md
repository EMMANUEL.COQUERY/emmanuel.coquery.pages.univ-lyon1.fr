---
title: Intergiciels et services - Rappels Spring
summary: "Rappels autour du framework Spring"
authors: [admin]
tags: [code]
categories: [cours]
date: "2020-09-09T00:00:00Z"
slides:
  theme: "white"
  highlight_style: "github"
---

# Intergiciels et services

## Rappels Spring

[M2TIW](http://master-info.univ-lyon1.fr/TIW) - [TIW1](https://forge.univ-lyon1.fr/tiw-is/tiw-is-2022-2023) - [Emmanuel Coquery](https://perso.liris.cnrs.fr/emmanuel.coquery/home)

{{< speaker_note >}}

- Only the speaker can read these notes
- Press `S` key to view
- Press `F` key to enter fullscreen mode

{{< /speaker_note >}}

---

## Composants

### (vision très simplifiée)

- Briques logiciels (objets)
- interagissant les unes avec les autres
- pour implémenter une application
- chaque ayant une responsabilité limité

---

## Spring

- Collection de frameworks et de bibliothèques
- basée sur la notion de composant (Spring Bean)
- implémentant/facilitant la mise en œuvre de "design patterns"

---

## Contexte Spring

- Ensemble de composants Spring
- instanciés et configurés
- rendus accessibles les uns aux autres\
  _e.g._ par injection via `@Autowired`
- mecanique de contexte: [Spring Core][spring-core]

---

## Exemple de contexte

### (extrait)

![contexte-simple](./diagramme-vm-simplifie.png)

---

## Exemple de contexte

### (vue d'ensemble)

![contexte-simple](./diagramme-vm-complet.png)

---

## Configuration par annotation Java

- Syntaxe: `@UneAnnotation(p1,p2,...)`
- Indique des informations utilisées par le framework pour:
  - Faire d'une classe un bean
  - Injecter un bean dans un autre\
    (_i.e._ le rendre accessible dans un champ)
  - Exposer un contrôleur sur une url
  - etc

---

## Spring MVC

- Ensemble de composants permettant de mettre en place une application Web
- Permet de définir les composants Web propres à l'application

---

### MVC

- `@Controller`: indique un composant Contrôleur, à combiner avec `@RequestMapping` pour le chemin
- `@Service`: indique un composant Métier

---

## Rest Controller

`@RestController`

- spécialisation de `@Controller`
- gère les aspects vue dans le cadre d'une API REST
  - négociation de contenu
  - récupération des paramètres\
    `@RequestBody`, `@PathVariable`, ...
  - chemins et méthodes HTTP
    `@GetMapping`, `@DeleteMapping`, etc

---

## Digression OpenAPI / Swagger

Format pour spécifier une API REST

- Permet de documenter
- Outils (interface web, genération de code)
- [Springfox][springfox] pour générer une spécification à partir du code

---

# Aller à [Spring Data/Security](../tiw1-08-spring-data-security)

[spring-core]: https://docs.spring.io/spring-framework/docs/current/spring-framework-reference/core.html
[springfox]: https://springfox.github.io/springfox/
