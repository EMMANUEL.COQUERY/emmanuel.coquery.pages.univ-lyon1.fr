---
title: Mappings Objet Relationnel
summary: "Cours sur les mappings entre objets et modèles de données relationnels et arbres."
authors: [admin]
tags: [data, code]
categories: [cours]
date: "2019-06-20T00:00:00Z"
slides:
  theme: "white"
  highlight_style: "github"
---

:!: en cours de construction :!:

# Mappings entre modèle Objet et modèle Relationnel

## mais aussi avec les modèles arborescents

{{< speaker_note >}}

- Only the speaker can read these notes
- Press `S` key to view
- Press `F` key to enter fullscreen mode

{{< /speaker_note >}}

---

## Persistance des objets, kesako ?

- Comment faire pour :
  - Utiliser des objets au niveau métier
  - Pouvoir sauver et récupérer des objets
  - Mettre à jour des objets de manière pérenne

---

## Utilisation d'un SGBD pour stocker les objets

- SGBD: fiable, requêtes, concurrence
- Ecrire du code pour :
  - Sauver un nouvel objet dans la BD
  - Sauver les modifications d'un objet
  - Récupérer un objet à partir de la BD
  - Faire des mises à jour pérennes

---

## Rappel: comment accéder aux données de manière classique

- Bibliothèque pour accéder au SGBD \
  (e.g. [JDBC](https://fr.wikipedia.org/wiki/Java_Database_Connectivity), [Python Database API](https://www.python.org/dev/peps/pep-0249/))
- Exécuter une requête
- Itérer sur le résultat
  - Traiter chaque ligne

---

## Exemple en Java

```java
Statement s = connection.createStatement();
ResultSet rs = s.executeQuery(
    "SELECT * FROM etudiant WHERE formation='m1info'");
while(rs.next()) {
    String nom = rs.getString("nom");
    long id = rs.getLong("id");
    System.out.println("Etudiant: "+nom+" ("+id+")");
}
```

---

## Exemple en Python

```python
cur = connection.cursor()
cur.execute("SELECT * FROM etudiant WHERE formation='m1info'")
for row in cur.fetchall():
    nom = row[1]  # nom
    id = row[0]  # numero
    print("Etudiant: " + nom + " (" + str(id) + ")")
connection.close()
```

---

## Exemple en Java

### classe Etudiant

```java
public class Etudiant {

    private int numero;
    private String nom;
    private String formation;

    public Etudiant(int numero, String nom, String formation) {
        this.numero = numero;
        this.nom = nom;
        this.formation = formation;
    }
```

---

### Méthode d'ajout en base

```java
  public void save(Connection cnx) throws SQLException {
      PreparedStatement pstat = cnx.prepareStatement(
        "INSERT INTO etudiant(numero, nom, formation) "
        +"VALUES (?,?,?)");
      pstat.setInt(1, numero);
      pstat.setString(2, nom);
      pstat.setString(3, formation);
      pstat.executeUpdate();
  }
```

---

### Méthode de mise à jour

```java
  public void update(Connection cnx) throws SQLException {
      PreparedStatement pstat = cnx.prepareStatement(
        "UPDATE etudiant "
        +"SET nom=?, formation=? "
        +"WHERE numero=?");
      pstat.setString(1, nom);
      pstat.setString(2, formation);
      pstat.setInt(3, numero);
      pstat.executeUpdate();
  }
```

---

### Recherche par numéro d'étudiant

```java
  public static Etudiant getByNum(int num, Connection cnx)
    throws SQLException {
      PreparedStatement pstat = cnx.prepareStatement(
        "SELECT numero,nom,formation FROM etudiant "
        +"WHERE numero=?");
      pstat.setInt(1, num);
      ResultSet rs = pstat.executeQuery();
      if (rs.next()) {
          return new Etudiant(rs.getInt("numero"),
                              rs.getString("nom"),
                              rs.getString("formation")  );
      } else {
          return null;
      }
  }
```

---

## Exemple en Python

### Classe Etudiant

```python
class Etudiant:
    def __init__(self, numero, nom, formation):
        self.numero = numero
        self.nom = nom
        self.formation = formation
```

---

### Méthode d'ajout en base

```python
    def save(self, cnx):
        cur = cnx.cursor()
        cur.execute(
            """
            INSERT INTO etudiant(numero,nom,formation)
            VALUES (%s,%s,%s)
            """,
            (self.numero, self.nom, self.formation),
        )
```

---

### Méthode de mise à jour

```python
    def update(self, cnx):
        cur = cnx.cursor()
        cur.execute(
            """
            UPDATE etudiant
            SET nom=%s, formation=%s
            WHERE numero=%s
            """,
            (self.nom, self.formation, self.numero),
        )
```

---

### Recherche par numéro d'étudiant

```python
def getEtudiantByNum(num, cnx):
    cur = cnx.cursor()
    cur.execute(
        """
        SELECT numero, nom, formation
        FROM etudiant
        WHERE numero=%s
        """,
        (num,))
    row = cur.fetchone()
    if row is not None:
        return Etudiant(row[0], row[1], row[2])
    else:
        return None
```

---

## Avantages

- Bonne maîtrise du comportement
- Bonnes performances

À condition de bien développer

---

## Inconvénients

- Code lourd à maintenir
- Certaines fonctionnalités sont pénibles à implémenter\
  (transactions, caches, parcours d'un graphe d'objets, ...)
- Pas de langage de haut niveau pour interroger les objets stockés

---

## Cadre applicatif (_framework_) dédié

- Attaquer le problème dans sa généralité
- Limiter la quantité de code à écrire
- Proposer des optimisations (e.g. caches)
- Fournir un langage de haut niveau
- Encapsuler les interactions avec la source de données
  - Pouvoir changer ainsément la source de données

=> ORM: _Object Relational Mapping_

---

## Implémentation en Java

- Java Persistence API (JPA)
  - Description des liens classes/tables
  - API `EntityManager`
- Fournisseurs (implémentations) de JPA
  - [Hibernate](https://hibernate.org/) (Redhat)
  - [EclipseLink](https://www.eclipse.org/eclipselink/)
  - [OpenJPA](http://openjpa.apache.org/) (Apache)

---

## Implémentations en Python

Pas d'API standardisée

- [SQLAlchemy](https://www.sqlalchemy.org/)
- [Django ORM](https://docs.djangoproject.com/en/2.2/topics/db/)

---

## ORM: Principes

### Décrire les liens entre

- les structures en mémoires\
  (classes, pointeurs)
- les structures de persistence \
  (tables, contraintes)

---

## ORM: Principes

### Traduire

- des demandes d'accès
  - à un champ (autre classe, collection)
  - pour récupérer un objet particulier
- en requêtes (SQL)

---

## ORM: Principes

### Gérer les objets

Instancier des objets\
selon le résultats des requêtes

Exécuter des requêtes\
pour mettre à jour les données en base

---

## Liens classes ↔︎ schéma

Cas simple

- classe ↔︎ table
- objet (instance) ↔︎ n-uplet dans une table
- champ ↔︎ attribut
- type SQL ↔︎ type Java ou Python (ou ...)

---

## Exemple

<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" width="528px" viewBox="-0.5 -0.5 528 206" content="&lt;mxfile modified=&quot;2019-09-20T09:10:23.273Z&quot; host=&quot;www.draw.io&quot; agent=&quot;Mozilla/5.0 (Macintosh; Intel Mac OS X 10.14; rv:68.0) Gecko/20100101 Firefox/68.0&quot; etag=&quot;ueBvN1ptZgyiRHTG721D&quot; version=&quot;11.2.9&quot; type=&quot;google&quot; pages=&quot;1&quot;&gt;&lt;diagram id=&quot;qqWe8XC84XqVHvskMs5r&quot; name=&quot;Page-1&quot;&gt;7Vltb9s2EP41Rj91kETZsT/GTtINy9AAGbC23yiRkohSokfRtZ1fv6NEStSLX9I4a4rNARLfwzuSd/foeFQmaJXvPki8zv4QhPJJ4JHdBN1MgmCBAvitgX0NTNGsBlLJSA35LfDInqgBPYNuGKFlR1EJwRVbd8FYFAWNVQfDUoptVy0RvLvqGqd0ADzGmA/RvxhRmfFijlr8V8rSzKwc+mYgx1bXOFJmmIitA6HbCVpJIVT9Ld+tKNehs2Gp7e4OjDb7krRQ5xh8eJovfl/+9ri6uYs+frmLtx/Zl/dmlm+Yb4y/t2pDGIYp602rvQ1EuWU5xwVIyzhjnNzjvdjolUuF469WWmZCsidRKMxhyAcAhqUyeUVeR+NRWwKs0YRxvhJcyGo15HnzOEk0DpoOfld9AJdiUxBKzCJay6zhh1Y2e/eqTUjxtUlgoO1pCfoPNnxeA93jUtmt25TpUYLLrFpPC5iztIDvMVhT2NrSxJFKRXcHE+Q3aYenhYqcKrkHFWNgibLvituWdf7MYJnDuIXBsCF62kzckgG+GD48gxvBgBvFJqdSTNA1wH6Awunsaj4J4HlGu2uIQSHyegywvzea18s/hRKt5OgmQsITwkTRt8h9ViTjNgNK2ixwmqgmyZYshai46vLKQOUax6xI7yurm7BHH00OnUcGNeDaLKDE2vIDRw2ppFBYOTKk2jLnLDocfiKHHDGkQGeSYvZapED/F4y3WjD8H14xwqMVwxSMxYmKwRR7ZsUIIib+20UDve2iMVsMiAEZ4jr8dSjcBNkcmt1WZ40HO5vqpOsgNXseGYqEJFS+jwXneF3SWqeRHH44wbCgncpRisG5NSYEEt9RDTta9aLduTpcTM3f2uNIQC/cB+VoDCIod2lVuLRLmo419RG+ihdh0LjdHUyqTy8mJlzrnV5IcMhXpbuYR0lwbLeZJoRhdKtWPTwDI/O0G1OgirZ+yXxQHi42WVs/jk2pQXludl4UWGIRp5NqtkAOq7/TPdW7c5Xrduq4+rN8PsRIipOAoAuRrtmic2acFxs4PRpno4FTJ6Nlj5ILBuwygfhun/4twzdMop8zdhocOyo0Xh+aFu81ONA9aDxTedtRuz0OxC729M9k0ESbkwMtBXQhCa9aWa0ycdtnr1n0Rd0rmnXb13DYpQTeSJfiz1+rTbkavg2hZ1xuDsXwFWI00uGPxqgBLx+j4QVwEBtKUmrvUpRHYnvbAssKgAH32rekBbnW7+r0BYnjsmRxl8B0x9Qn5/tnTcNfpka62RlWVsLeCgX4+6lS9LypBWpL5PsWaI0rqWP9QCWDsFW9Xct67d7xfOo6tZExPXXNDzy46qZUnWydhwRxGDAdIYDFJOXQ83zr7neMFGaFBzj9lHPD7PHPD3u8qv00Vu7Lx95EfSL7fYLWgRhMVHG0cfsFtB1eTX8YbYNzedvl7PQEYy9IzvDnIGevNvZfa3w3N9GluAli+36/Vm//R4Ju/wE=&lt;/diagram&gt;&lt;/mxfile&gt;" onclick="(function(svg){var src=window.event.target||window.event.srcElement;while (src!=null&amp;&amp;src.nodeName.toLowerCase()!='a'){src=src.parentNode;}if(src==null){if(svg.wnd!=null&amp;&amp;!svg.wnd.closed){svg.wnd.focus();}else{var r=function(evt){if(evt.data=='ready'&amp;&amp;evt.source==svg.wnd){svg.wnd.postMessage(decodeURIComponent(svg.getAttribute('content')),'*');window.removeEventListener('message',r);}};window.addEventListener('message',r);svg.wnd=window.open('https://www.draw.io/?client=1&amp;lightbox=1&amp;edit=_blank');}}})(this);" style="cursor:pointer;max-width:100%;max-height:206px;"><defs/><g><path d="M 161 44 L 161 27.5 Q 161 14 147.5 14 L 14.5 14 Q 1 14 1 27.5 L 1 44" fill="#008cff" stroke="#000000" stroke-width="2" stroke-miterlimit="10" pointer-events="none"/><path d="M 1 44 L 1 90.5 Q 1 104 14.5 104 L 147.5 104 Q 161 104 161 90.5 L 161 44" fill="none" stroke="#000000" stroke-width="2" stroke-miterlimit="10" pointer-events="none"/><path d="M 1 44 L 161 44" fill="none" stroke="#000000" stroke-width="2" stroke-miterlimit="10" pointer-events="none"/><g fill="#FFFFFF" font-family="Helvetica" text-anchor="middle" font-size="14px"><text x="80" y="34.5">Etudiant</text></g><g fill="#000000" font-family="Helvetica" font-size="12px"><text x="6.5" y="61.5">numero: 12345678</text><text x="6.5" y="75.5">nom: "Toto"</text><text x="6.5" y="89.5">formation: "m1info"</text></g><path d="M 161 144 L 161 127.5 Q 161 114 147.5 114 L 14.5 114 Q 1 114 1 127.5 L 1 144" fill="#008cff" stroke="#000000" stroke-width="2" stroke-miterlimit="10" pointer-events="none"/><path d="M 1 144 L 1 190.5 Q 1 204 14.5 204 L 147.5 204 Q 161 204 161 190.5 L 161 144" fill="none" stroke="#000000" stroke-width="2" stroke-miterlimit="10" pointer-events="none"/><path d="M 1 144 L 161 144" fill="none" stroke="#000000" stroke-width="2" stroke-miterlimit="10" pointer-events="none"/><g fill="#FFFFFF" font-family="Helvetica" text-anchor="middle" font-size="14px"><text x="80" y="134.5">Etudiant</text></g><g fill="#000000" font-family="Helvetica" font-size="12px"><text x="6.5" y="161.5">numero: 23456789</text><text x="6.5" y="175.5">nom: "Titi"</text><text x="6.5" y="189.5">formation: "m2bioinfo"</text></g><rect x="327" y="20" width="200" height="180" fill="#ffffff" stroke="#c0c0c0" pointer-events="none"/><g transform="translate(327.5,20.5)"><switch><foreignObject style="overflow:visible;" pointer-events="all" width="198" height="178" requiredFeatures="http://www.w3.org/TR/SVG11/feature#Extensibility"><div xmlns="http://www.w3.org/1999/xhtml" style="display: inline-block; font-size: 12px; font-family: Helvetica; color: rgb(0, 0, 0); line-height: 1.2; vertical-align: top; width: 199px; height: 179px; overflow: hidden; white-space: nowrap; text-align: left;"><table style="width: 100% ; height: 100% ; border-collapse: collapse" width="100%" cellpadding="4" border="1"><tbody><tr style="background-color: #a7c942 ; color: #ffffff ; border: 1px solid #98bf21"><th align="left">numero</th><th align="left">nom</th><th align="left">formation</th></tr><tr style="border: 1px solid #98bf21"><td>12345678</td><td>'Toto'</td><td>'m1info'</td></tr><tr style="background-color: #eaf2d3 ; border: 1px solid #98bf21"><td>23456789</td><td>'Titi'<br /></td><td>'m2bioinfo'</td></tr><tr style="border: 1px solid #98bf21"><td><br /></td><td><br /></td><td><br /></td></tr><tr style="background-color: #eaf2d3 ; border: 1px solid #98bf21"><td><br /></td><td><br /></td><td><br /></td></tr></tbody></table></div></foreignObject><text x="99" y="95" fill="#000000" text-anchor="middle" font-size="12px" font-family="Helvetica">[Not supported by viewer]</text></switch></g><path d="M 327 23 L 327 0 L 527 0 L 527 23" fill="#ffffff" stroke="#000000" stroke-miterlimit="10" pointer-events="none"/><path d="M 327 23 L 327 200 L 527 200 L 527 23" fill="none" stroke="#000000" stroke-miterlimit="10" pointer-events="none"/><path d="M 327 23 L 527 23" fill="none" stroke="#000000" stroke-miterlimit="10" pointer-events="none"/><g fill="#000000" font-family="Helvetica" font-weight="bold" text-anchor="middle" font-size="12px"><text x="426.5" y="16">etudiant</text></g><path d="M 161 59 L 235 59 Q 245 59 245 67.5 L 245 71.75 Q 245 76 255 76 L 321.63 76" fill="none" stroke="#000000" stroke-miterlimit="10" pointer-events="none"/><path d="M 326.88 76 L 319.88 79.5 L 321.63 76 L 319.88 72.5 Z" fill="#000000" stroke="#000000" stroke-miterlimit="10" pointer-events="none"/><path d="M 161 159 L 234 159 Q 244 159 244 149 L 244 120 Q 244 110 254 110 L 320.63 110" fill="none" stroke="#000000" stroke-miterlimit="10" pointer-events="none"/><path d="M 325.88 110 L 318.88 113.5 L 320.63 110 L 318.88 106.5 Z" fill="#000000" stroke="#000000" stroke-miterlimit="10" pointer-events="none"/></g></svg>

---

## Identifiants et clés

Identifier chaque instance\
par une donnée

(la base de données ne connaît pas les pointeurs)

champ(s) ↔︎ clé primaire

Également utile pour les problématiques de cache

---

## Exemple de définition en Java

```java
package mif04.orm;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity // annotation indiquant une classe
        // à lier à la base de données
public class Etudiant {
  @Id // le champ numéro identifie un étudiant
  private int numero;
  private String nom;
  private String formation;

  // ...
}
```

---

## Exemple de définition en Python

```python
from sqlalchemy import Column, Integer, String
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()


class Etudiant(Base):
    __tablename__ = "etudiant"

    numero = Column(Integer, primary_key=true)
    nom = Column(String)
    formation = Column(String)

```

---

## Côté base de données

### (en PostgreSQL)

```sql
CREATE TABLE etudiant(
  numero INTEGER PRIMARY KEY,
  nom text,
  formation text
)
```

---

## Création d'un Entity Manager (Java)

```java
EntityManager em =
  Persistence
    .createEntityManagerFactory("pu-orm")
    .createEntityManager();
```

À faire une seule fois par contexte d'exécution

---

## Exemple de création d'objet (Java)

```java
    Etudiant e = new Etudiant();
    e.setNom("Toto");
    e.setFormation("m1info");
    e.setNumero(12345678);
    em.getTransaction().begin();
    em.persist(e);
    em.getTransaction().commit();
```
