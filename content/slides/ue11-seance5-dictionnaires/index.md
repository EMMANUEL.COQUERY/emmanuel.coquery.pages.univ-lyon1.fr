---
title: Introduction à la programmation - séance 5
summary: "Séance 5 - dictionnaires"
authors: [admin]
tags: [code]
categories: [cours]
date: "2023-01-12T00:00:00Z"
slides:
  theme: "white"
  highlight_style: "github"
---

# Introduction à la programmation

## Séance 5 - dictionnaires

[Emmanuel Coquery](https://perso.liris.cnrs.fr/emmanuel.coquery/home)

https://perso.liris.cnrs.fr/emmanuel.coquery/home/enseignement/ue11/

---

# Résumé

- expressions, types, variables
- instructions, affectation, print, input
- structures de contrôle (if, for, while)
- fonctions
- chaînes de caractères (str), tableaux (list)

---

# Contrôle sur machine

---

# Tri à bulles

Principe: parcourir le tableau et inverser les cases consécutives qui ne sont
pas dans le bon ordre, puis répéter cette action jusqu'à ce que le tableau soit
trié.

---

# Dictionnaires

- Tableau: cases indexées (repérées, désignées) par position
- Dictionnaire: cases indexées par des valeurs de base (int, float, str, bool)

---

## Clés et valeurs

- clé (key): valeur utilisée pour indexée une case
- valeur (value): valeur contenue dans la case
- entrée (entry): paire (clé,valeur) correspondant à une case

---

## Création, accès

- Nouveau dictionnaire: `{}` ou `dict()`
- Nouvelle entrée

  `mon_dictionnaire[cle] = valeur`

- Lire la valeur d'une entrée: `mon_dictionnaire[cle]`

---

## Parcours de dictionnaire

Boucle `for`: par des clés

```
for c in mon_dictionnaire:
    print("La valeur associée à ", c,
          "est", mon_dictionnaire[c])
```

---

## Parcours de dictionnaire - suite

Boucle `for`: par des valeurs

```
for v in mon_dictionnaire.values():
    print("La valeur ", v, " est dans le dictionnaire")
```

---

## Parcours de dictionnaire - suite 2

Boucle `for`: par des entrées

```
for c,v in mon_dictionnaire.items():
    print("La valeur associée à ", c,
          "est", v)
```

---

## Test d'appartenance

Vérifier si une clé est dans un dictionnaire

```
cle in mon_dictionnaire
```

---

## Exercice: comptage de mots

Lire un fichier texte dont le nom est saisi au clavier.
Pour chaque mot donner le nombre fois où il apparait

---

## Notation

```
{ "salut": 5, "bonjour": 7 }
```

---

## Dictionnaires hétérogènes

Les valeurs d'un dictionnaire n'ont pas toujours le même type

Utile pour structurer l'information

```
{
  "nom": "Blanc-Sec",
  "prenom": "Adèle",
  "age": 25
}
```

---

## Imbrication de structures

On peut avoir des tableaux ou des dictionnaires comme valeurs

```
{ "nom":"Blanc-Sec" , "adresses": [
  { "ville": "Meudon" },
  { "ville": "Paris", "arrondissement": 14,
    "rue": "rue Bezout" , "numero": 43 }
]}
```

---

## Fichiers JSON

Format de fichier texte contenant dans données sous forme de tableaux et de dictionnaire

Utilisant la syntaxe JavaScript Object Notation

Syntaxe proche de celle de Python pour les tableaux et les dictionnaires

---

### Lire et écrire du JSON

```
import json
# lecture depuis un fichier
with open(mon_fichier) as f:
  mes_donnees = json.load(f)
# ecriture dans un fichier
with open(mon_fichier, 'w') as f:
  json.dump(mes_donnees, f)
```

---

### Exercice: villes d'un carnet d'adresses

- Écrire un ficher JSON contenant un tableau de personnes, ces personnes ayant la structure suivante:

```
{ "nom": "...", "prenom": "...",
  adresses:[{"ville": "...",
             "rue": "...",
             "numero": "..."}]}
```

- Via un programme, lire ce fichier, puis écrire un autre fichier JSON contenant le tableau des villes qui apparaissent dans le carnet d'adresses.
  - Chaque ville doit apparaître au plus une fois.
