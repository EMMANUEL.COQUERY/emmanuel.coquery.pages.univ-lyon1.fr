---
title: Introduction à l'analyse des données - séance 6
summary: "Séance 6 - indexes multi-niveaux"
authors: [admin]
tags: [code]
categories: [cours]
slides:
  theme: "white"
  highlight_style: "github"
---

## Introduction à l'analyse de données

### Séance 6 - indexes multi-niveaux

[Emmanuel Coquery](https://perso.liris.cnrs.fr/emmanuel.coquery/home)

<https://perso.liris.cnrs.fr/emmanuel.coquery/home/enseignement/ue8/>

---

## Vu précédement

- Données tabulaires
- Pandas, Dataframes, Series, indexes
- Filtrer les données (colonnes, lignes)
- Combiner des données, (concat, jointure, aggrégation)
- Figures

Terminer la séance précédente

---

## Index à plusieurs valeurs

MultiIndex en pandas

- Identifier une ligne avec une **combinaison** de valeurs
- _e.g._ une paire de valeurs (nom, prénom)

---

## Niveaux (level)

- chaque composant de l'index correspond à un niveau (level)
- le niveau peut être un numéro (à partir de 0) et/ou un nom
- noms de niveaux modifiables via:

  ```python
  mon_index.set_level(
    "nouveau_nom",
    level="ancien_niveau")
  ```

---

## Valeurs de l'index

```python
mon_index.get_level_values(le_niveau)
```

---

## Digression: Changements d'indexes

Changer l'index d'une DataFrame / Series

```python
# Transforme une colonne en index
ma_df.set_index("ma_colonne")
```

ou bien

```python
# Change l'index en en utilisant un autre
ma_df.set_index(un_index)
```

Attention, il est possible d'obtenir ainsi plusieurs lignes avec la même valeur d'index

---

## Obtenir des indexes multiples

Par set_index

```python
ma_df.set_index(["colonne1", "colonne2"])
```

Par groupby

```python
ma_df.groupby(by=["colonne1", "colonne2"]).mon_aggregation()
```

---

## Exercices set_index

- Obtenir la DataFrame indexée par Subject et Space
- Obtenir la DataFrame indexée par Subject, Space et l'index original avec un niveau nommé essai (on peut utiliser .rename sur un index)

---

## Exercices groupby

- Faire une dataframe du temps de réponse moyen par Name_A, Name_B
- Faire une dataframe du temps de réponse max par sujet et par différence de distance (symbolique)
- Faire une dataframe du nombre, puis du taux de bonnes réponses par Mode, Side et Space
