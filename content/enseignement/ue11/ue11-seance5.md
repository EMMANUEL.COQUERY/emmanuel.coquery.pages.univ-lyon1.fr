# Notebook de la séance 5

Exemple de tableau


```python
[8, 5, 3, 12]
```




    [8, 5, 3, 12]



Accès à une case du tableau


```python
t = [8, 5, 3, 12]
t[1]
```




    5



Affectation sur une case du tableau


```python
t[1] = 42
t
```




    [8, 42, 3, 12]




```python
tt = [ [1, 2], [7, 5], [] ]
tt[1]
```




    [7, 5]




```python
(tt[1])[0]
```




    7



Boucle for sur un tableau avec un affichage à chaque tour


```python
for v in t:
    print("J'ai trouvé "+str(v))
```

    J'ai trouvé 8
    J'ai trouvé 42
    J'ai trouvé 3
    J'ai trouvé 12


Boucle avec position


```python
for pos in range(0,len(t)):
    print("J'ai trouvé "+str(t[pos])+" à la position " + str(pos))
```

    J'ai trouvé 8 à la position 0
    J'ai trouvé 42 à la position 1
    J'ai trouvé 3 à la position 2
    J'ai trouvé 12 à la position 3



```python
print(tt)
for tab in tt:
    print("J'ai trouvé un tableau de longueur "+str(len(tab)))
    for v in tab:
        print("- dedans il y a "+str(v))
    print("Fin boucle interne sur tab")
print("Fin boucle externe sur tt")
```

    [[1, 2], [7, 5], []]
    J'ai trouvé un tableau de longueur 2
    - dedans il y a 1
    - dedans il y a 2
    Fin boucle interne sur tab
    J'ai trouvé un tableau de longueur 2
    - dedans il y a 7
    - dedans il y a 5
    Fin boucle interne sur tab
    J'ai trouvé un tableau de longueur 0
    Fin boucle interne sur tab
    Fin boucle externe sur tt


Ajout d'une case


```python
t = ["ab", "cd"]
t.append("ef")
print(t)
```

    ['ab', 'cd', 'ef']


Le '+' entre chaînes de caractères ne les modifie pas


```python
s = "abcd"
print(s+"ef")
print(s)
```

    abcdef
    abcd


Append modifie les tableaux


```python
t = [1,2,3]
print(t)
t.append(4)
print(t)
```

    [1, 2, 3]
    [1, 2, 3, 4]


Attention append ne renvoie pas ce qu'on pense:


```python
print(t.append(5))
```

    None



```python
[1,2] + [8,9]
```




    [1, 2, 8, 9]



`+` ne modifie pas `t`, il fabrique un nouveau tableau avec les cases de `t` et les cases du tableau `[9,10]`


```python
t = [1,2]
print(t+[9,10])
print(t)
```

    [1, 2, 9, 10]
    [1, 2]


Différentes modifications de tableau


```python
t = []
for i in range(0,3):
    t.append(i)
print(t)
for i in range(0,3):
    t[i] = i*i
print(t)
for i in range(0,3):
    t[i] = t[i]+i
print(t)
```

    [0, 1, 2]
    [0, 1, 4]
    [0, 2, 6]


Une variante


```python
t = []
for i in range(0,3):
    t.append(i+10)
print(t)
for i in range(0,3):
    t[i] = t[i]+i
print(t)
```

    [10, 11, 12]
    [10, 12, 14]


La première série de modifications, mais avec des affichages pour comprendre


```python
t = []
print("t: "+str(t))
for i in range(0,3):
    t.append(i)
    print("Dans la 1ere boucle, i: "+str(i)+", t: "+str(t))
print("Après la première boucle, t: "+str(t))
for i in range(0,3):
    t[i] = i*i
    print("Dans la 2eme boucle, i: "+str(i)+", t: "+str(t))
print("Après la 2eme boucle, t: "+str(t))
for i in range(0,3):
    t[i] = t[i]+i
    print("Dans la 3eme boucle, i: "+str(i)+", t: "+str(t))
print("Après la 3eme boucle, t: "+str(t))
```

    t: []
    Dans la 1ere boucle, i: 0, t: [0]
    Dans la 1ere boucle, i: 1, t: [0, 1]
    Dans la 1ere boucle, i: 2, t: [0, 1, 2]
    Après la première boucle, t: [0, 1, 2]
    Dans la 2eme boucle, i: 0, t: [0, 1, 2]
    Dans la 2eme boucle, i: 1, t: [0, 1, 2]
    Dans la 2eme boucle, i: 2, t: [0, 1, 4]
    Après la 2eme boucle, t: [0, 1, 4]
    Dans la 3eme boucle, i: 0, t: [0, 1, 4]
    Dans la 3eme boucle, i: 1, t: [0, 2, 4]
    Dans la 3eme boucle, i: 2, t: [0, 2, 6]
    Après la 3eme boucle, t: [0, 2, 6]


Calcul de moyenne sur un tableau fixé


```python
t = [ 8,12,15,13,18]
somme = 0
for v in t:
    somme = somme + v
moyenne = somme / len(t)
moyenne
```




    13.2



Avec une trace


```python
t = [ 8,12,15,13,18]
somme = 0
print("Avant la boucle, somme: "+str(somme)+", t: "+str(t))
for v in t:
    somme = somme + v
    print("À la fin du tour de boucle, v: "+str(v)+", somme: "+str(somme)+", t: "+str(t))
print("À la fin de la boucle, somme: "+str(somme)+", t: "+str(t))
moyenne = somme / len(t)
moyenne
```

    Avant la boucle, somme: 0, t: [8, 12, 15, 13, 18]
    À la fin du tour de boucle, v: 8, somme: 8, t: [8, 12, 15, 13, 18]
    À la fin du tour de boucle, v: 12, somme: 20, t: [8, 12, 15, 13, 18]
    À la fin du tour de boucle, v: 15, somme: 35, t: [8, 12, 15, 13, 18]
    À la fin du tour de boucle, v: 13, somme: 48, t: [8, 12, 15, 13, 18]
    À la fin du tour de boucle, v: 18, somme: 66, t: [8, 12, 15, 13, 18]
    À la fin de la boucle, somme: 66, t: [8, 12, 15, 13, 18]





    13.2



Calcul maximum et du minimum


```python
t = [ 8,12,15,3,5]
maximum = t[0]
minimum = t[0]
for v in t[1:] :
    if v > maximum:
        maximum = v
    if v < minimum:
        minimum = v
print("Maximum: "+str(maximum)+", minimum: "+str(minimum))
```

    Maximum: 15, minimum: 3



```python
t = [ 8,12,15,3,5]
maximum = t[0]
minimum = t[0]
print("Avant la boucle, maximum: "+str(maximum)+", minimum: "+str(minimum))
for v in t[1:] :
    print("Début de boucle, v: "+str(v)+", maximum: "+str(maximum)+", minimum: "+str(minimum))
    if v > maximum:
        print("On change le maximum")
        maximum = v
    if v < minimum:
        print("On change le minimum")
        minimum = v
    print("Fin de boucle, v: "+str(v)+", maximum: "+str(maximum)+", minimum: "+str(minimum))
print("Maximum: "+str(maximum)+", minimum: "+str(minimum))
```

    Avant la boucle, maximum: 8, minimum: 8
    Début de boucle, v: 12, maximum: 8, minimum: 8
    On change le maximum
    Fin de boucle, v: 12, maximum: 12, minimum: 8
    Début de boucle, v: 15, maximum: 12, minimum: 8
    On change le maximum
    Fin de boucle, v: 15, maximum: 15, minimum: 8
    Début de boucle, v: 3, maximum: 15, minimum: 8
    On change le minimum
    Fin de boucle, v: 3, maximum: 15, minimum: 3
    Début de boucle, v: 5, maximum: 15, minimum: 3
    Fin de boucle, v: 5, maximum: 15, minimum: 3
    Maximum: 15, minimum: 3

