# Correction contrôle 4

Saisir 3 nombres et les placer dans des variables a, b et c


```python
a = int(input("Entrer la valeur de a"))
b = int(input("Entrer la valeur de b"))
c = int(input("Entrer la valeur de c"))
```

    Entrer la valeur de a 1
    Entrer la valeur de b 3
    Entrer la valeur de c 2


Donner la somme de a, b et c


```python
a+b+c
```




    6



Afficher la plus grande valeur entre a, b et c


```python
if a < b:
    if b < c:
        print(c)
    else:
        print(b)
else:
    if a < c:
        print(c)
    else:
        print(a)
```

    3


Saisir du texte au clavier et le placer dans une variable d


```python
d = input("Saisir du texte")
```

    Saisir du texte Du texte


Faire une boucle qui affiche la saie lettre par lettre


```python
for lettre in d:
    print(lettre)
```

    D
    u
     
    t
    e
    x
    t
    e

