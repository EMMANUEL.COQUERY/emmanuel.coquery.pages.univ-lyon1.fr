---
# Course title, summary, and position.
linktitle: "UE11: Introduction à la programmation d'expériences"
summary: "Cours d'introduction à la programmation d'expériences pour le master Santé de l'ISTR"
weight: 506

# Page metadata.
title: "UE11: Introduction à la programmation d'expériences"
draft: false # Is this a draft? true/false
toc: true # Show table of contents? true/false
type: docs # Do not modify.
authors: ["admin"]
tags: ["code"]
featured: true

# Add menu entry to sidebar.
# - name: Declare this menu item as a parent with ID `name`.
# - weight: Position of link in menu.
menu:
  ue11:
    name: Accueil
    weight: 1
---

Code APOGEE: STR1034M

[Plateforme Jupyter](https://jupyter.univ-lyon1.fr/)

[Tomuss](https://tomus.univ-lyon1.fr)

- Séance 1 & 2: [Diapositives]({{< ref "/slides/ue11-seance1-introduction" >}}), [Notebook séance 1]({{< ref "ue11-seance1" >}}), [Notebook séance 2]({{< ref "ue11-seance2" >}})
- Séance 3: [Diapositives]({{< ref "/slides/ue11-seance3-controle" >}}), [Notebook séance 3]({{< ref "ue11-seance3" >}})
- Séance 4: [Diapositives]({{< ref "/slides/ue11-seance4-controle-str" >}}), [Notebook séance 4]({{< ref "ue11-seance4" >}})
- Séance 5: [Diapositives]({{< ref "/slides/ue11-seance5-tableaux" >}}), [Notebook séance 5]({{< ref "ue11-seance5" >}})
- Séance 6: [Diapositives]({{< ref "/slides/ue11-seance6-fonctions" >}}), [corrigé contrôle 4]({{< ref "ue11-controle4" >}})
- Séance 7: [Notebook]({{< ref "ue11-seance7" >}})
- Séance 8: [Notebook]({{< ref "ue11-seance8" >}})
