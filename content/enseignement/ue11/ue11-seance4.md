# Notebook de la séance 4

Une boucle for simple


```python
for n in range(10,15):
    print(n)
```

    10
    11
    12
    13
    14


Avec un `if` imbriqué


```python
for n in range(10,15):
    if n % 2 == 0: # % calcule le reste de la division entière
        # ici on teste si n est pair
        print(n)
```

    10
    12
    14


Somme de nombres


```python
total = 0
for n in range(1,10):
  total = total + n
print("La somme des entiers de 1 à 9 est "+str(total))
```

    La somme des entiers de 1 à 9 est 45


Variante avec le total affiché au fur et à mesure


```python
total = 0
for n in range(1,10):
    total = total + n
    if n >= 2 :
        print("La somme des entiers de 1 à "+str(n)+" est "+str(total))
```

    La somme des entiers de 1 à 2 est 3
    La somme des entiers de 1 à 3 est 6
    La somme des entiers de 1 à 4 est 10
    La somme des entiers de 1 à 5 est 15
    La somme des entiers de 1 à 6 est 21
    La somme des entiers de 1 à 7 est 28
    La somme des entiers de 1 à 8 est 36
    La somme des entiers de 1 à 9 est 45



```python
maxi = int(input("Entrer le nombre dont on veut la factorielle"))
total = 1
for n in range(1, maxi+1):
    total = total * n
    print("La factorielle de "+str(n)+" est "+str(total))
```

    Entrer le nombre dont on veut la factorielle 5


    La factorielle de 1 est 1
    La factorielle de 2 est 2
    La factorielle de 3 est 6
    La factorielle de 4 est 24
    La factorielle de 5 est 120


Quel est premier le nombre n tel que 1+ ... + n est supérieur à 10 000 ?


```python
total = 0
n = 0
while total <= 10000 :
    n = n + 1
    total = total + n
print("Le nombre cherché est "+str(n)+" et la somme vaut "+str(total))
```

    Le nombre cherché est 141 et la somme vaut 10011



```python
141*142//2
```




    10011



## Chaînes de caractères


```python
len("Toto")
```




    4




```python
"abcdef"[2:4]
```




    'cd'




```python
"Gâteau au chocolat"[0:7]
```




    'Gâteau '




```python
"Gâteau au chocolat"[0:-8]
```




    'Gâteau au '




```python
"Gâteau au chocolat"[-8:len("Gâteau au chocolat")]
```




    'chocolat'




```python
s = "Gâteau au chocolat"
s[-8:len(s)]
```




    'chocolat'




```python
s = "Gâteau au chocolat"
s[-8:]
```




    'chocolat'




```python
s = "Gâteau au chocolat"
s[:6]
```




    'Gâteau'




```python
s = "Gâteau au chocolat"
s[5]
```




    'u'


