# Notebook de la séance 2

Quelques comparaisons


```python
3 == 3
```




    True




```python
3 == 2
```




    False




```python
2*3 + 10 > 8*4
```




    False




```python
6 > 8 or 18 <= 20
```




    True



Attention, les expressions ci-dessous sont piégeuses, à éviter:


```python
6 > 8 or 18
```




    18




```python
18 or 6 > 8
```




    18



Comparaisons sur les chaînes de caractères

Attention aux minuscules et aux majuscules.


```python
"abeille" < "arbre"
```




    True




```python
"abeille" < "Arbre"
```




    False




```python
"abeille" < "Zoo"
```




    False




```python
"abeille".upper() < "Zoo".upper()
```




    True




```python
"abeille".lower() < "Zoo".lower()
```




    True




```python
"échasse" > "feu"
```




    True




```python
"echasse" > "feu"
```




    False



Conversions



```python
int("3")
```




    3




```python
"3"+"4"
```




    '34'




```python
int("3") + int("4")
```




    7




```python
"3"
```




    '3'




```python
3 * "3"
```




    '333'




```python
int(3 * "3")
```




    333




```python
int(3 * "3") // 111
```




    3




```python
int(3 * "3") // 111 == 3
```




    True




```python
"3" * "3"
```


    ---------------------------------------------------------------------------

    TypeError                                 Traceback (most recent call last)

    Cell In[22], line 1
    ----> 1 "3" * "3"


    TypeError: can't multiply sequence by non-int of type 'str'



```python
"Mon résultat est "+(56*18)
```


    ---------------------------------------------------------------------------

    TypeError                                 Traceback (most recent call last)

    Cell In[23], line 1
    ----> 1 "Mon résultat est "+(56*18)


    TypeError: can only concatenate str (not "int") to str



```python
"Mon résultat est " + str(56*18)
```




    'Mon résultat est 1008'



Imports


```python
import math
```


```python
math.sin(3.14)
```




    0.0015926529164868282



## Variables


```python
sous_total = 5 + 3 * 12
```


```python
sous_total
```




    41




```python
pinocchio = "long nez"
```


```python
"Il a un " + pinocchio
```




    'Il a un long nez'




```python
sous_total + 7
```




    48



## Print


```python
3
4
5
```




    5




```python
le_carre = 3**2
print(le_carre)
message = "Le carré de 3 est " + str(le_carre)
print(message)
```

    9
    Le carré de 3 est 9


# Un bout de code pour extraire 2 racines d'un trinôme


```python
a = 1
b = 12
c = 35
delta = b ** 2 - 4 * a * c
print(delta)
# il faudrait tester delta ...
x1 = (-b + math.sqrt(delta)) / (2 * a)
x2 = (-b - math.sqrt(delta)) / (2 * a)
poly = str(a)+" x^2 + "+str(b)+" x + "+str(c)
print("Les solutions de "+poly+" sont "+str(x1)+" et "+str(x2))
```

    4
    Les solutions de 1 x^2 + 12 x + 35 sont -5.0 et -7.0

