Utilisation d'un module (`os`) pour interagir avec le système.


```python
import os
import random
random.seed()
```

Lister les fichier et sous-dossiers d'un dossier


```python
os.listdir("images")
```




    ['NF-1001.jpg',
     'NF-1002.jpg',
     'NF-1003.jpg',
     'SF-1001.jpg',
     'SF-1002.jpg',
     'SF-1003.jpg',
     'NM-1007.jpg',
     'NM-1010.jpg',
     'SM-1007.jpg',
     'SM-1010.jpg',
     '.ipynb_checkpoints']



Mettre les fichiers d'images dans un tableau `images`


```python
images = []
for fichier in os.listdir("images"):
    if fichier[-4:] == ".jpg" :   # c'est une image
        images.append(fichier)
images
```




    ['NF-1001.jpg',
     'NF-1002.jpg',
     'NF-1003.jpg',
     'SF-1001.jpg',
     'SF-1002.jpg',
     'SF-1003.jpg',
     'NM-1007.jpg',
     'NM-1010.jpg',
     'SM-1007.jpg',
     'SM-1010.jpg']



Mélanger le tableau images


```python
random.shuffle(images)
images1 = images[:len(images)//2]
images2 = images[len(images)//2:]
print(images1)
print(images2)
```

    ['SM-1007.jpg', 'NF-1003.jpg', 'SF-1003.jpg', 'SM-1010.jpg', 'SF-1001.jpg']
    ['NF-1002.jpg', 'NM-1007.jpg', 'NM-1010.jpg', 'NF-1001.jpg', 'SF-1002.jpg']


Tester si un élément est dans un tableau


```python
print(images)
print("'SM-1007.jpg' dans images: "+str('SM-1007.jpg' in images))
print("'KM-1007.jpg' dans images: "+str('KM-1007.jpg' in images))
```

    ['SM-1007.jpg', 'NF-1003.jpg', 'SF-1003.jpg', 'SM-1010.jpg', 'SF-1001.jpg', 'NF-1002.jpg', 'NM-1007.jpg', 'NM-1010.jpg', 'NF-1001.jpg', 'SF-1002.jpg']
    'SM-1007.jpg' dans images: True
    'KM-1007.jpg' dans images: False


Génération de deux blocs d'images respectant la contrainte "pas deux fois la même personne dans un bloc"


```python
images1 = []
images2 = []
random.shuffle(images)

for img in images:
    if img[0] == 'S':
        img_a_trouver = 'N'+img[1:]
    else:
        img_a_trouver = 'S'+img[1:]
    if img_a_trouver in images1:
        images2.append(img)
    else:
        images1.append(img)
        
print(images1)
print(images2)
```

    ['NM-1007.jpg', 'NM-1010.jpg', 'SF-1001.jpg', 'SF-1003.jpg', 'SF-1002.jpg']
    ['SM-1007.jpg', 'NF-1002.jpg', 'SM-1010.jpg', 'NF-1001.jpg', 'NF-1003.jpg']


On réarrange l'ordre  dans chaque bloc pour respecter la contrainte "pas k images de suite de la même catégorie"


```python
images1 = ['NM-1007.jpg', 'NM-1010.jpg', 'SF-1001.jpg', 'SF-1003.jpg', 'SF-1002.jpg']
images1b = []
k = 3 # max 2 images de suites dans la même catégorie
m = 0 # nombre d'images de la catégories de l'image regardée
cat = '' # catégorie des dernières images

mises_de_cote = []

for img in images1:
    if cat == img[:2]: # est-on dans la même catégorie qu'avant ?
        if m >= k-1: # si on est déjà au max
            # on met l'image de côté
            mises_de_cote.append(img)
        else:
            images1b.append(img)
            m = m + 1
    else: # on change de catégorie
        images1b.append(img)
        cat = img[:2] # on change la catégorie
        m = 1
print(images1b)
print(mises_de_cote)
```

    ['NM-1007.jpg', 'NM-1010.jpg', 'SF-1001.jpg', 'SF-1003.jpg']
    ['SF-1002.jpg']


On place ce code dans une fonction


```python
def force_contrainte_categorie(images, k):
    """
    Cette fonction prend en argument un tableau d'images 
    et renvoie deux tableaux:
    - un premier tableau comportant une suite d'images trouves 
      dans `images`, en conservant l'ordre et de façon à ce 
      que la contrainte de catégorie soit respectée
    - un deuxième tableau contenant les images restantes
    
    k est le nombre d'images maximum qu'on peut trouver 
    de suite dans une même catégorie
    """
    images_resultat = []
    m = 0 # nombre d'images de la catégories de l'image regardée
    cat = '' # catégorie des dernières images

    mises_de_cote = []

    for img in images:
        if cat == img[:2]: # est-on dans la même catégorie qu'avant ?
            if m >= k: # si on est déjà au max
                # on met l'image de côté
                mises_de_cote.append(img)
            else:
                images_resultat.append(img)
                m = m + 1
        else: # on change de catégorie
            images_resultat.append(img)
            cat = img[:2] # on change la catégorie
            m = 1
    # renvoyer le résultat, i.e. les deux tableaux
    return (images_resultat, mises_de_cote)
```


```python
force_contrainte_categorie(
    ['NM-1007.jpg', 'NM-1010.jpg', 'SF-1001.jpg', 
     'SF-1003.jpg', 'SF-1002.jpg'],
    2)
```




    (['NM-1007.jpg', 'NM-1010.jpg', 'SF-1001.jpg', 'SF-1003.jpg'], ['SF-1002.jpg'])




```python
force_contrainte_categorie(
    ['NM-1007.jpg', 'NM-1010.jpg', 'SF-1001.jpg', 
     'SF-1003.jpg', 'SF-1002.jpg'],
    1)
```




    (['NM-1007.jpg', 'SF-1001.jpg'], ['NM-1010.jpg', 'SF-1003.jpg', 'SF-1002.jpg'])




```python
force_contrainte_categorie(
    ['SF-1003.jpg', 'NM-1007.jpg', 
     'NM-1010.jpg', 'SF-1001.jpg', 
     'SF-1002.jpg'],
    2)
```




    (['SF-1003.jpg', 'NM-1007.jpg', 'NM-1010.jpg', 'SF-1001.jpg', 'SF-1002.jpg'],
     [])


