# Notebook séance 3


```python
import math
```

## Conditionnelles


```python
x = -12
y = x if x >= 0 else -x
y
```




    12




```python
x = 12
y = x if x >= 0 else -x
y
```




    12




```python
x = 5
z = 0
u = 0
if x >= 0 :
    y = x * 2
    z = y * 3
else:
    y = x * 3
    u = y + 2
print("z vaut "+str(z))
print("u vaut "+str(u))
```

    z vaut 30
    u vaut 0



```python
x = -5
z = 0
u = 0
if x >= 0 :
    y = x * 2
    z = y * 3
else:
    y = x * 3
    u = y + 2
print(z)
print(u)
```

    0
    -13



```python
age = int(input("Quel âge ? "))
if age <= 20 :
  print("Je suis jeune")
else:
  print("Je suis jeune aussi, car c'est dans la tête")
```

    Quel âge ?  18


    Je suis jeune



```python
age = int(input("Quel âge ? "))
if age > 20 :
  if age <= 80 :
    print("Je suis jeune aussi, car c'est dans la tête")
  else:
    print("Je ne suis plus aussi jeune qu'avant")
else:
  print("Je suis jeune")
```

    Quel âge ?  86


    Je ne suis plus aussi jeune qu'avant


On reprend le calcul des racines du trinôme


```python
a = float(input("Valeur de a:"))
b = float(input("Valeur de b:"))
c = float(input("Valeur de c:"))
poly = str(a)+" x^2 + "+str(b)+" x + "+str(c)
delta = b ** 2 - 4 * a * c
print(delta)
if delta > 0:
    x1 = (-b + math.sqrt(delta)) / (2 * a)
    x2 = (-b - math.sqrt(delta)) / (2 * a)
    print("Les solutions de "+poly+" sont "+str(x1)+" et "+str(x2))
else:
    if delta == 0:
        x = -b / (2 * a)
        print("Le trinôme "+poly+" a une solution: "+str(x))
    else:
        print("Le trinôme "+poly+" n'a pas de solution")

```

    Valeur de a: 12
    Valeur de b: 1
    Valeur de c: 50


    -2399.0
    Le trinôme 12.0 x^2 + 1.0 x + 50.0 n'a pas de solution

