# Notebook de la séance 1


```python
2+3
```




    5




```python
abs(-3.14)
```




    3.14



Nombre qui représente B


```python
ord("B")
```




    66



Nombre qui représente o


```python
ord("o")
```




    111



## Calculs simples

Sur des _int_:


```python
2+3+4
```




    9




```python
2*3+4
```




    10




```python
4+2*3
```




    10




```python
(8+2) // 4
```




    2




```python
2 ** 3
```




    8




```python
3 ** 2
```




    9



Sur des _floats_


```python
3.5 + 2.8
```




    6.3




```python
2.0 * 3.5
```




    7.0




```python
4.0 ** 0.5
```




    2.0




```python
1.0 / 3.0
```




    0.3333333333333333



Sur des chaînes de caractères _str_:


```python
"abcd"+"efgh"
```




    'abcdefgh'




```python
"aBcDEF".lower()
```




    'abcdef'




```python
"coquery".upper()
```




    'COQUERY'



Sur des _bool_:


```python
True and False
```




    False




```python
True or False
```




    True




```python
not False
```




    True




```python
not (True and False)
```




    True




```python
(not True) and False
```




    False


