---
title: TP Nettoyage de données
linktitle: TP Nettoyage de données
toc: true
type: docs
draft: false
menu:
  iqd:
    weight: 3

# Prev/next pager order (if `docs_section_pager` enabled in `params.toml`)
weight: 3
---

Ce TP a pour but de se confronter à des problèmes que l'on peut recontrer lors de l'intégration d'un jeu de données au sein d'une base: compréhension des données et correction.

Remarque: ce TP peut s'effectuer sur la base de donnée PostgreSQL du département [doc](https://forge.univ-lyon1.fr/bd-pedago/bd-pedago/blob/master/README.md).

## Évaluation

Ce TP sera évalué lors d'un contrôle la semaine du 30/10/2023 (à priori en début de la séance du 02/11/2023).

## Données

On considère les données concernant les subventions de l'état aux associations, [disponibles sur www.data.gouv.fr](https://www.data.gouv.fr/fr/search/?q=plf+jaune).
Dans le cadre de ce TP, on se concentrera dans un premier temps sur les données des subventions 2016 rendues disponibles dans le cas projet de loi de finances (PLF) 2018.

Fichier csv à télécharger: [copie locale](projet-de-loi-de-finances-pour-2018-plf-2018-donnees-de-lannexe-jaune-effort-fin.csv) ou [www.data.gouv.fr](https://www.data.gouv.fr/fr/datasets/r/f9f47d17-6c70-4828-8e75-2a44d88a9133).

Le code suivant permet de créer une table postgresql afin d'y importer les données csv brutes:

```sql
CREATE TABLE s2016 (
	"Programme 2016" DECIMAL NOT NULL,
	"SIREN" VARCHAR NOT NULL,
	"NIC" VARCHAR,
	"Dénomination" VARCHAR NOT NULL,
	"Montant" DECIMAL NOT NULL,
	"Objet" VARCHAR NOT NULL,
	"Parlementaire" VARCHAR,
	"Réserve 2016" VARCHAR,
	"Convention 2016" VARCHAR,
	"COG : code département" VARCHAR,
	"COG : code commune" DECIMAL,
	"COG : ville ou pays" VARCHAR NOT NULL,
	"Nomenclature juridique" DECIMAL NOT NULL,
	"Code NAF" VARCHAR NOT NULL,
	"Situation SIRENE" VARCHAR,
	"RNA" VARCHAR
);
```

Le code suivant permet d'insérer les données du csv via le shell `psql`:

```sql
\copy s2016 from projet-de-loi-de-finances-pour-2018-plf-2018-donnees-de-lannexe-jaune-effort-fin.csv csv delimiter ';' encoding 'utf-8' header;
```

On pourra renommer les attributs de la table afin de simplifier l'écriture des requêtes dans la suite du TP.

## Nettoyage des données numériques

Certaines données devraient être des données numériques mais les attributs correspondant ont une valeur textuelle.

> Comprendre pourquoi c'est le cas et corriger les données afin de pouvoir changer le type dans un type numérique

Aide: [documentation sur les expressions rationnelles](https://www.postgresql.org/docs/14/functions-matching.html)

> Regarder les différentes valeurs de l'attribut "Réserve 2016" et faire les modifications nécessaires pour en faire un attribut booléen.

## Schéma des données

Essayer de comprendre un peu les données, on pourra s'aider de la version excel disponible [ici](https://www.budget.gouv.fr/sites/performance_publique/files/farandole/ressources/2018/pap/pdf/jaunes/Jaune2018_liste_associations_subventionnees_2016.xlsx) ([version locale](Jaune2018_liste_associations_subventionnees_2016.xlsx)) comprenant des onglets sur la signification de différents codes utilisés.

> Vérifier qu'il existe des lignes qui ne diffèrent que par le montant de la subvention.

> Expliquer pourquoi, du point de vue métier, le montant de la subvention n'est cependant pas un attribut qui devrait faire partie de la clé.

Essayer de deviner quelles dépendances fonctionnelles devraient être satisfaites dans la table.

> Vérifier la satisfaction de ces dépendances.

> Pour les dépendances non satisfaites, déterminer si les cas de violation sont dus à des erreurs.
> Corriger ces erreurs au besoin.

Sans prendre en compte l'attribut "Parlementaire", proposer un schéma relationnel pour représenter les données, créer les tables et y insérer les données.

Il sera peut-être nécessaire d'ajouter des identifiants artificiels, en particulier pour distinguer des subventions faites à une même association pour un même objet.
Il est possible d'utiliser [des séquences](https://www.postgresql.org/docs/14/sql-createsequence.html) pour générer ces identifiants.

## Traitement des parlementaires

L'attribut parlementaire n'est pas en première forme normale (plusieurs valeurs sont dans un même attribut).

> Comprendre en regardant les données le type d'information contenu dans cette colonne, modifier votre schéma relationnel et corriger les données pour y remédier.

Aide: la fonction [`regexp_split_to_table`](https://www.postgresql.org/docs/14/functions-string.html) peut être utile ici.

## Intégration des données d'autres années

Les données de subventions aux associations sont disponibles à partir de l'année 2010 (PLF 2012), les CSV sont les suivants:

[2010](https://www.data.gouv.fr/fr/datasets/r/6d74e8a2-c4f6-443d-b3d1-813b7fa66019),
[2011](https://www.data.gouv.fr/fr/datasets/r/84f0ab90-2324-4c7e-ac25-4a839d53a97e),
[2012](https://www.data.gouv.fr/fr/datasets/r/26ccec6f-15c3-441a-b3ec-91afe3379601),
[2013](https://www.data.gouv.fr/fr/datasets/r/c86f9203-1324-4ef6-87ad-758912a0b7af),
[2014](https://www.data.gouv.fr/fr/datasets/r/4132bcaa-5c36-4ed9-bc2e-a4cd30876041),
[2015](https://www.data.gouv.fr/fr/datasets/r/410edfe5-1fac-4d0b-b812-4a65768d031f),
[2016](https://www.data.gouv.fr/fr/datasets/r/f9f47d17-6c70-4828-8e75-2a44d88a9133).

En regardant ces fichiers, on peut constater que leur schéma évolue d'année en année.
Si l'on souhaite analyser ces subventions sur plusieurs années, il faut intégrer ces informations dans le schéma précédent.

> Reprendre le travail précédent pour l'adapter aux années 2010 à 2015 et regrouper les données en un seul schéma.
