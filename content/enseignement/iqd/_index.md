---
# Course title, summary, and position.
linktitle: "Supports pour l'UE Intégration et Qualité des données"
summary: "Intégration et Qualité des données"
weight: 502

# Page metadata.
title: "Intégration et Qualité des données"
draft: false # Is this a draft? true/false
toc: true # Show table of contents? true/false
type: docs # Do not modify.
authors: ["admin"]
tags: ["data"]
featured: true

# Add menu entry to sidebar.
# - name: Declare this menu item as a parent with ID `name`.
# - weight: Position of link in menu.
menu:
  tiw2:
    name: Généralités
    weight: 1
---
