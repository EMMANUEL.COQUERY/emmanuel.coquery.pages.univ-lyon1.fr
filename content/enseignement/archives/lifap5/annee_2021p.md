---
# Course title, summary, and position.
linktitle: "Archive: LIFAP5 2021P"
summary: "Programmation fonctionnelle. Principes fondamentaux. Application à la programmation Web avec le style fonctionnel en Javascript"
weight: 221

# Page metadata.
title: "Archive: LIFAP5 2021P"
draft: false # Is this a draft? true/false
# toc: true # Show table of contents? true/false
type: docs # Do not modify.

tags: [code]
# Add menu entry to sidebar.
# - name: Declare this menu item as a parent with ID `name`.
# - weight: Position of link in menu.
menu:
  lifap5:
    name: Archive 2021P
    weight: 10
---

L'unité d'enseignement [LIAFP5 : programmation fonctionnelle pour le WEB](http://offre-de-formations.univ-lyon1.fr/ue-16369-12/programmation-fonctionnelle-pour-le-web.html) prolonge [LIFAP2 : algorithmique et Programmation Récursive](http://offre-de-formations.univ-lyon1.fr/ue-16359-12/algorithmique-et-programmation-recursive.html) avec les concepts de la programmation fonctionnelle dans l’objectif de fournir aux étudiants les méthodes et pratiques de la programmation fonctionnelle contemporaine. Cette UE fournit les bases nécessaires pour aborder la programmation web et les nouveaux paradigmes de programmation vus en L3 puis en Master. La méthode pédagogique s’appuiera fortement sur la pratique de la programmation dans le langage JavaScript (js) en utilisant le navigateur web Firefox.

## Semestre Printemps 2021

**Session 2 le 01/07/2021 à 14h en amphi Gouy**,

- convocation: cf https://sciences-licence.univ-lyon1.fr/examens/convocation/mes-examens
- le [sujet d'ECA/CCF de session 1](LIFAP5-2021P-CCF.pdf) et son [corrigé](LIFAP5-2021P-CCF-corrige.pdf) sont disponibles en bas de page dans la section [Annales](#annales).
- seul document autorisé: une feuille manuscrite recto-verso (comme pour l'ECA de session 1)
- si vous passez cette épreuve, la note de l'épreuve de session 2 remplacera celle de l'ECA de session 1 (que la note soit meilleure ou plus mauvaise)
- si vous avez déjà validé le semestre par compensation et que vous passez cette épreuve, il faudra renoncer à cette compensation

**Attention les 1 mars, 8 mars et 15 mars, changement dans l'emploi du temps, pas forcément encore répercuté dans ADE:**

- lundi 1 mars, 08h00-11h15 TP
- lundi 8 mars, 08h00-9h30 CM, 9h45-11h15: TD
- lundi 15 mars, 09h45-10h45 contrôle (+20 min pour les étudiants bénéficiant d'un 1/3 temps)

Liens généraux

- Code APOGEE [INF2026L](http://offre-de-formations.univ-lyon1.fr/ue-16369-12/programmation-fonctionnelle-pour-le-web.html)
- [Dépôt des ressources pédagogiques](https://sourcesup.renater.fr/projects/lifap5/) (réservé aux enseignants)

### COVID

- Lien visio CM distanciel: https://classe-info.univ-lyon1.fr/coq-clw-fvd-oit
- [#lifap5 sur le Rocket.Chat institutionnel](https://chat-info.univ-lyon1.fr/group/lifap5) ([Lien d'invitation](https://go.rocket.chat/invite?host=chat-info.univ-lyon1.fr&path=invite%2FMKZBG5))

Salles de TD en visio:

- Groupe A (Emmanuel Coquery): https://classe-info.univ-lyon1.fr/coq-clw-fvd-oit
- Groupe B (Sylvain Brandel): https://classe-info.univ-lyon1.fr/bra-a66-bj1-c3l
- Groupe C (Xavier Urbain): https://classe-info.univ-lyon1.fr/urb-mg2-4dp-3v8
- Groupe D (Julien Emmanuel): https://classe-info.univ-lyon1.fr/emm-ecd-7rd-d7w

### Supports

**2021-01-18 - CM1**: [Diapositives](LIFAP5-2021P-CM1.pdf), [enregistrement](https://scalelite-info.univ-lyon1.fr/playback/presentation/2.0/playback.html?meetingId=01581cbfbd5c4c94b3173a3133cc48d62c8e9c96-1610953102981)

**2021-01-25 - TP1**: [Sujet du TP1](TP1/LIFAP5-2021P-TP1.html), [Corrigé](TP1/LIFAP5-TP1-correction.zip).
Le lancement du TP aura lieu en visioconférence [sur le BBB du CM](https://classe-info.univ-lyon1.fr/coq-clw-fvd-oit),
les questions pourront être posées pendant le TP sur [#lifap5](https://chat-info.univ-lyon1.fr/group/lifap5).

**2021-02-01 - CM2 et TD1**:

- CM: [Diapositives](LIFAP5-2021P-CM2.pdf), [enregistrement](https://scalelite-info.univ-lyon1.fr/playback/presentation/2.0/playback.html?meetingId=01581cbfbd5c4c94b3173a3133cc48d62c8e9c96-1612162766033).
- TD: [Sujet TD1](LIFAP5-2021P-TD1.pdf), [Corrigé](LIFAP5-2021P-TD1-correction.pdf). Pour les salles visio, voir la section [COVID](#COVID).

**2021-02-22 - CM3 et TD2**:

- CM: [Diapositives](LIFAP5-2021P-CM3.pdf), [enregistrement](https://scalelite-info.univ-lyon1.fr/playback/presentation/2.0/playback.html?meetingId=01581cbfbd5c4c94b3173a3133cc48d62c8e9c96-1613976957281).
- TD: [Sujet TD2](LIFAP5-2021P-TD2.pdf), [Corrigé](LIFAP5-2021P-TD2-correction.pdf).

**2021-03-01 - TP2**: [Sujet du TP2](TP2/LIFAP5-2021P-TP2.html), [corrigé](TP2/LIFAP5-2021P-TP2-correction.zip).
Le lancement du TP aura lieu en visioconférence [sur le BBB du CM](https://classe-info.univ-lyon1.fr/coq-clw-fvd-oit) ([enregistrement CM IIFE + lancement du TP](https://scalelite-info.univ-lyon1.fr/playback/presentation/2.0/playback.html?meetingId=01581cbfbd5c4c94b3173a3133cc48d62c8e9c96-1614582105975)),
les questions pourront être posées pendant le TP sur [#lifap5](https://chat-info.univ-lyon1.fr/group/lifap5).

**2021-03-08 - CM4 et TD3/4**:

- CM: [Diapositives](LIFAP5-2021P-CM4.pdf), [enregistrement](https://scalelite-info.univ-lyon1.fr/playback/presentation/2.0/playback.html?meetingId=01581cbfbd5c4c94b3173a3133cc48d62c8e9c96-1615186738366).
- TD: [Sujet TD3 (et TD4)](LIFAP5-2021P-TD3.pdf)

**2021-03-15 - Contrôle**: voir [Instructions](cci) {< relref "cci.md" >}

**2021-03-22 - TP3 + fin du CM4**: [Sujet du TP3](TP3/LIFAP5-2021P-TP3.html), [Correction](TP3/LIFAP5-TP3-correction.zip). Le lancement du TP aura lieu en visioconférence [sur le BBB du CM](https://classe-info.univ-lyon1.fr/coq-clw-fvd-oit), [enregistrement](https://scalelite-info.univ-lyon1.fr/playback/presentation/2.0/playback.html?meetingId=01581cbfbd5c4c94b3173a3133cc48d62c8e9c96-1616396285669).

**2021-03-29 - Lancement projet + TD**:

- Lancement du projet [sur le BBB du CM](https://classe-info.univ-lyon1.fr/coq-clw-fvd-oit), [enregistrement](https://scalelite-info.univ-lyon1.fr/playback/presentation/2.0/playback.html?meetingId=01581cbfbd5c4c94b3173a3133cc48d62c8e9c96-1616997567352).
- TD: [Sujet (TD3 et) TD4](LIFAP5-2021P-TD3.pdf), [corrigé](LIFAP5-2021P-TD3-correction.pdf).

**2021-05-10 - CM Révision**: sur le [BBB habituel](https://classe-info.univ-lyon1.fr/coq-clw-fvd-oit), [enregistrement](https://scalelite-info.univ-lyon1.fr/playback/presentation/2.0/playback.html?meetingId=01581cbfbd5c4c94b3173a3133cc48d62c8e9c96-1620626377199).

**2021-05-18 - CCF**: Le CCF aura lieu **mardi** 18 mai à 14h
en présentiel en amphi 2 et 5, vous devez avoir reçu une convocation de la scolarité de licence.

#### Annales

[CCF 2017](LIFAP5-2017P-CCF.pdf), [CCF 2018](LIFAP5-2018P-CCF.pdf), [CCF 2019](LIFAP5-2019P-CCF.pdf),
[CCF 2021P](LIFAP5-2021P-CCF.pdf) ([corrige](LIFAP5-2021P-CCF-corrige.pdf))
