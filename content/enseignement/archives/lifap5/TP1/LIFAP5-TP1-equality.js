undefined == null;
null === undefined;

var example;
example == undefined;
example === undefined;
example == null;
example === null;

example = null;
example == undefined;
example === undefined
example == null;
example === null;

var sExample, iExample, bExample;
sExample = '1';
iExample = 1;
bExample = true;
sExample == iExample;
sExample === iExample;
iExample == bExample;
iExample === bExample;
sExample == bExample;
sExample === bExample;

var oExample0 = {a : 0};
var oExample1 = {a : 0};
oExample0 == oExample0;
oExample0 === oExample0;
oExample0 == oExample1;

var tExample0 = [0];
var tExample1 = [0];
tExample0 == tExample0;
tExample0 === tExample0;
tExample0 == tExample1;
tExample0[0] == tExample1[0];
tExample0[0] === tExample1[0];

"" == false;       
"" === false;       
"" == 0;           
true == "true";     
true == "1";        
"10" + 5 == 15;
"10" + 5 == 105;
"10" + 5 === 105;
