---
# Course title, summary, and position.
linktitle: "Archive: Projet 2021P"
summary: "Archive: Projet LIFAP5 printemps 2021"
weight: 221

# Page metadata.
title: "Archive: Projet LIFAP5 printemps 2021: duels de citations"
draft: false # Is this a draft? true/false
# toc: true # Show table of contents? true/false
type: docs # Do not modify.
math: true

tags: []
# Add menu entry to sidebar.
# - name: Declare this menu item as a parent with ID `name`.
# - weight: Position of link in menu.
menu:
  lifap5:
    name: "Archive: Projet 2021"
    weight: 2
---

## Introduction

### Présentation du projet

L'objectif de ce projet est de mettre en pratique ce qui a été vu dans l'UE LIFAP5 à travers la réalisation de la partie client, entièrement en JavaScript dans le navigateur et sans serveur, d'une application de classement des meilleures citations issues de séries et de films.

L'application va permettre de réaliser des _duels_ de citations et d'ajouter de nouvelles citations aux citations déjà présentes.
L'intégralité du [serveur REST][serveur] (backend) ainsi qu'une [version de départ du client][archive-client] vous sont fournis.

_Votre tâche consiste à ajouter des fonctionnalités au client pour pouvoir effectuer des duels, consulter le classement des citations selon différentes méthodologies de classement et enfin ajouter ou modifier des citations._

### Informations importantes

- Le projet est à réaliser en monôme ou en binôme constitué au sein du même groupe de TD. Donner dans https://tomuss.univ-lyon1.fr:

  - l'URL de votre dépôt sur https://forge.univ-lyon1.fr/
  - le Nom de votre binôme

Le serveur https://lifap5.univ-lyon1.fr est en ligne, sa page d'accueil contient toutes les informations utiles.

## Duels de citations: partie serveur

Cette partie est entièrement réalisée par l'équipe pédagogique. Le serveur https://lifap5.univ-lyon1.fr est en production, accessible publiquement sur internet. Le code du serveur contenant ainsi que l'intégralité des ressources associées sont publiquement accessible sur [la forge de Lyon1][forge-backend].

Si vous constatez des bugs ou des fonctionnalités manquantes, utilisez le [gestionnaire de ticket][forge-backend-issues]. Les issues et a fortiori les _merge requests_ pertinentes seront favorablement valorisées dans l'évaluation.

### Fonctionnalités de l'API REST

Le serveur gère deux types de données:

- Utilisateur: chaque utilisateur possède une clé d'API qui lui permet d'interagir avec le serveur. Il est possible de vérifier sa clé d'API, par contre il n'est pas possible de changer un utilisateur.

- Citation: en plus d'un identifiant et du texte de la citation, on dispose des informations suivantes: le personnage, la série ou le film d'origine de la citation, un lien vers une image du personnage, la direction du personnage dans l'image, le login de l'utilisateur ayant ajouté l'image et enfin un dictionnaire indiquant le nombre de duels gagnés et perdus face à chaque autre citation. Il est possible de récupérer les informations d'une ou de toutes les citations, d'ajouter une nouvelle citation ou de modifier une des citations que l'on a précédement ajouté. Enfin il est possible e déclarer le résultat d'un duel de citations.

L'ensemble de ces ressources est décrit sur l'[API du serveur backend][api-backend].
Toutes les opérations envoyant des données au serveur (i.e. lorsque le verbe HTTP n'est pas GET) nécessitent une authentification en ajoutant un header `x-api-key` contenant la clé d'API qui vous sera fournie via [tomuss][tomuss].

## Duel de citations: partie client

### Code de départ

Un client de départ est fourni dans l'archive [client.zip][archive-client].
Cette archive comprend trois fichiers:

- `index.html` contient le squelette de la page, rempli avec des données fictives.
  Cela permet d'avoir une idée de ce à quoi pourra ressembler l'application une fois que l'on utilisera les données du serveur.
- `bulma.min.css` Il s'agit de la bibliothèque CSS [Bulma][bulma], qui fourni un ensemble de styles CSS
  fournissant un ensemble de composants simples. Cette bibliothèque n'inclus pas de Javascript, il faudra donc
  coder les interactions utilisateur de votre côté. Ce fichier est chargé par `index.html`.
- `client.js` contient du code Javascript de départ pour le projet.
  Ce code comprend l'interaction liée au changement d'onglet, l'affichage d'une boîte de dialogue "Utilisateur" et l'affichage de l'utilisateur correspondant à la clé d'API de la constante `apiKey`.
  Chacune de ces fonctionnalités correspond à autant d'exemples dont vous pourrez vous inspirer.

### Fonctionnalités à implémenter

Dans ce projet, il est demandé de compléter le client en ajoutant les fonctionnalités suivantes:

- **Affichage de l'ensemble des citations du serveur (2 points)**: il s'agit ici de remplacer les données de la table de l'onglet "Toutes les citations" par les données issues du [serveur lifap5][serveur], disponibles sur la route `/citations` ([cf doc serveur][api-backend]).
- **Affichage d'un duel aléatoire (4 points)**: tirer deux citations au hasard parmi l'ensemble des citations disponibles sur le serveur et les utiliser pour remplacer les citations de l'onglet "Voter". Lorsque l'information est disponible, orienter l'image du personnage de façon à ce qu'il regarde vers la citation adverse (comme dans le fichier `index.html` fourni).
- **Connexion (4 points)**: modifier la boîte de dialogue de l'utilisateur de façon à pouvoir saisir une clé d'API dans un champ de type `password`, puis permettre de vérifier que cette clé fonction en utilisant la route `/whoami` (comme dans le code fourni, la différence est que le code fourni utilise la clé déclarée en constante). Changer l'affichage du bouton de façon à afficher `Connexion` si la clé d'API n'est pas valable (y compris si elle est vide). CHanger également cet affichage pour afficher le login de l'utilisateur et un bouton "Déconnexion" à la place du bouton connexion si la clé d'API est valide.
- **Vote (4 points)**: implémenter la fonctionnalité de vote utilisant la route `/citations/duels` et les deux boutons de vote de la page "Voter".
- **Détails d'une citation (4 points)**: dans le tableau "Toutes les citations", permettre l'affichage des détails d'une citation lors d'un clic sur la citation ou sur un bouton que vous aurez ajouté. Cet affichage doit comprendre tous les champs du schéma "Citation" de [l'API du serveur][api-backend], à l'exception de `_id` et de `scores`. Vous êtes libres de la manière d'afficher ces informations: boîte modale, élément flottant apparaissant sur le côté, basculement vers un troisième "tab", etc.
- **Tableau des scores (2 points)**: lors de l'affichage des détails d'une citations, afficher les scores sous forme d'un tableau mentionnant le nombre de victoires (`wins`), de défaites (`looses`) et le début de la citation "adverse" concernée.
- **Filtre de citation (2 points)**: permettre pour chaque colonne textuelle du tableau "Toutes les citations" d'ajouter un filtre (sous forme d'un champ input de type texte) qui ne gardera que les lignes pour lesquelles le champ de la colonne contient la chaîne de caractères du champ input.
- **Tri du tableau des citations (4 points)**: Permettre de trier le tableau des citations par clic sur le titre d'une colonne. Si la colonne cliquée est déjà utilisée pour déterminer l'ordre des lignes du tableau, le clic inversera cet ordre. On ajoutera un symbole ou une icône dans le titre de colonne pour indiquer la colonne utilisée pour le tri ainsi que le sens du tri.
- **Classement de citations par nombre de victoires absolu (2 points)**: Ajuster la valeur de la colonne "Classement" en fonction du nombre total de victoires obtenues par chaque citation.
- **Classement des citations par ratio (2 points)**: Calculer le score par ratio de chaque citation comme étant la somme pour chaque entrée du ratio suivant: $\frac{wins - looses}{wins + looses}$. Afficher le score dans le tableau des citations.
- **Choix du mode de classement (2 points)**: Permettre de choisir le mode de calcul du classement parmi ceux cités plus haut à l'aide d'un menu déroulant. Pour le classement par ratio, ajouter l'option de normaliser le score en le divisant par le nombre d'adversaires. Permettre de trier le tableau en fonction du classement.
- **Ajout de citation (4 points)**: Permettre d'ajouter une citation via la route POST `/citations` (cf [doc backend][api-backend]). On créera un formulaire pour saisir les données correspondant aux champs indiqués dans le schéma "CitationSubmit". Les champs "quote", "origin" et "character" sont obligatoire. On ne soumettra la requête au serveur que si tous les champs obligatoires sont remplis. Dans le cas contraire on affichera un message d'erreur à l'utilisateur.
- **Modification de citation (4 points)**: Pour les citations qui ont été crées par l'utilisateur, ajouter dans le tableau un lien ou un bouton permettant de modifier les données de la citation. Cela signifie qu'il faudra afficher un formulaire de modification pré-rempli avec les données de la citation et soumettre les modifications via un PUT sur la route `/citation/{citation_id}` (cf [doc API][api-backend]).
- **Votre fonctionnalité**: Vous pouvez proposer vos propres fonctionnalités. Afin qu'elles puissent être prise en compte, il faudra demander à un enseignant de l'UE son aval et le nombre de points que cette fonctionnalité pourra vous rapporter. En l'absence de réponse d'un enseignant, la fonctionnalité rapportera zéro point.

### Qualité de code

Il est demandé de fournir un code aillant une qualité minimale:

- Le code doit être bien indenté.
- Chaque fonction doit posséder un commentaire expliquant ce qu'elle fait, ses arguments en entrée et ce qu'elle renvoie. On pourra s'inspirer des commentaires du code fourni pour un exemple de formattage de ces commentaires.
- Les fonctions devront être courtes (moins de 20 lignes) et les lignes faire moins de 80 caractères. Ce dernier point peut être [vérifié par `eslint`][eslint-max-len]. Il faudra donc découper votre code et vos fonctionnalités complexes en functions simples.
- Les fonctions qui ne sont pas directement liées à l'affichage (par exemple une fonction qui calcule le score d'une citation) devront être testées. Il est possible pour cela de reprendre le système de tests (via [mocha][mocha] et [chai][chai])mis en place pour les TPs 2 et 3.

De plus, le style impératif est interdit, sauf pour modifier la page web. Par exemple, il est autorisé de faire des affectation sur les champs `onclick` ou `innerHTML`, mais il est interdit de faire des boucles avec un compteur. Il s'agit d'un exercice de style afin de montrer que vous êtes capable d'adopter le style fonctionnel pour programmer.

### Bibliothèques

L'utilisation de bibliothèques Javascript extérieures (non fournies dans l'[archive de départ][archive-client]) est interdite, sauf dans le cadre des tests.
Il est cependant possible d'utiliser un autre framework CSS. Attention cependant, il ne faut pas que ce framework inclus du code Javascript.
Par exemple, Bootstrap n'est pas utilisable car il intègre du code Javascript.

## Évaluation

Chaque fonctionnalité pourra rapporter un certain nombre de points (qui sera prochainement ajouté à leur description ci-dessus).
La somme des points obtenus par les fonctionnalités sera au maximum 20, quelle que soit le nombre de fonctionnalités implémentées.
Cette somme sera ensuite multipliée par un coefficient entre 0,4 et 1. Ce coefficient sera établit en fonction du respect des consignes indiquées dans la section "Qualité de code" ci-dessus.

Une soutenance sera organisée le 3 mai 2021. Elle incluera une démonstration et des questions.
Votre horaire de passage, le lien de la visio et l'enseignant qui vous évaluera sont indiqués dans [tomuss].

Si vous avez fait votre projet avec une autre personne, vérifiez que vous avez bien
la même heure de soutenance et que vous êtes avec le même enseignant.
Si ce n'est pas le cas prévenez-moi au plus tôt.

La durée des soutenances a été raccourcie à **7 minutes** à cause du nombre de groupes.
Connectez-vous 5 minutes avant votre horaire
(vous assisterez donc en partie à la soutenance du groupe précédent).

Prévoyez 4 minutes max de démonstration: votre objectif est de montrer que les fonctionnalités que vous avez implémenté
marchent correctement. Si vous avez implémenté plus de 20 points de fonctionnalités,
choisissez celles qui fonctionnent le mieux pour 20 points.

Si ce n'est pas fait, versionnez votre code sur un projet forge (sur https://forge.univ-lyon1.fr).
Saisissez l'URL du projet forge dans la case URL_Projet_LIFAP5 sur tomuss.
**Donnez les droits de "Reporter" à l'enseignant qui vous évaluera.**

[serveur]: https://lifap5.univ-lyon1.fr
[archive-client]: https://perso.liris.cnrs.fr/emmanuel.coquery/enseignement/lifap5/projet-2021p/client.zip
[forge-backend]: https://forge.univ-lyon1.fr/inf2026l-lifap5/lifap5-backend-2021p
[forge-backend-issues]: https://forge.univ-lyon1.fr/inf2026l-lifap5/lifap5-backend-2021p/-/issues
[api-backend]: https://lifap5.univ-lyon1.fr/api-docs/
[bulma]: https://bulma.io/
[eslint-max-len]: https://eslint.org/docs/rules/max-len
[mocha]: https://mochajs.org/
[chai]: http://chaijs.com/
[tomuss]: https://tomuss.univ-lyon1.fr
