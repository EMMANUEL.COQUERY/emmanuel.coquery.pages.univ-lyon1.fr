---
# Course title, summary, and position.
linktitle: "LIFAP5 - TP 3/4 : Interactions serveur asynchrones"
summary: "LIFAP5 - TP 3/4 : Interactions serveur asynchrones"
weight: 206

# Page metadata.
title: "LIFAP5 - TP 3/4 : Interactions serveur asynchrones"
draft: false # Is this a draft? true/false
# toc: true # Show table of contents? true/false
type: docs # Do not modify.

tags: [code]
# Add menu entry to sidebar.
# - name: Declare this menu item as a parent with ID `name`.
# - weight: Position of link in menu.
menu:
  lifap5:
    name: TP3
    weight: 7
---

Comme dans les TP précédents, il faut télécharger et ouvrir localement les fichiers suivants, l'ensemble des fichiers est disponible dans l'archive [`LIFAP5-TP3-fichiers.zip`](./LIFAP5-TP3-fichiers.zip).

On aura également besoin des fichiers de données suivantes disponible sur [le serveur](https://perso.liris.cnrs.fr/emmanuel.coquery/enseignement/lifap5/TP3/) :

- [`nouvelles.json`](https://perso.liris.cnrs.fr/emmanuel.coquery/enseignement/lifap5/TP3/nouvelles.json)
- [`annuaire.json`](https://perso.liris.cnrs.fr/emmanuel.coquery/enseignement/lifap5/TP3/annuaire.json)

Ce TP 3/4 est sur deux séances, il fait suite au TP2.
Le point de départ correspond à peu près au résultat attendu en fin de TP2,
où on a une page d'affichage des nouvelles avec un filtre sur le mois et l'année souhaités.

L'objectif du TP 3/4 est de réorganiser le code pour pouvoir gérer un tableau de nouvelle obtenu de façon **asynchrone**,
alors que dans le TP2 ces nouvelles étaient stockés _dans une constante_.

Globalement, **il s'agit d'écrire un programme le plus fonctionnellement possible** :

- avec un maximum d'initialisations `const`, _sans utiliser d'affectation ou de `let`_ (et sans `var` qui est de toute façon à bannir);
- en utilisant les fonctions `map`, `reduce` et `filter` des tableaux sans _utiliser de boucles `for` ou `while`_.

---

# Exercice 0 : chargement de nouvelles

Cet exercice est une prise en main du code de départ.

- expliquer comment fonctionne la fonction `elimine_doublons_trie` est pourquoi elle est utile dans le code ;
- expliquer ce que fait la fonction `mois_de_annee` ;
- expliquer ce que font les fonctions `maj_annees` et `change_annee` et _quand_ elles sont utilisées ;
- réécrire la fonction `liste_to_options` en style fonctionnel (dans le style de `mois_de_annee` par exemple) ;
- modifier la fonction `maj_mois` pour la liste des mois ne contiennent _que les mois de l'année sélectionnée_ en utilisant `liste_to_options` et `mois_de_annee`.

# Exercice 1 : chargement dynamique des nouvelles : prise en main

Au lieu d'utiliser une liste de nouvelles statique, fournie directement dans le code JavaScript par la constante globale `donnees_exemple`, on va vouloir charger dynamiquement les nouvelles depuis un fichier téléchargé par le navigateur.

La fonction fournie `charge_donnees(url, callback)` charge, _de façon asynchrone_, le fichier _json_ à l'adresse `url`, puis appelle la fonction `callback` en lui passant en argument le contenu du fichier chargé.
Cette fonction utilise l'API [`fetch`](https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API) vue en cours et pourra servir d'exemple pour les exercices suivants. Vous pouvez aussi vous servir directement de `fetch` en utilisant les promesses, sans passer par `charge_donnees`.

- Pour commencer, faire un appel à `charge_donnees(nouvellesUrl, console.log);` dans la console et observer le résultat.
- Remplacer l'appel à `maj_annees(donnees_exemple)` dans `init_menus` pour charger les nouvelles contenues dans le fichier [`nouvelles.json`](https://perso.liris.cnrs.fr/emmanuel.coquery/enseignement/lifap5/TP3/nouvelles.json) en utilisant `charge_donnees`. On utilisera `maj_annees` dans le _callback_ passé en paramètre de `charge_donnees` et on **s'interdira** de faire une affectation sur la constante `donnees_exemple`. Vous pourrez utiliser pour valeur de `nouvellesUrl` l'adresse suivante : https://perso.liris.cnrs.fr/emmanuel.coquery/enseignement/lifap5/TP3/getjson.php?file=nouvelles.json (remarquez la présence du script PHP dans l'URL. Ce script permet de résoudre les problèmes liés aux erreurs "Cross-Origin Request Blocked").

A ce stade, le téléchargement doit fonctionner mais l'affichage des nouvelles filtrées n'est pas encore correct. **Ne passez pas à la suite si vous ne comprenez pas pourquoi**.

# Exercice 2 : Passage d'information via des fermetures : se passer de `donnees_exemple`

- _Déplacer la constante_ `donnees_exemple` dans le fichier `LIFAP5-TP3-test.js`. Sa valeur n'est plus disponible dans `LIFAP5-TP3.js` : la page ne sera plus fonctionnelle et des erreurs JavaScript seront affichées dans la console.
- _Ajouter un argument_ **dans toutes les fonctions qui utilisaient `donnees_exemple`** pour les rendre paramétriques et corriger les appels à ces fonctions pour passer le paramètre. _Suivez les erreurs de la console pour y parvenir_.

_À la fin de cet exercice_, l'affichage des nouvelles doit être fonctionnel et utiliser les bonnes données chargées dynamiquement **sans utiliser de variables globales**.

_Attention à la gestion des événements_: il faut bien réfléchir à ce que l'on range dans les champs `onchange` des menus : on y place _une fonction_ qui n'attend pas d'arguments et qui sera empilée dans la _task queue_ du navigateur.
Comme cette fonction doit accéder aux nouvelles courantes, **il faut utiliser** une _fermeture_ qui va capturer ces nouvelles.

# Exercice 3 : Affichage du contenu des nouvelles

Pour le moment, on affiche juste la liste des titres des nouvelles.
On souhaite en afficher aussi le contenu, mais seulement lorsque l'on clique sur le titre de la nouvelle.
On souhaite également que l'affichage d'un contenu masque les autres contenus.
Pour cela on propose de procéder de la manière suivante:

- Modifier `formate_titre` pour que chaque élément de la liste contiennent un élément `div` avec le contenu de la nouvelle.
- Attribuer un attribut `id` unique à chacun de ces `<div>` ainsi qu'à chacun des éléments `<li>` qui le contient, cela permettra de les manipuler via `document.getElementById`.
- Utiliser la fonction `masque_affiche_contenus` pour réagir au clic sur un titre (_i.e._ faire une affectation sur le champ [`onclick`](https://developer.mozilla.org/en-US/docs/Web/API/GlobalEventHandlers/onclick) de l'élément contenant le titre). Il faudra bien enregistrer _une fonction_ pour chaque titre.
- Cacher toutes les nouvelles en mettant par défaut le style `display` de tous les contenus à "none".

**Il est attendu d'arriver à cet exercice en fin de la première séance de TP. Si ce n'est pas le cas, le faire entre les deux séances.**

## Aide

- Le plus pratique pour générer les identifiants des `<li>` et des `<div>` est de modifier la liste des une bonne foi pour toute en leur donnant comme identifiant leur position dans le tableau.
- Dans les méthodes d'itération sur les listes (_e.g._ [`map`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/map)), la fonction passée en argument **peut prendre en plus de l'élément à traiter**, un deuxième argument correspondant à son index dans le tableau.
- [`document.getElementById`](https://developer.mozilla.org/en-US/docs/Web/API/Document/getElementById) peut être utilisé pour accéder à un élément en fonction de son `id`.
- _Plus pratique encore_, [`Document.querySelectorAll()`](https://developer.mozilla.org/en-US/docs/Web/API/Document/querySelectorAll) permet de retrouver _tous les éléments qui sont sélectionné par un sélecteur CSS_ comme par exemple `document.querySelectorAll("#elt-nouvelles ul li > div")`. Question : _que fait ce sélecteur ?_
- L'attribut [`style`](https://developer.mozilla.org/en-US/docs/Web/API/HTMLElement/style) des éléments contient lui-même un champ `display` qui représente la valeur du style CSS [`display`](https://developer.mozilla.org/en-US/docs/Web/CSS/display).
- Si vous manipulez le DOM avec [`ParentNode.children`](https://developer.mozilla.org/en-US/docs/Web/API/ParentNode/children), **attention**, la colelction retournée est un _pseudo_ tableau, qui n'a pas de méthode `map` : il faut le transformer avec par exemple `Array.from()`

---

# Exercice 4 : l'annuaire de nouvelles

**On ne peut pas faire cet exercice sans avoir terminé le précédent, vu qu'il s'agit d'une extension.**

Le fichier [`annuaire.json`](https://perso.liris.cnrs.fr/emmanuel.coquery/enseignement/lifap5/TP3/annuaire.json) contient une structure JSON avec des liens vers des listes de nouvelles.

- Ajouter à la page un menu déroulant supplémentaire permettant de choisir la liste de nouvelles contenues dans `annuaire.json`. Un choix dans ce menu déclenchera le chargement des données correspondant à la bonne liste de nouvelles, comme dans les exercices précédents
- Traiter les cas d'erreurs : quand on choisit un fichier de nouvelle inexistant, on doit avoir un message d'erreur qui s'affiche dans la page web et pas un arrêt brutal de l'exécution du JavaScript avec l'erreur lisible uniquement dans la console. Pour cela, utiliser la méthode [`catch`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise/catch) des promesses

Afin d'afficher un message d'erreur à l'utilisateur en cas de problème de chargement, on utilisera directement l'API [`fetch`](https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API) vue en cours au lieu de la fonction `charge_donnees`.
