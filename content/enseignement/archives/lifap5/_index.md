---
# Course title, summary, and position.
linktitle: "LIFAP5: Programmation Fonctionnelle pour le Web"
summary: "Programmation fonctionnelle. Principes fondamentaux. Application à la programmation Web avec le style fonctionnel en Javascript"
weight: 204

# Page metadata.
title: "LIFAP5: Programmation Fonctionnelle pour le Web"
draft: false # Is this a draft? true/false
# toc: true # Show table of contents? true/false
type: docs # Do not modify.

tags: [code]
# Add menu entry to sidebar.
# - name: Declare this menu item as a parent with ID `name`.
# - weight: Position of link in menu.
menu:
  lifap5:
    name: Accueil
    weight: 1
---

L'unité d'enseignement [LIAFP5 : programmation fonctionnelle pour le WEB](http://offre-de-formations.univ-lyon1.fr/ue-16369-12/programmation-fonctionnelle-pour-le-web.html) prolonge [LIFAP2 : algorithmique et Programmation Récursive](http://offre-de-formations.univ-lyon1.fr/ue-16359-12/algorithmique-et-programmation-recursive.html) avec les concepts de la programmation fonctionnelle dans l’objectif de fournir aux étudiants les méthodes et pratiques de la programmation fonctionnelle contemporaine. Cette UE fournit les bases nécessaires pour aborder la programmation web et les nouveaux paradigmes de programmation vus en L3 puis en Master. La méthode pédagogique s’appuiera fortement sur la pratique de la programmation dans le langage JavaScript (js) en utilisant le navigateur web Firefox.

## Semestre Printemps 2022

Liens généraux

- Code APOGEE [INF2026L](http://offre-de-formations.univ-lyon1.fr/ue-16369-12/programmation-fonctionnelle-pour-le-web.html)
- [Dépôt des ressources pédagogiques](https://forge.univ-lyon1.fr/inf2026l-lifap5/lifap5-interne) (réservé aux enseignants)

### Suivi à distance

- Lien visio CM distanciel: https://classe-info.univ-lyon1.fr/coq-clw-fvd-oit
- [#lifap5 sur le Rocket.Chat institutionnel](https://chat-info.univ-lyon1.fr/group/lifap5) ([Lien d'invitation](https://go.rocket.chat/invite?host=chat-info.univ-lyon1.fr&path=invite%2FMKZBG5))

### Supports

**2022-01-31 - CM1**: [Diapositives](LIFAP5-2022P-CM1.pdf), [code](LIFAP5-2022P-CM1-code.zip)

**2022-02-07 - TP1**: [Sujet](tp1), [corrigé](tp1/LIFAP5-TP1-correction.zip)

**2022-02-28**:

- CM2: [Diapositives](LIFAP5-2022P-CM2.pdf)
- TD1: [Sujet](LIFAP5-2022P-TD1.pdf), [corrigé](LIFAP5-2022P-TD1-correction.pdf)

**2022-03-07**:

- CM3: [Diapositives](LIFAP5-2022P-CM3.pdf)
- TD2: [Sujet](LIFAP5-2022P-TD2.pdf), [corrigé](LIFAP5-2022P-TD2-correction.pdf)

**2022-03-14**:

- TP2: [Sujet](tp2), [Fichiers](tp2/LIFAP5-TP2.zip), [corrigé](tp2/LIFAP5-TP2-correction.zip)
- **Contrôle** à 11h30, vous recevrez un mail avec votre amphi et votre place.

**2022-03-21**:

- CM4: [Diapositives](LIFAP5-2022P-CM4.pdf)
- TD3/4: [Sujet](LIFAP5-2022P-TD3.pdf) (les deux premiers exercices), [corrigé](LIFAP5-2022P-TD3-correction-1-2.pdf)

**2022-03-28 - TP3**: [Sujet](tp3)

**2022-04-04**:

- CM5: présentation du [projet](projet_2022p)
- TD3/4: [Sujet](LIFAP5-2022P-TD3.pdf) (deux dernier exercices), [corrigé complet](LIFAP5-2022P-TD3-correction.pdf)

**2022-04-11 - Démarrage du projet**: [Sujet du projet](projet_2022p)

#### Annales

[CCF 2017](LIFAP5-2017P-CCF.pdf), [CCF 2018](LIFAP5-2018P-CCF.pdf), [CCF 2019](LIFAP5-2019P-CCF.pdf),
[CCF 2021P](LIFAP5-2021P-CCF.pdf) ([corrige](LIFAP5-2021P-CCF-corrige.pdf)), [CCF 2022P](LIFAP5-2022P-CCF.pdf)

## Autres pages LIFAP5
