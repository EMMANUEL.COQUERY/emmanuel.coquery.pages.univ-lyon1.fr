"use strict";
function ajaxPromise(url) {
  return new Promise(function(resolve, reject) {
    console.log(`ajaxPromise [${url}] ...`);
    let request = new XMLHttpRequest();
    request.open("GET", url);
    request.overrideMimeType("text/json");
    request.onload = function() {
      if (request.status === 200) {
        console.log(`Done [${url}] ...`);
        resolve(request.response);
      } else
        reject(Error(`Network error on [${url}] : ${request.statusText}` ));
    };
    request.onerror = () => reject(Error(`Network error on [${url}] ...`));
    request.send();
  });
}
//from https://github.com/mdn/js-examples
