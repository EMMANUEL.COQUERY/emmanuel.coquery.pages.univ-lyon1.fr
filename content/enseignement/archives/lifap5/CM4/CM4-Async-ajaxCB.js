"use strict";
function ajaxCB(url, callback) {
    console.log(`ajaxCB [${url}] ...`);
    let request = new XMLHttpRequest();
    request.open("GET", url);
    request.overrideMimeType("text/json");
    request.onload = function() {
      if (request.status === 200) {
        console.log("Done [" + url + "]");
        callback(request.responseText, undefined);
      } else {
        callback(undefined, Error(`Network error on [${url}] : ${request.statusText}` ));
      }
    };
    request.onerror = () => callback(undefined, Error(`Network error on [${url}] ...`));
    request.send();
}
