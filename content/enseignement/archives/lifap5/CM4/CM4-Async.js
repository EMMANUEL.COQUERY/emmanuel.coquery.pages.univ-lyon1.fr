"use strict";
/*global ajaxCB, ajaxPromise*/

/*********** HELPERS ***********************/
// URL de base où chercher les fichiers JSON
const baseUrl =
  "https://perso.liris.cnrs.fr/emmanuel.coquery/home/enseignement/lifap5/CM4/";
const getUrl = () => baseUrl + elt("input-name").value.trim();
// par paresse pure
const elt = (id) => document.getElementById(id);
// pour l'affichage du contenu des tableaux JSON dans la page à l'élément id
const appendJSONArray = (id) => (str) => {
  elt(id).innerHTML +=
    JSON.parse(str).map(JSON.stringify).join("<br/>") + "<br/><br/>";
};

// retourne la liste des noms de fichiers choisis dans l'interface avec les checkbox
function checkedFiles() {
  let nbFiles = elt("file-list").children.length;
  let r = [];
  for (let i = 1; i <= nbFiles; ++i) {
    const e = elt("file-ex" + i);
    if (e.checked === true) r.push(baseUrl + e.value.trim());
  }
  return r;
}

/*********** LOAD ONE FILE ***********************/
// version avec le callback "classique" de XHR
function loadSingleCB() {
  ajaxCB(getUrl(), function (data, error) {
    if (!!error) console.error(error);
    else appendJSONArray("content-single")(data);
  });
}

// version avec la version promesse de XHR
function loadSinglePromise() {
  ajaxPromise(getUrl())
    .then(appendJSONArray("content-single"))
    .catch((error) => console.error(error));
}

// version directe avec l'API fetch
// https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch
function loadSingleFetch() {
  const url = getUrl();
  console.log(`ajaxFetch [${url}] ...`);
  fetch(url)
    .then((response) => response.text())
    .then((data) => appendJSONArray("content-single")(data))
    // on pourrait simplement écrire
    // .then(appendJSONArray("content-single"))
    .then(() => console.log(`Done [${url}] ...`))
    .catch((error) =>
      console.error(new Error(`Network error on [${url}]  : ${error}`))
    );
}

/*********** LOAD MANY FILES ***********************/
function loadFilesCB() {
  let filesArray = checkedFiles();
  // on va compter le nombre de fichiers qui restent à télécharger
  // chaque callback va décrémenter le compteur
  let done = filesArray.length;
  let result = [];
  // au final c'est assez affreux
  filesArray.forEach(function (x, idx) {
    ajaxCB(x, function (data, error) {
      if (!!error) console.log(error);
      else {
        result[idx] = data;
        done--;
        if (done === 0) {
          result.forEach(appendJSONArray("content-files"));
        }
      }
    });
  });
}

function loadFilesPromise() {
  let filesArray = checkedFiles();
  // là on va simplement créer un tableau de promesses
  let promises = filesArray.map((str) => ajaxPromise(str));

  // et utiliser Promise.all pour savoir si elles sont **toutes** résolues
  // /!\ IMPORTANT DE REMARQUER L'ORDRE DE RESOLUTION DANS LA CONSOLE /!\
  Promise.all(promises)
    .then((values) => {
      values.forEach(appendJSONArray("content-files"));
    })
    .catch(console.log);
}

function loadFilesFetch() {
  let filesArray = checkedFiles();
  let promises = filesArray.map((str) =>
    fetch(str).then((response) => response.text())
  );

  Promise.all(promises)
    .then((values) => {
      values.forEach(appendJSONArray("content-files"));
    })
    .catch(console.log);
}

/*********** EVENT HANDLERS ***********************/

document.addEventListener(
  "DOMContentLoaded",
  function () {
    elt("callback-single-btn").onclick = loadSingleCB;
    elt("promise-single-btn").onclick = loadSinglePromise;
    elt("fetch-single-btn").onclick = loadSingleFetch;
    elt("reset-single-btn").onclick = () =>
      (elt("content-single").innerHTML = "");

    elt("callback-btn").onclick = loadFilesCB;
    elt("promise-btn").onclick = loadFilesPromise;
    elt("fetch-btn").onclick = loadFilesFetch;
    elt("reset-btn").onclick = () => (elt("content-files").innerHTML = "");
  },
  false
);
