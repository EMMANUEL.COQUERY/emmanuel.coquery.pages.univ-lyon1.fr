---
# Course title, summary, and position.
linktitle: "Archives"
summary: "Support pour d'anciens cours"
weight: 600

# Page metadata.
title: "Archives"
draft: false # Is this a draft? true/false
toc: true # Show table of contents? true/false
type: docs # Do not modify.
authors: ["admin"]
tags: ["data"]
featured: true

# Add menu entry to sidebar.
# - name: Declare this menu item as a parent with ID `name`.
# - weight: Position of link in menu.
menu:
  archives:
    name: accueil
    weight: 1
---
