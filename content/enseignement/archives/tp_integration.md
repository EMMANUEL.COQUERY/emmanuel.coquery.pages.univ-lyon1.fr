---
title: TP Prise en main de Talend
linktitle: TP Talend
toc: true
type: docs
draft: false
menu:
  tiw2:
    weight: 2

# Prev/next pager order (if `docs_section_pager` enabled in `params.toml`)
weight: 2
---

Ce TP a pour objectif de prendre en main le logiciel [Talend Data Integration](https://fr.talend.com/products/data-integration/data-integration-open-studio/) prendre "Autres versions" pour une installation avec JVM existante, _e.g_ sous Linux.

Ce logiciel permet, entre autres, et définir des processus de chargement de données.
Ces processus, dits ETL (Extract, Transform, Load), peuvent être édités graphiquement dans l'IDE Talend, cette dernière générant alors du code Java pour effectuer en pratique la transformation et le chargement des données dans une base, par exemple dans un entrepôt de données. Un processus consiste en un ensemble de composants, chacun chargé d'un tâche précise: lire un certain fichier, filtrer des lignes, effectuer un mapping de colonnes, faire une transformation de valeurs, fusionner des données, ou encore écrire le résultat dans un fichier ou l'insérer dans un table d'un SGBD.

Pour ce TP, on utilisera la base PostgreSQL située à l'adresse `bd-pedago.univ-lyon1.fr` avec comme login celui fourni dans tomuss (login_postgres).
Par exemple pour se connecter avec le login `toto`, la commande est:

```shell
psql -h bd-pedago.univ-lyon1.fr -U toto 
```

## Démarrage

Installer et lancer talend ([téléchargement sur le site officiel](https://fr.talend.com/products/data-integration/data-integration-open-studio/)).

Créer un nouveau projet.
Dans ce TP, on travaillera sur les données contenues dans l'archive [data-edo.zip](http://liris.cnrs.fr/~ecoquery/files/data-edo.zip).

L'arborescence `Jobs` contient un ensemble de processus illustrant l'utilisation des différents composants disponibles.

## Première migration de données

Créer un nouveau Job. Dans la palette à droite, sélectionner le composant Fichier -> Lecture -> tFileInputDelimited, puis glisser déposer le composant dans l'éditeur. Sélectionner le composant tFileInputDelimited_xx dans l'éditeur, puis choisissez l'onglet Component dans la zone du bas. Changer le nom du fichier CSV pour indiquer l'emplacement du fichier `reg2011.txt`.

Regarder le contenu de ce fichier dans un éditeur de texte. Revenez dans Talend et changez le caractère séparateur de champ. Indiquer le bon nombre de lignes d'entête, puis éditez le schéma (bouton ...) et ajoutez pour chaque colonne du fichier `reg2011.txt` la description de son schéma.

Depuis la palette, ajouter un composant Logs & Erreurs -> tLogRow. Cliquez-droit sur le tFileInputDelimited_xx -> Ligne -> Main, puis sur cliquez sur le tLogRow_xx pour établir une connexion entre les deux. En bas, passez dans l'onglet "Exécuter (Job toto)", lancer l'exécution et vérifiez rapidement la cohérence de ce qui s'affiche.

Créer une nouvelle table `regions_test(numero,nom)` dans votre base de données. Dans Talend, ajouter un composant Bases de données -> DB Specifics -> Postgresql -> tPostgresqlOutput. Ouvrez l'arborescence Métadonnées sur la gauche et créez une nouvelle connexion. Renseignez les champs de façon à vous connecter au bon schéma dans la base PostgreSQL. Puis faites un clic droit sur la connexion -> Récupérer le schéma. Revenir sur votre composant tPostgresqlOutput. Indiquez Référenciel comme type de propriété et choisissez Votre nouvelle connexion (bouton ...), puis indiquez la table regions_test. Pour le schéma, indiquez également Référenciel et choisissez à nouveau regions_test.

Ajouter un composant Transformation -> tMap. Supprimez la connexion tFileInputDelimited -> tLogRow et remplacez la par une connexion vers le tMap. Ajoutez une autre connexion, entre le tMap et le tPostgresqlOutput. Si vous n'avez pas eu de proposition pour récupérer le schéma du composant cible, éditez le schéma du tPostgresqlOutput et cliquez sur la double flèche vers la gauche pour copier le schéma de la BD vers le schéma d'entrée. Sélectionnez le tMap et lancer l'éditeur de mapping (bouton ...) depuis l'onglet Component. Faire un glissez déposer du champ REGION vers le champ numero et du champ NCC vers le champs nom.
Il est possible que les types numériques nécessite une transformation.
Celle-ci peut se faire en intercalant un composant tConvertType entre le tMap et le tPostgresqlOutput.

Ajoutez enfin une connexion vers le tLogRow, que vous aurez configuré préalablement pour utiliser le schéma de la table `regions_test`. Effectuer un mapping identique à celui vers le tPostgresqlOutput. Lancez le job et vérifiez qu'il a correctement fonctionné.
Ajouter un log à la sortie du tPostgresqlOutput pour vérifier ce qui a été inséré en base.
Remarque: il est possible de désactiver un composant via le clic droit, par exemple pour ne garder qu'un seul log actif.

## Jointures dans l'ETL

Ajouter au référentiel le fichier `depts2011.txt`, puis créer un nouveau composant pour lire ce fichier en utilisant cette fois-ci le référentiel pour configurer le composant (comme pour la connexion Postgresql).

Créer une table `departements_test(numero,nom,nom_region)`. Créer un composant pour écrire dans cette table (il peut être utile de mettre à jour les schémas dans les métadonnées connexions Talend). Ajouter un composant Transformation -> tJoin, puis une connexion depuis le composant qui lit dans `depts2011.txt` vers ce tJoin et une connexion depuis le tMap vers ce tJoin. Connecter ensuite le tJoin vers un nouveau tMap, ce dernier étant connecté à la sortie dans la table `departements_test`.
Vérifier les différents schémas (e.g. cohérence du schéma de sortie du premier tMap avec le schéma d'entrée lookup du tJoin).
Dans les propriété du tJoin, spécifier l'attribut de jointure (region).
Ajouter si nécessaire un composant de conversion de type.

Tester.

## Pour aller plus loin

Récupérer les données qui concernent les subventions aux associations sur les années 2010 à 2012 sur [data.gouv.fr](https://www.data.gouv.fr/fr/datasets/plf-jaune-associations-subventionnees/).
Créer une table correspondant aux champs du fichier `plf-2014-jaune-associations-subventionne-es-2012.csv` auxquelles on ajoute les nom des départements et des régions concernées.
Utiliser Talend pour charger les données des 3 fichiers contenant les montants des subventions (en prenant soin de les enrichir avec les données précédement chargées sur les départments et les régions).

Remarque: les schéma des 3 fichiers csv ne sont pas exactement les mêmes et il faudra utiliser de nouveaux composants (comme ceux disponibles dans Transformation -> Champs) pour les traiter.
