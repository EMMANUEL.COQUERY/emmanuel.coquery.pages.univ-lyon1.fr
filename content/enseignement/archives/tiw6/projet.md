---
title: "Projet 2019-2020: trafic, météo et pollution"
linktitle: Projet 2019-2020
toc: true
type: docs
draft: false
menu:
  tiw6:
    weight: 3

# Prev/next pager order (if `docs_section_pager` enabled in `params.toml`)
weight: 3
---

Dans ce projet, on va s'intéresser à récupérer et recouper des données de météorologie, de pollution et de trafic routier sur l'agglomération Lyonnaise.

## Organisation du projet

### Groupes d'étudiants

Le projet est à réaliser par groupes de 4 à 5 étudiants.
Chaque groupe devra comprendre au moins deux étudiants de chaque parcours (2 DS et 2 TIW).

Les groupes seront constitués lors de la première séance de TP du projet (mardi 14 janvier 2020 à 8h).
Chaque groupe devra s'identifier auprès d'Emmanuel Coquery lors de cette séance.

### Hébergement

Les projet seront hébergés sur la plateforme Hadoop déjà utilisée pour les TPs Spark de l'UE TIW6.

Chaque groupe se verra attribué une IP (typiquement une des IPs attribuée précédemment à un des membres du groupe) et devra choisir le compte d'un ses membres pour exécuter l'ensemble des processus du projet.

### Soutenances

Le projet sera évalué sur la base de soutenances qui auront lieu le 11 février 2020 aux créneaux fixés dans tomuss.

Chaque groupe disposera d'un quart d'heure de soutenance:

- 7 minutes de présentation (c'est court !)
- 8 minutes de questions

Prévoyez quelques diapositives pour appuyer votre discours (par exemple pour illustrer les différents flux de données ou expliquer vos analyses)

Pensez à placer le code source de votre projet complet dans un dépôt forge et indiquez l'URL de ce dépôt dans la case tomuss `URL_depôt_sources`. Si vous avez utilisé plusieurs dépôts, référencez les dans un dépôt principal via [`git submodule`](https://git-scm.com/docs/git-submodule). Il peut être important que nous ayons accès à votre historique de commit.

## Objectifs

Ce projet vise à acquérir des données de différentes sources de l'agglomération Lyonnaise concernant le trafic routier, la météo et le niveau de pollution constaté.
Des sources sont proposées plus loin dans la [section Données](#données).
En utilisant ces données, on construira un système d'alerte à la pollution.

Le parcours typique d'une donnée dans le système sera le suivant:

1. Acquisition via un script ou un programme dédié appelé régulièrement en utilisant par exemple [cron](https://www.adminschoice.com/crontab-quick-reference).
2. Transformation des données via une topologie Storm.
   Cette étape pourra produire des événements d'alerte qui pourront soit être de nouvelles données à traiter, soit placées dans un répertoire d'alerte sur HDFS.
3. Archivage des données dans HDFS (ou une surcouche comme [Hive](https://hive.apache.org/))

La transmission des données entre les différentes briques du système se fera par l'intermédiaire de [Kafka](http://kafka.apache.org/).

Il est suggéré de commencer par acquérir les données concernant les niveaux de pollution constatés pour simplement lever une alerte en cas de seuil de pollution élevé.

On pourra ensuite compléter le système:

- en intégrant des sources de données supplémentaires, qui pourront être des services Web ou des dépôts de données ouvertes;
- en constituant une archive de telles données;
- en construisant un modèle de prédiction des alertes à la pollution basées sur les données ainsi collectées;
- en analysant la pertinence des prédictions réalisées.

## Données

Quelques sources de données utilisables pour le projet sont indiquées ci-dessous:

Des données concernant le niveau de qualité de l'air peuvent être accédées via le site [api.atmo-aura.fr](http://api.atmo-aura.fr/documentation).
Ce service nécessite la création d'un compte gratuit.

Des relevés de stations météo sont disponibles via le [Météo France](https://donneespubliques.meteofrance.fr/?fond=produit&id_produit=90&id_rubrique=32).
Les archives de ce site ont été copiées [ici](http://192.168.76.159/data/meteo/synop/).

Il est également possible d'utiliser les données d'[OpenWeather](https://openweathermap.org/api), moyennant une inscription gratuite.

Des données de trafic routier sont disponibles [ici](http://192.168.76.159/data/trafic/).
Attention, l'usage de ces données est limité au cadre de ce projet.
Si vous souhaitez en faire un autre usage, il faut s'inscrire sur le site [Données du grand Lyon](https://data.beta.grandlyon.com) et demander l'accès.

Vous êtes encouragés à utiliser d'autres sources de données ouvertes.

## Détails techniques

### Connexion Kafka

> Attention, comme pour HDFS, l'infrastructure Kafka est partagée.
> Pensez à bien nommer vos topics de manière distincte des autres groupes, par exemple `grp-4-atmo` et pas simplment `atmo`.

Les serveurs `zookeeper` sont les machines `node-9`, `node-10` et `node-11` sur le port `2181`.
Les brokers `kafka` sont les machines `node-12`, `node-13`, `node-5` et `node-6` sur le port `9092`.

Exemples de connexion:

Producteur

```shell
kafka-console-producer --broker-list node-5:9092 --topic test-topic
```

Consommateur

```shell
kafka-console-consumer --bootstrap-server node-5:9092 --from-beginning --topic test-topic
```

### Storm

Pour utiliser Storm, le plus simple est de l'installer sur la machine du cluster qui vous a été attribuée (sinon Storm ne pourra pas accéder aux différents services du cluster comme Kafka ou Hive), de manière similaire au [TP Storm](https://forge.univ-lyon1.fr/EMMANUEL.COQUERY/tp-storm-2018).

Si vous êtes sur les machines `node-9`, `node-10` ou `node-11`, un zookeeper est déjà démarré (c'est le zookeeper du cluster qui est utilisé par Kafka).
Il faut donc changer le port du zookeeper que vous allez démarrer pour Storm.
Pour cela il faut ajouter la ligne suivante dans le fichier `conf/storm.yaml` de Storm (il est possible de changer 30564 par un autre numéro en cas de conflit):

```yaml
storm.zookeeper.port: 30564
```

### Hive

- Adresse du Metastore Hive: `node-2`, port `9083`
- Adresse du HiveServer: `node-2`, port `10000`
- URL hive: `jdbc:hive2://node-2:10000/default`
  username: votre nom d'utilisateur (pxxxxxxx)
  pas de mot de passe
- WareHouse dir: `/user/hive/warehouse`

Afin de séparer vos tables de celles des autres groupes, vous pouvez procéder comme suit (bien remplacer pxxxxxxx par votre nom d'utilisateur):

- En utilisant la commande `beeline`, vous connecter en utilisant les paramètres ci-dessus (pas de mot de passe),
  puis créer une base de données à votre nom:
  ```beeline
  !connect jdbc:hive2://node-2:10000/default pxxxxxxx
  create database pxxxxxxx;
  ```
  puis se déconnecter.
- Par la suite, changer `default` en `pxxxxxxx` dans l'URL JDBC:
  `jdbc:hive2://node-2:10000/pxxxxxxx`

### Récupérer les fichiers XML de circulation

Il est possible d'adapter le script suivant à vos besoins:

```shell
# On boucle sur tous les fichiers présents dans l'index sur serveur
for i in $(curl http://192.168.76.159/data/trafic/ | sed -ne 's/^.*\(etat_troncons.*xml\).*$/\1/p')
do
  if ! [ -f $i ] # si le fichier n'est pas dans le répertoire courant
  then
    wget "http://192.168.76.159/data/trafic/$i" # on télécharge le fichier manquant
  fi
done
```
