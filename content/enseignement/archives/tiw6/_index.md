---
# Course title, summary, and position.
linktitle: "TIW6: Big Data Analytics"
summary: "Gestion de données à grande échelle"
weight: 506

# Page metadata.
title: "TIW6: Big Data Analytics"
draft: false # Is this a draft? true/false
toc: true # Show table of contents? true/false
type: docs # Do not modify.
authors: ["admin"]
tags: ["data"]
featured: true

# Add menu entry to sidebar.
# - name: Declare this menu item as a parent with ID `name`.
# - weight: Position of link in menu.
menu:
  tiw6:
    name: TPs
    weight: 1
---

# Big Data

## Année 2021-2022

- [Tutoriel Scala](https://forge.univ-lyon1.fr/EMMANUEL.COQUERY/tutoriel-scala-bda)
- [Spark: découverte](https://forge.univ-lyon1.fr/EMMANUEL.COQUERY/tp-spark-2018), [corrigé](https://forge.univ-lyon1.fr/EMMANUEL.COQUERY/tp-spark-2018-correction)
- [Spark: partionnement](https://forge.univ-lyon1.fr/tiw6-bda/tp-spark2-2021) à rendre pour le **14/11/2021**.
- [TP Storm](https://forge.univ-lyon1.fr/EMMANUEL.COQUERY/tp-storm-2018)
- [Projet 2021-2022](projet_2021_2022)

## Années précédentes

- [Projet]({{< relref "projet.md" >}})
- [Storm](https://forge.univ-lyon1.fr/EMMANUEL.COQUERY/tp-storm-2018) - [cours N. Lumineau](http://liris.cnrs.fr/~nluminea/teaching/M2TIW6/M2TIWDS-BigDataAnalytics-CM-Velocity.pdf) - prévoir une machine personnelle (manque de salle TP)
- [MiniProjet]({{< relref "mini_projet_storm.md" >}}) à rendre pour le 07 février 2021
