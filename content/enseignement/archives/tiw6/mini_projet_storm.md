---
title: "Archive: Mini Projet 2020-2021: course de tortues - suite"
linktitle: "Archive: MiniProjet 2020-2021"
toc: true
type: docs
draft: false
menu:
  tiw6:
    weight: 2

# Prev/next pager order (if `docs_section_pager` enabled in `params.toml`)
weight: 2
---

Ce mini projet est à rendre pour le dimanche 7 février 2021 (cf [Rendu](#Rendu)).
Il sera réalisé en binome (tels que définis pour les TP précédents).

## Contexte

Dans ce projet, on reprend le cadre des courses de tortues du [TP Storm](https://forge.univ-lyon1.fr/EMMANUEL.COQUERY/tp-storm-2018). En particulier, l'état de la course à un instant _t_ est décrit dans un document JSON semblable au document suivant (qui est une simplification de celui utilisé dans le TP Storm):

```
{ "tortoises":[
  {"id":0,"top":523,"position":4},
  {"id":1,"top":523,"position":11},
  {"id":2,"top":523,"position":15},
  {"id":3,"top":523,"position":5},
  {"id":4,"top":523,"position":11},
  {"id":5,"top":523,"position":14},
  {"id":6,"top":523,"position":248},
  {"id":7,"top":523,"position":8},
  {"id":8,"top":523,"position":15},
  {"id":9,"top":523,"position":1}
] }
```

Cependant les informations sur la course de tortues ne sont pas fournies directement par un Spout dans une topologie Storm, mais elle sont accessible via un serveur Web `tortues.ecoquery.os.univ-lyon1.fr` (visible uniquement depuis le campus ou en VPN).
Ce serveur mets à disposition des informations sur 4 courses:

- Tiny (10 tortues): http://tortues.ecoquery.os.univ-lyon1.fr:8080/tiny
- Small (100 tortues): http://tortues.ecoquery.os.univ-lyon1.fr:8080/small
- Medium (500 tortues): http://tortues.ecoquery.os.univ-lyon1.fr:8080/medium
- Large (2000 tortues): http://tortues.ecoquery.os.univ-lyon1.fr:8080/large

Ces informations sont mises à jours toutes les 3 secondes (c'est-à-dire qu'il y a un top toutes les 3 secondes).

Par ailleurs on distingue différentes catégories de tortues selon la manière dont elle avancent.
L'avancement de chaque tortue peut être décrit par des paramètres (qui dépendent du type de paramètres).
Note: On considère que la position d'une tortue correspond au nombre de pas qu'elle a effectué depuis le début de la course.

- Les tortues **régulières** avancent toujours à la même vitesse (paramètre: la vitesse en nombre de pas entre deux tops)
- Les tortues **cycliques** ont une vitesse d'avancement différente à chaque top dans une fenêtre, cette fenêtre est ensuite répétée. Par exemple une telle tortue peut avance de 3 pas au top 1, 5 pas au top 2, 1 pas au top 3, puis à nouveau 3 pas au top 4. Comme elle avancé autant au top 4 qu'au top 1, sa fenetre est de taille 3 et la suite de vitesses 3,5,1 va se répéter dans le reste de la course. La tortue va ainsi avancer de 5 pas au top 5, 1 pas au top 6, 3 pas au top 7, etc. Remarque: deux tortues cycliques peuvent avoir des tailles de fenêtres différentes (paramètre: tableau des vitesses dans la fenêtre).
- Les tortues **distraites** avancent d'un nombre de pas aléatoire, mais toujours compris entre une valeur min et une valeur max (paramètres vitesses min et max)
- Les tortues **fatiguées** s'endorment au fur et à mesure qu'elles avancent. Leur vitesse diminue à un rythme constant jusqu'à tomber à 0. Ces tortues se réveillent alors et recommencent à accélérer (au même rythme qu'elles ont ralenti) jusqu'à atteindre le vitesse initiale, puis elles recommencent alors à s'endormir. Il est possible que le rythme de (décroissance) soit différent au moment de l'arrêt de la tortue et au moment où elle termine sa réaccélération à sa vitesse de départ. On prendra la convention que les tortues fatiguées et cycliques sont considérées comme fatiguées, mais pas comme cycliques. (paramètres: vitesse initiale et rythme de (dé)croissance).

## Travail demandé

Il vous est demandé, pour chaque tortue, de retrouver sa catégorie et ses paramètres.

Vous êtes libres d'utiliser la méthode que vous souhaitez (programme autonome, topologie storm, stockage de données en HDFS avec calculs en Spark, tâches `cron`, ...).
Les langages de programmations autorisés sont Python, Java et Scala.
Le/les programme(s) devront pouvoir s'exécuter au sein du cluster (mais vous pouvez les mettre au point sur votre machine personnelle).

Il est à noter que le serveur ne dispose pas des archives des tops précédents.
Il faudra donc archiver vous-même les différents états de la course.
Vous pouvez lancer votre propre instance du serveur sur votre machine personnelle via le jar téléchargeable ici: https://perso.liris.cnrs.fr/emmanuel.coquery/files/tortues-server-0.0.1-SNAPSHOT.jar

## Rendu

Il est demandé de déposer une archive zip dans la case `Rendu_MiniProjet` de tomuss au plus tard le dimanche 7 février 2021.
Cette archive zip comprendra le code source de vos programmes, un script shell permettant de compiler et de lancer l'analyse, ainsi qu'un fichier `README.md` dans lequel:

- on rappellera les membres du binôme
- on décrira le ou les programmes implémentés, les algorithmes mis au point pour retrouver les catégories de tortues et les paramètres
- On indiquera le format de sortie de l'analyse avec suffisement de détails pour que le correcteur puisse vérifier les résultats obtenus.
