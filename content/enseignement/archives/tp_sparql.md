---
title: "TP SPARQL: révision et requêtes fédérées"
linktitle: TP SPARQL
toc: true
type: docs
draft: false
menu:
  tiw2:
    weight: 4

# Prev/next pager order (if `docs_section_pager` enabled in `params.toml`)
weight: 4
---

Ce TP a pour but de réviser le requêtage de graphe RDF via SPARQL et d'utiliser la capacité d'un point d'accès à utiliser les données disponibles via un autre point d'accès.

## Données

Les données sont hébergées sur un serveur [blazegraph](https://blazegraph.com/) installé à l'URL indiquée dans [tomuss](https://tomusss.univ-lyon1.fr) dans la case `BlazeGraph`.
Cette URL sera référencée par _BGUniv_ dans la suite

> Attention, ne pas utiliser la partie de mise à jour des données (_UPDATE_) qui n'est pas protégée correctement dans la configuration actuelle.

Il s'agit des données provenant du _dump_ de [LinkedBrainz](http://linkedbrainz.org/), qui contient des données provenant de [MusicBrainz](https://musicbrainz.org/)

La majorité des informations suit le vocabulaire [MO, _The Music Ontology_](http://musicontology.com/).
La section [_Getting Started_](http://musicontology.com/docs/getting-started.html) permet de bien comprendre comment sont modélisées les informations dans ce type d'ontologie.

## Requêtes pour réviser

Bien veiller à **limiter le nombre de réponses à 10** pour vos requêtes (via `limit 10`), par exemple pour récupérer 10 groupes de musique:

```sparql
prefix mo: <http://purl.org/ontology/mo/>

select * where {
  ?g rdf:type mo:MusicGroup.
}
limit 10
```

Sauf mention contraire, **ne pas utiliser** `distinct` dans le `select` car ce type de requête est trop coûteux.

Bien utiliser le vocabulaire [MO](http://musicontology.com/specification/) pour connaître les bon prédicats et/ou classes à utiliser.
Dans la suite, on abrégera `http://purl.org/ontology/mo/` par `mo` (comme dans le requête ci-dessus).

Traduire les requêtes suivantes en SPARQL:

1. Donner l'URI des groupes dont le nom (`foaf:name`) est "Genesis"
2. Donner des pistes d'un des groupes "Genesis" (une piste a un `mo:track_number` et un groupe `foaf:made` une piste).
3. Donner le nombre de pistes créées par chacun des groupes appelés Genesis. En déduire l'URI du groupe bien connu.
4. Donner le nombre d'albums de ce groupe. Un album `mo:track` une piste. On pourra utiliser `distinct` dans le `count`.
5. Donner la liste des titres d'albums de ce groupe (on pourra utiliser `distinct`).

## Requêtes fédérées

### Installation locale de blazegraph
Afin de travailler sur le requêtage fédéré, télécharger le fichier [blazegraph.jar](https://github.com/blazegraph/database/releases/download/BLAZEGRAPH_RELEASE_2_1_5/blazegraph.jar).
Lancer le serveur avec la commande suivante (en s'assurant d'utiliser java version 8):
```
java -jar blazegraph.jar
```

Charger les données suivantes dans votre instance de blazegraph via l'onglet update (type _RDF Data_, format _N-Triples_):
```nt
<http://musicbrainz.org/artist/401c3991-b76b-499d-8082-9f2df958ef78#_> <http://www.w3.org/2002/07/owl#sameAs> <http://www.wikidata.org/entity/Q144622>.
<http://musicbrainz.org/artist/661288a1-6d34-46aa-900b-77d18aa82d21#_> <http://www.w3.org/2002/07/owl#sameAs> <http://www.wikidata.org/entity/Q321092>.
<http://musicbrainz.org/artist/8b96b0d8-9c36-42d1-b525-627db897d82f#_> <http://www.w3.org/2002/07/owl#sameAs> <http://www.wikidata.org/entity/Q321332>.
<http://musicbrainz.org/artist/8e3fcd7d-bda1-4ca0-b987-b8528d2ee74e#_> <http://www.w3.org/2002/07/owl#sameAs> <http://www.wikidata.org/entity/Q151012>.

```

### Syntaxe `SERVICE`

L'exemple suivant permet de récupérer dans l'instance blazegraph de l'université le nom des personnes sujet d'un des triplets sameAs ci-dessus (en remplaçant _BGUniv_ par la bonne URL):

```sparql
prefix owl: <http://www.w3.org/2002/07/owl#>
prefix foaf: <http://xmlns.com/foaf/0.1/>
prefix mo: <http://purl.org/ontology/mo/>

SELECT * WHERE {
  ?pers owl:sameAs [].
  SERVICE <BGUniv/sparql> {
    ?pers foaf:name ?name.
  }
}
```

Les requêtes utilisant un point d'accès SPARQL supplémentaire via `SERVICE` sont dites requêtes fédérées.

### Requêtes fédérées

Dans la suite on utilisera 3 magasins (_store_) RDF: votre base locale, _BGUniv_ et _WIKIData_ ([Interface de requêtage](https://query.wikidata.org/), le point d'accès à spécifier dans `SERVICE` est https://query.wikidata.org/sparql).

1. Donner le type dans _BGUniv_ des différents éléments présents votre base.
2. Donner le nom (`rdfs:label`) donné dans _WIKIData_ pour les différents éléments référencés dans votre base.
3. Trouver les prédicats relient dans _WIKIData_ deux éléments de votre base.
4. Restreindre les résultats de la requête précédente de façon à ce que le sujet du prédicat soit une personne (i.e. un `SoloMusicArtist`) et que son objet soit un groupe (i.e. un `MusicGroup`). Vérifier le sens du prédicat en utilsant l'URL du prédicat obtenu comme adresse dans votre navigateur (Wikidata fonctionne selon le principe du LinkedOpenData).
5. Trouver le nom des membres du groupe Genesis qui ne sont pas référencés dans _BGUniv_, mais qui sont référencés dans _WIKIData_.
6. Faire évoluer la requête précédente pour trouver des personnes dans _BGUniv_ ayant le même nom que des membres de Genesis référencés dans _WIKIData_. On pourra dans un deuxième temps exclure ceux qui sont déjà référencés comme membres de Genesis dans _BGUniv_.
7. Parmi les personnes trouvées à la question précédente, identifier celles qui ont produit (`foaf:made`) quelque chose (typiquement une piste ou un album) avec un des membre de Genesis connu par _BGUniv_.
8. Mettre à jour la requête précédente de façon à générer des triplets `owl:sameAs` entre les personnes ainsi trouvées dans _WIKIData_ et _BGUniv_ via un `CONSTRUCT` au lieu d'un `SELECT`.

