# Exercice 1

## Question 1.1

```sql
SELECT sujet as a
FROM triplet
WHERE predicat = '<http://xmlns.com/foaf/0.1/name>'
  AND objet = '"Toto"'
```

## Question 1.2

```sql
SELECT t1.sujet as a, t2.sujet as b
FROM triplet t1, triplet t2, triplet t3
WHERE t1.objet = t3.sujet
  AND t2.objet = t3.sujet
  AND t3.objet = '"Toto"'
  AND t1.sujet <> t2.sujet
  AND t1.predicat = '<http://xmlns.com/foaf/0.1/knows>'
  AND t2.predicat = '<http://xmlns.com/foaf/0.1/knows>'
  AND t3.predicat = '<http://xmlns.com/foaf/0.1/name>'
```

## Question 1.3

```sql
SELECT t1.sujet as a, t1.objet as b, t2.objet as c
FROM triplet t1
LEFT OUTER JOIN triplet t2
             ON t2.sujet = t1.objet
             AND t2.predicat = '<http://xmlns.com/foaf/0.1/knows>'
WHERE t1.predicat = '<http://xmlns.com/foaf/0.1/knows>'
```

## Question 1.4

```sql
SELECT t1.sujet as sujet, t2.objet as predicat, t1.objet as objet
FROM triplet t1, triplet t2
WHERE t2.predicat='<http://www.w3.org/2000/01/rdf-schema#subPropertyOf>'
AND t1.predicat = t2.sujet
MINUS
SELECT sujet, predicat, objet
FROM triplet
```

# Exercice 2

## Question 2.1

```javascript
db.logs.aggregate([
  {
    $group: {
      _id: "$rack",
      recent: { $max: "$ts" },
    },
  },
]);
```

## Question 2.2

```javascript
db.logs.aggregate([
  {
    $group: {
      _id: "$app",
      lvlMin: { $min: "$level" },
    },
  },
  {
    $match: {
      lvlMin: { $gt: 1 },
    },
  },
]);
```

## Question 2.3

```javascript
db.logs.aggregate([
  {
    $group: {
      _id: { rack: "$rack", app: "$app" },
      dummy: { $max: "$ts" },
    },
  },
  {
    $group: {
      _id: "$_id.rack",
      apps: { $push: "$_id.app" },
    },
  },
]);
```

## Question 2.4

```javascript
db.logs.aggregate([
    { $match: { level: 0 }},
    { $group: {
        _id: { rack: "$rack", app: "$app",
               jour: { $dateTrunc: { date: "$ts", unit: "day" } } },
        nbLogs: { $sum: 1 },
    }},
    { $group: {
        _id { rack: "$_id.rack", jour: "$_id.jour" },
        nbLogs: { $sum: "$nbLogs" },
        nbApps: { $sum: 1 },
    }},
    { $group: {
        _id: "$_id.jour",
        nbLogMax: { $max: "$nbLogs" },
        racks: { $push: { rack: "$_id.rack",
                          nbLogs:"$nbLogs",
                          nbApps: "$nbApps" } },
    }},
    { $unwind: "$racks" },
    { $match: {
        $expr: { $eq: [ "$nbLogMax", "$racks.nbLogs" ]}
    }},
    { $project: {
        jour: "$_id",
        rack: "$racks.rack",
        nbApps: "$racks.nbApps",
    }}
])
```

# Exercice 3

```xml
<tables>
  <table name="equipe">
    {
        for $e in //equipe
        return
            <r><v a="nom">{$e/nom/text()}</v>
               <v a="ville">{$e/ville/text()}</v></r>
    }
  </table>
  <table name="joueur">
    {
        for $e in //joueur
        return
            <r><v a="nom">{$e/nom/text()}</v>
               <v a="age">{$e/age/text()}</v></r>
    }
  </table>
  <table name="membre">
    {
        for $j in //joueur
        return
            <r><v a="equipe">{$j/../nom/text()}</v>
               <v a="joueur">{$j/nom/text()}</v>
               <v a="saison">{string($j/@saison)}</v></r>
    }
    {
        for $jr in //joueurref, $j in //joueur
        where $jr/@ref = $j/@id
        return
            <r><v a="equipe">{$jr/../nom/text()}</v>
               <v a="joueur">{$j/nom/text()}</v>
               <v a="saison">{string($jr/@saison)}</v></r>
    }
  </table>
<tables>
```
