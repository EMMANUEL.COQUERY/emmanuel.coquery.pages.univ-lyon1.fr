---
# Course title, summary, and position.
linktitle: "Logiciels utiles pour MIF04"
summary: "Logiciels utiles pour MIF04."
weight: 404

# Page metadata.
title: "Logiciels utiles pour MIF04"
draft: false # Is this a draft? true/false
# toc: true # Show table of contents? true/false
type: docs # Do not modify.

tags: [code, data]
# Add menu entry to sidebar.
# - name: Declare this menu item as a parent with ID `name`.
# - weight: Position of link in menu.
menu:
  mif04:
    name: Logiciels
    weight: 5
---

## Logiciels

- [Atelier XQuery](https://perso.liris.cnrs.fr/~ecoquery/files/atelier-xquery.jar) utilisé en TD XML.
  À lancer avec

  ```bash
  java -jar atelier-xquery.jar
  ```

- [Binaire Linux pour mongo](https://perso.liris.cnrs.fr/~ecoquery/files/mongo.gz)
  à télécharger et décompresser s'il n'est pas disponible en salle TP. Pour décompresser:
  ```bash
  gunzip mongo.gz
  chmod +x mongo
  ```
  puis lancer avec
  ```bash
  ./mongo -u "mif04" -p "mif04" --authenticationDatabase "mif04" "mongodb://bd-pedago.univ-lyon1.fr:27017/mif04"
  ```
  ⚠️ ce binaire fait 45Mo...
