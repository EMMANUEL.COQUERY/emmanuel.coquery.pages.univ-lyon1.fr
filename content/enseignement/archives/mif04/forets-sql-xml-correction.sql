
/*1.
    Donner, pour chaque arbre de la parcelle 613376 (IDP), sa hauteur totale (HTOT).

    <!DOCTYPE arbre [
    <!ELEMENT arbre(hauteur)>
    <!ELEMENT hauteur (#PCDATA)>
    ]>
*/

SELECT XMLElement(name "arbre",
                  XMLElement(name "hauteur", HTOT)) as vxml
FROM arbres
WHERE IDP = 613376 and htot is not null;
                  

/*2.
    Reprendre la question précédente et ajouter un attribut id (valeur obtenue via A) dans l'élément arbre.

    <!DOCTYPE arbre [
    <!ELEMENT arbre(hauteur)>
    <!ATTLIST arbre id CDATA #REQUIRED>
    <!ELEMENT hauteur (#PCDATA)>
    ]>
*/

SELECT XMLElement(name "arbre",
                  XMLAttributes(A as "id"),
                  XMLElement(name "hauteur", HTOT)) as vxml
FROM arbres
WHERE IDP = 613376;

/*3.
    Ajouter, lorsqu'elle est disponible, le nom de l'espèce (code: ESPAR, nom obtenu via la vue documentation), toujours pour la parcelle 613376.

    <!DOCTYPE arbre [
    <!ELEMENT arbre(hauteur,espece?)>
    <!ATTLIST arbre id CDATA #REQUIRED>
    <!ELEMENT hauteur (#PCDATA)>
    <!ELEMENT espece (#PCDATA)>
    ]>
*/


SELECT XMLElement(name "arbre",
                  XMLAttributes(A as "id"),
                  XMLElement(name "hauteur", HTOT),
                  XMLForest(libelle as "espece")
                  ) as vxml
FROM arbres 
LEFT OUTER JOIN 
    (SELECT * FROM documentation WHERE DONNEE = 'ESPAR') doc 
    ON CODE = ESPAR
WHERE IDP = 613376;

/*4.
    Donner pour chaque espèce (valeur distincte ESPAR) son nom (si disponible) et le nombre d'arbres de cette espece.

    <!DOCTYPE espece [
    <!ELEMENT espece (nom?,quantite)>
    <!ATTLIST espece code CDATA #REQUIRED>
    <!ELEMENT nom (#PCDATA)>
    <!ELEMENT quantite (#PCDATA)>
    ]> 
*/

SELECT XMLElement(name "espece",
                  XMLAttributes(ESPAR as "code"),
                  XMLForest(LIBELLE as "nom",
                            count(*) as "quantite")
                  ) as vxml
FROM arbres
LEFT OUTER JOIN
  (SELECT * 
   FROM documentation
   WHERE DONNEE = 'ESPAR') doc
  ON CODE = ESPAR
GROUP BY ESPAR, LIBELLE;


/*5.
    Pour chaque parcelle dont l'identifiant (IDP) est inférieur ou égal à 600200, donner l'identifiant de la parcelle, le nombre d'arbres de cette parcelle et la liste des espèces présentes dans cette parcelle (son code ESPAR sous forme d'attribut XML et pour celles dont on le connait, leur nom sous forme de texte).

    <!DOCTYPE parcelle [
    <!ELEMENT parcelle (espece*)>
    <!ATTLIST parcelle id CDATA #REQUIRED>
    <!ELEMENT espece (#PCDATA)>
    <!ATTLIST espece code CDATA #REQUIRED>
    ]>
*/

SELECT XMLElement(name "parcelle",
                  XMLAttributes(IDP as "id"),
                  XMLAgg(XMLElement(name "espece",
                                    XMLAttributes(espar as "code"),
                                    LIBELLE))
                 ) as vxml
FROM arbres ar
LEFT OUTER JOIN
  (SELECT * 
   FROM documentation
   WHERE DONNEE = 'ESPAR') doc
  ON CODE = ESPAR
WHERE IDP <= 600200
  -- on evite les doublons
  AND NOT EXISTS (select 1 FROM arbres ar2 
                  WHERE ar.IDP = ar2.IDP AND ar.espar = ar2.espar 
                    AND ar2.A < ar.A )
GROUP BY idp;

-- deuxième version




/*6.
    Pour chaque parcelle dont l'identifiant (IDP) est inférieur ou égal à 600200, donner l'identifiant de la parcelle, le nombre d'arbres de cette parcelle et la liste des espèces présentes dans cette parcelle (son code ESPAR sous forme d'attribut XML et pour celles dont on le connait, leur nom sous forme de texte).

    <!DOCTYPE parcelle [
    <!ELEMENT parcelle (nb,espece*)>
    <!ATTLIST parcelle id CDATA #REQUIRED>
    <!ELEMENT nb (#PCDATA)>
    <!ELEMENT espece (#PCDATA)>
    <!ATTLIST espece code CDATA #REQUIRED>
    ]>
*/

SELECT XMLElement(name "parcelle",
                  XMLAttributes(ar1.IDP as "id"),
                  XMLForest(count(*) as "nb"),
                  (SELECT XMLAgg(XMLElement(name "espece",
                                            XMLAttributes(ESPAR as "code"),
                                            LIBELLE)) AS especes
                   FROM (SELECT DISTINCT ESPAR, IDP FROM arbres) ar2
                   LEFT OUTER JOIN
                        (SELECT * FROM documentation WHERE DONNEE = 'ESPAR') doc
                        ON CODE = ESPAR
                   WHERE ar2.idp = ar1.idp
                   GROUP BY IDP)) as vxml                  
FROM arbres ar1
WHERE ar1.IDP <= 600200
GROUP BY ar1.IDP;

/*7.
    Pour les espèces présentes dans entre 2 et 5 parcelles, donner leur code, leur nom et la liste des parcelles (IDP) contenant des arbres de cette espèce.

    <!DOCTYPE espece [
    <!ELEMENT espece (nom,parcelle+)>
    <!ATTLIST espece code CDATA #REQUIRED>
    <!ELEMENT nom (#PCDATA)>
    <!ELEMENT parcelle EMPTY>
    <!ATTLIST parcelle id CDATA #REQUIRED>
    ]>
*/

SELECT (XMLElement(name "espece",
                   XMLAttributes(ESPAR as "code"),
                   XMLElement(name "nom", LIBELLE),
                   XMLAgg(XMLElement(name "parcelle",XMLAttributes(idp as "id")))
                   )
        ) as vxml
FROM (SELECT DISTINCT ESPAR, IDP FROM arbres) parc_esp
LEFT OUTER JOIN
     (SELECT LIBELLE, CODE FROM documentation WHERE DONNEE = 'ESPAR') esp
     ON CODE = ESPAR
GROUP BY ESPAR, LIBELLE
HAVING COUNT(distinct idp) BETWEEN 2 AND 5;

/*8.
    Reprendre la question précédente en ajoutant le nombre d'arbres de l'espèce concernée dans la parcelle concernée sous forme de texte dans l'élément parcelle.

    <!DOCTYPE espece [
    <!ELEMENT espece (nom,parcelle+)>
    <!ATTLIST espece code CDATA #REQUIRED>
    <!ELEMENT nom (#PCDATA)>
    <!ELEMENT parcelle (#PCDATA)>
    <!ATTLIST parcelle id CDATA #REQUIRED>
    ]>
*/

SELECT (XMLElement(name "espece",
                   XMLAttributes(ESPAR as "code"),
                   XMLElement(name "nom", LIBELLE),
                   XMLAgg(XMLElement(name "parcelle",
                                    XMLAttributes(idp as "id"),
                                    nb))
                   )
        ) as vxml
FROM (SELECT ESPAR, IDP, count(*) as nb FROM arbres GROUP BY ESPAR, IDP) parc_esp
LEFT OUTER JOIN
     (SELECT LIBELLE, CODE FROM documentation WHERE DONNEE = 'ESPAR') esp
     ON CODE = ESPAR
GROUP BY ESPAR, LIBELLE
HAVING COUNT(distinct idp) BETWEEN 2 AND 5;

