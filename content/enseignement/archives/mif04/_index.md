---
# Course title, summary, and position.
linktitle: "MIF04: Gestion de Données pour le Web"
summary: "Modèles de données et technologies utilisés dans le cadre du Web."
weight: 404

# Page metadata.
title: "MIF04: Gestion de Données pour le Web"
draft: false # Is this a draft? true/false
# toc: true # Show table of contents? true/false
type: docs # Do not modify.

tags: [code, data]
# Add menu entry to sidebar.
# - name: Declare this menu item as a parent with ID `name`.
# - weight: Position of link in menu.
menu:
  mif04:
    name: Accueil
    weight: 1
---

## Année 2022-2023

L'UE évolue, [page nouvelle UE](https://forge.univ-lyon1.fr/mif24-bdnosql/mif24-bdnosql).

## Année 2021-2022

Invitation vers le salon Rocket: https://go.rocket.chat/invite?host=chat-info.univ-lyon1.fr&path=invite%2FsXnLAS

### Supports

- CM ORM: [diapositives](mapping-objets-relationnel-xml.pdf)
- TP ORM: Faire un fork gitlab du [projet du TP](https://forge.univ-lyon1.fr/EMMANUEL.COQUERY/mif04-gdw-tp-orm). Il est possible de supprimer le lien de fork après l'initialisation du projet. Ce TP n'est **pas** à rendre.\
  [Corrigé](https://forge.univ-lyon1.fr/EMMANUEL.COQUERY/mif04-gdw-tp-orm-correction)
- CM Arbres - XML [diapositives](mdd-arbres.pdf)
- TD Arbres - XML: [énoncé](mif04-gdw-td1-enonce.pdf), [exemples XML pour le TD](InscriptionXML.zip), [corrigé](mif04-gdw-td1-correction.pdf)
- TP SQL/XML: [énoncé](tp_sql_xml), [corrigé](forets-sql-xml-correction.sql)
- CM RDF/SPARQL: [diapositives](rdf-s-sparql-stores.pdf)
- TD RDF: [énoncé](mif04-gdw-td2-enonce.pdf), [corrigé](mif04-gdw-td2-correction.pdf)
- TP SPARQL: [énoncé](tp-sparql-restaurants.html), [corrigé](tp-sparql-restaurants-correction.html)
- TD RDFS: [énoncé](mif04-gdw-td3-enonce.pdf), [corrigé](mif04-gdw-td3-correction.pdf)
- CM Algèbre: [diapositives](algebres-de-collections.pdf), [requetes](code-alg-coll)
- TD Algèbre: [énoncé](MIF04-TD-Algebre-enonce.pdf), [corrigé](MIF04-TD-Algebre-correction.pdf)
- TP MongoDB: [énoncé](tp-aggregation-pipeline), [corrigé](tp-aggregation-pipeline-correction.mongodb)

### Épreuves & Coefficients

- QCM1 (ORM & XML): 25% - le vendredi 12 novembre 2021
- QCM2 (RDF(S), SPARQL & Mongo): 25% - le vendredi 14 janvier 2022
- Examen: 50% (seule cette épreuve donne lieu à une seconde session)

### Annales d'examen

[2013](mif18-exam-2013-s1.pdf),
[2014](mif18-exam-2014-s1.pdf),
[2015](mif18-exam-2015-s1.pdf),
[2016](mif04-examen-2016-2017-s1.pdf),
[2017](mif04-examen-2017-2018-s1i.pdf),
[2018](mif04-examen-2018-2019-s1.pdf),
[2019](mif04-examen-2019-2020-s1.pdf),
[2020](mif04-examen-2020-2021-s1.pdf),
[2021](mif04-examen-2021-2022-s1.pdf) ([corrigé](mif04-examen-2021-2022-s1-corrige), [BBB](https://scalelite-info.univ-lyon1.fr/playback/presentation/2.3/514be0833f3339fc1b5110968f76d29369c161c5-1644316265552))

### Séances

| Date                      | Heure | Thème       | Type     | Groupes       |
| ------------------------- | ----- | ----------- | -------- | ------------- |
| mardi 14 septembre 2021   | 14:00 | ORM         | CM       | A, B, C, D, E |
|                           | 15:45 | ORM1        | TP       | A             |
|                           | 17:30 | ORM2        | TP       | A             |
| lundi 20 septembre 2021   | 14:00 | ORM1        | TP       | D             |
|                           | 15:45 | ORM1        | TP       | E             |
| mardi 21 septembre 2021   | 14:00 | ORM1        | TP       | B             |
|                           | 15:45 | ORM1        | TP       | C             |
| lundi 27 septembre 2021   | 14:00 | ORM2        | TP       | D             |
|                           | 15:45 | ORM2        | TP       | E             |
| mardi 28 septembre 2021   | 14:00 | ORM2        | TP       | B             |
|                           | 15:45 | ORM2        | TP       | C             |
| mardi 5 octobre 2021      | 14:00 | Arb-Sch     | CM       | A, B, C, D, E |
|                           | 15:45 | Arb-Sch     | TD       | A             |
| lundi 11 octobre 2021     | 14:00 | Arb-Sch     | TD       | B             |
|                           | 17:30 | Arb-Sch     | TD       | D             |
| mardi 12 octobre 2021     | 14:00 | Arb-Qry     | CM       | A, B, C, D, E |
|                           | 15:45 | Arb-Qry     | TD       | A             |
|                           | 17:30 | SQL/XML     | TP       | A             |
| lundi 18 octobre 2021     | 14:00 | Arb-Sch     | TD       | C             |
|                           | 15:30 | Arb-Sch     | TD       | E             |
| mardi 19 octobre 2021     | 14:00 | Arb-Qry     | TD       | B             |
|                           | 15:45 | Arb-Qry     | TD       | D             |
| jeudi 21 octobre 2021     | 14:00 | SQL/XML     | TP       | B             |
|                           | 15:45 | SQL/XML     | TP       | D             |
| lundi 25 octobre 2021     | 14:00 | Arb-Qry     | TD       | C             |
|                           | 15:45 | Arb-Qry     | TD       | E             |
| mardi 26 octobre 2021     | 14:00 | SQL/XML     | TP       | C             |
|                           | 15:45 | SQL/XML     | TP       | E             |
| mardi 2 novembre 2021     | 14:00 | RDF/SPARQL  | CM       | A, B, C, D, E |
|                           | 15:45 | SPARQL      | TD       | A             |
|                           | 17:30 | SPARQL      | TP       | A             |
| lundi 8 novembre 2021     | 15:45 | SPARQL      | TD       | B             |
|                           | 17:30 | SPARQL      | TD       | D             |
| mardi 9 novembre 2021     | 14:00 | RDFS/SHACL  | CM       | A, B, C, D, E |
|                           | 15:45 | RDFS/SHACL  | TD       | A             |
| vendredi 12 novembre 2021 | 15:45 |             | Contrôle | D, E          |
|                           | 17:30 |             | Contrôle | A, B, C       |
| lundi 15 novembre 2021    | 14:00 | SPARQL      | TD       | C             |
|                           | 15:45 | SPARQL      | TD       | E             |
| mardi 16 novembre 2021    | 14:00 | SPARQL      | TP       | B             |
|                           | 15:45 | SPARQL      | TP       | D             |
| lundi 22 novembre 2021    | 14:00 | SPARQL      | TP       | C             |
|                           | 15:45 | SPARQL      | TP       | E             |
| mardi 23 novembre 2021    | 14:00 | RDFS/SHACL  | TD       | B             |
|                           | 15:45 | RDFS/SHACL  | TD       | D             |
| jeudi 25 novembre 2021    | 14:00 | RDFS/SHACL  | TD       | C             |
|                           | 15:45 | RDFS/SHACL  | TD       | E             |
| mardi 30 novembre 2021    | 14:00 | NoSQL       | CM       | A, B, C, D, E |
|                           | 15:45 | Alg.Coll.   | CM       | A, B, C, D, E |
|                           | 17:30 | Alg.Coll.   | TD       | A             |
| lundi 6 décembre 2021     | 15:45 | Alg.Coll.   | TD       | B             |
| mardi 7 décembre 2021     | 14:00 | Alg.Coll. 2 | CM       | A, B, C, D, E |
|                           | 15:45 | Alg.Coll. 2 | TD       | A             |
| vendredi 10 décembre 2021 | 14:00 | Alg.Coll.   | TD       | E             |
| lundi 13 décembre 2021    | 14:00 | Alg.Coll. 2 | TD       | B             |
|                           | 15:45 | Alg.Coll.   | TD       | D             |
| mardi 14 décembre 2021    | 14:00 | Alg.Coll.   | TD       | C             |
|                           | 15:45 | Alg.Coll. 2 | TD       | E             |
| jeudi 16 décembre 2021    | 14:00 | Alg.Coll. 2 | TD       | D             |
|                           | 15:45 | Alg.Coll. 2 | TD       | C             |
| lundi 3 janvier 2022      | 14:00 | Mongo 1     | TP       | B             |
|                           | 15:45 | Mongo 1     | TP       | C             |
| mardi 4 janvier 2022      | 14:00 | Mongo 1     | TP       | A             |
|                           | 15:45 | Mongo 1     | TP       | D             |
| vendredi 7 janvier 2022   | 14:00 | Mongo 1     | TP       | E             |
| lundi 10 janvier 2022     | 14:00 | Mongo 2     | TP       | C             |
|                           | 15:45 | Mongo 2     | TP       | B             |
| mardi 11 janvier 2022     | 14:00 | Mongo 2     | TP       | A             |
|                           | 15:45 | Mongo 2     | TP       | D             |
|                           | 17:30 | Mongo 2     | TP       | E             |
| vendredi 14 janvier 2022  | 15:45 |             | Contrôle | A, B, C       |
|                           | 17:30 |             | Contrôle | D, E          |
|                           |       |             |          |               |
|                           |       |             |          | A, B, C, D, E |
