---
# Course title, summary, and position.
linktitle: "MIF04: Archive 2019"
summary: "Modèles de données et technologies utilisés dans le cadre du Web."
weight: 404

# Page metadata.
title: "MIF04: Gestion de Données pour le Web (2019/2020)"
draft: false # Is this a draft? true/false
toc: true # Show table of contents? true/false
type: docs # Do not modify.

tags: [code, data]
# Add menu entry to sidebar.
# - name: Declare this menu item as a parent with ID `name`.
# - weight: Position of link in menu.
menu:
  mif04:
    name: Archive 2019
    weight: 10
---

## Année 2019-2020

### Supports

#### CM

- [ORM](mif04/mapping-objets-relationnel-xml.pdf)
- [Arbres](mif04/mdd-arbres.pdf)
- [RDF(S) / SPARQL](mif04/rdf-s-sparql-stores.pdf)
- [NoSQL](https://liris.cnrs.fr/~fduchate/ens/MIF04/cm-SGBD-NoSQL.pdf)
- À venir

#### TDs

Corrections des TDs:

- [XML (Namespaces, Schema)](mif04/mif04-gdw-td1-correction.pdf)
- [RDF / SPARQL](mif04/mif04-gdw-td2-correction.pdf)
- [RDFS](mif04/mif04-gdw-td3-correction.pdf)
- [Mongo - Map/Reduce](mif04/MIF04-GDW-MapReduce-correction.pdf)

#### TPs

- TP ORM: Faire un fork gitlab du [projet du TP](https://forge.univ-lyon1.fr/EMMANUEL.COQUERY/mif04-gdw-tp-orm). Il est possible de supprimer le lien de fork après l'initialisation du projet. Ce TP n'est **pas** à rendre.\
  [Corrigé](https://forge.univ-lyon1.fr/EMMANUEL.COQUERY/mif04-gdw-tp-orm-correction)
- [TP SQL/XML]({{< relref "tp_sql_xml.md" >}})
- [TP SPARQL](mif04/tp-sparql-restaurants.html)
- [TP Mongo / MapReduce - partie 1](mif04/MIF04-GDW-TP-MapReduce-1.pdf)
- [TP Mongo / MapReduce - partie 2](mif04/MIF04-GDW-TP-MapReduce-2.pdf)

#### Logiciels

- [Atelier XQuery](https://perso.liris.cnrs.fr/~ecoquery/files/atelier-xquery.jar) utilisé en TD XML.
  À lancer avec

  ```bash
  java -jar atelier-xquery.jar
  ```

- [Binaire Linux pour mongo](https://perso.liris.cnrs.fr/~ecoquery/files/mongo.gz)
  à télécharger et décompresser s'il n'est pas disponible en salle TP. Pour décompresser:
  ```bash
  gunzip mongo.gz
  chmod +x mongo
  ```
  puis lancer avec
  ```bash
  ./mongo -u "mif04" -p "mif04" --authenticationDatabase "mif04" "mongodb://bd-pedago.univ-lyon1.fr:27017/mif04"
  ```
  ⚠️ ce binaire fait 45Mo...

### Épreuves & Coefficients

- QCM1 (ORM & XML): 25%
- QCM2 (RDF(S) & Mongo): 25% - le vendredi 13 décembre
- Examen: 50% - le mardi 7 janvier (sous réserves, seule cette épreuve donne lieu à une seconde session)

### Séances

| Date          | Heure | Type         | Groupes | Sujet                                                                |
| ------------- | ----- | ------------ | ------- | -------------------------------------------------------------------- |
| mar. 24 sept. | 14h   | CM           | BCD     | [ORM](mif04/mapping-objets-relationnel-xml.pdf)                      |
|               | 15h45 | TP           | BD      | [ORM](https://forge.univ-lyon1.fr/EMMANUEL.COQUERY/mif04-gdw-tp-orm) |
| mar. 1 oct.   | 14h   | TP           | BCD     | [ORM](https://forge.univ-lyon1.fr/EMMANUEL.COQUERY/mif04-gdw-tp-orm) |
|               | 15h45 | TP           | C       | [ORM](https://forge.univ-lyon1.fr/EMMANUEL.COQUERY/mif04-gdw-tp-orm) |
| mar. 8 oct.   | 14h   | CM           | ABCD    | [Arbres (1)](mif04/mdd-arbres.pdf)                                   |
|               | 15h45 | TD           | A       | [XML (Namespaces, Schema)](mif04/mif04-gdw-td1-enonce.pdf)           |
| ven. 11 oct.  | 14h   | CM           | A       | [ORM](mif04/mapping-objets-relationnel-xml.pdf)                      |
|               | 15h45 | TD           | C       | [XML (Namespaces, Schema)](mif04/mif04-gdw-td1-enonce.pdf)           |
|               | 15h45 | TP           | A       | [ORM](https://forge.univ-lyon1.fr/EMMANUEL.COQUERY/mif04-gdw-tp-orm) |
| mar. 15 oct.  | 14h   | CM           | ABCD    | [Arbres (2)](mif04/mdd-arbres.pdf)                                   |
|               | 15h45 | TP           | A       | [SQL/XML]({{< relref "tp_sql_xml.md" >}})                            |
|               | 15h45 | TD           | B       | [XML (Namespaces, Schema)](mif04/mif04-gdw-td1-enonce.pdf)           |
| ven. 18 oct.  | 14h   | TP           | A       | [ORM](https://forge.univ-lyon1.fr/EMMANUEL.COQUERY/mif04-gdw-tp-orm) |
|               | 15h45 | TD           | A       | [XML (Query)](mif04/mif04-gdw-td1-enonce.pdf)                        |
|               | 15h45 | TD           | D       | [XML (Namespaces, Schema)](mif04/mif04-gdw-td1-enonce.pdf)           |
| mar. 22 oct.  | 14h   | TP           | BD      | [SQL/XML]({{< relref "tp_sql_xml.md" >}})                            |
|               | 15h45 | TP           | C       | [SQL/XML]({{< relref "tp_sql_xml.md" >}})                            |
| mar. 29 oct.  | 14h   | TD           | BD      | [XML (Query)](mif04/mif04-gdw-td1-enonce.pdf)                        |
|               | 15h45 | TD           | C       | [XML (Query)](mif04/mif04-gdw-td1-enonce.pdf)                        |
| mar. 5 nov.   | 14h   | CM           | ABCD    | RDF/SPARQL                                                           |
|               | 15h45 | CM           | ABCD    | RDFS/SHACL                                                           |
| ven. 8 nov.   | 14h   | TD           | AC      | RDF/SPARQL                                                           |
| mar. 12 nov.  | 13h30 | CM           | ABCD    | NoSQL                                                                |
|               | 15h45 | TP           | A       | [TP SPARQL](mif04/tp-sparql-restaurants.html)                        |
|               | 15h45 | TD           | BD      | RDF/SPARQL                                                           |
| ven. 15 nov.  | 14h   | TD           | A       | RDF/SPARQL                                                           |
|               | 15h45 | Contrôle QCM | ABCD    | ORM + Arbres (XML / JSON)                                            |
| mar. 19 nov.  | 14h   | TP           | BD      | [TP SPARQL](mif04/tp-sparql-restaurants.html)                        |
|               | 15h45 | TP           | C       | [TP SPARQL](mif04/tp-sparql-restaurants.html)                        |
| mar. 26 nov.  | 14h   | TD           | BD      | RDFS                                                                 |
|               | 15h45 | TD           | C       | RDFS                                                                 |
| mar. 3 déc.   | 14h   | CM           | ABCD    | Mongo                                                                |
|               | 15h45 | CM           | ABCD    | Map/Reduce                                                           |
| mer. 4 déc.   | 14h   | TD           | B       | Mongo                                                                |
| ven. 6 déc.   | 14h   | TD           | AC      | Mongo                                                                |
|               | 15h45 | TP           | AC      | Mongo                                                                |
|               | 15h45 | TD           | D       | Mongo                                                                |
| lun. 9 déc.   | 14h   | TP           | B       | Mongo                                                                |
|               | 15h45 | TP           | D       | Mongo                                                                |
| mar. 10 déc.  | 14h   | TD           | AD      | Mongo (2)                                                            |
|               | 15h45 | TP           | AD      | Mongo (2)                                                            |
|               | 15h45 | TD           | BC      | Mongo (2)                                                            |
| mer. 11 déc.  | 14h   | TP           | C       | Mongo (2)                                                            |
| jeu. 12 déc.  | 8h    | TP           | B       | Mongo (2)                                                            |
| ven. 13 déc.  | 14h   | Contrôle QCM | ABCD    | RDF(S) + Mongo                                                       |
