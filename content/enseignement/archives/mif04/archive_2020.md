---
# Course title, summary, and position.
linktitle: "MIF04: Archive 2020"
summary: "Modèles de données et technologies utilisés dans le cadre du Web."
weight: 404

# Page metadata.
title: "MIF04: Gestion de Données pour le Web (2020/2021)"
draft: false # Is this a draft? true/false
toc: true # Show table of contents? true/false
type: docs # Do not modify.

tags: [code, data]
# Add menu entry to sidebar.
# - name: Declare this menu item as a parent with ID `name`.
# - weight: Position of link in menu.
menu:
  mif04:
    name: Archive 2020
    weight: 10
---

## Année 2020-2021

**Le contrôle du vendredi 6/11/2020 est reporté**, afin de regrouper cette épreuve avec les autres épreuves présentielles du M1 informatique.

Lien visio CM: https://classe-info.univ-lyon1.fr/coq-g5k-nk8-2up
Invitation chat-info (Rocket):

- CM: https://go.rocket.chat/invite?host=chat-info.univ-lyon1.fr&path=invite%2FRdTiMJ
- TP: https://go.rocket.chat/invite?host=chat-info.univ-lyon1.fr&path=invite%2FucLvve

Les liens pour les visio TD en comodal seront indiqués dans chat-info sur le canal du CM (mif04-gdw-cm).

### Organisation présentiel /distanciel

Pour la durée du confinement, les cours, TD et TP auront lieu en distanciel.

- Les cours seront diffusés en direct via BBB.
  Le lien donné ci-dessus.
  Les questions pourront être posées via https://chat-info.univ-lyon1.fr/ dans le canal `mif04-gdw-cm`.
- Les TD se feront en visioconférence.
  Le lien sera envoyé à toutes et tous peu avant la séance dans le canal `mif04-gdw-cm` de https://chat-info.univ-lyon1.fr/.
  Attention le système de visioconférence utilisé pourra changer selon l'intervenant.
- Les TPs se feront en distanciel, l'interaction à distance se fera via https://chat-info.univ-lyon1.fr/ dans le canal `mif04-gdw-tp`.

~~Merci aux étudiants qui ne sont pas inscrits sur les salons mif04-gdw-xxx de me faire signe par mail. Je ferai des séries d'inscriptions régulièrement.~~
Pour les salons mif04-gdw-xxx, utilisez les liens d'invitation en haut de la page.

#### Alternants:

- L'appel sera fait et et les présences reportées dans tomuss (y compris en amphi).
- **Le premier CM est dupliqué pour le groupe A.** Ce groupe commence donc la **semaine du 6 octobre** (S41, bleu en présentiel).

### Supports

#### CM

- [ORM](mif04/mapping-objets-relationnel-xml.pdf)
- [Arbres](mif04/mdd-arbres.pdf)
- [RDF(S) / SPARQL](mif04/rdf-s-sparql-stores.pdf)
- [NoSQL](https://perso.liris.cnrs.fr/fabien.duchateau/docs/ens/MIF04/cm-SGBD-NoSQL.pdf)
- À venir

#### TDs

Corrections des TDs:

- [XML (Namespaces, Schema, Requêtes)](mif04/mif04-gdw-td1-correction.pdf), [Exemples XML pour le TD](mif04/InscriptionXML.zip)
- [RDF / SPARQL](mif04/mif04-gdw-td2-correction.pdf)
- [RDFS](mif04/mif04-gdw-td3-enonce.pdf)
<!-- - [Mongo - Map/Reduce](mif04/MIF04-GDW-MapReduce-correction.pdf) -->

#### TPs

- TP ORM: Faire un fork gitlab du [projet du TP](https://forge.univ-lyon1.fr/EMMANUEL.COQUERY/mif04-gdw-tp-orm). Il est possible de supprimer le lien de fork après l'initialisation du projet. Ce TP n'est **pas** à rendre.\
  [Corrigé](https://forge.univ-lyon1.fr/EMMANUEL.COQUERY/mif04-gdw-tp-orm-correction)
  **Ce TP est sur deux séances**
- [TP SQL/XML]({{< relref "tp_sql_xml.md" >}})
- [TP SPARQL](mif04/tp-sparql-restaurants.html), [correction](mif04/tp-sparql-restaurants-correction.html),
- [TP Mongo / MapReduce - partie 1](mif04/MIF04-GDW-TP-MapReduce-1.pdf)
- [TP Mongo / MapReduce - partie 2](mif04/MIF04-GDW-TP-MapReduce-2.pdf), [Correction](mif04/corrige-tp-mr-2.js)
