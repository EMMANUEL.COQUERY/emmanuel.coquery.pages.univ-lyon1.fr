---
# Course title, summary, and position.
linktitle: "UE8: Introduction à l'analyse de données"
summary: "Cours d'introduction à l'analyse de données pour le master Santé de l'ISTR"
weight: 506

# Page metadata.
title: "UE8: Introduction à l'analyse de données"
draft: false # Is this a draft? true/false
toc: false # Show table of contents? true/false
type: docs # Do not modify.
authors: ["admin"]
tags: ["code"]
featured: true
# show_related: false

# Add menu entry to sidebar.
# - name: Declare this menu item as a parent with ID `name`.
# - weight: Position of link in menu.
menu:
  ue8:
    name: Page de l'UE
    weight: 1
---

Code APOGEE: STR2054M

[Tomuss](https://tomuss.univ-lyon1.fr)

- 14/09/2023: [Diapositives]({{< ref "/slides/ue8-seance1-introduction" >}})
- 21/09/2023: [Diapositives]({{< ref "/slides/ue8-seance2-jupyter-pandas" >}}), [Notebook]({{< ref "ue8-seance2" >}})
- 29/09/2023: [Diapositives]({{< ref "/slides/ue8-seance3-requetes-avancees" >}}), [Notebook]({{< ref "ue8-seance3" >}})
- 06/10/2023: [Diapositives]({{< ref "/slides/ue8-seance4-requetes-avancees-suite" >}}), [Notebook]({{< ref "ue8-seance4" >}})
- 13/10/2023: [Diapositives]({{< ref "/slides/ue8-seance5-visualisation" >}}), [Notebook]({{< ref "ue8-seance5" >}})
- 20/10/2022: [Diapositives]({{< ref "/slides/ue8-seance6-indexes-multiniveaux" >}}), [Notebook]({{< ref "ue8-seance6" >}})
- 03/11/2023: [Diapositives]({{< ref "/slides/ue8-seance7-indexes-multiniveaux-suite" >}}), [Notebook]({{< ref "ue8-seance7" >}})
- 09/11/2023: [Diapositives]({{< ref "/slides/ue8-seance8-cubes" >}}), [Notebook]({{< ref "ue8-seance8" >}})
- 24/11/2023: [Diapositives]({{< ref "/slides/ue8-seance9-sql-introduction" >}}), [Notebook]({{< ref "ue8-seance9" >}})
- 01/12/2023: [Notebook]({{< ref "ue8-seance10" >}})
- 08/12/2023: [Notebook]({{< ref "ue8-seance11" >}})
- 15/12/2023: [Diapositives]({{< ref "/slides/ue8-seance12-graphique-reproductibilite" >}}), [Notebook]({{< ref "ue8-seance12" >}})

[Corrigé contrôle 5]({{< ref "ue8-notebook-controle5" >}}),
[Corrigé contrôle 6]({{< ref "ue8-notebook-controle6" >}})
