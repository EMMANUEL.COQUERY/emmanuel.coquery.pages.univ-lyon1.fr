# Notebook séance 7

## Chargement des données

On ajoute aussi des colonnes utiles


```python
import pandas
import matplotlib.pyplot as plt
import matplotlib as mpl
from matplotlib import cm

df = pandas.read_excel("Donnees_M2_RD.xlsx")
df["diff_dist"] = df["Dist_A"] - df["Dist_B"]
df["reussi"] = (((df["Dist_A"] > df["Dist_B"]) & (df["Response"] == 2)) 
                 | ((df["Dist_A"] < df["Dist_B"]) & (df["Response"] == 1)))
```

## Exercices set_index

Obtenir la DataFrame indexée par Subject et Space


```python
df_ss = df.set_index(["Subject","Space"])
df_ss
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th></th>
      <th>Name_A</th>
      <th>Name_B</th>
      <th>Dist_A</th>
      <th>Dist_B</th>
      <th>Mode</th>
      <th>Side</th>
      <th>Response</th>
      <th>RT</th>
      <th>diff_dist</th>
      <th>reussi</th>
    </tr>
    <tr>
      <th>Subject</th>
      <th>Space</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th rowspan="5" valign="top">P_ADI_331</th>
      <th>E</th>
      <td>0</td>
      <td>2</td>
      <td>2</td>
      <td>4</td>
      <td>Dic</td>
      <td>D</td>
      <td>2</td>
      <td>18865</td>
      <td>-2</td>
      <td>False</td>
    </tr>
    <tr>
      <th>E</th>
      <td>1</td>
      <td>4</td>
      <td>4</td>
      <td>1</td>
      <td>Dic</td>
      <td>D</td>
      <td>2</td>
      <td>13157</td>
      <td>3</td>
      <td>True</td>
    </tr>
    <tr>
      <th>E</th>
      <td>4</td>
      <td>3</td>
      <td>3</td>
      <td>2</td>
      <td>Dic</td>
      <td>D</td>
      <td>1</td>
      <td>11628</td>
      <td>1</td>
      <td>False</td>
    </tr>
    <tr>
      <th>E</th>
      <td>2</td>
      <td>4</td>
      <td>4</td>
      <td>1</td>
      <td>Dic</td>
      <td>D</td>
      <td>1</td>
      <td>10068</td>
      <td>3</td>
      <td>False</td>
    </tr>
    <tr>
      <th>E</th>
      <td>1</td>
      <td>2</td>
      <td>2</td>
      <td>4</td>
      <td>Dic</td>
      <td>D</td>
      <td>1</td>
      <td>11801</td>
      <td>-2</td>
      <td>True</td>
    </tr>
    <tr>
      <th>...</th>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th rowspan="5" valign="top">P_VAR_330</th>
      <th>I</th>
      <td>0</td>
      <td>1</td>
      <td>3</td>
      <td>5</td>
      <td>Dio</td>
      <td>D</td>
      <td>1</td>
      <td>7626</td>
      <td>-2</td>
      <td>True</td>
    </tr>
    <tr>
      <th>I</th>
      <td>3</td>
      <td>2</td>
      <td>5</td>
      <td>1</td>
      <td>Dio</td>
      <td>D</td>
      <td>2</td>
      <td>6349</td>
      <td>4</td>
      <td>True</td>
    </tr>
    <tr>
      <th>I</th>
      <td>2</td>
      <td>0</td>
      <td>4</td>
      <td>2</td>
      <td>Dio</td>
      <td>D</td>
      <td>2</td>
      <td>9031</td>
      <td>2</td>
      <td>True</td>
    </tr>
    <tr>
      <th>I</th>
      <td>0</td>
      <td>2</td>
      <td>2</td>
      <td>1</td>
      <td>Dio</td>
      <td>D</td>
      <td>2</td>
      <td>16323</td>
      <td>1</td>
      <td>True</td>
    </tr>
    <tr>
      <th>I</th>
      <td>0</td>
      <td>3</td>
      <td>5</td>
      <td>1</td>
      <td>Dio</td>
      <td>D</td>
      <td>2</td>
      <td>10139</td>
      <td>4</td>
      <td>True</td>
    </tr>
  </tbody>
</table>
<p>9594 rows × 10 columns</p>
</div>



Obtenir la DataFrame indexée par Subject, Space et l’index original avec un niveau nommé essai (on peut utiliser .rename sur un index)


```python
df_sse = df.set_index([
    "Subject",
    "Space",
    df.index.rename("essai")])
df_sse
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th></th>
      <th></th>
      <th>Name_A</th>
      <th>Name_B</th>
      <th>Dist_A</th>
      <th>Dist_B</th>
      <th>Mode</th>
      <th>Side</th>
      <th>Response</th>
      <th>RT</th>
      <th>diff_dist</th>
      <th>reussi</th>
    </tr>
    <tr>
      <th>Subject</th>
      <th>Space</th>
      <th>essai</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th rowspan="5" valign="top">P_ADI_331</th>
      <th rowspan="5" valign="top">E</th>
      <th>0</th>
      <td>0</td>
      <td>2</td>
      <td>2</td>
      <td>4</td>
      <td>Dic</td>
      <td>D</td>
      <td>2</td>
      <td>18865</td>
      <td>-2</td>
      <td>False</td>
    </tr>
    <tr>
      <th>1</th>
      <td>1</td>
      <td>4</td>
      <td>4</td>
      <td>1</td>
      <td>Dic</td>
      <td>D</td>
      <td>2</td>
      <td>13157</td>
      <td>3</td>
      <td>True</td>
    </tr>
    <tr>
      <th>2</th>
      <td>4</td>
      <td>3</td>
      <td>3</td>
      <td>2</td>
      <td>Dic</td>
      <td>D</td>
      <td>1</td>
      <td>11628</td>
      <td>1</td>
      <td>False</td>
    </tr>
    <tr>
      <th>3</th>
      <td>2</td>
      <td>4</td>
      <td>4</td>
      <td>1</td>
      <td>Dic</td>
      <td>D</td>
      <td>1</td>
      <td>10068</td>
      <td>3</td>
      <td>False</td>
    </tr>
    <tr>
      <th>4</th>
      <td>1</td>
      <td>2</td>
      <td>2</td>
      <td>4</td>
      <td>Dic</td>
      <td>D</td>
      <td>1</td>
      <td>11801</td>
      <td>-2</td>
      <td>True</td>
    </tr>
    <tr>
      <th>...</th>
      <th>...</th>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th rowspan="5" valign="top">P_VAR_330</th>
      <th rowspan="5" valign="top">I</th>
      <th>9589</th>
      <td>0</td>
      <td>1</td>
      <td>3</td>
      <td>5</td>
      <td>Dio</td>
      <td>D</td>
      <td>1</td>
      <td>7626</td>
      <td>-2</td>
      <td>True</td>
    </tr>
    <tr>
      <th>9590</th>
      <td>3</td>
      <td>2</td>
      <td>5</td>
      <td>1</td>
      <td>Dio</td>
      <td>D</td>
      <td>2</td>
      <td>6349</td>
      <td>4</td>
      <td>True</td>
    </tr>
    <tr>
      <th>9591</th>
      <td>2</td>
      <td>0</td>
      <td>4</td>
      <td>2</td>
      <td>Dio</td>
      <td>D</td>
      <td>2</td>
      <td>9031</td>
      <td>2</td>
      <td>True</td>
    </tr>
    <tr>
      <th>9592</th>
      <td>0</td>
      <td>2</td>
      <td>2</td>
      <td>1</td>
      <td>Dio</td>
      <td>D</td>
      <td>2</td>
      <td>16323</td>
      <td>1</td>
      <td>True</td>
    </tr>
    <tr>
      <th>9593</th>
      <td>0</td>
      <td>3</td>
      <td>5</td>
      <td>1</td>
      <td>Dio</td>
      <td>D</td>
      <td>2</td>
      <td>10139</td>
      <td>4</td>
      <td>True</td>
    </tr>
  </tbody>
</table>
<p>9594 rows × 10 columns</p>
</div>



## Exercices groupby

Faire une dataframe du temps de réponse moyen par Name_A, Name_B


```python
df.groupby(by=["Name_A", "Name_B"])["RT"].mean()
```




    Name_A  Name_B
    0       1         12232.680585
            2         12606.089770
            3         11913.616667
            4         11866.710417
    1       0         11688.033333
            2         12291.152083
            3         11958.879167
            4         11601.102083
    2       0         11608.336117
            1         12040.025000
            3         11978.183716
            4         11357.747917
    3       0         12121.487500
            1         12884.901879
            2         12542.287500
            4         11793.595833
    4       0         12300.860417
            1         11875.852083
            2         12172.350000
            3         12170.338205
    Name: RT, dtype: float64



Faire une dataframe du temps de réponse max par sujet et par différence de distance (symbolique)


```python
df.groupby(by=["Subject","diff_dist"])["RT"].max()
```




    Subject    diff_dist
    P_ADI_331  -4           16642
               -3           15652
               -2           18865
               -1           16347
                1           18067
                            ...  
    P_VAR_330  -1           45627
                1           80729
                2           41271
                3           35480
                4           38072
    Name: RT, Length: 192, dtype: int64



Faire une dataframe du nombre, puis du taux de bonnes réponses par Mode, Side et Space


```python
total = df.groupby(by=["Mode", "Side", "Space"])["Subject"].count()
reussi = df[df["reussi"]].groupby(by=["Mode", "Side", "Space"])["Subject"].count()
df_mss = pandas.DataFrame(total).rename(columns={"Subject":"total"})
df_mss["reussis"] = reussi
df_mss["taux_reussis"] = df_mss["reussis"]/df_mss["total"]
df_mss
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th></th>
      <th></th>
      <th>total</th>
      <th>reussis</th>
      <th>taux_reussis</th>
    </tr>
    <tr>
      <th>Mode</th>
      <th>Side</th>
      <th>Space</th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th rowspan="4" valign="top">Dic</th>
      <th rowspan="2" valign="top">D</th>
      <th>E</th>
      <td>1199</td>
      <td>905</td>
      <td>0.754796</td>
    </tr>
    <tr>
      <th>I</th>
      <td>1198</td>
      <td>1037</td>
      <td>0.865609</td>
    </tr>
    <tr>
      <th rowspan="2" valign="top">G</th>
      <th>E</th>
      <td>1199</td>
      <td>919</td>
      <td>0.766472</td>
    </tr>
    <tr>
      <th>I</th>
      <td>1199</td>
      <td>1040</td>
      <td>0.867389</td>
    </tr>
    <tr>
      <th rowspan="4" valign="top">Dio</th>
      <th rowspan="2" valign="top">D</th>
      <th>E</th>
      <td>1200</td>
      <td>824</td>
      <td>0.686667</td>
    </tr>
    <tr>
      <th>I</th>
      <td>1200</td>
      <td>940</td>
      <td>0.783333</td>
    </tr>
    <tr>
      <th rowspan="2" valign="top">G</th>
      <th>E</th>
      <td>1199</td>
      <td>898</td>
      <td>0.748957</td>
    </tr>
    <tr>
      <th>I</th>
      <td>1200</td>
      <td>1083</td>
      <td>0.902500</td>
    </tr>
  </tbody>
</table>
</div>



## Graphiques 3D

Faire un scatter plot du taux de réussite en fonction de la différence en mètres d’une part et de l’arrondit du temps de réaction en dixièmes de seconde d’autre part.

Intégration de la différence de distance en mètres à partir du code de la séance 5


```python
dist_i_m = pandas.DataFrame(
    { "Dist_I_m": [ 0.2, 0.3, 0.4, 0.6, 0.8 ] },
    index = [1,2,3,4,5]
)
dist_e_m = pandas.DataFrame(
    { "Dist_E_m": [ 2, 3, 4, 6, 8 ] },
    index = [1,2,3,4,5]
)
df_e = df[df['Space'] == 'E']
df1 = pandas.merge(df_e, dist_e_m, left_on='Dist_A', right_index=True)
df2 = df1.rename(columns={ 'Dist_E_m': 'Dist_A_m' })
df3 = pandas.merge(df2, dist_e_m, left_on='Dist_B', right_index=True)
df_e_m = df3.rename(columns={ 'Dist_E_m': 'Dist_B_m' })
df_i = df[df['Space'] == 'I']
df1 = pandas.merge(df_i, dist_i_m, left_on='Dist_A', right_index=True)
df2 = df1.rename(columns={ 'Dist_I_m': 'Dist_A_m' })
df3 = pandas.merge(df2, dist_i_m, left_on='Dist_B', right_index=True)
df_i_m = df3.rename(columns={ 'Dist_I_m': 'Dist_B_m' })
df_m = pandas.concat([df_e_m,df_i_m])
# Nouvelle ligne pour ajouter la différence de distances en mètres
df_m["diff_dist_m"] = df_m["Dist_A_m"] - df_m["Dist_B_m"]
df_m
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Subject</th>
      <th>Name_A</th>
      <th>Name_B</th>
      <th>Dist_A</th>
      <th>Dist_B</th>
      <th>Mode</th>
      <th>Space</th>
      <th>Side</th>
      <th>Response</th>
      <th>RT</th>
      <th>diff_dist</th>
      <th>reussi</th>
      <th>Dist_A_m</th>
      <th>Dist_B_m</th>
      <th>diff_dist_m</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>P_ADI_331</td>
      <td>0</td>
      <td>2</td>
      <td>2</td>
      <td>4</td>
      <td>Dic</td>
      <td>E</td>
      <td>D</td>
      <td>2</td>
      <td>18865</td>
      <td>-2</td>
      <td>False</td>
      <td>3.0</td>
      <td>6.0</td>
      <td>-3.0</td>
    </tr>
    <tr>
      <th>4</th>
      <td>P_ADI_331</td>
      <td>1</td>
      <td>2</td>
      <td>2</td>
      <td>4</td>
      <td>Dic</td>
      <td>E</td>
      <td>D</td>
      <td>1</td>
      <td>11801</td>
      <td>-2</td>
      <td>True</td>
      <td>3.0</td>
      <td>6.0</td>
      <td>-3.0</td>
    </tr>
    <tr>
      <th>7</th>
      <td>P_ADI_331</td>
      <td>0</td>
      <td>3</td>
      <td>2</td>
      <td>4</td>
      <td>Dic</td>
      <td>E</td>
      <td>D</td>
      <td>1</td>
      <td>13237</td>
      <td>-2</td>
      <td>True</td>
      <td>3.0</td>
      <td>6.0</td>
      <td>-3.0</td>
    </tr>
    <tr>
      <th>36</th>
      <td>P_ADI_331</td>
      <td>3</td>
      <td>1</td>
      <td>2</td>
      <td>4</td>
      <td>Dic</td>
      <td>E</td>
      <td>D</td>
      <td>1</td>
      <td>9523</td>
      <td>-2</td>
      <td>True</td>
      <td>3.0</td>
      <td>6.0</td>
      <td>-3.0</td>
    </tr>
    <tr>
      <th>51</th>
      <td>P_ADI_331</td>
      <td>2</td>
      <td>4</td>
      <td>2</td>
      <td>4</td>
      <td>Dio</td>
      <td>E</td>
      <td>G</td>
      <td>1</td>
      <td>11331</td>
      <td>-2</td>
      <td>True</td>
      <td>3.0</td>
      <td>6.0</td>
      <td>-3.0</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>9498</th>
      <td>P_VAR_330</td>
      <td>4</td>
      <td>2</td>
      <td>4</td>
      <td>3</td>
      <td>Dic</td>
      <td>I</td>
      <td>G</td>
      <td>2</td>
      <td>4811</td>
      <td>1</td>
      <td>True</td>
      <td>0.6</td>
      <td>0.4</td>
      <td>0.2</td>
    </tr>
    <tr>
      <th>9503</th>
      <td>P_VAR_330</td>
      <td>3</td>
      <td>0</td>
      <td>4</td>
      <td>3</td>
      <td>Dic</td>
      <td>I</td>
      <td>G</td>
      <td>2</td>
      <td>6628</td>
      <td>1</td>
      <td>True</td>
      <td>0.6</td>
      <td>0.4</td>
      <td>0.2</td>
    </tr>
    <tr>
      <th>9526</th>
      <td>P_VAR_330</td>
      <td>0</td>
      <td>1</td>
      <td>4</td>
      <td>3</td>
      <td>Dic</td>
      <td>I</td>
      <td>G</td>
      <td>2</td>
      <td>5458</td>
      <td>1</td>
      <td>True</td>
      <td>0.6</td>
      <td>0.4</td>
      <td>0.2</td>
    </tr>
    <tr>
      <th>9535</th>
      <td>P_VAR_330</td>
      <td>2</td>
      <td>4</td>
      <td>4</td>
      <td>3</td>
      <td>Dic</td>
      <td>I</td>
      <td>G</td>
      <td>1</td>
      <td>8956</td>
      <td>1</td>
      <td>False</td>
      <td>0.6</td>
      <td>0.4</td>
      <td>0.2</td>
    </tr>
    <tr>
      <th>9546</th>
      <td>P_VAR_330</td>
      <td>2</td>
      <td>3</td>
      <td>4</td>
      <td>3</td>
      <td>Dio</td>
      <td>I</td>
      <td>D</td>
      <td>2</td>
      <td>13525</td>
      <td>1</td>
      <td>True</td>
      <td>0.6</td>
      <td>0.4</td>
      <td>0.2</td>
    </tr>
  </tbody>
</table>
<p>9594 rows × 15 columns</p>
</div>



On ajoute l'arrondi au 1/10 de second de RT


```python
df_m["RT_round"] = df_m["RT"] // 1000
df_m
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Subject</th>
      <th>Name_A</th>
      <th>Name_B</th>
      <th>Dist_A</th>
      <th>Dist_B</th>
      <th>Mode</th>
      <th>Space</th>
      <th>Side</th>
      <th>Response</th>
      <th>RT</th>
      <th>diff_dist</th>
      <th>reussi</th>
      <th>Dist_A_m</th>
      <th>Dist_B_m</th>
      <th>diff_dist_m</th>
      <th>RT_round</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>P_ADI_331</td>
      <td>0</td>
      <td>2</td>
      <td>2</td>
      <td>4</td>
      <td>Dic</td>
      <td>E</td>
      <td>D</td>
      <td>2</td>
      <td>18865</td>
      <td>-2</td>
      <td>False</td>
      <td>3.0</td>
      <td>6.0</td>
      <td>-3.0</td>
      <td>18</td>
    </tr>
    <tr>
      <th>4</th>
      <td>P_ADI_331</td>
      <td>1</td>
      <td>2</td>
      <td>2</td>
      <td>4</td>
      <td>Dic</td>
      <td>E</td>
      <td>D</td>
      <td>1</td>
      <td>11801</td>
      <td>-2</td>
      <td>True</td>
      <td>3.0</td>
      <td>6.0</td>
      <td>-3.0</td>
      <td>11</td>
    </tr>
    <tr>
      <th>7</th>
      <td>P_ADI_331</td>
      <td>0</td>
      <td>3</td>
      <td>2</td>
      <td>4</td>
      <td>Dic</td>
      <td>E</td>
      <td>D</td>
      <td>1</td>
      <td>13237</td>
      <td>-2</td>
      <td>True</td>
      <td>3.0</td>
      <td>6.0</td>
      <td>-3.0</td>
      <td>13</td>
    </tr>
    <tr>
      <th>36</th>
      <td>P_ADI_331</td>
      <td>3</td>
      <td>1</td>
      <td>2</td>
      <td>4</td>
      <td>Dic</td>
      <td>E</td>
      <td>D</td>
      <td>1</td>
      <td>9523</td>
      <td>-2</td>
      <td>True</td>
      <td>3.0</td>
      <td>6.0</td>
      <td>-3.0</td>
      <td>9</td>
    </tr>
    <tr>
      <th>51</th>
      <td>P_ADI_331</td>
      <td>2</td>
      <td>4</td>
      <td>2</td>
      <td>4</td>
      <td>Dio</td>
      <td>E</td>
      <td>G</td>
      <td>1</td>
      <td>11331</td>
      <td>-2</td>
      <td>True</td>
      <td>3.0</td>
      <td>6.0</td>
      <td>-3.0</td>
      <td>11</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>9498</th>
      <td>P_VAR_330</td>
      <td>4</td>
      <td>2</td>
      <td>4</td>
      <td>3</td>
      <td>Dic</td>
      <td>I</td>
      <td>G</td>
      <td>2</td>
      <td>4811</td>
      <td>1</td>
      <td>True</td>
      <td>0.6</td>
      <td>0.4</td>
      <td>0.2</td>
      <td>4</td>
    </tr>
    <tr>
      <th>9503</th>
      <td>P_VAR_330</td>
      <td>3</td>
      <td>0</td>
      <td>4</td>
      <td>3</td>
      <td>Dic</td>
      <td>I</td>
      <td>G</td>
      <td>2</td>
      <td>6628</td>
      <td>1</td>
      <td>True</td>
      <td>0.6</td>
      <td>0.4</td>
      <td>0.2</td>
      <td>6</td>
    </tr>
    <tr>
      <th>9526</th>
      <td>P_VAR_330</td>
      <td>0</td>
      <td>1</td>
      <td>4</td>
      <td>3</td>
      <td>Dic</td>
      <td>I</td>
      <td>G</td>
      <td>2</td>
      <td>5458</td>
      <td>1</td>
      <td>True</td>
      <td>0.6</td>
      <td>0.4</td>
      <td>0.2</td>
      <td>5</td>
    </tr>
    <tr>
      <th>9535</th>
      <td>P_VAR_330</td>
      <td>2</td>
      <td>4</td>
      <td>4</td>
      <td>3</td>
      <td>Dic</td>
      <td>I</td>
      <td>G</td>
      <td>1</td>
      <td>8956</td>
      <td>1</td>
      <td>False</td>
      <td>0.6</td>
      <td>0.4</td>
      <td>0.2</td>
      <td>8</td>
    </tr>
    <tr>
      <th>9546</th>
      <td>P_VAR_330</td>
      <td>2</td>
      <td>3</td>
      <td>4</td>
      <td>3</td>
      <td>Dio</td>
      <td>I</td>
      <td>D</td>
      <td>2</td>
      <td>13525</td>
      <td>1</td>
      <td>True</td>
      <td>0.6</td>
      <td>0.4</td>
      <td>0.2</td>
      <td>13</td>
    </tr>
  </tbody>
</table>
<p>9594 rows × 16 columns</p>
</div>



Calcul du taux de réussite


```python
total_essais = df_m.groupby(by=["diff_dist_m", "RT_round"])["RT"].count().rename("total")
df_scatter = pandas.DataFrame(total_essais)
essais_reussis = df_m[df_m["reussi"]].groupby(by=["diff_dist_m", "RT_round"])["RT"].count().rename("reussis").reindex(index=df_scatter.index, fill_value=0)
df_scatter["reussis"] = essais_reussis
df_scatter["taux"] = essais_reussis / total_essais
fig, ax = plt.subplots(subplot_kw={'projection': '3d'})
ax.scatter(df_scatter.index.get_level_values(0), df_scatter.index.get_level_values(1), df_scatter['taux'])
```




    <mpl_toolkits.mplot3d.art3d.Path3DCollection at 0x7fe6ca63a1f0>




    
![png](output_22_1.png)
    

