# Notebook séance 10

Reprise des exercices Pandas en SQL
On commence par se connecter à la base et à importer pandas.


```python
%load_ext sql
%config SqlMagic.dsn_filename = "connections.ini"
%sql --section bd-pedago
%sql --connections
import pandas
```


<span style="None">Connecting to &#x27;bd-pedago&#x27;</span>


La table de la séance9 existe toujours, pas besoin de la recréer

## Projections et filtres simples

Donner les valeurs de la colonne “RT”


```sql
%%sql
SELECT "RT"
FROM df
```


<span style="None">Running query in &#x27;bd-pedago&#x27;</span>



<span style="color: green">9594 rows affected.</span>





<table>
    <thead>
        <tr>
            <th>RT</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>18865</td>
        </tr>
        <tr>
            <td>13157</td>
        </tr>
        <tr>
            <td>11628</td>
        </tr>
        <tr>
            <td>10068</td>
        </tr>
        <tr>
            <td>11801</td>
        </tr>
        <tr>
            <td>12117</td>
        </tr>
        <tr>
            <td>16347</td>
        </tr>
        <tr>
            <td>13237</td>
        </tr>
        <tr>
            <td>12589</td>
        </tr>
        <tr>
            <td>10973</td>
        </tr>
    </tbody>
</table>
<span style="font-style:italic;text-align:center;">Truncated to <a href="https://jupysql.ploomber.io/en/latest/api/configuration.html#displaylimit">displaylimit</a> of 10.</span>



Donner les différentes valeurs possible pour la colonne “Subject”

On utilise pour cela le mot clé `DISTINCT`, placé juste après le `SELECT`:


```sql
%%sql
SELECT DISTINCT "Subject"
FROM df
```


<span style="None">Running query in &#x27;bd-pedago&#x27;</span>



<span style="color: green">24 rows affected.</span>





<table>
    <thead>
        <tr>
            <th>Subject</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>P_SOA_337</td>
        </tr>
        <tr>
            <td>P_GRF_322</td>
        </tr>
        <tr>
            <td>P_LAC_354</td>
        </tr>
        <tr>
            <td>P_BEH_340</td>
        </tr>
        <tr>
            <td>P_GRC_341</td>
        </tr>
        <tr>
            <td>P_LEG_335</td>
        </tr>
        <tr>
            <td>P_MOE_339</td>
        </tr>
        <tr>
            <td>P_VAL_329</td>
        </tr>
        <tr>
            <td>P_CAV_333</td>
        </tr>
        <tr>
            <td>P_BOC_342</td>
        </tr>
    </tbody>
</table>
<span style="font-style:italic;text-align:center;">Truncated to <a href="https://jupysql.ploomber.io/en/latest/api/configuration.html#displaylimit">displaylimit</a> of 10.</span>



Le distinct peut fonctionner sur plusieurs colonnes, par exemple si la table data contient:

| A  | B  | C  |
|----|----|----|
| a  | b  | e  |
| a  | c  | f  |
| d  | b  | g  |
| a  | b  | h  |

La requête

```sql
SELECT DISTINCT A, B
FROM data
```

produira

| A  | B  |
|----|----|
| a  | b  |
| a  | c  |
| d  | b  |

Filtrer la dataframe pour ne garder que les lignes dont le “RT” est plus petit que 10 000.


```sql
%%sql
SELECT *
FROM df
WHERE "RT" < 10000
```


<span style="None">Running query in &#x27;bd-pedago&#x27;</span>



<span style="color: green">4528 rows affected.</span>





<table>
    <thead>
        <tr>
            <th>index</th>
            <th>Subject</th>
            <th>Name_A</th>
            <th>Name_B</th>
            <th>Dist_A</th>
            <th>Dist_B</th>
            <th>Mode</th>
            <th>Space</th>
            <th>Side</th>
            <th>Response</th>
            <th>RT</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>20</td>
            <td>P_ADI_331</td>
            <td>1</td>
            <td>2</td>
            <td>3</td>
            <td>5</td>
            <td>Dic</td>
            <td>E</td>
            <td>D</td>
            <td>1</td>
            <td>8564</td>
        </tr>
        <tr>
            <td>24</td>
            <td>P_ADI_331</td>
            <td>1</td>
            <td>4</td>
            <td>1</td>
            <td>5</td>
            <td>Dic</td>
            <td>E</td>
            <td>D</td>
            <td>1</td>
            <td>7805</td>
        </tr>
        <tr>
            <td>27</td>
            <td>P_ADI_331</td>
            <td>2</td>
            <td>0</td>
            <td>3</td>
            <td>1</td>
            <td>Dic</td>
            <td>E</td>
            <td>D</td>
            <td>2</td>
            <td>9533</td>
        </tr>
        <tr>
            <td>29</td>
            <td>P_ADI_331</td>
            <td>1</td>
            <td>4</td>
            <td>3</td>
            <td>2</td>
            <td>Dic</td>
            <td>E</td>
            <td>D</td>
            <td>2</td>
            <td>9887</td>
        </tr>
        <tr>
            <td>33</td>
            <td>P_ADI_331</td>
            <td>0</td>
            <td>4</td>
            <td>5</td>
            <td>2</td>
            <td>Dic</td>
            <td>E</td>
            <td>D</td>
            <td>2</td>
            <td>8152</td>
        </tr>
        <tr>
            <td>35</td>
            <td>P_ADI_331</td>
            <td>3</td>
            <td>1</td>
            <td>1</td>
            <td>4</td>
            <td>Dic</td>
            <td>E</td>
            <td>D</td>
            <td>1</td>
            <td>8414</td>
        </tr>
        <tr>
            <td>36</td>
            <td>P_ADI_331</td>
            <td>3</td>
            <td>1</td>
            <td>2</td>
            <td>4</td>
            <td>Dic</td>
            <td>E</td>
            <td>D</td>
            <td>1</td>
            <td>9523</td>
        </tr>
        <tr>
            <td>40</td>
            <td>P_ADI_331</td>
            <td>0</td>
            <td>4</td>
            <td>2</td>
            <td>5</td>
            <td>Dic</td>
            <td>E</td>
            <td>D</td>
            <td>1</td>
            <td>8459</td>
        </tr>
        <tr>
            <td>59</td>
            <td>P_ADI_331</td>
            <td>1</td>
            <td>2</td>
            <td>1</td>
            <td>4</td>
            <td>Dio</td>
            <td>E</td>
            <td>G</td>
            <td>1</td>
            <td>9281</td>
        </tr>
        <tr>
            <td>60</td>
            <td>P_ADI_331</td>
            <td>4</td>
            <td>2</td>
            <td>1</td>
            <td>5</td>
            <td>Dio</td>
            <td>E</td>
            <td>G</td>
            <td>1</td>
            <td>7671</td>
        </tr>
    </tbody>
</table>
<span style="font-style:italic;text-align:center;">Truncated to <a href="https://jupysql.ploomber.io/en/latest/api/configuration.html#displaylimit">displaylimit</a> of 10.</span>




```python
%sqlcmd explore --table df
```



```python
df_subjects = %sql SELECT DISTINCT "Subject" FROM df
```


<span style="None">Running query in &#x27;bd-pedago&#x27;</span>



<span style="color: green">24 rows affected.</span>



```python
df_subjects.DataFrame().to_excel("subjects.xlsx")
```

On change le nombre de ligne affichées à 50


```python
%config SqlMagic.displaylimit = 50
```


```sql
%%sql
SELECT DISTINCT "Subject"
FROM df
```


<span style="None">Running query in &#x27;bd-pedago&#x27;</span>



<span style="color: green">24 rows affected.</span>





<table>
    <thead>
        <tr>
            <th>Subject</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>P_SOA_337</td>
        </tr>
        <tr>
            <td>P_GRF_322</td>
        </tr>
        <tr>
            <td>P_LAC_354</td>
        </tr>
        <tr>
            <td>P_BEH_340</td>
        </tr>
        <tr>
            <td>P_GRC_341</td>
        </tr>
        <tr>
            <td>P_LEG_335</td>
        </tr>
        <tr>
            <td>P_MOE_339</td>
        </tr>
        <tr>
            <td>P_VAL_329</td>
        </tr>
        <tr>
            <td>P_CAV_333</td>
        </tr>
        <tr>
            <td>P_BOC_342</td>
        </tr>
        <tr>
            <td>P_ALM_345</td>
        </tr>
        <tr>
            <td>P_ADI_331</td>
        </tr>
        <tr>
            <td>P_GAM_338</td>
        </tr>
        <tr>
            <td>P_CON_336</td>
        </tr>
        <tr>
            <td>P_VAR_330</td>
        </tr>
        <tr>
            <td>P_GHM_334</td>
        </tr>
        <tr>
            <td>P_BOA_321</td>
        </tr>
        <tr>
            <td>P_BLR_321</td>
        </tr>
        <tr>
            <td>P_TAI_343</td>
        </tr>
        <tr>
            <td>P_CAR_327</td>
        </tr>
        <tr>
            <td>P_AMY_346</td>
        </tr>
        <tr>
            <td>P_BAM_347</td>
        </tr>
        <tr>
            <td>P_ROS_336</td>
        </tr>
        <tr>
            <td>P_BLC_325</td>
        </tr>
    </tbody>
</table>




```python
# On remet le nombre de lignes affichées à 10
%config SqlMagic.displaylimit = 10
```

## Combiner les projections et les filtres

Indiquer la valeur de “RT” pour les essais en mode “Dic”.


```sql
%%sql
SELECT "RT"
FROM df
WHERE "Mode" = 'Dic'
```


<span style="None">Running query in &#x27;bd-pedago&#x27;</span>



<span style="color: green">4795 rows affected.</span>





<table>
    <thead>
        <tr>
            <th>RT</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>18865</td>
        </tr>
        <tr>
            <td>13157</td>
        </tr>
        <tr>
            <td>11628</td>
        </tr>
        <tr>
            <td>10068</td>
        </tr>
        <tr>
            <td>11801</td>
        </tr>
        <tr>
            <td>12117</td>
        </tr>
        <tr>
            <td>16347</td>
        </tr>
        <tr>
            <td>13237</td>
        </tr>
        <tr>
            <td>12589</td>
        </tr>
        <tr>
            <td>10973</td>
        </tr>
    </tbody>
</table>
<span style="font-style:italic;text-align:center;">Truncated to <a href="https://jupysql.ploomber.io/en/latest/api/configuration.html#displaylimit">displaylimit</a> of 10.</span>



Créer une Series indiquant pour chaque essai si la réponse est juste ou non. Une réponse est juste si la distance A est inférieure à la distance B et la réponse est 1 ou si la distance A est supérieure à la distance B et la réponse est 2.


```sql
%%sql
SELECT index,
       (("Dist_A" < "Dist_B" AND "Response" = 1)
        OR ("Dist_A" > "Dist_B" AND "Response" = 2)) 
       AS juste
FROM df
```


<span style="None">Running query in &#x27;bd-pedago&#x27;</span>



<span style="color: green">9594 rows affected.</span>





<table>
    <thead>
        <tr>
            <th>index</th>
            <th>juste</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>0</td>
            <td>False</td>
        </tr>
        <tr>
            <td>1</td>
            <td>True</td>
        </tr>
        <tr>
            <td>2</td>
            <td>False</td>
        </tr>
        <tr>
            <td>3</td>
            <td>False</td>
        </tr>
        <tr>
            <td>4</td>
            <td>True</td>
        </tr>
        <tr>
            <td>5</td>
            <td>False</td>
        </tr>
        <tr>
            <td>6</td>
            <td>True</td>
        </tr>
        <tr>
            <td>7</td>
            <td>True</td>
        </tr>
        <tr>
            <td>8</td>
            <td>True</td>
        </tr>
        <tr>
            <td>9</td>
            <td>True</td>
        </tr>
    </tbody>
</table>
<span style="font-style:italic;text-align:center;">Truncated to <a href="https://jupysql.ploomber.io/en/latest/api/configuration.html#displaylimit">displaylimit</a> of 10.</span>



Pour avoir la colonne "juste" intégrée avec les autres, on peut faire une requête qui récupère tout df plus cette colonne:


```sql
%%sql
SELECT *,
       (("Dist_A" < "Dist_B" AND "Response" = 1)
        OR ("Dist_A" > "Dist_B" AND "Response" = 2)) 
       AS juste
FROM df
```


<span style="None">Running query in &#x27;bd-pedago&#x27;</span>



<span style="color: green">9594 rows affected.</span>





<table>
    <thead>
        <tr>
            <th>index</th>
            <th>Subject</th>
            <th>Name_A</th>
            <th>Name_B</th>
            <th>Dist_A</th>
            <th>Dist_B</th>
            <th>Mode</th>
            <th>Space</th>
            <th>Side</th>
            <th>Response</th>
            <th>RT</th>
            <th>juste</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>0</td>
            <td>P_ADI_331</td>
            <td>0</td>
            <td>2</td>
            <td>2</td>
            <td>4</td>
            <td>Dic</td>
            <td>E</td>
            <td>D</td>
            <td>2</td>
            <td>18865</td>
            <td>False</td>
        </tr>
        <tr>
            <td>1</td>
            <td>P_ADI_331</td>
            <td>1</td>
            <td>4</td>
            <td>4</td>
            <td>1</td>
            <td>Dic</td>
            <td>E</td>
            <td>D</td>
            <td>2</td>
            <td>13157</td>
            <td>True</td>
        </tr>
        <tr>
            <td>2</td>
            <td>P_ADI_331</td>
            <td>4</td>
            <td>3</td>
            <td>3</td>
            <td>2</td>
            <td>Dic</td>
            <td>E</td>
            <td>D</td>
            <td>1</td>
            <td>11628</td>
            <td>False</td>
        </tr>
        <tr>
            <td>3</td>
            <td>P_ADI_331</td>
            <td>2</td>
            <td>4</td>
            <td>4</td>
            <td>1</td>
            <td>Dic</td>
            <td>E</td>
            <td>D</td>
            <td>1</td>
            <td>10068</td>
            <td>False</td>
        </tr>
        <tr>
            <td>4</td>
            <td>P_ADI_331</td>
            <td>1</td>
            <td>2</td>
            <td>2</td>
            <td>4</td>
            <td>Dic</td>
            <td>E</td>
            <td>D</td>
            <td>1</td>
            <td>11801</td>
            <td>True</td>
        </tr>
        <tr>
            <td>5</td>
            <td>P_ADI_331</td>
            <td>2</td>
            <td>1</td>
            <td>2</td>
            <td>3</td>
            <td>Dic</td>
            <td>E</td>
            <td>D</td>
            <td>2</td>
            <td>12117</td>
            <td>False</td>
        </tr>
        <tr>
            <td>6</td>
            <td>P_ADI_331</td>
            <td>2</td>
            <td>1</td>
            <td>3</td>
            <td>4</td>
            <td>Dic</td>
            <td>E</td>
            <td>D</td>
            <td>1</td>
            <td>16347</td>
            <td>True</td>
        </tr>
        <tr>
            <td>7</td>
            <td>P_ADI_331</td>
            <td>0</td>
            <td>3</td>
            <td>2</td>
            <td>4</td>
            <td>Dic</td>
            <td>E</td>
            <td>D</td>
            <td>1</td>
            <td>13237</td>
            <td>True</td>
        </tr>
        <tr>
            <td>8</td>
            <td>P_ADI_331</td>
            <td>2</td>
            <td>0</td>
            <td>4</td>
            <td>2</td>
            <td>Dic</td>
            <td>E</td>
            <td>D</td>
            <td>2</td>
            <td>12589</td>
            <td>True</td>
        </tr>
        <tr>
            <td>9</td>
            <td>P_ADI_331</td>
            <td>2</td>
            <td>1</td>
            <td>4</td>
            <td>2</td>
            <td>Dic</td>
            <td>E</td>
            <td>D</td>
            <td>2</td>
            <td>10973</td>
            <td>True</td>
        </tr>
    </tbody>
</table>
<span style="font-style:italic;text-align:center;">Truncated to <a href="https://jupysql.ploomber.io/en/latest/api/configuration.html#displaylimit">displaylimit</a> of 10.</span>



On peut en faire une vue en ajoutant devant `CREATE OR REPLACE VIEW un_nom AS`, ce qui permettra d'utiliser le résultat comme s'il avait été mis dans une table `un_nom`. La différence est qu'une vue est toujours recalculée et a donc toujours des données correctes.


```sql
%%sql
DROP VIEW IF EXISTS dfj;
CREATE OR REPLACE VIEW dfj AS
SELECT *,
       (("Dist_A" < "Dist_B" AND "Response" = 1)
        OR ("Dist_A" > "Dist_B" AND "Response" = 2)) 
       AS juste
FROM df
```


<span style="None">Running query in &#x27;bd-pedago&#x27;</span>





<table>
    <thead>
        <tr>
        </tr>
    </thead>
    <tbody>
    </tbody>
</table>




```sql
%%sql
SELECT *
FROM dfj
```


<span style="None">Running query in &#x27;bd-pedago&#x27;</span>



<span style="color: green">9594 rows affected.</span>





<table>
    <thead>
        <tr>
            <th>index</th>
            <th>Subject</th>
            <th>Name_A</th>
            <th>Name_B</th>
            <th>Dist_A</th>
            <th>Dist_B</th>
            <th>Mode</th>
            <th>Space</th>
            <th>Side</th>
            <th>Response</th>
            <th>RT</th>
            <th>juste</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>0</td>
            <td>P_ADI_331</td>
            <td>0</td>
            <td>2</td>
            <td>2</td>
            <td>4</td>
            <td>Dic</td>
            <td>E</td>
            <td>D</td>
            <td>2</td>
            <td>18865</td>
            <td>False</td>
        </tr>
        <tr>
            <td>1</td>
            <td>P_ADI_331</td>
            <td>1</td>
            <td>4</td>
            <td>4</td>
            <td>1</td>
            <td>Dic</td>
            <td>E</td>
            <td>D</td>
            <td>2</td>
            <td>13157</td>
            <td>True</td>
        </tr>
        <tr>
            <td>2</td>
            <td>P_ADI_331</td>
            <td>4</td>
            <td>3</td>
            <td>3</td>
            <td>2</td>
            <td>Dic</td>
            <td>E</td>
            <td>D</td>
            <td>1</td>
            <td>11628</td>
            <td>False</td>
        </tr>
        <tr>
            <td>3</td>
            <td>P_ADI_331</td>
            <td>2</td>
            <td>4</td>
            <td>4</td>
            <td>1</td>
            <td>Dic</td>
            <td>E</td>
            <td>D</td>
            <td>1</td>
            <td>10068</td>
            <td>False</td>
        </tr>
        <tr>
            <td>4</td>
            <td>P_ADI_331</td>
            <td>1</td>
            <td>2</td>
            <td>2</td>
            <td>4</td>
            <td>Dic</td>
            <td>E</td>
            <td>D</td>
            <td>1</td>
            <td>11801</td>
            <td>True</td>
        </tr>
        <tr>
            <td>5</td>
            <td>P_ADI_331</td>
            <td>2</td>
            <td>1</td>
            <td>2</td>
            <td>3</td>
            <td>Dic</td>
            <td>E</td>
            <td>D</td>
            <td>2</td>
            <td>12117</td>
            <td>False</td>
        </tr>
        <tr>
            <td>6</td>
            <td>P_ADI_331</td>
            <td>2</td>
            <td>1</td>
            <td>3</td>
            <td>4</td>
            <td>Dic</td>
            <td>E</td>
            <td>D</td>
            <td>1</td>
            <td>16347</td>
            <td>True</td>
        </tr>
        <tr>
            <td>7</td>
            <td>P_ADI_331</td>
            <td>0</td>
            <td>3</td>
            <td>2</td>
            <td>4</td>
            <td>Dic</td>
            <td>E</td>
            <td>D</td>
            <td>1</td>
            <td>13237</td>
            <td>True</td>
        </tr>
        <tr>
            <td>8</td>
            <td>P_ADI_331</td>
            <td>2</td>
            <td>0</td>
            <td>4</td>
            <td>2</td>
            <td>Dic</td>
            <td>E</td>
            <td>D</td>
            <td>2</td>
            <td>12589</td>
            <td>True</td>
        </tr>
        <tr>
            <td>9</td>
            <td>P_ADI_331</td>
            <td>2</td>
            <td>1</td>
            <td>4</td>
            <td>2</td>
            <td>Dic</td>
            <td>E</td>
            <td>D</td>
            <td>2</td>
            <td>10973</td>
            <td>True</td>
        </tr>
    </tbody>
</table>
<span style="font-style:italic;text-align:center;">Truncated to <a href="https://jupysql.ploomber.io/en/latest/api/configuration.html#displaylimit">displaylimit</a> of 10.</span>



Indiquer les “Subject” qui ont répondu en moins de 4 000 (“RT” inférieur à 4 000)


```sql
%%sql
SELECT DISTINCT "Subject"
FROM df
WHERE "RT" < 4000
```


<span style="None">Running query in &#x27;bd-pedago&#x27;</span>



<span style="color: green">13 rows affected.</span>





<table>
    <thead>
        <tr>
            <th>Subject</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>P_GRC_341</td>
        </tr>
        <tr>
            <td>P_LEG_335</td>
        </tr>
        <tr>
            <td>P_MOE_339</td>
        </tr>
        <tr>
            <td>P_CAV_333</td>
        </tr>
        <tr>
            <td>P_BOC_342</td>
        </tr>
        <tr>
            <td>P_ALM_345</td>
        </tr>
        <tr>
            <td>P_CON_336</td>
        </tr>
        <tr>
            <td>P_VAR_330</td>
        </tr>
        <tr>
            <td>P_GHM_334</td>
        </tr>
        <tr>
            <td>P_BLR_321</td>
        </tr>
    </tbody>
</table>
<span style="font-style:italic;text-align:center;">Truncated to <a href="https://jupysql.ploomber.io/en/latest/api/configuration.html#displaylimit">displaylimit</a> of 10.</span>



## Agrégations globales

Donner la moyenne de “RT”.


```sql
%%sql
SELECT avg("RT") AS moyenne
FROM df
```


<span style="None">Running query in &#x27;bd-pedago&#x27;</span>



<span style="color: green">1 rows affected.</span>





<table>
    <thead>
        <tr>
            <th>moyenne</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>12050.088597039817</td>
        </tr>
    </tbody>
</table>



Donner le plus grand “RT” pour le “Subject” “P_ROS_336”


```sql
%%sql
SELECT max("RT") AS moyenne
FROM df
WHERE "Subject" = 'P_ROS_336'
```


<span style="None">Running query in &#x27;bd-pedago&#x27;</span>



<span style="color: green">1 rows affected.</span>





<table>
    <thead>
        <tr>
            <th>moyenne</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>54405</td>
        </tr>
    </tbody>
</table>



Donner l’écart-type de “RT” pour les essais réussis (utiliser .std())


```sql
%%sql
SELECT stddev("RT") AS ecart_type
FROM dfj
WHERE juste
```


<span style="None">Running query in &#x27;bd-pedago&#x27;</span>



<span style="color: green">1 rows affected.</span>





<table>
    <thead>
        <tr>
            <th>ecart_type</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>6505.738105682207</td>
        </tr>
    </tbody>
</table>



## Groupes simples

Donner la moyenne du temps de réaction (“RT”) pour chaque “Subject”.


```sql
%%sql
SELECT "Subject", avg("RT") as Moyenne_RT
FROM df
GROUP BY "Subject"
```


<span style="None">Running query in &#x27;bd-pedago&#x27;</span>



<span style="color: green">24 rows affected.</span>





<table>
    <thead>
        <tr>
            <th>Subject</th>
            <th>moyenne_rt</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>P_SOA_337</td>
            <td>10051.175000000000</td>
        </tr>
        <tr>
            <td>P_GRF_322</td>
            <td>15652.556390977444</td>
        </tr>
        <tr>
            <td>P_LAC_354</td>
            <td>15617.710000000000</td>
        </tr>
        <tr>
            <td>P_BEH_340</td>
            <td>13187.195000000000</td>
        </tr>
        <tr>
            <td>P_GRC_341</td>
            <td>11213.115000000000</td>
        </tr>
        <tr>
            <td>P_LEG_335</td>
            <td>10904.325000000000</td>
        </tr>
        <tr>
            <td>P_MOE_339</td>
            <td>8470.4475000000000000</td>
        </tr>
        <tr>
            <td>P_VAL_329</td>
            <td>13661.730000000000</td>
        </tr>
        <tr>
            <td>P_CAV_333</td>
            <td>10551.257500000000</td>
        </tr>
        <tr>
            <td>P_BOC_342</td>
            <td>12779.680000000000</td>
        </tr>
    </tbody>
</table>
<span style="font-style:italic;text-align:center;">Truncated to <a href="https://jupysql.ploomber.io/en/latest/api/configuration.html#displaylimit">displaylimit</a> of 10.</span>



Donner le nombre d’essais pour chaque “Subject” en utilisant `COUNT(*)`.


```sql
%%sql
SELECT "Subject", COUNT(*) as nombre_essais
FROM df
GROUP BY "Subject"
```


<span style="None">Running query in &#x27;bd-pedago&#x27;</span>



<span style="color: green">24 rows affected.</span>





<table>
    <thead>
        <tr>
            <th>Subject</th>
            <th>nombre_essais</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>P_SOA_337</td>
            <td>400</td>
        </tr>
        <tr>
            <td>P_GRF_322</td>
            <td>399</td>
        </tr>
        <tr>
            <td>P_LAC_354</td>
            <td>400</td>
        </tr>
        <tr>
            <td>P_BEH_340</td>
            <td>400</td>
        </tr>
        <tr>
            <td>P_GRC_341</td>
            <td>400</td>
        </tr>
        <tr>
            <td>P_LEG_335</td>
            <td>400</td>
        </tr>
        <tr>
            <td>P_MOE_339</td>
            <td>400</td>
        </tr>
        <tr>
            <td>P_VAL_329</td>
            <td>400</td>
        </tr>
        <tr>
            <td>P_CAV_333</td>
            <td>400</td>
        </tr>
        <tr>
            <td>P_BOC_342</td>
            <td>400</td>
        </tr>
    </tbody>
</table>
<span style="font-style:italic;text-align:center;">Truncated to <a href="https://jupysql.ploomber.io/en/latest/api/configuration.html#displaylimit">displaylimit</a> of 10.</span>



Donner le nombre d’essais réussis pour chaque “Subject” en utilisant un filtre puis `count(*)`.


```sql
%%sql
SELECT "Subject", COUNT(*) as essais_reussis
FROM dfj
WHERE juste
GROUP BY "Subject"

```


<span style="None">Running query in &#x27;bd-pedago&#x27;</span>



<span style="color: green">24 rows affected.</span>





<table>
    <thead>
        <tr>
            <th>Subject</th>
            <th>essais_reussis</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>P_SOA_337</td>
            <td>305</td>
        </tr>
        <tr>
            <td>P_GRF_322</td>
            <td>291</td>
        </tr>
        <tr>
            <td>P_LAC_354</td>
            <td>319</td>
        </tr>
        <tr>
            <td>P_BEH_340</td>
            <td>337</td>
        </tr>
        <tr>
            <td>P_GRC_341</td>
            <td>322</td>
        </tr>
        <tr>
            <td>P_LEG_335</td>
            <td>323</td>
        </tr>
        <tr>
            <td>P_MOE_339</td>
            <td>350</td>
        </tr>
        <tr>
            <td>P_VAL_329</td>
            <td>280</td>
        </tr>
        <tr>
            <td>P_CAV_333</td>
            <td>345</td>
        </tr>
        <tr>
            <td>P_BOC_342</td>
            <td>299</td>
        </tr>
    </tbody>
</table>
<span style="font-style:italic;text-align:center;">Truncated to <a href="https://jupysql.ploomber.io/en/latest/api/configuration.html#displaylimit">displaylimit</a> of 10.</span>


