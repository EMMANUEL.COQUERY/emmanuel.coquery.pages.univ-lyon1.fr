# Corrigé contrôle 6


```python
import pandas
```

1. Charger les données du fichier excel


```python
df = pandas.read_excel("Donnees_M2_RD.xlsx")
```

2. Afficher la plus grande valeur de la colonne RT.


```python
df["RT"].max()
```




    103152



3. Placer dans une DataFrame df_1 les essais dont la réponse est 1.
Placer dans une DataFrame df_2 les essais dont la réponse est 2.



```python
df_1 = pandas.DataFrame(df[df["Response"] == 1])
df_2 = pandas.DataFrame(df[df["Response"] == 2])
```

4. Ajouter à la DataFrame df_1 une colonne booléenne « juste » indiquant si la réponse est correcte (c’est-à-dire si Dist_A est plus petit que Dist_B).
Ajouter à la DataFrame df_2 une colonne booléenne « juste » indiquant si la réponse est correcte (c’est-à-dire si Dist_B est plus petit que Dist_A).


```python
df_1["juste"] = (df_1["Dist_A"] < df_1["Dist_B"])
df_2["juste"] = (df_2["Dist_A"] > df_2["Dist_B"])
```

5. Placer dans df_j l’assemblage des deux DataFrames df_1 et df_2.


```python
df_j = pandas.concat([df_1, df_2])
```

6.	Créer une Series compte_total comptant le nombre d’essais pour chaque Subject et chaque Response.
Créer une Series compte_juste comptant le nombre d’essais justes pour chaque Subject et chaque Response.


```python
compte_total = df_j.groupby(by=["Subject", "Response"])["RT"].count()
compte_juste = df_j[df_j["juste"]].groupby(by=["Subject", "Response"])["RT"].count()
```

7. Créer une Series taux_juste indiquant le pourcentage de réponses justes pour chaque Subject et chaque Response.


```python
taux_juste = (compte_juste / compte_total * 100).rename("taux_juste")
```


```python
taux_juste
```




    Subject    Response
    P_ADI_331  1           84.042553
               2           80.188679
    P_ALM_345  1           79.807692
               2           82.291667
    P_AMY_346  1           78.873239
               2           82.887701
    P_BAM_347  1           80.434783
               2           75.925926
    P_BEH_340  1           80.444444
               2           89.142857
    P_BLC_325  1           75.000000
               2           79.234973
    P_BLR_321  1           80.838323
               2           72.413793
    P_BOA_321  1           84.693878
               2           83.743842
    P_BOC_342  1           72.811060
               2           77.049180
    P_CAR_327  1           72.222222
               2           64.830508
    P_CAV_333  1           86.432161
               2           86.069652
    P_CON_336  1           79.820628
               2           87.570621
    P_GAM_338  1           85.643564
               2           86.363636
    P_GHM_334  1           82.323232
               2           81.683168
    P_GRC_341  1           80.500000
               2           80.500000
    P_GRF_322  1           75.418994
               2           70.909091
    P_LAC_354  1           82.162162
               2           77.674419
    P_LEG_335  1           78.873239
               2           82.887701
    P_MOE_339  1           85.714286
               2           89.473684
    P_ROS_336  1           82.464455
               2           86.243386
    P_SOA_337  1           73.542601
               2           79.661017
    P_TAI_343  1           83.720930
               2           75.438596
    P_VAL_329  1           68.867925
               2           71.276596
    P_VAR_330  1           83.139535
               2           75.000000
    Name: taux_juste, dtype: float64


