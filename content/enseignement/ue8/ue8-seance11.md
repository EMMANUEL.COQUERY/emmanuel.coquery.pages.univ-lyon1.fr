# Notebook seance 11


```python
%load_ext sql
%config SqlMagic.dsn_filename = "connections.ini"
%sql --section bd-pedago
%sql --connections
import pandas
```


<span style="None">Connecting to &#x27;bd-pedago&#x27;</span>


Donner le nombre d’essais pour chaque combinaison de “Name_A” et “Name_B”.


```sql
%%sql
SELECT "Name_A", "Name_B", count(*) as nombre_essais
FROM df
GROUP BY "Name_A", "Name_B"
```


<span style="None">Running query in &#x27;bd-pedago&#x27;</span>



<span style="color: green">20 rows affected.</span>





<table>
    <thead>
        <tr>
            <th>Name_A</th>
            <th>Name_B</th>
            <th>nombre_essais</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>4</td>
            <td>1</td>
            <td>480</td>
        </tr>
        <tr>
            <td>4</td>
            <td>3</td>
            <td>479</td>
        </tr>
        <tr>
            <td>2</td>
            <td>0</td>
            <td>479</td>
        </tr>
        <tr>
            <td>1</td>
            <td>3</td>
            <td>480</td>
        </tr>
        <tr>
            <td>0</td>
            <td>2</td>
            <td>479</td>
        </tr>
        <tr>
            <td>0</td>
            <td>3</td>
            <td>480</td>
        </tr>
        <tr>
            <td>3</td>
            <td>0</td>
            <td>480</td>
        </tr>
        <tr>
            <td>0</td>
            <td>4</td>
            <td>480</td>
        </tr>
        <tr>
            <td>3</td>
            <td>2</td>
            <td>480</td>
        </tr>
        <tr>
            <td>4</td>
            <td>2</td>
            <td>480</td>
        </tr>
    </tbody>
</table>
<span style="font-style:italic;text-align:center;">Truncated to <a href="https://jupysql.ploomber.io/en/latest/api/configuration.html#displaylimit">displaylimit</a> of 10.</span>



Donner le meilleur temps de réaction moyen par “Subject”, c’est-à-dire la meilleure moyenne de temps de réaction, chaque moyenne étant calculée pour un “Subject” donné.


```sql
%%sql
SELECT min(rt_moy) as plus_petit_rt_moy
FROM (SELECT avg("RT") as rt_moy
      FROM df
      GROUP BY "Subject") moyennes
```


<span style="None">Running query in &#x27;bd-pedago&#x27;</span>



<span style="color: green">1 rows affected.</span>





<table>
    <thead>
        <tr>
            <th>plus_petit_rt_moy</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>8352.7425000000000000</td>
        </tr>
    </tbody>
</table>




```sql
%%sql
WITH moyennes AS 
(SELECT avg("RT") as rt_moy
 FROM df
 GROUP BY "Subject")

SELECT min(rt_moy) as plus_petit_rt_moy
FROM moyennes
```


<span style="None">Running query in &#x27;bd-pedago&#x27;</span>



<span style="color: green">1 rows affected.</span>





<table>
    <thead>
        <tr>
            <th>plus_petit_rt_moy</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>8352.7425000000000000</td>
        </tr>
    </tbody>
</table>



Créer une DataFrame avec les données suivantes:

| Space | Dist | DistM |
| ----- | ---- | ----- |
| I | 1 | 0.2 |
| I | 2 | 0.3 |
| I | 3 | 0.4 |
| I | 4 | 0.6 |
| I | 5 | 0.8 |
| E | 1 | 2 |
| E | 2 | 3 |
| E | 3 | 4 |
| E | 4 | 6 |
| E | 5 | 8 |


```python
list(range(1,6))*2
```




    [1, 2, 3, 4, 5, 1, 2, 3, 4, 5]




```python
ie = ["I"]*5 + ["E"]*5
dists = list(range(1,6))*2
df_m = pandas.DataFrame({"Space": ie, "Dist": dists, "DistM": [0.2, 0.3, 0.4, 0.6, 0.8, 2, 3, 4, 6, 8]})
df_m
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Space</th>
      <th>Dist</th>
      <th>DistM</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>I</td>
      <td>1</td>
      <td>0.2</td>
    </tr>
    <tr>
      <th>1</th>
      <td>I</td>
      <td>2</td>
      <td>0.3</td>
    </tr>
    <tr>
      <th>2</th>
      <td>I</td>
      <td>3</td>
      <td>0.4</td>
    </tr>
    <tr>
      <th>3</th>
      <td>I</td>
      <td>4</td>
      <td>0.6</td>
    </tr>
    <tr>
      <th>4</th>
      <td>I</td>
      <td>5</td>
      <td>0.8</td>
    </tr>
    <tr>
      <th>5</th>
      <td>E</td>
      <td>1</td>
      <td>2.0</td>
    </tr>
    <tr>
      <th>6</th>
      <td>E</td>
      <td>2</td>
      <td>3.0</td>
    </tr>
    <tr>
      <th>7</th>
      <td>E</td>
      <td>3</td>
      <td>4.0</td>
    </tr>
    <tr>
      <th>8</th>
      <td>E</td>
      <td>4</td>
      <td>6.0</td>
    </tr>
    <tr>
      <th>9</th>
      <td>E</td>
      <td>5</td>
      <td>8.0</td>
    </tr>
  </tbody>
</table>
</div>




```python
%sql  --persist df_m
```


<span style="None">Running query in &#x27;bd-pedago&#x27;</span>



<span style="color: green">Success! Persisted df_m to the database.</span>



```sql
%%sql 
select * 
from df_m
```


<span style="None">Running query in &#x27;bd-pedago&#x27;</span>



<span style="color: green">10 rows affected.</span>





<table>
    <thead>
        <tr>
            <th>index</th>
            <th>Space</th>
            <th>Dist</th>
            <th>DistM</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>0</td>
            <td>I</td>
            <td>1</td>
            <td>0.2</td>
        </tr>
        <tr>
            <td>1</td>
            <td>I</td>
            <td>2</td>
            <td>0.3</td>
        </tr>
        <tr>
            <td>2</td>
            <td>I</td>
            <td>3</td>
            <td>0.4</td>
        </tr>
        <tr>
            <td>3</td>
            <td>I</td>
            <td>4</td>
            <td>0.6</td>
        </tr>
        <tr>
            <td>4</td>
            <td>I</td>
            <td>5</td>
            <td>0.8</td>
        </tr>
        <tr>
            <td>5</td>
            <td>E</td>
            <td>1</td>
            <td>2.0</td>
        </tr>
        <tr>
            <td>6</td>
            <td>E</td>
            <td>2</td>
            <td>3.0</td>
        </tr>
        <tr>
            <td>7</td>
            <td>E</td>
            <td>3</td>
            <td>4.0</td>
        </tr>
        <tr>
            <td>8</td>
            <td>E</td>
            <td>4</td>
            <td>6.0</td>
        </tr>
        <tr>
            <td>9</td>
            <td>E</td>
            <td>5</td>
            <td>8.0</td>
        </tr>
    </tbody>
</table>
<span style="font-style:italic;text-align:center;">Truncated to <a href="https://jupysql.ploomber.io/en/latest/api/configuration.html#displaylimit">displaylimit</a> of 10.</span>



Écrire une requête qui ajoute des colonnes “Dist_A_m” et “Dist_B_m” en effectuant deux jointures avec la DataFrame ci-dessus. 


```sql
%%sql
SELECT df.*, dfa."DistM" AS "Dist_A_m", dfb."DistM" AS "Dist_B_m"
FROM df, df_m AS dfa, df_m AS dfb
WHERE df."Space" = dfa."Space"
  AND df."Space" = dfb."Space"
  AND df."Dist_A" = dfa."Dist"
  AND df."Dist_B" = dfb."Dist"
ORDER BY df.index
```


<span style="None">Running query in &#x27;bd-pedago&#x27;</span>



<span style="color: green">9594 rows affected.</span>





<table>
    <thead>
        <tr>
            <th>index</th>
            <th>Subject</th>
            <th>Name_A</th>
            <th>Name_B</th>
            <th>Dist_A</th>
            <th>Dist_B</th>
            <th>Mode</th>
            <th>Space</th>
            <th>Side</th>
            <th>Response</th>
            <th>RT</th>
            <th>Dist_A_m</th>
            <th>Dist_B_m</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>0</td>
            <td>P_ADI_331</td>
            <td>0</td>
            <td>2</td>
            <td>2</td>
            <td>4</td>
            <td>Dic</td>
            <td>E</td>
            <td>D</td>
            <td>2</td>
            <td>18865</td>
            <td>3.0</td>
            <td>6.0</td>
        </tr>
        <tr>
            <td>1</td>
            <td>P_ADI_331</td>
            <td>1</td>
            <td>4</td>
            <td>4</td>
            <td>1</td>
            <td>Dic</td>
            <td>E</td>
            <td>D</td>
            <td>2</td>
            <td>13157</td>
            <td>6.0</td>
            <td>2.0</td>
        </tr>
        <tr>
            <td>2</td>
            <td>P_ADI_331</td>
            <td>4</td>
            <td>3</td>
            <td>3</td>
            <td>2</td>
            <td>Dic</td>
            <td>E</td>
            <td>D</td>
            <td>1</td>
            <td>11628</td>
            <td>4.0</td>
            <td>3.0</td>
        </tr>
        <tr>
            <td>3</td>
            <td>P_ADI_331</td>
            <td>2</td>
            <td>4</td>
            <td>4</td>
            <td>1</td>
            <td>Dic</td>
            <td>E</td>
            <td>D</td>
            <td>1</td>
            <td>10068</td>
            <td>6.0</td>
            <td>2.0</td>
        </tr>
        <tr>
            <td>4</td>
            <td>P_ADI_331</td>
            <td>1</td>
            <td>2</td>
            <td>2</td>
            <td>4</td>
            <td>Dic</td>
            <td>E</td>
            <td>D</td>
            <td>1</td>
            <td>11801</td>
            <td>3.0</td>
            <td>6.0</td>
        </tr>
        <tr>
            <td>5</td>
            <td>P_ADI_331</td>
            <td>2</td>
            <td>1</td>
            <td>2</td>
            <td>3</td>
            <td>Dic</td>
            <td>E</td>
            <td>D</td>
            <td>2</td>
            <td>12117</td>
            <td>3.0</td>
            <td>4.0</td>
        </tr>
        <tr>
            <td>6</td>
            <td>P_ADI_331</td>
            <td>2</td>
            <td>1</td>
            <td>3</td>
            <td>4</td>
            <td>Dic</td>
            <td>E</td>
            <td>D</td>
            <td>1</td>
            <td>16347</td>
            <td>4.0</td>
            <td>6.0</td>
        </tr>
        <tr>
            <td>7</td>
            <td>P_ADI_331</td>
            <td>0</td>
            <td>3</td>
            <td>2</td>
            <td>4</td>
            <td>Dic</td>
            <td>E</td>
            <td>D</td>
            <td>1</td>
            <td>13237</td>
            <td>3.0</td>
            <td>6.0</td>
        </tr>
        <tr>
            <td>8</td>
            <td>P_ADI_331</td>
            <td>2</td>
            <td>0</td>
            <td>4</td>
            <td>2</td>
            <td>Dic</td>
            <td>E</td>
            <td>D</td>
            <td>2</td>
            <td>12589</td>
            <td>6.0</td>
            <td>3.0</td>
        </tr>
        <tr>
            <td>9</td>
            <td>P_ADI_331</td>
            <td>2</td>
            <td>1</td>
            <td>4</td>
            <td>2</td>
            <td>Dic</td>
            <td>E</td>
            <td>D</td>
            <td>2</td>
            <td>10973</td>
            <td>6.0</td>
            <td>3.0</td>
        </tr>
    </tbody>
</table>
<span style="font-style:italic;text-align:center;">Truncated to <a href="https://jupysql.ploomber.io/en/latest/api/configuration.html#displaylimit">displaylimit</a> of 10.</span>


