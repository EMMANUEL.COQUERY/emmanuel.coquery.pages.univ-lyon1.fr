# Séance 5


```python
import pandas
```

## Préparation des données

Lecture du fichier excel, puis merge avec les bonnes distances en mètres.


```python
df = pandas.read_excel("Donnees_M2_RD.xlsx")
dist_i_m = pandas.DataFrame(
    { "Dist_I_m": [ 0.2, 0.3, 0.4, 0.6, 0.8 ] },
    index = [1,2,3,4,5]
)
dist_e_m = pandas.DataFrame(
    { "Dist_E_m": [ 2, 3, 4, 6, 8 ] },
    index = [1,2,3,4,5]
)
df_e = df[df['Space'] == 'E']
df1 = pandas.merge(df_e, dist_e_m, left_on='Dist_A', right_index=True)
df2 = df1.rename(columns={ 'Dist_E_m': 'Dist_A_m' })
df3 = pandas.merge(df2, dist_e_m, left_on='Dist_B', right_index=True)
df_e_m = df3.rename(columns={ 'Dist_E_m': 'Dist_B_m' })
df_i = df[df['Space'] == 'I']
df1 = pandas.merge(df_i, dist_i_m, left_on='Dist_A', right_index=True)
df2 = df1.rename(columns={ 'Dist_I_m': 'Dist_A_m' })
df3 = pandas.merge(df2, dist_i_m, left_on='Dist_B', right_index=True)
df_i_m = df3.rename(columns={ 'Dist_I_m': 'Dist_B_m' })
df_m = pandas.concat([df_e_m,df_i_m])
df_m
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Subject</th>
      <th>Name_A</th>
      <th>Name_B</th>
      <th>Dist_A</th>
      <th>Dist_B</th>
      <th>Mode</th>
      <th>Space</th>
      <th>Side</th>
      <th>Response</th>
      <th>RT</th>
      <th>Dist_A_m</th>
      <th>Dist_B_m</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>P_ADI_331</td>
      <td>0</td>
      <td>2</td>
      <td>2</td>
      <td>4</td>
      <td>Dic</td>
      <td>E</td>
      <td>D</td>
      <td>2</td>
      <td>18865</td>
      <td>3.0</td>
      <td>6.0</td>
    </tr>
    <tr>
      <th>4</th>
      <td>P_ADI_331</td>
      <td>1</td>
      <td>2</td>
      <td>2</td>
      <td>4</td>
      <td>Dic</td>
      <td>E</td>
      <td>D</td>
      <td>1</td>
      <td>11801</td>
      <td>3.0</td>
      <td>6.0</td>
    </tr>
    <tr>
      <th>7</th>
      <td>P_ADI_331</td>
      <td>0</td>
      <td>3</td>
      <td>2</td>
      <td>4</td>
      <td>Dic</td>
      <td>E</td>
      <td>D</td>
      <td>1</td>
      <td>13237</td>
      <td>3.0</td>
      <td>6.0</td>
    </tr>
    <tr>
      <th>36</th>
      <td>P_ADI_331</td>
      <td>3</td>
      <td>1</td>
      <td>2</td>
      <td>4</td>
      <td>Dic</td>
      <td>E</td>
      <td>D</td>
      <td>1</td>
      <td>9523</td>
      <td>3.0</td>
      <td>6.0</td>
    </tr>
    <tr>
      <th>51</th>
      <td>P_ADI_331</td>
      <td>2</td>
      <td>4</td>
      <td>2</td>
      <td>4</td>
      <td>Dio</td>
      <td>E</td>
      <td>G</td>
      <td>1</td>
      <td>11331</td>
      <td>3.0</td>
      <td>6.0</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>9498</th>
      <td>P_VAR_330</td>
      <td>4</td>
      <td>2</td>
      <td>4</td>
      <td>3</td>
      <td>Dic</td>
      <td>I</td>
      <td>G</td>
      <td>2</td>
      <td>4811</td>
      <td>0.6</td>
      <td>0.4</td>
    </tr>
    <tr>
      <th>9503</th>
      <td>P_VAR_330</td>
      <td>3</td>
      <td>0</td>
      <td>4</td>
      <td>3</td>
      <td>Dic</td>
      <td>I</td>
      <td>G</td>
      <td>2</td>
      <td>6628</td>
      <td>0.6</td>
      <td>0.4</td>
    </tr>
    <tr>
      <th>9526</th>
      <td>P_VAR_330</td>
      <td>0</td>
      <td>1</td>
      <td>4</td>
      <td>3</td>
      <td>Dic</td>
      <td>I</td>
      <td>G</td>
      <td>2</td>
      <td>5458</td>
      <td>0.6</td>
      <td>0.4</td>
    </tr>
    <tr>
      <th>9535</th>
      <td>P_VAR_330</td>
      <td>2</td>
      <td>4</td>
      <td>4</td>
      <td>3</td>
      <td>Dic</td>
      <td>I</td>
      <td>G</td>
      <td>1</td>
      <td>8956</td>
      <td>0.6</td>
      <td>0.4</td>
    </tr>
    <tr>
      <th>9546</th>
      <td>P_VAR_330</td>
      <td>2</td>
      <td>3</td>
      <td>4</td>
      <td>3</td>
      <td>Dio</td>
      <td>I</td>
      <td>D</td>
      <td>2</td>
      <td>13525</td>
      <td>0.6</td>
      <td>0.4</td>
    </tr>
  </tbody>
</table>
<p>9594 rows × 12 columns</p>
</div>



## Une première courbe

- Créer une Series contenant le temps de réaction moyen en fonction de la distance A en mètres
- Tracer la courbe correspondant à la série
- Créer la série et tracer la courbe correspondante pour la distance B.


```python
rt_by_distam = df_m.groupby(by='Dist_A_m')['RT'].mean()
rt_by_distam.plot()
rt_by_distbm = df_m.groupby(by='Dist_B_m')['RT'].mean()
rt_by_distbm.plot()
```




    <AxesSubplot:xlabel='Dist_B_m'>




    
![png](output_5_1.png)
    


## Box plot temps de réaction vs différence de distance

- Ajouter au besoin une colonne pour la différence entre Dist_A et Dist_B
- Faire un box plot du temps de réaction selon la différence de distance
- Faire un deuxième box plot en se limitant aux essais réussis et un troisième pour les échecs



```python
df_m['diff_dist'] = df_m['Dist_A'] - df_m['Dist_B']
df_m.plot.box(by='diff_dist', column='RT', showfliers=False)
```




    RT    AxesSubplot(0.125,0.125;0.775x0.755)
    dtype: object




    
![png](output_7_1.png)
    


Les essais réussis


```python
reussites = (
    ((df_m['Response'] == 1) & (df_m['Dist_A'] < df_m['Dist_B'])) 
  | ((df_m['Response'] == 2) & (df_m['Dist_A'] > df_m['Dist_B']))
)
df_m["reussit"] = reussites
df_m
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Subject</th>
      <th>Name_A</th>
      <th>Name_B</th>
      <th>Dist_A</th>
      <th>Dist_B</th>
      <th>Mode</th>
      <th>Space</th>
      <th>Side</th>
      <th>Response</th>
      <th>RT</th>
      <th>Dist_A_m</th>
      <th>Dist_B_m</th>
      <th>diff_dist</th>
      <th>reussit</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>P_ADI_331</td>
      <td>0</td>
      <td>2</td>
      <td>2</td>
      <td>4</td>
      <td>Dic</td>
      <td>E</td>
      <td>D</td>
      <td>2</td>
      <td>18865</td>
      <td>3.0</td>
      <td>6.0</td>
      <td>-2</td>
      <td>False</td>
    </tr>
    <tr>
      <th>4</th>
      <td>P_ADI_331</td>
      <td>1</td>
      <td>2</td>
      <td>2</td>
      <td>4</td>
      <td>Dic</td>
      <td>E</td>
      <td>D</td>
      <td>1</td>
      <td>11801</td>
      <td>3.0</td>
      <td>6.0</td>
      <td>-2</td>
      <td>True</td>
    </tr>
    <tr>
      <th>7</th>
      <td>P_ADI_331</td>
      <td>0</td>
      <td>3</td>
      <td>2</td>
      <td>4</td>
      <td>Dic</td>
      <td>E</td>
      <td>D</td>
      <td>1</td>
      <td>13237</td>
      <td>3.0</td>
      <td>6.0</td>
      <td>-2</td>
      <td>True</td>
    </tr>
    <tr>
      <th>36</th>
      <td>P_ADI_331</td>
      <td>3</td>
      <td>1</td>
      <td>2</td>
      <td>4</td>
      <td>Dic</td>
      <td>E</td>
      <td>D</td>
      <td>1</td>
      <td>9523</td>
      <td>3.0</td>
      <td>6.0</td>
      <td>-2</td>
      <td>True</td>
    </tr>
    <tr>
      <th>51</th>
      <td>P_ADI_331</td>
      <td>2</td>
      <td>4</td>
      <td>2</td>
      <td>4</td>
      <td>Dio</td>
      <td>E</td>
      <td>G</td>
      <td>1</td>
      <td>11331</td>
      <td>3.0</td>
      <td>6.0</td>
      <td>-2</td>
      <td>True</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>9498</th>
      <td>P_VAR_330</td>
      <td>4</td>
      <td>2</td>
      <td>4</td>
      <td>3</td>
      <td>Dic</td>
      <td>I</td>
      <td>G</td>
      <td>2</td>
      <td>4811</td>
      <td>0.6</td>
      <td>0.4</td>
      <td>1</td>
      <td>True</td>
    </tr>
    <tr>
      <th>9503</th>
      <td>P_VAR_330</td>
      <td>3</td>
      <td>0</td>
      <td>4</td>
      <td>3</td>
      <td>Dic</td>
      <td>I</td>
      <td>G</td>
      <td>2</td>
      <td>6628</td>
      <td>0.6</td>
      <td>0.4</td>
      <td>1</td>
      <td>True</td>
    </tr>
    <tr>
      <th>9526</th>
      <td>P_VAR_330</td>
      <td>0</td>
      <td>1</td>
      <td>4</td>
      <td>3</td>
      <td>Dic</td>
      <td>I</td>
      <td>G</td>
      <td>2</td>
      <td>5458</td>
      <td>0.6</td>
      <td>0.4</td>
      <td>1</td>
      <td>True</td>
    </tr>
    <tr>
      <th>9535</th>
      <td>P_VAR_330</td>
      <td>2</td>
      <td>4</td>
      <td>4</td>
      <td>3</td>
      <td>Dic</td>
      <td>I</td>
      <td>G</td>
      <td>1</td>
      <td>8956</td>
      <td>0.6</td>
      <td>0.4</td>
      <td>1</td>
      <td>False</td>
    </tr>
    <tr>
      <th>9546</th>
      <td>P_VAR_330</td>
      <td>2</td>
      <td>3</td>
      <td>4</td>
      <td>3</td>
      <td>Dio</td>
      <td>I</td>
      <td>D</td>
      <td>2</td>
      <td>13525</td>
      <td>0.6</td>
      <td>0.4</td>
      <td>1</td>
      <td>True</td>
    </tr>
  </tbody>
</table>
<p>9594 rows × 14 columns</p>
</div>




```python
df_m[df_m['reussit']].plot.box(by='diff_dist', column='RT', showfliers=False)
df_m[~df_m['reussit']].plot.box(by='diff_dist', column='RT', showfliers=False)
```




    RT    AxesSubplot(0.125,0.125;0.775x0.755)
    dtype: object




    
![png](output_10_1.png)
    



    
![png](output_10_2.png)
    

