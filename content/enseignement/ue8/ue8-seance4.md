# Séance 4


```python
import pandas
df = pandas.read_excel("Donnees_M2_RD.xlsx")
```


```python
dist_i_m = pandas.DataFrame(
    { "Dist_I_m": [ 0.2, 0.3, 0.4, 0.6, 0.8 ] },
    index = [1,2,3,4,5]
)
dist_i_m
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Dist_I_m</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>1</th>
      <td>0.2</td>
    </tr>
    <tr>
      <th>2</th>
      <td>0.3</td>
    </tr>
    <tr>
      <th>3</th>
      <td>0.4</td>
    </tr>
    <tr>
      <th>4</th>
      <td>0.6</td>
    </tr>
    <tr>
      <th>5</th>
      <td>0.8</td>
    </tr>
  </tbody>
</table>
</div>




```python
dist_e_m = pandas.DataFrame(
    { "Dist_E_m": [ 2, 3, 4, 6, 8 ] },
    index = [1,2,3,4,5]
)
dist_e_m
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Dist_E_m</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>1</th>
      <td>2</td>
    </tr>
    <tr>
      <th>2</th>
      <td>3</td>
    </tr>
    <tr>
      <th>3</th>
      <td>4</td>
    </tr>
    <tr>
      <th>4</th>
      <td>6</td>
    </tr>
    <tr>
      <th>5</th>
      <td>8</td>
    </tr>
  </tbody>
</table>
</div>




```python
df_e = df[df['Space'] == 'E']
df1 = pandas.merge(df_e, dist_e_m, left_on='Dist_A', right_index=True)
df2 = df1.rename(columns={ 'Dist_E_m': 'Dist_A_m' })
df3 = pandas.merge(df2, dist_e_m, left_on='Dist_B', right_index=True)
df_e_m = df3.rename(columns={ 'Dist_E_m': 'Dist_B_m' })
df_e_m
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Subject</th>
      <th>Name_A</th>
      <th>Name_B</th>
      <th>Dist_A</th>
      <th>Dist_B</th>
      <th>Mode</th>
      <th>Space</th>
      <th>Side</th>
      <th>Response</th>
      <th>RT</th>
      <th>Dist_A_m</th>
      <th>Dist_B_m</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>P_ADI_331</td>
      <td>0</td>
      <td>2</td>
      <td>2</td>
      <td>4</td>
      <td>Dic</td>
      <td>E</td>
      <td>D</td>
      <td>2</td>
      <td>18865</td>
      <td>3</td>
      <td>6</td>
    </tr>
    <tr>
      <th>4</th>
      <td>P_ADI_331</td>
      <td>1</td>
      <td>2</td>
      <td>2</td>
      <td>4</td>
      <td>Dic</td>
      <td>E</td>
      <td>D</td>
      <td>1</td>
      <td>11801</td>
      <td>3</td>
      <td>6</td>
    </tr>
    <tr>
      <th>7</th>
      <td>P_ADI_331</td>
      <td>0</td>
      <td>3</td>
      <td>2</td>
      <td>4</td>
      <td>Dic</td>
      <td>E</td>
      <td>D</td>
      <td>1</td>
      <td>13237</td>
      <td>3</td>
      <td>6</td>
    </tr>
    <tr>
      <th>36</th>
      <td>P_ADI_331</td>
      <td>3</td>
      <td>1</td>
      <td>2</td>
      <td>4</td>
      <td>Dic</td>
      <td>E</td>
      <td>D</td>
      <td>1</td>
      <td>9523</td>
      <td>3</td>
      <td>6</td>
    </tr>
    <tr>
      <th>51</th>
      <td>P_ADI_331</td>
      <td>2</td>
      <td>4</td>
      <td>2</td>
      <td>4</td>
      <td>Dio</td>
      <td>E</td>
      <td>G</td>
      <td>1</td>
      <td>11331</td>
      <td>3</td>
      <td>6</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>9364</th>
      <td>P_VAR_330</td>
      <td>1</td>
      <td>0</td>
      <td>5</td>
      <td>2</td>
      <td>Dic</td>
      <td>E</td>
      <td>D</td>
      <td>2</td>
      <td>8296</td>
      <td>8</td>
      <td>3</td>
    </tr>
    <tr>
      <th>9374</th>
      <td>P_VAR_330</td>
      <td>2</td>
      <td>1</td>
      <td>5</td>
      <td>2</td>
      <td>Dic</td>
      <td>E</td>
      <td>D</td>
      <td>2</td>
      <td>12260</td>
      <td>8</td>
      <td>3</td>
    </tr>
    <tr>
      <th>9412</th>
      <td>P_VAR_330</td>
      <td>4</td>
      <td>3</td>
      <td>5</td>
      <td>2</td>
      <td>Dio</td>
      <td>E</td>
      <td>D</td>
      <td>2</td>
      <td>9414</td>
      <td>8</td>
      <td>3</td>
    </tr>
    <tr>
      <th>9433</th>
      <td>P_VAR_330</td>
      <td>3</td>
      <td>1</td>
      <td>5</td>
      <td>2</td>
      <td>Dio</td>
      <td>E</td>
      <td>D</td>
      <td>2</td>
      <td>16334</td>
      <td>8</td>
      <td>3</td>
    </tr>
    <tr>
      <th>9437</th>
      <td>P_VAR_330</td>
      <td>1</td>
      <td>2</td>
      <td>5</td>
      <td>2</td>
      <td>Dio</td>
      <td>E</td>
      <td>D</td>
      <td>1</td>
      <td>8802</td>
      <td>8</td>
      <td>3</td>
    </tr>
  </tbody>
</table>
<p>4797 rows × 12 columns</p>
</div>




```python
df_i = df[df['Space'] == 'I']
df1 = pandas.merge(df_i, dist_i_m, left_on='Dist_A', right_index=True)
df2 = df1.rename(columns={ 'Dist_I_m': 'Dist_A_m' })
df3 = pandas.merge(df2, dist_i_m, left_on='Dist_B', right_index=True)
df_i_m = df3.rename(columns={ 'Dist_I_m': 'Dist_B_m' })
df_i_m
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Subject</th>
      <th>Name_A</th>
      <th>Name_B</th>
      <th>Dist_A</th>
      <th>Dist_B</th>
      <th>Mode</th>
      <th>Space</th>
      <th>Side</th>
      <th>Response</th>
      <th>RT</th>
      <th>Dist_A_m</th>
      <th>Dist_B_m</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>150</th>
      <td>P_ADI_331</td>
      <td>2</td>
      <td>3</td>
      <td>3</td>
      <td>4</td>
      <td>Dic</td>
      <td>I</td>
      <td>D</td>
      <td>2</td>
      <td>14608</td>
      <td>0.4</td>
      <td>0.6</td>
    </tr>
    <tr>
      <th>180</th>
      <td>P_ADI_331</td>
      <td>4</td>
      <td>3</td>
      <td>3</td>
      <td>4</td>
      <td>Dic</td>
      <td>I</td>
      <td>D</td>
      <td>1</td>
      <td>9086</td>
      <td>0.4</td>
      <td>0.6</td>
    </tr>
    <tr>
      <th>207</th>
      <td>P_ADI_331</td>
      <td>4</td>
      <td>2</td>
      <td>3</td>
      <td>4</td>
      <td>Dic</td>
      <td>I</td>
      <td>G</td>
      <td>1</td>
      <td>7251</td>
      <td>0.4</td>
      <td>0.6</td>
    </tr>
    <tr>
      <th>213</th>
      <td>P_ADI_331</td>
      <td>0</td>
      <td>4</td>
      <td>3</td>
      <td>4</td>
      <td>Dic</td>
      <td>I</td>
      <td>G</td>
      <td>1</td>
      <td>9298</td>
      <td>0.4</td>
      <td>0.6</td>
    </tr>
    <tr>
      <th>250</th>
      <td>P_ADI_331</td>
      <td>1</td>
      <td>0</td>
      <td>3</td>
      <td>4</td>
      <td>Dio</td>
      <td>I</td>
      <td>D</td>
      <td>1</td>
      <td>11246</td>
      <td>0.4</td>
      <td>0.6</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>9498</th>
      <td>P_VAR_330</td>
      <td>4</td>
      <td>2</td>
      <td>4</td>
      <td>3</td>
      <td>Dic</td>
      <td>I</td>
      <td>G</td>
      <td>2</td>
      <td>4811</td>
      <td>0.6</td>
      <td>0.4</td>
    </tr>
    <tr>
      <th>9503</th>
      <td>P_VAR_330</td>
      <td>3</td>
      <td>0</td>
      <td>4</td>
      <td>3</td>
      <td>Dic</td>
      <td>I</td>
      <td>G</td>
      <td>2</td>
      <td>6628</td>
      <td>0.6</td>
      <td>0.4</td>
    </tr>
    <tr>
      <th>9526</th>
      <td>P_VAR_330</td>
      <td>0</td>
      <td>1</td>
      <td>4</td>
      <td>3</td>
      <td>Dic</td>
      <td>I</td>
      <td>G</td>
      <td>2</td>
      <td>5458</td>
      <td>0.6</td>
      <td>0.4</td>
    </tr>
    <tr>
      <th>9535</th>
      <td>P_VAR_330</td>
      <td>2</td>
      <td>4</td>
      <td>4</td>
      <td>3</td>
      <td>Dic</td>
      <td>I</td>
      <td>G</td>
      <td>1</td>
      <td>8956</td>
      <td>0.6</td>
      <td>0.4</td>
    </tr>
    <tr>
      <th>9546</th>
      <td>P_VAR_330</td>
      <td>2</td>
      <td>3</td>
      <td>4</td>
      <td>3</td>
      <td>Dio</td>
      <td>I</td>
      <td>D</td>
      <td>2</td>
      <td>13525</td>
      <td>0.6</td>
      <td>0.4</td>
    </tr>
  </tbody>
</table>
<p>4797 rows × 12 columns</p>
</div>




```python
df_m = pandas.concat([df_e_m,df_i_m])
df_m
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Subject</th>
      <th>Name_A</th>
      <th>Name_B</th>
      <th>Dist_A</th>
      <th>Dist_B</th>
      <th>Mode</th>
      <th>Space</th>
      <th>Side</th>
      <th>Response</th>
      <th>RT</th>
      <th>Dist_A_m</th>
      <th>Dist_B_m</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>P_ADI_331</td>
      <td>0</td>
      <td>2</td>
      <td>2</td>
      <td>4</td>
      <td>Dic</td>
      <td>E</td>
      <td>D</td>
      <td>2</td>
      <td>18865</td>
      <td>3.0</td>
      <td>6.0</td>
    </tr>
    <tr>
      <th>4</th>
      <td>P_ADI_331</td>
      <td>1</td>
      <td>2</td>
      <td>2</td>
      <td>4</td>
      <td>Dic</td>
      <td>E</td>
      <td>D</td>
      <td>1</td>
      <td>11801</td>
      <td>3.0</td>
      <td>6.0</td>
    </tr>
    <tr>
      <th>7</th>
      <td>P_ADI_331</td>
      <td>0</td>
      <td>3</td>
      <td>2</td>
      <td>4</td>
      <td>Dic</td>
      <td>E</td>
      <td>D</td>
      <td>1</td>
      <td>13237</td>
      <td>3.0</td>
      <td>6.0</td>
    </tr>
    <tr>
      <th>36</th>
      <td>P_ADI_331</td>
      <td>3</td>
      <td>1</td>
      <td>2</td>
      <td>4</td>
      <td>Dic</td>
      <td>E</td>
      <td>D</td>
      <td>1</td>
      <td>9523</td>
      <td>3.0</td>
      <td>6.0</td>
    </tr>
    <tr>
      <th>51</th>
      <td>P_ADI_331</td>
      <td>2</td>
      <td>4</td>
      <td>2</td>
      <td>4</td>
      <td>Dio</td>
      <td>E</td>
      <td>G</td>
      <td>1</td>
      <td>11331</td>
      <td>3.0</td>
      <td>6.0</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>9498</th>
      <td>P_VAR_330</td>
      <td>4</td>
      <td>2</td>
      <td>4</td>
      <td>3</td>
      <td>Dic</td>
      <td>I</td>
      <td>G</td>
      <td>2</td>
      <td>4811</td>
      <td>0.6</td>
      <td>0.4</td>
    </tr>
    <tr>
      <th>9503</th>
      <td>P_VAR_330</td>
      <td>3</td>
      <td>0</td>
      <td>4</td>
      <td>3</td>
      <td>Dic</td>
      <td>I</td>
      <td>G</td>
      <td>2</td>
      <td>6628</td>
      <td>0.6</td>
      <td>0.4</td>
    </tr>
    <tr>
      <th>9526</th>
      <td>P_VAR_330</td>
      <td>0</td>
      <td>1</td>
      <td>4</td>
      <td>3</td>
      <td>Dic</td>
      <td>I</td>
      <td>G</td>
      <td>2</td>
      <td>5458</td>
      <td>0.6</td>
      <td>0.4</td>
    </tr>
    <tr>
      <th>9535</th>
      <td>P_VAR_330</td>
      <td>2</td>
      <td>4</td>
      <td>4</td>
      <td>3</td>
      <td>Dic</td>
      <td>I</td>
      <td>G</td>
      <td>1</td>
      <td>8956</td>
      <td>0.6</td>
      <td>0.4</td>
    </tr>
    <tr>
      <th>9546</th>
      <td>P_VAR_330</td>
      <td>2</td>
      <td>3</td>
      <td>4</td>
      <td>3</td>
      <td>Dio</td>
      <td>I</td>
      <td>D</td>
      <td>2</td>
      <td>13525</td>
      <td>0.6</td>
      <td>0.4</td>
    </tr>
  </tbody>
</table>
<p>9594 rows × 12 columns</p>
</div>



df.groupby(by='Subject').max()


```python

    Donner le temps de réaction maximal par sujet.
    Donner le temps de réaction moyen par premier nom (Name_a).
    Donner le temps de réaction moyen selon la distance en mètres du premier prénom prononcé.
    Donner le sujet ayant le plus faible temps de réaction moyen (utiliser .idxmin()).

```


```python
df.groupby(by='Subject').max()['RT']
df.groupby(by='Name_A').mean()['RT']
df_m.groupby(by='Dist_A_m').mean()['RT']
df.groupby(by='Subject').mean()['RT'].idxmin()
```




    'P_CON_336'


