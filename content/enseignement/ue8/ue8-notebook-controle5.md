# Notebook Contrôle 5


```python
import pandas
```

1. Charger les données du fichier Donnees_M2_RD.xlsx dans une DataFrame placée dans une variable df.


```python
df = pandas.read_excel('Donnees_M2_RD.xlsx')
```

2. Afficher les valeurs de la colonne Subject de df. On ne veut pas de doublon dans cet affichage.


```python
df["Subject"].drop_duplicates()
```




    0       P_ADI_331
    400     P_ALM_345
    800     P_AMY_346
    1200    P_BAM_347
    1600    P_BEH_340
    2000    P_BLC_325
    2399    P_BLR_321
    2798    P_BOA_321
    3197    P_BOC_342
    3597    P_CAR_327
    3995    P_CAV_333
    4395    P_CON_336
    4795    P_GAM_338
    5195    P_GHM_334
    5595    P_GRC_341
    5995    P_GRF_322
    6394    P_LAC_354
    6794    P_LEG_335
    7194    P_MOE_339
    7594    P_ROS_336
    7994    P_SOA_337
    8394    P_TAI_343
    8794    P_VAL_329
    9194    P_VAR_330
    Name: Subject, dtype: object



3. Placer dans une DataFrame df_d les essais de la valeur pour Side est D.\
   Placer dans une DataFrame df_g les essais de la valeur pour Side est G.


```python
df_d = df[df["Side"] == 'D']
df_g = df[df["Side"] == 'G']
```

4. Créer une Series s_rt_d contenant la moyenne des RT par Subject pour les essais ayant Side qui vaut D.\
   Créer une deuxième Series s_rt_g sur le même principe, mais pour les essais ayant Side qui vaut G.


```python
s_rt_d = df_d.groupby(by="Subject")["RT"].mean()
s_rt_g = df_g.groupby(by="Subject")["RT"].mean()
```

5. Indiquer les Subjects dont le RT pour les essais ayant Side valant D est en moyenne inférieur à celui pour les essais ayant Side valant G.


```python
# Assembler les deux Series en une DataFrame
s_rt = pandas.concat([s_rt_d.rename("RT_D"), s_rt_g.rename("RT_G")], axis=1, join="inner")
s_rt[s_rt["RT_D"] < s_rt["RT_G"]]
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>RT_D</th>
      <th>RT_G</th>
    </tr>
    <tr>
      <th>Subject</th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>P_BLC_325</th>
      <td>11311.120603</td>
      <td>11421.280</td>
    </tr>
    <tr>
      <th>P_GHM_334</th>
      <td>10762.345000</td>
      <td>11679.255</td>
    </tr>
    <tr>
      <th>P_SOA_337</th>
      <td>9864.230000</td>
      <td>10238.120</td>
    </tr>
  </tbody>
</table>
</div>



6. Ranger dans la variable df_min_max une DataFrame contenant les valeurs minimales et maximales de RT pour chaque Subject.


```python
s_rt_min = df.groupby(by="Subject")["RT"].min().rename("min")
s_rt_max = df.groupby(by="Subject")["RT"].max().rename("max")
# une autre manière de faire pour assembler 2 Series en une DataFrame
df_min_max = pandas.DataFrame({"min": s_rt_min, "max":s_rt_max}) 
df_min_max
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>min</th>
      <th>max</th>
    </tr>
    <tr>
      <th>Subject</th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>P_ADI_331</th>
      <td>4851</td>
      <td>18865</td>
    </tr>
    <tr>
      <th>P_ALM_345</th>
      <td>3181</td>
      <td>51807</td>
    </tr>
    <tr>
      <th>P_AMY_346</th>
      <td>2703</td>
      <td>29020</td>
    </tr>
    <tr>
      <th>P_BAM_347</th>
      <td>3500</td>
      <td>24332</td>
    </tr>
    <tr>
      <th>P_BEH_340</th>
      <td>4582</td>
      <td>50600</td>
    </tr>
    <tr>
      <th>P_BLC_325</th>
      <td>4728</td>
      <td>32306</td>
    </tr>
    <tr>
      <th>P_BLR_321</th>
      <td>2885</td>
      <td>44985</td>
    </tr>
    <tr>
      <th>P_BOA_321</th>
      <td>8414</td>
      <td>89205</td>
    </tr>
    <tr>
      <th>P_BOC_342</th>
      <td>2851</td>
      <td>103152</td>
    </tr>
    <tr>
      <th>P_CAR_327</th>
      <td>4088</td>
      <td>65254</td>
    </tr>
    <tr>
      <th>P_CAV_333</th>
      <td>3105</td>
      <td>36168</td>
    </tr>
    <tr>
      <th>P_CON_336</th>
      <td>3180</td>
      <td>37280</td>
    </tr>
    <tr>
      <th>P_GAM_338</th>
      <td>4703</td>
      <td>52262</td>
    </tr>
    <tr>
      <th>P_GHM_334</th>
      <td>3238</td>
      <td>33643</td>
    </tr>
    <tr>
      <th>P_GRC_341</th>
      <td>3191</td>
      <td>38528</td>
    </tr>
    <tr>
      <th>P_GRF_322</th>
      <td>6248</td>
      <td>42200</td>
    </tr>
    <tr>
      <th>P_LAC_354</th>
      <td>5909</td>
      <td>93540</td>
    </tr>
    <tr>
      <th>P_LEG_335</th>
      <td>3936</td>
      <td>49580</td>
    </tr>
    <tr>
      <th>P_MOE_339</th>
      <td>3585</td>
      <td>34250</td>
    </tr>
    <tr>
      <th>P_ROS_336</th>
      <td>3996</td>
      <td>54405</td>
    </tr>
    <tr>
      <th>P_SOA_337</th>
      <td>4842</td>
      <td>31123</td>
    </tr>
    <tr>
      <th>P_TAI_343</th>
      <td>6486</td>
      <td>62619</td>
    </tr>
    <tr>
      <th>P_VAL_329</th>
      <td>5112</td>
      <td>80916</td>
    </tr>
    <tr>
      <th>P_VAR_330</th>
      <td>2918</td>
      <td>80729</td>
    </tr>
  </tbody>
</table>
</div>


