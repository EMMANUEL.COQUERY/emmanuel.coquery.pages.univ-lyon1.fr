# Notebook Séance 2 

UE8: Introduction à l'analyse de données


```python
print("Bonjour")
```

    Bonjour



```python
sous_total = 5 + 3 * 12
sous_total
```




    41



Un morceau de code pour illustrer l'usage des variables


```python
prix_bombon = 0.10
prix_croissant = 1.0
prix_pain = 1.2
total = prix_bombon * 8
total = total + prix_croissant * 5
total = total + prix_pain * 2
print("Je dois "+str(total)+" euros")
```

    Je dois 8.2 euros



```python
import pandas
```


```python
df = pandas.read_excel("Donnees_M2_RD.xlsx")
df
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Subject</th>
      <th>Name_A</th>
      <th>Name_B</th>
      <th>Dist_A</th>
      <th>Dist_B</th>
      <th>Mode</th>
      <th>Space</th>
      <th>Side</th>
      <th>Response</th>
      <th>RT</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>P_ADI_331</td>
      <td>0</td>
      <td>2</td>
      <td>2</td>
      <td>4</td>
      <td>Dic</td>
      <td>E</td>
      <td>D</td>
      <td>2</td>
      <td>18865</td>
    </tr>
    <tr>
      <th>1</th>
      <td>P_ADI_331</td>
      <td>1</td>
      <td>4</td>
      <td>4</td>
      <td>1</td>
      <td>Dic</td>
      <td>E</td>
      <td>D</td>
      <td>2</td>
      <td>13157</td>
    </tr>
    <tr>
      <th>2</th>
      <td>P_ADI_331</td>
      <td>4</td>
      <td>3</td>
      <td>3</td>
      <td>2</td>
      <td>Dic</td>
      <td>E</td>
      <td>D</td>
      <td>1</td>
      <td>11628</td>
    </tr>
    <tr>
      <th>3</th>
      <td>P_ADI_331</td>
      <td>2</td>
      <td>4</td>
      <td>4</td>
      <td>1</td>
      <td>Dic</td>
      <td>E</td>
      <td>D</td>
      <td>1</td>
      <td>10068</td>
    </tr>
    <tr>
      <th>4</th>
      <td>P_ADI_331</td>
      <td>1</td>
      <td>2</td>
      <td>2</td>
      <td>4</td>
      <td>Dic</td>
      <td>E</td>
      <td>D</td>
      <td>1</td>
      <td>11801</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>9589</th>
      <td>P_VAR_330</td>
      <td>0</td>
      <td>1</td>
      <td>3</td>
      <td>5</td>
      <td>Dio</td>
      <td>I</td>
      <td>D</td>
      <td>1</td>
      <td>7626</td>
    </tr>
    <tr>
      <th>9590</th>
      <td>P_VAR_330</td>
      <td>3</td>
      <td>2</td>
      <td>5</td>
      <td>1</td>
      <td>Dio</td>
      <td>I</td>
      <td>D</td>
      <td>2</td>
      <td>6349</td>
    </tr>
    <tr>
      <th>9591</th>
      <td>P_VAR_330</td>
      <td>2</td>
      <td>0</td>
      <td>4</td>
      <td>2</td>
      <td>Dio</td>
      <td>I</td>
      <td>D</td>
      <td>2</td>
      <td>9031</td>
    </tr>
    <tr>
      <th>9592</th>
      <td>P_VAR_330</td>
      <td>0</td>
      <td>2</td>
      <td>2</td>
      <td>1</td>
      <td>Dio</td>
      <td>I</td>
      <td>D</td>
      <td>2</td>
      <td>16323</td>
    </tr>
    <tr>
      <th>9593</th>
      <td>P_VAR_330</td>
      <td>0</td>
      <td>3</td>
      <td>5</td>
      <td>1</td>
      <td>Dio</td>
      <td>I</td>
      <td>D</td>
      <td>2</td>
      <td>10139</td>
    </tr>
  </tbody>
</table>
<p>9594 rows × 10 columns</p>
</div>




```python
rt = df['RT']
```


```python
rt
```




    0       18865
    1       13157
    2       11628
    3       10068
    4       11801
            ...  
    9589     7626
    9590     6349
    9591     9031
    9592    16323
    9593    10139
    Name: RT, Length: 9594, dtype: int64




```python
subjects = df['Subject']
subjects.drop_duplicates()
```




    0       P_ADI_331
    400     P_ALM_345
    800     P_AMY_346
    1200    P_BAM_347
    1600    P_BEH_340
    2000    P_BLC_325
    2399    P_BLR_321
    2798    P_BOA_321
    3197    P_BOC_342
    3597    P_CAR_327
    3995    P_CAV_333
    4395    P_CON_336
    4795    P_GAM_338
    5195    P_GHM_334
    5595    P_GRC_341
    5995    P_GRF_322
    6394    P_LAC_354
    6794    P_LEG_335
    7194    P_MOE_339
    7594    P_ROS_336
    7994    P_SOA_337
    8394    P_TAI_343
    8794    P_VAL_329
    9194    P_VAR_330
    Name: Subject, dtype: object




```python
rt.min()
```




    2703




```python
df.min()
```




    Subject     P_ADI_331
    Name_A              0
    Name_B              0
    Dist_A              1
    Dist_B              1
    Mode              Dic
    Space               E
    Side                D
    Response            1
    RT               2703
    dtype: object




```python
qs = [ (q+1)/10 for q in range(9) ]
print(qs)
rt.quantile(qs)
```

    [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9]





    0.1     6223.0
    0.2     7427.2
    0.3     8370.9
    0.4     9315.0
    0.5    10262.5
    0.6    11304.6
    0.7    12761.3
    0.8    15054.2
    0.9    19872.0
    Name: RT, dtype: float64




```python
rt
```




    0       18865
    1       13157
    2       11628
    3       10068
    4       11801
            ...  
    9589     7626
    9590     6349
    9591     9031
    9592    16323
    9593    10139
    Name: RT, Length: 9594, dtype: int64




```python
filtre = (rt < 11000)
df[filtre]
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Subject</th>
      <th>Name_A</th>
      <th>Name_B</th>
      <th>Dist_A</th>
      <th>Dist_B</th>
      <th>Mode</th>
      <th>Space</th>
      <th>Side</th>
      <th>Response</th>
      <th>RT</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>3</th>
      <td>P_ADI_331</td>
      <td>2</td>
      <td>4</td>
      <td>4</td>
      <td>1</td>
      <td>Dic</td>
      <td>E</td>
      <td>D</td>
      <td>1</td>
      <td>10068</td>
    </tr>
    <tr>
      <th>9</th>
      <td>P_ADI_331</td>
      <td>2</td>
      <td>1</td>
      <td>4</td>
      <td>2</td>
      <td>Dic</td>
      <td>E</td>
      <td>D</td>
      <td>2</td>
      <td>10973</td>
    </tr>
    <tr>
      <th>15</th>
      <td>P_ADI_331</td>
      <td>1</td>
      <td>3</td>
      <td>4</td>
      <td>3</td>
      <td>Dic</td>
      <td>E</td>
      <td>D</td>
      <td>2</td>
      <td>10828</td>
    </tr>
    <tr>
      <th>17</th>
      <td>P_ADI_331</td>
      <td>3</td>
      <td>0</td>
      <td>3</td>
      <td>4</td>
      <td>Dic</td>
      <td>E</td>
      <td>D</td>
      <td>1</td>
      <td>10438</td>
    </tr>
    <tr>
      <th>18</th>
      <td>P_ADI_331</td>
      <td>4</td>
      <td>2</td>
      <td>3</td>
      <td>2</td>
      <td>Dic</td>
      <td>E</td>
      <td>D</td>
      <td>2</td>
      <td>10932</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>9588</th>
      <td>P_VAR_330</td>
      <td>0</td>
      <td>3</td>
      <td>3</td>
      <td>1</td>
      <td>Dio</td>
      <td>I</td>
      <td>D</td>
      <td>2</td>
      <td>6153</td>
    </tr>
    <tr>
      <th>9589</th>
      <td>P_VAR_330</td>
      <td>0</td>
      <td>1</td>
      <td>3</td>
      <td>5</td>
      <td>Dio</td>
      <td>I</td>
      <td>D</td>
      <td>1</td>
      <td>7626</td>
    </tr>
    <tr>
      <th>9590</th>
      <td>P_VAR_330</td>
      <td>3</td>
      <td>2</td>
      <td>5</td>
      <td>1</td>
      <td>Dio</td>
      <td>I</td>
      <td>D</td>
      <td>2</td>
      <td>6349</td>
    </tr>
    <tr>
      <th>9591</th>
      <td>P_VAR_330</td>
      <td>2</td>
      <td>0</td>
      <td>4</td>
      <td>2</td>
      <td>Dio</td>
      <td>I</td>
      <td>D</td>
      <td>2</td>
      <td>9031</td>
    </tr>
    <tr>
      <th>9593</th>
      <td>P_VAR_330</td>
      <td>0</td>
      <td>3</td>
      <td>5</td>
      <td>1</td>
      <td>Dio</td>
      <td>I</td>
      <td>D</td>
      <td>2</td>
      <td>10139</td>
    </tr>
  </tbody>
</table>
<p>5505 rows × 10 columns</p>
</div>




```python
df[(df['RT'] < 11000)]
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Subject</th>
      <th>Name_A</th>
      <th>Name_B</th>
      <th>Dist_A</th>
      <th>Dist_B</th>
      <th>Mode</th>
      <th>Space</th>
      <th>Side</th>
      <th>Response</th>
      <th>RT</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>3</th>
      <td>P_ADI_331</td>
      <td>2</td>
      <td>4</td>
      <td>4</td>
      <td>1</td>
      <td>Dic</td>
      <td>E</td>
      <td>D</td>
      <td>1</td>
      <td>10068</td>
    </tr>
    <tr>
      <th>9</th>
      <td>P_ADI_331</td>
      <td>2</td>
      <td>1</td>
      <td>4</td>
      <td>2</td>
      <td>Dic</td>
      <td>E</td>
      <td>D</td>
      <td>2</td>
      <td>10973</td>
    </tr>
    <tr>
      <th>15</th>
      <td>P_ADI_331</td>
      <td>1</td>
      <td>3</td>
      <td>4</td>
      <td>3</td>
      <td>Dic</td>
      <td>E</td>
      <td>D</td>
      <td>2</td>
      <td>10828</td>
    </tr>
    <tr>
      <th>17</th>
      <td>P_ADI_331</td>
      <td>3</td>
      <td>0</td>
      <td>3</td>
      <td>4</td>
      <td>Dic</td>
      <td>E</td>
      <td>D</td>
      <td>1</td>
      <td>10438</td>
    </tr>
    <tr>
      <th>18</th>
      <td>P_ADI_331</td>
      <td>4</td>
      <td>2</td>
      <td>3</td>
      <td>2</td>
      <td>Dic</td>
      <td>E</td>
      <td>D</td>
      <td>2</td>
      <td>10932</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>9588</th>
      <td>P_VAR_330</td>
      <td>0</td>
      <td>3</td>
      <td>3</td>
      <td>1</td>
      <td>Dio</td>
      <td>I</td>
      <td>D</td>
      <td>2</td>
      <td>6153</td>
    </tr>
    <tr>
      <th>9589</th>
      <td>P_VAR_330</td>
      <td>0</td>
      <td>1</td>
      <td>3</td>
      <td>5</td>
      <td>Dio</td>
      <td>I</td>
      <td>D</td>
      <td>1</td>
      <td>7626</td>
    </tr>
    <tr>
      <th>9590</th>
      <td>P_VAR_330</td>
      <td>3</td>
      <td>2</td>
      <td>5</td>
      <td>1</td>
      <td>Dio</td>
      <td>I</td>
      <td>D</td>
      <td>2</td>
      <td>6349</td>
    </tr>
    <tr>
      <th>9591</th>
      <td>P_VAR_330</td>
      <td>2</td>
      <td>0</td>
      <td>4</td>
      <td>2</td>
      <td>Dio</td>
      <td>I</td>
      <td>D</td>
      <td>2</td>
      <td>9031</td>
    </tr>
    <tr>
      <th>9593</th>
      <td>P_VAR_330</td>
      <td>0</td>
      <td>3</td>
      <td>5</td>
      <td>1</td>
      <td>Dio</td>
      <td>I</td>
      <td>D</td>
      <td>2</td>
      <td>10139</td>
    </tr>
  </tbody>
</table>
<p>5505 rows × 10 columns</p>
</div>




```python
df
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Subject</th>
      <th>Name_A</th>
      <th>Name_B</th>
      <th>Dist_A</th>
      <th>Dist_B</th>
      <th>Mode</th>
      <th>Space</th>
      <th>Side</th>
      <th>Response</th>
      <th>RT</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>P_ADI_331</td>
      <td>0</td>
      <td>2</td>
      <td>2</td>
      <td>4</td>
      <td>Dic</td>
      <td>E</td>
      <td>D</td>
      <td>2</td>
      <td>18865</td>
    </tr>
    <tr>
      <th>1</th>
      <td>P_ADI_331</td>
      <td>1</td>
      <td>4</td>
      <td>4</td>
      <td>1</td>
      <td>Dic</td>
      <td>E</td>
      <td>D</td>
      <td>2</td>
      <td>13157</td>
    </tr>
    <tr>
      <th>2</th>
      <td>P_ADI_331</td>
      <td>4</td>
      <td>3</td>
      <td>3</td>
      <td>2</td>
      <td>Dic</td>
      <td>E</td>
      <td>D</td>
      <td>1</td>
      <td>11628</td>
    </tr>
    <tr>
      <th>3</th>
      <td>P_ADI_331</td>
      <td>2</td>
      <td>4</td>
      <td>4</td>
      <td>1</td>
      <td>Dic</td>
      <td>E</td>
      <td>D</td>
      <td>1</td>
      <td>10068</td>
    </tr>
    <tr>
      <th>4</th>
      <td>P_ADI_331</td>
      <td>1</td>
      <td>2</td>
      <td>2</td>
      <td>4</td>
      <td>Dic</td>
      <td>E</td>
      <td>D</td>
      <td>1</td>
      <td>11801</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>9589</th>
      <td>P_VAR_330</td>
      <td>0</td>
      <td>1</td>
      <td>3</td>
      <td>5</td>
      <td>Dio</td>
      <td>I</td>
      <td>D</td>
      <td>1</td>
      <td>7626</td>
    </tr>
    <tr>
      <th>9590</th>
      <td>P_VAR_330</td>
      <td>3</td>
      <td>2</td>
      <td>5</td>
      <td>1</td>
      <td>Dio</td>
      <td>I</td>
      <td>D</td>
      <td>2</td>
      <td>6349</td>
    </tr>
    <tr>
      <th>9591</th>
      <td>P_VAR_330</td>
      <td>2</td>
      <td>0</td>
      <td>4</td>
      <td>2</td>
      <td>Dio</td>
      <td>I</td>
      <td>D</td>
      <td>2</td>
      <td>9031</td>
    </tr>
    <tr>
      <th>9592</th>
      <td>P_VAR_330</td>
      <td>0</td>
      <td>2</td>
      <td>2</td>
      <td>1</td>
      <td>Dio</td>
      <td>I</td>
      <td>D</td>
      <td>2</td>
      <td>16323</td>
    </tr>
    <tr>
      <th>9593</th>
      <td>P_VAR_330</td>
      <td>0</td>
      <td>3</td>
      <td>5</td>
      <td>1</td>
      <td>Dio</td>
      <td>I</td>
      <td>D</td>
      <td>2</td>
      <td>10139</td>
    </tr>
  </tbody>
</table>
<p>9594 rows × 10 columns</p>
</div>




```python
df[(df['Name_A'] == 0) & (df['RT'] > 14000)]
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Subject</th>
      <th>Name_A</th>
      <th>Name_B</th>
      <th>Dist_A</th>
      <th>Dist_B</th>
      <th>Mode</th>
      <th>Space</th>
      <th>Side</th>
      <th>Response</th>
      <th>RT</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>P_ADI_331</td>
      <td>0</td>
      <td>2</td>
      <td>2</td>
      <td>4</td>
      <td>Dic</td>
      <td>E</td>
      <td>D</td>
      <td>2</td>
      <td>18865</td>
    </tr>
    <tr>
      <th>13</th>
      <td>P_ADI_331</td>
      <td>0</td>
      <td>3</td>
      <td>4</td>
      <td>3</td>
      <td>Dic</td>
      <td>E</td>
      <td>D</td>
      <td>2</td>
      <td>14330</td>
    </tr>
    <tr>
      <th>45</th>
      <td>P_ADI_331</td>
      <td>0</td>
      <td>1</td>
      <td>1</td>
      <td>2</td>
      <td>Dic</td>
      <td>E</td>
      <td>D</td>
      <td>2</td>
      <td>16246</td>
    </tr>
    <tr>
      <th>118</th>
      <td>P_ADI_331</td>
      <td>0</td>
      <td>3</td>
      <td>4</td>
      <td>5</td>
      <td>Dio</td>
      <td>E</td>
      <td>D</td>
      <td>2</td>
      <td>14368</td>
    </tr>
    <tr>
      <th>372</th>
      <td>P_ADI_331</td>
      <td>0</td>
      <td>2</td>
      <td>3</td>
      <td>2</td>
      <td>Dic</td>
      <td>E</td>
      <td>G</td>
      <td>2</td>
      <td>14043</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>9549</th>
      <td>P_VAR_330</td>
      <td>0</td>
      <td>3</td>
      <td>5</td>
      <td>4</td>
      <td>Dio</td>
      <td>I</td>
      <td>D</td>
      <td>2</td>
      <td>65102</td>
    </tr>
    <tr>
      <th>9583</th>
      <td>P_VAR_330</td>
      <td>0</td>
      <td>4</td>
      <td>4</td>
      <td>5</td>
      <td>Dio</td>
      <td>I</td>
      <td>D</td>
      <td>2</td>
      <td>45627</td>
    </tr>
    <tr>
      <th>9585</th>
      <td>P_VAR_330</td>
      <td>0</td>
      <td>3</td>
      <td>2</td>
      <td>3</td>
      <td>Dio</td>
      <td>I</td>
      <td>D</td>
      <td>2</td>
      <td>16671</td>
    </tr>
    <tr>
      <th>9586</th>
      <td>P_VAR_330</td>
      <td>0</td>
      <td>1</td>
      <td>2</td>
      <td>3</td>
      <td>Dio</td>
      <td>I</td>
      <td>D</td>
      <td>1</td>
      <td>18002</td>
    </tr>
    <tr>
      <th>9592</th>
      <td>P_VAR_330</td>
      <td>0</td>
      <td>2</td>
      <td>2</td>
      <td>1</td>
      <td>Dio</td>
      <td>I</td>
      <td>D</td>
      <td>2</td>
      <td>16323</td>
    </tr>
  </tbody>
</table>
<p>480 rows × 10 columns</p>
</div>




```python
df[(df['Name_A'] == 0) & (df['RT'] > 14000)]['RT'].mean()
```




    21400.008333333335


