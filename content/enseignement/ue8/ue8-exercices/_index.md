---
# Course title, summary, and position.
linktitle: "UE8: Exercices"
summary: "Exercices pour le cours d'introduction à l'analyse de données pour le master Santé de l'ISTR"
weight: 1

# Page metadata.
title: "UE8: Exercices"
draft: false # Is this a draft? true/false
toc: true # Show table of contents? true/false
type: docs # Do not modify.
authors: ["admin"]
tags: ["code"]
featured: true

# Add menu entry to sidebar.
# - name: Declare this menu item as a parent with ID `name`.
# - weight: Position of link in menu.
menu:
  ue8:
    name: Exercices
    weight: 2
---

[Page de l'UE]({{< ref "/enseignement/ue8" >}})

[Notebook de corrigés]({{< ref "exercices/" >}})

## Exercices Pandas

Dans ces exercices, ont travaillera sur les données du fichier `Donnees_M2_RD.xlsx`

### Projections et filtres simples

Donner les valeurs de la colonne "RT"

Donner les différentes valeurs possible pour la colonne "Subject"

Filtrer la dataframe pour ne garder que les lignes dont le "RT" est plus petit que 10 000.

### Combiner les projections et les filtres

Indiquer la valeur de "RT" pour les essais en mode "Dic".

Créer une Series indiquant pour chaque essai si la réponse est juste ou non.
Une réponse est juste di la distance A est inférieure à la distance B et la réponse est 1 ou si la distance A est supérieure à la distance B et la réponse est 2.

Indiquer les "Suject" qui ont répondu en moins de 4 000 ("RT" inférieur à 4 000)

### Agrégations globales

Donner la moyenne de "RT".

Donner le plus grand "RT" pour le "Subject" "P_ROS_336"

Donner l'écart-type de "RT" pour les essais réussis (utiliser `.std()`)

### Création de colonnes

Ajouter une colonne "nombre" valant 1

Ajouter une colonne "reussi" qui indique si l'essai est réussi.

Ajouter une colonne "nombre_reussis" qui vaut 1 si l'essai est réussi et 0 sinon.

### Groupes simples

Donner la moyenne du temps de réaction ("RT") pour chaque "Subject".

Donner le nombre d'essais pour chaque "Subject" en utilisant `.count()`.

Donner le nombre d'essais pour chaque "Subject" en utilisant `.sum()` et la colonne "nombre".

Donner le nombre d'essais réussis pour chaque "Subject" en utilisant un filtre puis `.count()`.

Donner le nombre d'essais réussis pour chaque "Subject" en utilisant `.sum()` et la colonne "nombre_reussis".

### Groupes à plusieurs attributs

Donner le nombre d'essais pour chaque combinaison de "Name_A" et "Name_B".

Donner le temps de réaction ("RT") maximal pour chaque combinaison de "Subject" et "Space".

### Aggrégations multiples

Donner le meilleur temps de réaction moyen par "Subject", c'est-à-dire la meilleure moyenne de temps de réaction, chaque moyenne étant calculée pour un "Subject" donné.

### Jointures

Créer une DataFrame avec les données suivantes:

| Space | Dist | DistM |
| ----- | ---- | ----- |
| I | 1 | 0.2 |
| I | 2 | 0.3 |
| I | 3 | 0.4 |
| I | 4 | 0.6 |
| I | 5 | 0.8 |
| E | 1 | 2 |
| E | 2 | 3 |
| E | 3 | 4 |
| E | 4 | 6 |
| E | 5 | 8 |

Créer une DataFrame (ou ajouter à la DataFrame des essais) des colonnes "Dist_A_m" et "Dist_B_m" en effectuant deux jointures avec la DataFrame ci-dessus.
Il pourra être utile de renommer la colonne "DistM", en "Dist_A_m" avant la première jointure, puis en "Dist_B_m" avant la deuxième jointure.
