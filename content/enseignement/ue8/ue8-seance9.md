# Notebook de la séance 9
Connexion à la base de donnée et chargement de Pandas


```python
%load_ext sql
%config SqlMagic.dsn_filename = "connections.ini"
%sql --section bd-pedago
%sql --connections
import pandas
```

<span style="None">Connecting to &#x27;bd-pedago&#x27;</span>


Chargement des données excel en base en passant par une dataframe df


```python
df = pandas.read_excel("Donnees_M2_RD.xlsx")
```


```python
%sql --persist-replace df
```


<span style="None">Running query in &#x27;bd-pedago&#x27;</span>



<span style="color: green">Success! Persisted df to the database.</span>


Récupérer les données de toute la table


```sql
%%sql
SELECT *
FROM df
```


<span style="None">Running query in &#x27;bd-pedago&#x27;</span>



<span style="color: green">9594 rows affected.</span>





<table>
    <thead>
        <tr>
            <th>index</th>
            <th>Subject</th>
            <th>Name_A</th>
            <th>Name_B</th>
            <th>Dist_A</th>
            <th>Dist_B</th>
            <th>Mode</th>
            <th>Space</th>
            <th>Side</th>
            <th>Response</th>
            <th>RT</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>0</td>
            <td>P_ADI_331</td>
            <td>0</td>
            <td>2</td>
            <td>2</td>
            <td>4</td>
            <td>Dic</td>
            <td>E</td>
            <td>D</td>
            <td>2</td>
            <td>18865</td>
        </tr>
        <tr>
            <td>1</td>
            <td>P_ADI_331</td>
            <td>1</td>
            <td>4</td>
            <td>4</td>
            <td>1</td>
            <td>Dic</td>
            <td>E</td>
            <td>D</td>
            <td>2</td>
            <td>13157</td>
        </tr>
        <tr>
            <td>2</td>
            <td>P_ADI_331</td>
            <td>4</td>
            <td>3</td>
            <td>3</td>
            <td>2</td>
            <td>Dic</td>
            <td>E</td>
            <td>D</td>
            <td>1</td>
            <td>11628</td>
        </tr>
        <tr>
            <td>3</td>
            <td>P_ADI_331</td>
            <td>2</td>
            <td>4</td>
            <td>4</td>
            <td>1</td>
            <td>Dic</td>
            <td>E</td>
            <td>D</td>
            <td>1</td>
            <td>10068</td>
        </tr>
        <tr>
            <td>4</td>
            <td>P_ADI_331</td>
            <td>1</td>
            <td>2</td>
            <td>2</td>
            <td>4</td>
            <td>Dic</td>
            <td>E</td>
            <td>D</td>
            <td>1</td>
            <td>11801</td>
        </tr>
        <tr>
            <td>5</td>
            <td>P_ADI_331</td>
            <td>2</td>
            <td>1</td>
            <td>2</td>
            <td>3</td>
            <td>Dic</td>
            <td>E</td>
            <td>D</td>
            <td>2</td>
            <td>12117</td>
        </tr>
        <tr>
            <td>6</td>
            <td>P_ADI_331</td>
            <td>2</td>
            <td>1</td>
            <td>3</td>
            <td>4</td>
            <td>Dic</td>
            <td>E</td>
            <td>D</td>
            <td>1</td>
            <td>16347</td>
        </tr>
        <tr>
            <td>7</td>
            <td>P_ADI_331</td>
            <td>0</td>
            <td>3</td>
            <td>2</td>
            <td>4</td>
            <td>Dic</td>
            <td>E</td>
            <td>D</td>
            <td>1</td>
            <td>13237</td>
        </tr>
        <tr>
            <td>8</td>
            <td>P_ADI_331</td>
            <td>2</td>
            <td>0</td>
            <td>4</td>
            <td>2</td>
            <td>Dic</td>
            <td>E</td>
            <td>D</td>
            <td>2</td>
            <td>12589</td>
        </tr>
        <tr>
            <td>9</td>
            <td>P_ADI_331</td>
            <td>2</td>
            <td>1</td>
            <td>4</td>
            <td>2</td>
            <td>Dic</td>
            <td>E</td>
            <td>D</td>
            <td>2</td>
            <td>10973</td>
        </tr>
    </tbody>
</table>
<span style="font-style:italic;text-align:center;">Truncated to <a href="https://jupysql.ploomber.io/en/latest/api/configuration.html#displaylimit">displaylimit</a> of 10.</span>



Si on veut seulement 2 colonnes


```sql
%%sql
SELECT "Subject", "RT"
FROM df
```


<span style="None">Running query in &#x27;bd-pedago&#x27;</span>



<span style="color: green">9594 rows affected.</span>





<table>
    <thead>
        <tr>
            <th>Subject</th>
            <th>RT</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>P_ADI_331</td>
            <td>18865</td>
        </tr>
        <tr>
            <td>P_ADI_331</td>
            <td>13157</td>
        </tr>
        <tr>
            <td>P_ADI_331</td>
            <td>11628</td>
        </tr>
        <tr>
            <td>P_ADI_331</td>
            <td>10068</td>
        </tr>
        <tr>
            <td>P_ADI_331</td>
            <td>11801</td>
        </tr>
        <tr>
            <td>P_ADI_331</td>
            <td>12117</td>
        </tr>
        <tr>
            <td>P_ADI_331</td>
            <td>16347</td>
        </tr>
        <tr>
            <td>P_ADI_331</td>
            <td>13237</td>
        </tr>
        <tr>
            <td>P_ADI_331</td>
            <td>12589</td>
        </tr>
        <tr>
            <td>P_ADI_331</td>
            <td>10973</td>
        </tr>
    </tbody>
</table>
<span style="font-style:italic;text-align:center;">Truncated to <a href="https://jupysql.ploomber.io/en/latest/api/configuration.html#displaylimit">displaylimit</a> of 10.</span>



En filtrant des lignes


```sql
%%sql
SELECT *
FROM df
WHERE "Space" = 'E'
  AND "Name_A" <> 0
  AND "Name_B" = 1
```


<span style="None">Running query in &#x27;bd-pedago&#x27;</span>



<span style="color: green">720 rows affected.</span>





<table>
    <thead>
        <tr>
            <th>index</th>
            <th>Subject</th>
            <th>Name_A</th>
            <th>Name_B</th>
            <th>Dist_A</th>
            <th>Dist_B</th>
            <th>Mode</th>
            <th>Space</th>
            <th>Side</th>
            <th>Response</th>
            <th>RT</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>5</td>
            <td>P_ADI_331</td>
            <td>2</td>
            <td>1</td>
            <td>2</td>
            <td>3</td>
            <td>Dic</td>
            <td>E</td>
            <td>D</td>
            <td>2</td>
            <td>12117</td>
        </tr>
        <tr>
            <td>6</td>
            <td>P_ADI_331</td>
            <td>2</td>
            <td>1</td>
            <td>3</td>
            <td>4</td>
            <td>Dic</td>
            <td>E</td>
            <td>D</td>
            <td>1</td>
            <td>16347</td>
        </tr>
        <tr>
            <td>9</td>
            <td>P_ADI_331</td>
            <td>2</td>
            <td>1</td>
            <td>4</td>
            <td>2</td>
            <td>Dic</td>
            <td>E</td>
            <td>D</td>
            <td>2</td>
            <td>10973</td>
        </tr>
        <tr>
            <td>14</td>
            <td>P_ADI_331</td>
            <td>2</td>
            <td>1</td>
            <td>1</td>
            <td>3</td>
            <td>Dic</td>
            <td>E</td>
            <td>D</td>
            <td>1</td>
            <td>13386</td>
        </tr>
        <tr>
            <td>16</td>
            <td>P_ADI_331</td>
            <td>4</td>
            <td>1</td>
            <td>1</td>
            <td>5</td>
            <td>Dic</td>
            <td>E</td>
            <td>D</td>
            <td>1</td>
            <td>13876</td>
        </tr>
        <tr>
            <td>19</td>
            <td>P_ADI_331</td>
            <td>3</td>
            <td>1</td>
            <td>2</td>
            <td>3</td>
            <td>Dic</td>
            <td>E</td>
            <td>D</td>
            <td>2</td>
            <td>12041</td>
        </tr>
        <tr>
            <td>35</td>
            <td>P_ADI_331</td>
            <td>3</td>
            <td>1</td>
            <td>1</td>
            <td>4</td>
            <td>Dic</td>
            <td>E</td>
            <td>D</td>
            <td>1</td>
            <td>8414</td>
        </tr>
        <tr>
            <td>36</td>
            <td>P_ADI_331</td>
            <td>3</td>
            <td>1</td>
            <td>2</td>
            <td>4</td>
            <td>Dic</td>
            <td>E</td>
            <td>D</td>
            <td>1</td>
            <td>9523</td>
        </tr>
        <tr>
            <td>41</td>
            <td>P_ADI_331</td>
            <td>4</td>
            <td>1</td>
            <td>1</td>
            <td>4</td>
            <td>Dic</td>
            <td>E</td>
            <td>D</td>
            <td>1</td>
            <td>12265</td>
        </tr>
        <tr>
            <td>63</td>
            <td>P_ADI_331</td>
            <td>4</td>
            <td>1</td>
            <td>4</td>
            <td>2</td>
            <td>Dio</td>
            <td>E</td>
            <td>G</td>
            <td>2</td>
            <td>9596</td>
        </tr>
    </tbody>
</table>
<span style="font-style:italic;text-align:center;">Truncated to <a href="https://jupysql.ploomber.io/en/latest/api/configuration.html#displaylimit">displaylimit</a> of 10.</span>



En filtrant des lignes et des colonnes


```sql
%%sql
-- un commentaire
SELECT "Subject", "RT"
FROM df
WHERE "Space" = 'E'
  AND "Name_A" <> 0
  AND "Name_B" = 1
```


<span style="None">Running query in &#x27;bd-pedago&#x27;</span>



<span style="color: green">720 rows affected.</span>





<table>
    <thead>
        <tr>
            <th>Subject</th>
            <th>RT</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>P_ADI_331</td>
            <td>12117</td>
        </tr>
        <tr>
            <td>P_ADI_331</td>
            <td>16347</td>
        </tr>
        <tr>
            <td>P_ADI_331</td>
            <td>10973</td>
        </tr>
        <tr>
            <td>P_ADI_331</td>
            <td>13386</td>
        </tr>
        <tr>
            <td>P_ADI_331</td>
            <td>13876</td>
        </tr>
        <tr>
            <td>P_ADI_331</td>
            <td>12041</td>
        </tr>
        <tr>
            <td>P_ADI_331</td>
            <td>8414</td>
        </tr>
        <tr>
            <td>P_ADI_331</td>
            <td>9523</td>
        </tr>
        <tr>
            <td>P_ADI_331</td>
            <td>12265</td>
        </tr>
        <tr>
            <td>P_ADI_331</td>
            <td>9596</td>
        </tr>
    </tbody>
</table>
<span style="font-style:italic;text-align:center;">Truncated to <a href="https://jupysql.ploomber.io/en/latest/api/configuration.html#displaylimit">displaylimit</a> of 10.</span>



En calculant une nouvelle colonne dans le SELECT


```sql
%%sql
SELECT "Subject", "Dist_A" - "Dist_B" as "DiffDist"
FROM df
```


<span style="None">Running query in &#x27;bd-pedago&#x27;</span>



<span style="color: green">9594 rows affected.</span>





<table>
    <thead>
        <tr>
            <th>Subject</th>
            <th>DiffDist</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>P_ADI_331</td>
            <td>-2</td>
        </tr>
        <tr>
            <td>P_ADI_331</td>
            <td>3</td>
        </tr>
        <tr>
            <td>P_ADI_331</td>
            <td>1</td>
        </tr>
        <tr>
            <td>P_ADI_331</td>
            <td>3</td>
        </tr>
        <tr>
            <td>P_ADI_331</td>
            <td>-2</td>
        </tr>
        <tr>
            <td>P_ADI_331</td>
            <td>-1</td>
        </tr>
        <tr>
            <td>P_ADI_331</td>
            <td>-1</td>
        </tr>
        <tr>
            <td>P_ADI_331</td>
            <td>-2</td>
        </tr>
        <tr>
            <td>P_ADI_331</td>
            <td>2</td>
        </tr>
        <tr>
            <td>P_ADI_331</td>
            <td>2</td>
        </tr>
    </tbody>
</table>
<span style="font-style:italic;text-align:center;">Truncated to <a href="https://jupysql.ploomber.io/en/latest/api/configuration.html#displaylimit">displaylimit</a> of 10.</span>



On peut remarquer que la table df reste inchangée:


```sql
%%sql
SELECT *
FROM df
```


<span style="None">Running query in &#x27;bd-pedago&#x27;</span>



<span style="color: green">9594 rows affected.</span>





<table>
    <thead>
        <tr>
            <th>index</th>
            <th>Subject</th>
            <th>Name_A</th>
            <th>Name_B</th>
            <th>Dist_A</th>
            <th>Dist_B</th>
            <th>Mode</th>
            <th>Space</th>
            <th>Side</th>
            <th>Response</th>
            <th>RT</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>0</td>
            <td>P_ADI_331</td>
            <td>0</td>
            <td>2</td>
            <td>2</td>
            <td>4</td>
            <td>Dic</td>
            <td>E</td>
            <td>D</td>
            <td>2</td>
            <td>18865</td>
        </tr>
        <tr>
            <td>1</td>
            <td>P_ADI_331</td>
            <td>1</td>
            <td>4</td>
            <td>4</td>
            <td>1</td>
            <td>Dic</td>
            <td>E</td>
            <td>D</td>
            <td>2</td>
            <td>13157</td>
        </tr>
        <tr>
            <td>2</td>
            <td>P_ADI_331</td>
            <td>4</td>
            <td>3</td>
            <td>3</td>
            <td>2</td>
            <td>Dic</td>
            <td>E</td>
            <td>D</td>
            <td>1</td>
            <td>11628</td>
        </tr>
        <tr>
            <td>3</td>
            <td>P_ADI_331</td>
            <td>2</td>
            <td>4</td>
            <td>4</td>
            <td>1</td>
            <td>Dic</td>
            <td>E</td>
            <td>D</td>
            <td>1</td>
            <td>10068</td>
        </tr>
        <tr>
            <td>4</td>
            <td>P_ADI_331</td>
            <td>1</td>
            <td>2</td>
            <td>2</td>
            <td>4</td>
            <td>Dic</td>
            <td>E</td>
            <td>D</td>
            <td>1</td>
            <td>11801</td>
        </tr>
        <tr>
            <td>5</td>
            <td>P_ADI_331</td>
            <td>2</td>
            <td>1</td>
            <td>2</td>
            <td>3</td>
            <td>Dic</td>
            <td>E</td>
            <td>D</td>
            <td>2</td>
            <td>12117</td>
        </tr>
        <tr>
            <td>6</td>
            <td>P_ADI_331</td>
            <td>2</td>
            <td>1</td>
            <td>3</td>
            <td>4</td>
            <td>Dic</td>
            <td>E</td>
            <td>D</td>
            <td>1</td>
            <td>16347</td>
        </tr>
        <tr>
            <td>7</td>
            <td>P_ADI_331</td>
            <td>0</td>
            <td>3</td>
            <td>2</td>
            <td>4</td>
            <td>Dic</td>
            <td>E</td>
            <td>D</td>
            <td>1</td>
            <td>13237</td>
        </tr>
        <tr>
            <td>8</td>
            <td>P_ADI_331</td>
            <td>2</td>
            <td>0</td>
            <td>4</td>
            <td>2</td>
            <td>Dic</td>
            <td>E</td>
            <td>D</td>
            <td>2</td>
            <td>12589</td>
        </tr>
        <tr>
            <td>9</td>
            <td>P_ADI_331</td>
            <td>2</td>
            <td>1</td>
            <td>4</td>
            <td>2</td>
            <td>Dic</td>
            <td>E</td>
            <td>D</td>
            <td>2</td>
            <td>10973</td>
        </tr>
    </tbody>
</table>
<span style="font-style:italic;text-align:center;">Truncated to <a href="https://jupysql.ploomber.io/en/latest/api/configuration.html#displaylimit">displaylimit</a> of 10.</span>




```sql
%%sql
SELECT "Subject", "Space", avg("RT") as "moy_RT", min("RT") as min_RT
FROM df
WHERE "Mode" = 'Dic'
GROUP BY "Subject", "Space"
```


<span style="None">Running query in &#x27;bd-pedago&#x27;</span>



<span style="color: green">48 rows affected.</span>





<table>
    <thead>
        <tr>
            <th>Subject</th>
            <th>Space</th>
            <th>moy_RT</th>
            <th>min_rt</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>P_ADI_331</td>
            <td>E</td>
            <td>11341.280000000000</td>
            <td>6277</td>
        </tr>
        <tr>
            <td>P_ADI_331</td>
            <td>I</td>
            <td>9017.5200000000000000</td>
            <td>5832</td>
        </tr>
        <tr>
            <td>P_ALM_345</td>
            <td>E</td>
            <td>14881.910000000000</td>
            <td>4696</td>
        </tr>
        <tr>
            <td>P_ALM_345</td>
            <td>I</td>
            <td>11411.900000000000</td>
            <td>3978</td>
        </tr>
        <tr>
            <td>P_AMY_346</td>
            <td>E</td>
            <td>10409.870000000000</td>
            <td>4080</td>
        </tr>
        <tr>
            <td>P_AMY_346</td>
            <td>I</td>
            <td>9199.4200000000000000</td>
            <td>2703</td>
        </tr>
        <tr>
            <td>P_BAM_347</td>
            <td>E</td>
            <td>9476.2800000000000000</td>
            <td>4166</td>
        </tr>
        <tr>
            <td>P_BAM_347</td>
            <td>I</td>
            <td>9578.7300000000000000</td>
            <td>3510</td>
        </tr>
        <tr>
            <td>P_BEH_340</td>
            <td>E</td>
            <td>14479.700000000000</td>
            <td>7171</td>
        </tr>
        <tr>
            <td>P_BEH_340</td>
            <td>I</td>
            <td>12668.770000000000</td>
            <td>5676</td>
        </tr>
    </tbody>
</table>
<span style="font-style:italic;text-align:center;">Truncated to <a href="https://jupysql.ploomber.io/en/latest/api/configuration.html#displaylimit">displaylimit</a> of 10.</span>


