# Notebook Séance 3

UE8: Introduction à l'analyse de données


```python
import pandas
```


```python
df = pandas.read_excel("Donnees_M2_RD.xlsx")
df
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Subject</th>
      <th>Name_A</th>
      <th>Name_B</th>
      <th>Dist_A</th>
      <th>Dist_B</th>
      <th>Mode</th>
      <th>Space</th>
      <th>Side</th>
      <th>Response</th>
      <th>RT</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>P_ADI_331</td>
      <td>0</td>
      <td>2</td>
      <td>2</td>
      <td>4</td>
      <td>Dic</td>
      <td>E</td>
      <td>D</td>
      <td>2</td>
      <td>18865</td>
    </tr>
    <tr>
      <th>1</th>
      <td>P_ADI_331</td>
      <td>1</td>
      <td>4</td>
      <td>4</td>
      <td>1</td>
      <td>Dic</td>
      <td>E</td>
      <td>D</td>
      <td>2</td>
      <td>13157</td>
    </tr>
    <tr>
      <th>2</th>
      <td>P_ADI_331</td>
      <td>4</td>
      <td>3</td>
      <td>3</td>
      <td>2</td>
      <td>Dic</td>
      <td>E</td>
      <td>D</td>
      <td>1</td>
      <td>11628</td>
    </tr>
    <tr>
      <th>3</th>
      <td>P_ADI_331</td>
      <td>2</td>
      <td>4</td>
      <td>4</td>
      <td>1</td>
      <td>Dic</td>
      <td>E</td>
      <td>D</td>
      <td>1</td>
      <td>10068</td>
    </tr>
    <tr>
      <th>4</th>
      <td>P_ADI_331</td>
      <td>1</td>
      <td>2</td>
      <td>2</td>
      <td>4</td>
      <td>Dic</td>
      <td>E</td>
      <td>D</td>
      <td>1</td>
      <td>11801</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>9589</th>
      <td>P_VAR_330</td>
      <td>0</td>
      <td>1</td>
      <td>3</td>
      <td>5</td>
      <td>Dio</td>
      <td>I</td>
      <td>D</td>
      <td>1</td>
      <td>7626</td>
    </tr>
    <tr>
      <th>9590</th>
      <td>P_VAR_330</td>
      <td>3</td>
      <td>2</td>
      <td>5</td>
      <td>1</td>
      <td>Dio</td>
      <td>I</td>
      <td>D</td>
      <td>2</td>
      <td>6349</td>
    </tr>
    <tr>
      <th>9591</th>
      <td>P_VAR_330</td>
      <td>2</td>
      <td>0</td>
      <td>4</td>
      <td>2</td>
      <td>Dio</td>
      <td>I</td>
      <td>D</td>
      <td>2</td>
      <td>9031</td>
    </tr>
    <tr>
      <th>9592</th>
      <td>P_VAR_330</td>
      <td>0</td>
      <td>2</td>
      <td>2</td>
      <td>1</td>
      <td>Dio</td>
      <td>I</td>
      <td>D</td>
      <td>2</td>
      <td>16323</td>
    </tr>
    <tr>
      <th>9593</th>
      <td>P_VAR_330</td>
      <td>0</td>
      <td>3</td>
      <td>5</td>
      <td>1</td>
      <td>Dio</td>
      <td>I</td>
      <td>D</td>
      <td>2</td>
      <td>10139</td>
    </tr>
  </tbody>
</table>
<p>9594 rows × 10 columns</p>
</div>




```python
df[(df['Dist_A'] >= 3)]
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Subject</th>
      <th>Name_A</th>
      <th>Name_B</th>
      <th>Dist_A</th>
      <th>Dist_B</th>
      <th>Mode</th>
      <th>Space</th>
      <th>Side</th>
      <th>Response</th>
      <th>RT</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>1</th>
      <td>P_ADI_331</td>
      <td>1</td>
      <td>4</td>
      <td>4</td>
      <td>1</td>
      <td>Dic</td>
      <td>E</td>
      <td>D</td>
      <td>2</td>
      <td>13157</td>
    </tr>
    <tr>
      <th>2</th>
      <td>P_ADI_331</td>
      <td>4</td>
      <td>3</td>
      <td>3</td>
      <td>2</td>
      <td>Dic</td>
      <td>E</td>
      <td>D</td>
      <td>1</td>
      <td>11628</td>
    </tr>
    <tr>
      <th>3</th>
      <td>P_ADI_331</td>
      <td>2</td>
      <td>4</td>
      <td>4</td>
      <td>1</td>
      <td>Dic</td>
      <td>E</td>
      <td>D</td>
      <td>1</td>
      <td>10068</td>
    </tr>
    <tr>
      <th>6</th>
      <td>P_ADI_331</td>
      <td>2</td>
      <td>1</td>
      <td>3</td>
      <td>4</td>
      <td>Dic</td>
      <td>E</td>
      <td>D</td>
      <td>1</td>
      <td>16347</td>
    </tr>
    <tr>
      <th>8</th>
      <td>P_ADI_331</td>
      <td>2</td>
      <td>0</td>
      <td>4</td>
      <td>2</td>
      <td>Dic</td>
      <td>E</td>
      <td>D</td>
      <td>2</td>
      <td>12589</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>9588</th>
      <td>P_VAR_330</td>
      <td>0</td>
      <td>3</td>
      <td>3</td>
      <td>1</td>
      <td>Dio</td>
      <td>I</td>
      <td>D</td>
      <td>2</td>
      <td>6153</td>
    </tr>
    <tr>
      <th>9589</th>
      <td>P_VAR_330</td>
      <td>0</td>
      <td>1</td>
      <td>3</td>
      <td>5</td>
      <td>Dio</td>
      <td>I</td>
      <td>D</td>
      <td>1</td>
      <td>7626</td>
    </tr>
    <tr>
      <th>9590</th>
      <td>P_VAR_330</td>
      <td>3</td>
      <td>2</td>
      <td>5</td>
      <td>1</td>
      <td>Dio</td>
      <td>I</td>
      <td>D</td>
      <td>2</td>
      <td>6349</td>
    </tr>
    <tr>
      <th>9591</th>
      <td>P_VAR_330</td>
      <td>2</td>
      <td>0</td>
      <td>4</td>
      <td>2</td>
      <td>Dio</td>
      <td>I</td>
      <td>D</td>
      <td>2</td>
      <td>9031</td>
    </tr>
    <tr>
      <th>9593</th>
      <td>P_VAR_330</td>
      <td>0</td>
      <td>3</td>
      <td>5</td>
      <td>1</td>
      <td>Dio</td>
      <td>I</td>
      <td>D</td>
      <td>2</td>
      <td>10139</td>
    </tr>
  </tbody>
</table>
<p>5757 rows × 10 columns</p>
</div>




```python
df[(df['Dist_A'] >= 3) & (df['Dist_B'] >= 3)]
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Subject</th>
      <th>Name_A</th>
      <th>Name_B</th>
      <th>Dist_A</th>
      <th>Dist_B</th>
      <th>Mode</th>
      <th>Space</th>
      <th>Side</th>
      <th>Response</th>
      <th>RT</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>6</th>
      <td>P_ADI_331</td>
      <td>2</td>
      <td>1</td>
      <td>3</td>
      <td>4</td>
      <td>Dic</td>
      <td>E</td>
      <td>D</td>
      <td>1</td>
      <td>16347</td>
    </tr>
    <tr>
      <th>10</th>
      <td>P_ADI_331</td>
      <td>1</td>
      <td>4</td>
      <td>4</td>
      <td>5</td>
      <td>Dic</td>
      <td>E</td>
      <td>D</td>
      <td>1</td>
      <td>14774</td>
    </tr>
    <tr>
      <th>13</th>
      <td>P_ADI_331</td>
      <td>0</td>
      <td>3</td>
      <td>4</td>
      <td>3</td>
      <td>Dic</td>
      <td>E</td>
      <td>D</td>
      <td>2</td>
      <td>14330</td>
    </tr>
    <tr>
      <th>15</th>
      <td>P_ADI_331</td>
      <td>1</td>
      <td>3</td>
      <td>4</td>
      <td>3</td>
      <td>Dic</td>
      <td>E</td>
      <td>D</td>
      <td>2</td>
      <td>10828</td>
    </tr>
    <tr>
      <th>17</th>
      <td>P_ADI_331</td>
      <td>3</td>
      <td>0</td>
      <td>3</td>
      <td>4</td>
      <td>Dic</td>
      <td>E</td>
      <td>D</td>
      <td>1</td>
      <td>10438</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>9579</th>
      <td>P_VAR_330</td>
      <td>4</td>
      <td>3</td>
      <td>3</td>
      <td>4</td>
      <td>Dio</td>
      <td>I</td>
      <td>D</td>
      <td>2</td>
      <td>12271</td>
    </tr>
    <tr>
      <th>9581</th>
      <td>P_VAR_330</td>
      <td>3</td>
      <td>2</td>
      <td>5</td>
      <td>4</td>
      <td>Dio</td>
      <td>I</td>
      <td>D</td>
      <td>1</td>
      <td>11327</td>
    </tr>
    <tr>
      <th>9582</th>
      <td>P_VAR_330</td>
      <td>1</td>
      <td>0</td>
      <td>3</td>
      <td>5</td>
      <td>Dio</td>
      <td>I</td>
      <td>D</td>
      <td>2</td>
      <td>15942</td>
    </tr>
    <tr>
      <th>9583</th>
      <td>P_VAR_330</td>
      <td>0</td>
      <td>4</td>
      <td>4</td>
      <td>5</td>
      <td>Dio</td>
      <td>I</td>
      <td>D</td>
      <td>2</td>
      <td>45627</td>
    </tr>
    <tr>
      <th>9589</th>
      <td>P_VAR_330</td>
      <td>0</td>
      <td>1</td>
      <td>3</td>
      <td>5</td>
      <td>Dio</td>
      <td>I</td>
      <td>D</td>
      <td>1</td>
      <td>7626</td>
    </tr>
  </tbody>
</table>
<p>2879 rows × 10 columns</p>
</div>




```python
rt = df['RT']
rt.median()
```




    10262.5



Donner le nombre de sujets.


```python
# subjects = df['Subject']
# subjects_uniques = subjects.drop_duplicates()
# subjects_uniques.count()
df['Subject'].drop_duplicates().count()
```




    24



Donner le nombre d’essais pour lesquels le deuxième prénom est Justin.



```python
justin_deuxieme = df[(df['Name_B'] == 3)]
justin_deuxieme['Subject'].count()
# justin_deuxieme
```




    1918



Donner le nombre d’essais ayant un temps de réponse supérieur à 20000.



```python
df[df['RT'] > 20000]['Subject'].count()
```




    943



Donner la moyenne des temps de réponse lorsque le prénom joué en premier est celui du sujet.


```python
rtm_0 = df[df['Name_A'] == 0]['RT'].mean()
rtm_g = df['RT'].mean()
(rtm_g - rtm_0) / rtm_g
```




    -0.00866465320898404



Les sujets ayant au moins une fois répondu en plus de 50000.


```python
df[df['RT'] > 50000]['Subject'].drop_duplicates()
```




    650     P_ALM_345
    1733    P_BEH_340
    2798    P_BOA_321
    3232    P_BOC_342
    3872    P_CAR_327
    4953    P_GAM_338
    6447    P_LAC_354
    7609    P_ROS_336
    8424    P_TAI_343
    9091    P_VAL_329
    9373    P_VAR_330
    Name: Subject, dtype: object




```python
df['RT'] / 10000
```




    0       1.8865
    1       1.3157
    2       1.1628
    3       1.0068
    4       1.1801
             ...  
    9589    0.7626
    9590    0.6349
    9591    0.9031
    9592    1.6323
    9593    1.0139
    Name: RT, Length: 9594, dtype: float64




```python
df[(df['Dist_A'] - df['Dist_B']) > 0]
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Subject</th>
      <th>Name_A</th>
      <th>Name_B</th>
      <th>Dist_A</th>
      <th>Dist_B</th>
      <th>Mode</th>
      <th>Space</th>
      <th>Side</th>
      <th>Response</th>
      <th>RT</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>1</th>
      <td>P_ADI_331</td>
      <td>1</td>
      <td>4</td>
      <td>4</td>
      <td>1</td>
      <td>Dic</td>
      <td>E</td>
      <td>D</td>
      <td>2</td>
      <td>13157</td>
    </tr>
    <tr>
      <th>2</th>
      <td>P_ADI_331</td>
      <td>4</td>
      <td>3</td>
      <td>3</td>
      <td>2</td>
      <td>Dic</td>
      <td>E</td>
      <td>D</td>
      <td>1</td>
      <td>11628</td>
    </tr>
    <tr>
      <th>3</th>
      <td>P_ADI_331</td>
      <td>2</td>
      <td>4</td>
      <td>4</td>
      <td>1</td>
      <td>Dic</td>
      <td>E</td>
      <td>D</td>
      <td>1</td>
      <td>10068</td>
    </tr>
    <tr>
      <th>8</th>
      <td>P_ADI_331</td>
      <td>2</td>
      <td>0</td>
      <td>4</td>
      <td>2</td>
      <td>Dic</td>
      <td>E</td>
      <td>D</td>
      <td>2</td>
      <td>12589</td>
    </tr>
    <tr>
      <th>9</th>
      <td>P_ADI_331</td>
      <td>2</td>
      <td>1</td>
      <td>4</td>
      <td>2</td>
      <td>Dic</td>
      <td>E</td>
      <td>D</td>
      <td>2</td>
      <td>10973</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>9588</th>
      <td>P_VAR_330</td>
      <td>0</td>
      <td>3</td>
      <td>3</td>
      <td>1</td>
      <td>Dio</td>
      <td>I</td>
      <td>D</td>
      <td>2</td>
      <td>6153</td>
    </tr>
    <tr>
      <th>9590</th>
      <td>P_VAR_330</td>
      <td>3</td>
      <td>2</td>
      <td>5</td>
      <td>1</td>
      <td>Dio</td>
      <td>I</td>
      <td>D</td>
      <td>2</td>
      <td>6349</td>
    </tr>
    <tr>
      <th>9591</th>
      <td>P_VAR_330</td>
      <td>2</td>
      <td>0</td>
      <td>4</td>
      <td>2</td>
      <td>Dio</td>
      <td>I</td>
      <td>D</td>
      <td>2</td>
      <td>9031</td>
    </tr>
    <tr>
      <th>9592</th>
      <td>P_VAR_330</td>
      <td>0</td>
      <td>2</td>
      <td>2</td>
      <td>1</td>
      <td>Dio</td>
      <td>I</td>
      <td>D</td>
      <td>2</td>
      <td>16323</td>
    </tr>
    <tr>
      <th>9593</th>
      <td>P_VAR_330</td>
      <td>0</td>
      <td>3</td>
      <td>5</td>
      <td>1</td>
      <td>Dio</td>
      <td>I</td>
      <td>D</td>
      <td>2</td>
      <td>10139</td>
    </tr>
  </tbody>
</table>
<p>4797 rows × 10 columns</p>
</div>




```python
df['diff_dist'] = df['Dist_A'] - df['Dist_B']
df
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Subject</th>
      <th>Name_A</th>
      <th>Name_B</th>
      <th>Dist_A</th>
      <th>Dist_B</th>
      <th>Mode</th>
      <th>Space</th>
      <th>Side</th>
      <th>Response</th>
      <th>RT</th>
      <th>diff_dist</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>P_ADI_331</td>
      <td>0</td>
      <td>2</td>
      <td>2</td>
      <td>4</td>
      <td>Dic</td>
      <td>E</td>
      <td>D</td>
      <td>2</td>
      <td>18865</td>
      <td>-2</td>
    </tr>
    <tr>
      <th>1</th>
      <td>P_ADI_331</td>
      <td>1</td>
      <td>4</td>
      <td>4</td>
      <td>1</td>
      <td>Dic</td>
      <td>E</td>
      <td>D</td>
      <td>2</td>
      <td>13157</td>
      <td>3</td>
    </tr>
    <tr>
      <th>2</th>
      <td>P_ADI_331</td>
      <td>4</td>
      <td>3</td>
      <td>3</td>
      <td>2</td>
      <td>Dic</td>
      <td>E</td>
      <td>D</td>
      <td>1</td>
      <td>11628</td>
      <td>1</td>
    </tr>
    <tr>
      <th>3</th>
      <td>P_ADI_331</td>
      <td>2</td>
      <td>4</td>
      <td>4</td>
      <td>1</td>
      <td>Dic</td>
      <td>E</td>
      <td>D</td>
      <td>1</td>
      <td>10068</td>
      <td>3</td>
    </tr>
    <tr>
      <th>4</th>
      <td>P_ADI_331</td>
      <td>1</td>
      <td>2</td>
      <td>2</td>
      <td>4</td>
      <td>Dic</td>
      <td>E</td>
      <td>D</td>
      <td>1</td>
      <td>11801</td>
      <td>-2</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>9589</th>
      <td>P_VAR_330</td>
      <td>0</td>
      <td>1</td>
      <td>3</td>
      <td>5</td>
      <td>Dio</td>
      <td>I</td>
      <td>D</td>
      <td>1</td>
      <td>7626</td>
      <td>-2</td>
    </tr>
    <tr>
      <th>9590</th>
      <td>P_VAR_330</td>
      <td>3</td>
      <td>2</td>
      <td>5</td>
      <td>1</td>
      <td>Dio</td>
      <td>I</td>
      <td>D</td>
      <td>2</td>
      <td>6349</td>
      <td>4</td>
    </tr>
    <tr>
      <th>9591</th>
      <td>P_VAR_330</td>
      <td>2</td>
      <td>0</td>
      <td>4</td>
      <td>2</td>
      <td>Dio</td>
      <td>I</td>
      <td>D</td>
      <td>2</td>
      <td>9031</td>
      <td>2</td>
    </tr>
    <tr>
      <th>9592</th>
      <td>P_VAR_330</td>
      <td>0</td>
      <td>2</td>
      <td>2</td>
      <td>1</td>
      <td>Dio</td>
      <td>I</td>
      <td>D</td>
      <td>2</td>
      <td>16323</td>
      <td>1</td>
    </tr>
    <tr>
      <th>9593</th>
      <td>P_VAR_330</td>
      <td>0</td>
      <td>3</td>
      <td>5</td>
      <td>1</td>
      <td>Dio</td>
      <td>I</td>
      <td>D</td>
      <td>2</td>
      <td>10139</td>
      <td>4</td>
    </tr>
  </tbody>
</table>
<p>9594 rows × 11 columns</p>
</div>




```python
rt_p = df[df['RT'] <= 20000]
rt_g = df[df['RT'] > 20000]
pandas.concat([rt_g,rt_p])
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Subject</th>
      <th>Name_A</th>
      <th>Name_B</th>
      <th>Dist_A</th>
      <th>Dist_B</th>
      <th>Mode</th>
      <th>Space</th>
      <th>Side</th>
      <th>Response</th>
      <th>RT</th>
      <th>diff_dist</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>400</th>
      <td>P_ALM_345</td>
      <td>4</td>
      <td>0</td>
      <td>3</td>
      <td>1</td>
      <td>Dio</td>
      <td>I</td>
      <td>D</td>
      <td>2</td>
      <td>26000</td>
      <td>2</td>
    </tr>
    <tr>
      <th>407</th>
      <td>P_ALM_345</td>
      <td>3</td>
      <td>4</td>
      <td>4</td>
      <td>2</td>
      <td>Dio</td>
      <td>I</td>
      <td>D</td>
      <td>2</td>
      <td>21956</td>
      <td>2</td>
    </tr>
    <tr>
      <th>411</th>
      <td>P_ALM_345</td>
      <td>0</td>
      <td>1</td>
      <td>4</td>
      <td>5</td>
      <td>Dio</td>
      <td>I</td>
      <td>D</td>
      <td>1</td>
      <td>27613</td>
      <td>-1</td>
    </tr>
    <tr>
      <th>428</th>
      <td>P_ALM_345</td>
      <td>3</td>
      <td>2</td>
      <td>5</td>
      <td>2</td>
      <td>Dio</td>
      <td>I</td>
      <td>D</td>
      <td>1</td>
      <td>20589</td>
      <td>3</td>
    </tr>
    <tr>
      <th>439</th>
      <td>P_ALM_345</td>
      <td>4</td>
      <td>2</td>
      <td>4</td>
      <td>5</td>
      <td>Dio</td>
      <td>I</td>
      <td>D</td>
      <td>1</td>
      <td>22400</td>
      <td>-1</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>9589</th>
      <td>P_VAR_330</td>
      <td>0</td>
      <td>1</td>
      <td>3</td>
      <td>5</td>
      <td>Dio</td>
      <td>I</td>
      <td>D</td>
      <td>1</td>
      <td>7626</td>
      <td>-2</td>
    </tr>
    <tr>
      <th>9590</th>
      <td>P_VAR_330</td>
      <td>3</td>
      <td>2</td>
      <td>5</td>
      <td>1</td>
      <td>Dio</td>
      <td>I</td>
      <td>D</td>
      <td>2</td>
      <td>6349</td>
      <td>4</td>
    </tr>
    <tr>
      <th>9591</th>
      <td>P_VAR_330</td>
      <td>2</td>
      <td>0</td>
      <td>4</td>
      <td>2</td>
      <td>Dio</td>
      <td>I</td>
      <td>D</td>
      <td>2</td>
      <td>9031</td>
      <td>2</td>
    </tr>
    <tr>
      <th>9592</th>
      <td>P_VAR_330</td>
      <td>0</td>
      <td>2</td>
      <td>2</td>
      <td>1</td>
      <td>Dio</td>
      <td>I</td>
      <td>D</td>
      <td>2</td>
      <td>16323</td>
      <td>1</td>
    </tr>
    <tr>
      <th>9593</th>
      <td>P_VAR_330</td>
      <td>0</td>
      <td>3</td>
      <td>5</td>
      <td>1</td>
      <td>Dio</td>
      <td>I</td>
      <td>D</td>
      <td>2</td>
      <td>10139</td>
      <td>4</td>
    </tr>
  </tbody>
</table>
<p>9594 rows × 11 columns</p>
</div>




```python

```
