# Notebook séance 12


```python
%load_ext sql
%config SqlMagic.dsn_filename = "connections.ini"
%sql --section bd-pedago
%sql --connections
import pandas
import matplotlib.pyplot as plt
import matplotlib as mpl
```


<span style="None">Connecting to &#x27;bd-pedago&#x27;</span>



```sql
%%sql
select * from dfj
```


<span style="None">Running query in &#x27;bd-pedago&#x27;</span>



<span style="color: green">9594 rows affected.</span>





<table>
    <thead>
        <tr>
            <th>index</th>
            <th>Subject</th>
            <th>Name_A</th>
            <th>Name_B</th>
            <th>Dist_A</th>
            <th>Dist_B</th>
            <th>Mode</th>
            <th>Space</th>
            <th>Side</th>
            <th>Response</th>
            <th>RT</th>
            <th>juste</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>0</td>
            <td>P_ADI_331</td>
            <td>0</td>
            <td>2</td>
            <td>2</td>
            <td>4</td>
            <td>Dic</td>
            <td>E</td>
            <td>D</td>
            <td>2</td>
            <td>18865</td>
            <td>False</td>
        </tr>
        <tr>
            <td>1</td>
            <td>P_ADI_331</td>
            <td>1</td>
            <td>4</td>
            <td>4</td>
            <td>1</td>
            <td>Dic</td>
            <td>E</td>
            <td>D</td>
            <td>2</td>
            <td>13157</td>
            <td>True</td>
        </tr>
        <tr>
            <td>2</td>
            <td>P_ADI_331</td>
            <td>4</td>
            <td>3</td>
            <td>3</td>
            <td>2</td>
            <td>Dic</td>
            <td>E</td>
            <td>D</td>
            <td>1</td>
            <td>11628</td>
            <td>False</td>
        </tr>
        <tr>
            <td>3</td>
            <td>P_ADI_331</td>
            <td>2</td>
            <td>4</td>
            <td>4</td>
            <td>1</td>
            <td>Dic</td>
            <td>E</td>
            <td>D</td>
            <td>1</td>
            <td>10068</td>
            <td>False</td>
        </tr>
        <tr>
            <td>4</td>
            <td>P_ADI_331</td>
            <td>1</td>
            <td>2</td>
            <td>2</td>
            <td>4</td>
            <td>Dic</td>
            <td>E</td>
            <td>D</td>
            <td>1</td>
            <td>11801</td>
            <td>True</td>
        </tr>
        <tr>
            <td>5</td>
            <td>P_ADI_331</td>
            <td>2</td>
            <td>1</td>
            <td>2</td>
            <td>3</td>
            <td>Dic</td>
            <td>E</td>
            <td>D</td>
            <td>2</td>
            <td>12117</td>
            <td>False</td>
        </tr>
        <tr>
            <td>6</td>
            <td>P_ADI_331</td>
            <td>2</td>
            <td>1</td>
            <td>3</td>
            <td>4</td>
            <td>Dic</td>
            <td>E</td>
            <td>D</td>
            <td>1</td>
            <td>16347</td>
            <td>True</td>
        </tr>
        <tr>
            <td>7</td>
            <td>P_ADI_331</td>
            <td>0</td>
            <td>3</td>
            <td>2</td>
            <td>4</td>
            <td>Dic</td>
            <td>E</td>
            <td>D</td>
            <td>1</td>
            <td>13237</td>
            <td>True</td>
        </tr>
        <tr>
            <td>8</td>
            <td>P_ADI_331</td>
            <td>2</td>
            <td>0</td>
            <td>4</td>
            <td>2</td>
            <td>Dic</td>
            <td>E</td>
            <td>D</td>
            <td>2</td>
            <td>12589</td>
            <td>True</td>
        </tr>
        <tr>
            <td>9</td>
            <td>P_ADI_331</td>
            <td>2</td>
            <td>1</td>
            <td>4</td>
            <td>2</td>
            <td>Dic</td>
            <td>E</td>
            <td>D</td>
            <td>2</td>
            <td>10973</td>
            <td>True</td>
        </tr>
    </tbody>
</table>
<span style="font-style:italic;text-align:center;">Truncated to <a href="https://jupysql.ploomber.io/en/latest/api/configuration.html#displaylimit">displaylimit</a> of 10.</span>




```sql
%%sql
with diff_dist as 
(SELECT *, ("Dist_A" - "Dist_B") as dd
 from dfj)
select dd, juste, avg("RT")
from diff_dist
group by dd, juste
```


<span style="None">Running query in &#x27;bd-pedago&#x27;</span>



<span style="color: green">16 rows affected.</span>





<table>
    <thead>
        <tr>
            <th>dd</th>
            <th>juste</th>
            <th>avg</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>2</td>
            <td>False</td>
            <td>15788.223048327138</td>
        </tr>
        <tr>
            <td>1</td>
            <td>True</td>
            <td>12531.617410387710</td>
        </tr>
        <tr>
            <td>-4</td>
            <td>False</td>
            <td>16226.410256410256</td>
        </tr>
        <tr>
            <td>-4</td>
            <td>True</td>
            <td>9553.0997732426303855</td>
        </tr>
        <tr>
            <td>2</td>
            <td>True</td>
            <td>10986.662393162393</td>
        </tr>
        <tr>
            <td>-2</td>
            <td>True</td>
            <td>11214.590833333333</td>
        </tr>
        <tr>
            <td>-1</td>
            <td>False</td>
            <td>15092.825127334465</td>
        </tr>
        <tr>
            <td>4</td>
            <td>True</td>
            <td>10295.532438478747</td>
        </tr>
        <tr>
            <td>-3</td>
            <td>False</td>
            <td>16255.831932773109</td>
        </tr>
        <tr>
            <td>-3</td>
            <td>True</td>
            <td>10463.882282996433</td>
        </tr>
    </tbody>
</table>
<span style="font-style:italic;text-align:center;">Truncated to <a href="https://jupysql.ploomber.io/en/latest/api/configuration.html#displaylimit">displaylimit</a> of 10.</span>




```sql
%%sql
select ("Dist_A" - "Dist_B") as dd, juste, avg("RT")
from dfj
group by dd, juste
```


<span style="None">Running query in &#x27;bd-pedago&#x27;</span>



<span style="color: green">16 rows affected.</span>





<table>
    <thead>
        <tr>
            <th>dd</th>
            <th>juste</th>
            <th>avg</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>2</td>
            <td>False</td>
            <td>15788.223048327138</td>
        </tr>
        <tr>
            <td>1</td>
            <td>True</td>
            <td>12531.617410387710</td>
        </tr>
        <tr>
            <td>-4</td>
            <td>False</td>
            <td>16226.410256410256</td>
        </tr>
        <tr>
            <td>-4</td>
            <td>True</td>
            <td>9553.0997732426303855</td>
        </tr>
        <tr>
            <td>2</td>
            <td>True</td>
            <td>10986.662393162393</td>
        </tr>
        <tr>
            <td>-2</td>
            <td>True</td>
            <td>11214.590833333333</td>
        </tr>
        <tr>
            <td>-1</td>
            <td>False</td>
            <td>15092.825127334465</td>
        </tr>
        <tr>
            <td>4</td>
            <td>True</td>
            <td>10295.532438478747</td>
        </tr>
        <tr>
            <td>-3</td>
            <td>False</td>
            <td>16255.831932773109</td>
        </tr>
        <tr>
            <td>-3</td>
            <td>True</td>
            <td>10463.882282996433</td>
        </tr>
    </tbody>
</table>
<span style="font-style:italic;text-align:center;">Truncated to <a href="https://jupysql.ploomber.io/en/latest/api/configuration.html#displaylimit">displaylimit</a> of 10.</span>




```python
dfs = %sql select ("Dist_A" - "Dist_B") as dd, juste, avg("RT") from dfj group by dd, juste
```


<span style="None">Running query in &#x27;bd-pedago&#x27;</span>



<span style="color: green">16 rows affected.</span>



```python
dfg = dfs.DataFrame()
dfg
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>dd</th>
      <th>juste</th>
      <th>avg</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>2</td>
      <td>False</td>
      <td>15788.223048327138</td>
    </tr>
    <tr>
      <th>1</th>
      <td>1</td>
      <td>True</td>
      <td>12531.617410387710</td>
    </tr>
    <tr>
      <th>2</th>
      <td>-4</td>
      <td>False</td>
      <td>16226.410256410256</td>
    </tr>
    <tr>
      <th>3</th>
      <td>-4</td>
      <td>True</td>
      <td>9553.0997732426303855</td>
    </tr>
    <tr>
      <th>4</th>
      <td>2</td>
      <td>True</td>
      <td>10986.662393162393</td>
    </tr>
    <tr>
      <th>5</th>
      <td>-2</td>
      <td>True</td>
      <td>11214.590833333333</td>
    </tr>
    <tr>
      <th>6</th>
      <td>-1</td>
      <td>False</td>
      <td>15092.825127334465</td>
    </tr>
    <tr>
      <th>7</th>
      <td>4</td>
      <td>True</td>
      <td>10295.532438478747</td>
    </tr>
    <tr>
      <th>8</th>
      <td>-3</td>
      <td>False</td>
      <td>16255.831932773109</td>
    </tr>
    <tr>
      <th>9</th>
      <td>-3</td>
      <td>True</td>
      <td>10463.882282996433</td>
    </tr>
    <tr>
      <th>10</th>
      <td>3</td>
      <td>False</td>
      <td>13009.027272727273</td>
    </tr>
    <tr>
      <th>11</th>
      <td>-2</td>
      <td>False</td>
      <td>15560.310924369748</td>
    </tr>
    <tr>
      <th>12</th>
      <td>-1</td>
      <td>True</td>
      <td>12509.724812030075</td>
    </tr>
    <tr>
      <th>13</th>
      <td>1</td>
      <td>False</td>
      <td>14076.217785843920</td>
    </tr>
    <tr>
      <th>14</th>
      <td>3</td>
      <td>True</td>
      <td>10506.656470588235</td>
    </tr>
    <tr>
      <th>15</th>
      <td>4</td>
      <td>False</td>
      <td>11753.484848484848</td>
    </tr>
  </tbody>
</table>
</div>




```python
sj = (dfg[dfg["juste"]]
      .set_index("dd")
      .sort_index()
      ["avg"]
     )
sf = (dfg[~dfg["juste"]]
      .set_index("dd")
      .sort_index()
      ["avg"]
     )
fig, ax = plt.subplots()
ax.set_xlabel("Différence de distance (symbolique)")
ax.set_ylabel("Temps de réaction moyen ($10^{-4}$ s)")
ax.plot(sj, label="rép. justes", color="green", linestyle="-")
ax.plot(sf, label="rép. fausses", color="orange", linestyle("--"))
ax.axis([-4,4,0,17000])
ax.legend()
None
```


    
![png](output_7_0.png)
    


Documentation matplotlib: <https://matplotlib.org/stable/users/explain/quick_start.html>
