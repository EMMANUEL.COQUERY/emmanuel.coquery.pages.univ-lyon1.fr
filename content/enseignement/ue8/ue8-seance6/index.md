# Notebook séance 6


```python
import pandas
df = pandas.read_excel("Donnees_M2_RD.xlsx")
```

## Histogramme

Faire un histogramme de la quantité d’essais selon le temps de réaction.

On fait l'histogramme sur la série qui contient uniquement les `RT`, car le tracé sur la dataframe est trop long.


```python
df["RT"].plot.hist(bins=10)
```




    <AxesSubplot:ylabel='Frequency'>




    
![png](output_3_1.png)
    


## Ajout des réussites


```python
reussites = (
    ((df['Response'] == 1) & (df['Dist_A'] < df['Dist_B'])) 
  | ((df['Response'] == 2) & (df['Dist_A'] > df['Dist_B']))
)
df["reussit"] = reussites
```

## Bar

Créer une dataframe contenant les essais en mode Dic(hotique) à gauche et enfaire une copie via

```python
df_copie = pandas.DataFrame(df_a_copier)
```

Ajouter un attribut `mode_side` contenant la valeur `"Dic_G"`

En créer deux autres versions: une pour `Dic` à droite et une pour `Dio` (tique) (sans différencier les côtés)

Assembler les 3 dataframes obtenues.

Faire un affichage de barres avec le nombre d’essais réussis (filtrer et compter).



```python
df_d = pandas.DataFrame(df[(df["Mode"] ==  "Dic") & (df["Side"] == "D")])
df_d["mode_side"] = "Dic_D"
df_g = pandas.DataFrame(df[(df["Mode"] ==  "Dic") & (df["Side"] == "G")])
df_g["mode_side"] = "Dic_G"
df_dio = pandas.DataFrame(df[df["Mode"] ==  "Dio"])
df_dio["mode_side"] = "Dio"
df_mode = pandas.concat([df_d, df_g, df_dio])
s_reussits_mode_side = df_mode[df_mode["reussit"]].groupby(by="mode_side")["Subject"].count()
s_mode_side = df_mode.groupby(by="mode_side")["Subject"].count()
df_norm = pandas.DataFrame(
    {
        "total": s_mode_side, 
        "reussits": s_reussits_mode_side
    }
)
df_norm["reussits_n"] = df_norm["reussits"] / df_norm["total"]
df_norm["reussits_n"].plot.bar()
```




    <AxesSubplot:xlabel='mode_side'>




    
![png](output_7_1.png)
    


## Changements d'indexes

Obtenir la DataFrame indexée par Subject et Space


```python
df.set_index(["Subject","Space"])
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th></th>
      <th>Name_A</th>
      <th>Name_B</th>
      <th>Dist_A</th>
      <th>Dist_B</th>
      <th>Mode</th>
      <th>Side</th>
      <th>Response</th>
      <th>RT</th>
      <th>reussit</th>
    </tr>
    <tr>
      <th>Subject</th>
      <th>Space</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th rowspan="5" valign="top">P_ADI_331</th>
      <th>E</th>
      <td>0</td>
      <td>2</td>
      <td>2</td>
      <td>4</td>
      <td>Dic</td>
      <td>D</td>
      <td>2</td>
      <td>18865</td>
      <td>False</td>
    </tr>
    <tr>
      <th>E</th>
      <td>1</td>
      <td>4</td>
      <td>4</td>
      <td>1</td>
      <td>Dic</td>
      <td>D</td>
      <td>2</td>
      <td>13157</td>
      <td>True</td>
    </tr>
    <tr>
      <th>E</th>
      <td>4</td>
      <td>3</td>
      <td>3</td>
      <td>2</td>
      <td>Dic</td>
      <td>D</td>
      <td>1</td>
      <td>11628</td>
      <td>False</td>
    </tr>
    <tr>
      <th>E</th>
      <td>2</td>
      <td>4</td>
      <td>4</td>
      <td>1</td>
      <td>Dic</td>
      <td>D</td>
      <td>1</td>
      <td>10068</td>
      <td>False</td>
    </tr>
    <tr>
      <th>E</th>
      <td>1</td>
      <td>2</td>
      <td>2</td>
      <td>4</td>
      <td>Dic</td>
      <td>D</td>
      <td>1</td>
      <td>11801</td>
      <td>True</td>
    </tr>
    <tr>
      <th>...</th>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th rowspan="5" valign="top">P_VAR_330</th>
      <th>I</th>
      <td>0</td>
      <td>1</td>
      <td>3</td>
      <td>5</td>
      <td>Dio</td>
      <td>D</td>
      <td>1</td>
      <td>7626</td>
      <td>True</td>
    </tr>
    <tr>
      <th>I</th>
      <td>3</td>
      <td>2</td>
      <td>5</td>
      <td>1</td>
      <td>Dio</td>
      <td>D</td>
      <td>2</td>
      <td>6349</td>
      <td>True</td>
    </tr>
    <tr>
      <th>I</th>
      <td>2</td>
      <td>0</td>
      <td>4</td>
      <td>2</td>
      <td>Dio</td>
      <td>D</td>
      <td>2</td>
      <td>9031</td>
      <td>True</td>
    </tr>
    <tr>
      <th>I</th>
      <td>0</td>
      <td>2</td>
      <td>2</td>
      <td>1</td>
      <td>Dio</td>
      <td>D</td>
      <td>2</td>
      <td>16323</td>
      <td>True</td>
    </tr>
    <tr>
      <th>I</th>
      <td>0</td>
      <td>3</td>
      <td>5</td>
      <td>1</td>
      <td>Dio</td>
      <td>D</td>
      <td>2</td>
      <td>10139</td>
      <td>True</td>
    </tr>
  </tbody>
</table>
<p>9594 rows × 9 columns</p>
</div>



Obtenir la DataFrame indexée par Subject, Space et l’index original avec un niveau nommé essai (on peut utiliser `.rename` sur un index)


```python
df.set_index(["Subject","Space", df.index.rename("essai")])
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th></th>
      <th></th>
      <th>Name_A</th>
      <th>Name_B</th>
      <th>Dist_A</th>
      <th>Dist_B</th>
      <th>Mode</th>
      <th>Side</th>
      <th>Response</th>
      <th>RT</th>
      <th>reussit</th>
    </tr>
    <tr>
      <th>Subject</th>
      <th>Space</th>
      <th>essai</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th rowspan="5" valign="top">P_ADI_331</th>
      <th rowspan="5" valign="top">E</th>
      <th>0</th>
      <td>0</td>
      <td>2</td>
      <td>2</td>
      <td>4</td>
      <td>Dic</td>
      <td>D</td>
      <td>2</td>
      <td>18865</td>
      <td>False</td>
    </tr>
    <tr>
      <th>1</th>
      <td>1</td>
      <td>4</td>
      <td>4</td>
      <td>1</td>
      <td>Dic</td>
      <td>D</td>
      <td>2</td>
      <td>13157</td>
      <td>True</td>
    </tr>
    <tr>
      <th>2</th>
      <td>4</td>
      <td>3</td>
      <td>3</td>
      <td>2</td>
      <td>Dic</td>
      <td>D</td>
      <td>1</td>
      <td>11628</td>
      <td>False</td>
    </tr>
    <tr>
      <th>3</th>
      <td>2</td>
      <td>4</td>
      <td>4</td>
      <td>1</td>
      <td>Dic</td>
      <td>D</td>
      <td>1</td>
      <td>10068</td>
      <td>False</td>
    </tr>
    <tr>
      <th>4</th>
      <td>1</td>
      <td>2</td>
      <td>2</td>
      <td>4</td>
      <td>Dic</td>
      <td>D</td>
      <td>1</td>
      <td>11801</td>
      <td>True</td>
    </tr>
    <tr>
      <th>...</th>
      <th>...</th>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th rowspan="5" valign="top">P_VAR_330</th>
      <th rowspan="5" valign="top">I</th>
      <th>9589</th>
      <td>0</td>
      <td>1</td>
      <td>3</td>
      <td>5</td>
      <td>Dio</td>
      <td>D</td>
      <td>1</td>
      <td>7626</td>
      <td>True</td>
    </tr>
    <tr>
      <th>9590</th>
      <td>3</td>
      <td>2</td>
      <td>5</td>
      <td>1</td>
      <td>Dio</td>
      <td>D</td>
      <td>2</td>
      <td>6349</td>
      <td>True</td>
    </tr>
    <tr>
      <th>9591</th>
      <td>2</td>
      <td>0</td>
      <td>4</td>
      <td>2</td>
      <td>Dio</td>
      <td>D</td>
      <td>2</td>
      <td>9031</td>
      <td>True</td>
    </tr>
    <tr>
      <th>9592</th>
      <td>0</td>
      <td>2</td>
      <td>2</td>
      <td>1</td>
      <td>Dio</td>
      <td>D</td>
      <td>2</td>
      <td>16323</td>
      <td>True</td>
    </tr>
    <tr>
      <th>9593</th>
      <td>0</td>
      <td>3</td>
      <td>5</td>
      <td>1</td>
      <td>Dio</td>
      <td>D</td>
      <td>2</td>
      <td>10139</td>
      <td>True</td>
    </tr>
  </tbody>
</table>
<p>9594 rows × 9 columns</p>
</div>


