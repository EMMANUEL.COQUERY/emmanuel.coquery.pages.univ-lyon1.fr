---
# Leave the homepage title empty to use the site title
title: ""
date: 2022-10-24
type: landing

sections:
  - block: about.biography
    id: about
    content:
      title: "" # Emmanuel Coquery
      # Choose a user profile to display (a folder name within `content/authors/`)
      username: admin
  # - block: collection
  #   id: posts
  #   content:
  #     title: Recent Posts
  #     subtitle: ""
  #     text: ""
  #     # Choose how many pages you would like to display (0 = all pages)
  #     count: 5
  #     # Filter on criteria
  #     filters:
  #       folders:
  #         - post
  #       author: ""
  #       category: ""
  #       tag: ""
  #       exclude_featured: false
  #       exclude_future: false
  #       exclude_past: false
  #       publication_type: ""
  #     # Choose how many pages you would like to offset by
  #     offset: 0
  #     # Page order: descending (desc) or ascending (asc) date.
  #     order: desc
  #   design:
  #     # Choose a layout view
  #     view: compact
  #     columns: "2"
  - block: portfolio
    id: projects
    content:
      title: Sujet de stages / de projet
      filters:
        folders:
          - project
      # Default filter index (e.g. 0 corresponds to the first `filter_button` instance below).
      default_button_index: 0
      # Filter toolbar (optional).
      # Add or remove as many filters (`filter_button` instances) as you like.
      # To show all items, set `tag` to "*".
      # To filter by a specific tag, set `tag` to an existing tag name.
      # To remove the toolbar, delete the entire `filter_button` block.
      buttons:
        - name: All
          tag: "*"
        - name: POM
          tag: pom
    design:
      # Choose how many columns the section has. Valid values: '1' or '2'.
      columns: "1"
      view: showcase
      # For Showcase view, flip alternate rows?
      flip_alt_rows: false
  # - block: markdown
  #   content:
  #     title: Gallery
  #     subtitle: ""
  #     text: |-
  #       {{< gallery album="demo" >}}
  #   design:
  #     columns: "1"
  - block: tag_cloud
    content:
      title: Popular Topics
    design:
      columns: "2"
  - block: contact
    id: contact
    content:
      title: Contact
      subtitle:
      # text: |-
      #   Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam mi diam, venenatis ut magna et, vehicula efficitur enim.
      # Contact (add or remove contact options as necessary)
      email: "emmanuel.coquery@univ-lyon1.fr"
      phone: "+33 4 72 44 58 25"
      # appointment_url: "https://calendly.com"
      address:
        street: |-
          Université Claude Bernard Lyon 1,
          Bâtiment Nautibus,
          23-25 Avenue Pierre de Coubertin
        city: "Villeurbanne"
        region: ""
        postcode: "69622 Cedex"
        country: "France"
        country_code: "FR"
      # directions: ""
      # office_hours:
      #   - "Monday 10:00 to 13:00"
      #   - "Wednesday 09:00 to 10:00"
      # Choose a map provider in `params.yaml` to show a map from these coordinates
      coordinates:
        latitude: "45.78215"
        longitude: "4.86572"
      # contact_links:
      #   - icon: twitter
      #     icon_pack: fab
      #     name: DM Me
      #     link: "https://twitter.com/Twitter"
      #   - icon: skype
      #     icon_pack: fab
      #     name: Skype Me
      #     link: "skype:echo123?call"
      #   - icon: video
      #     icon_pack: fas
      #     name: Zoom Me
      #     link: "https://zoom.com"
      # Automatically link email and phone or display as text?
      autolink: true
      # Email form provider
      # form:
      #   provider: netlify
      #   formspree:
      #     id:
      #   netlify:
      #     # Enable CAPTCHA challenge to reduce spam?
      #     captcha: false
    design:
      columns: "2"
---
