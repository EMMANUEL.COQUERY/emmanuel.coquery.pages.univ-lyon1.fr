---
# Display name
title: Emmanuel Coquery

# Full name (for SEO)
first_name: Emmanuel
last_name: Coquery

# Status emoji
status:
  icon: ☕️

# Is this the primary user of the site?
superuser: true

# Role/position
role: Maître de conférences en Informatique

# Organizations/Affiliations
organizations:
  - name: LIRIS
    url: "https://liris.cnrs.fr"
  - name: Université Claude Bernard Lyon 1
    url: "https://www.univ-lyon1.fr"

# Short bio (displayed in user profile at end of posts)
# bio: My research interests include distributed robotics, mobile computing and programmable matter.

interests:
  - Experimentations in computer science
  - Security policies (languages, inference)
  - Data management
  - Declarative approaches and languages for data mining
  - Service specification and composition
  - Languages and types

education:
  courses:
    - course: PhD in Computer Science
      institution: INRIA et Université Paris 6
      year: 2004

# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/widgets/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.
social:
  - icon: envelope
    icon_pack: fas
    link: "#contact" # For a direct email link, use "mailto:test@example.org".
  #- icon: twitter
  #  icon_pack: fab
  #  link: https://twitter.com/GeorgeCushen
  #- icon: google-scholar
  #  icon_pack: ai
  #  link: https://scholar.google.co.uk/citations?user=sIwtMXoAAAAJ
  #- icon: github
  #  icon_pack: fab
  #  link: https://github.com/gcushen
  # Link to a PDF of your resume/CV from the About widget.
  # To enable, copy your resume/CV to `static/files/cv.pdf` and uncomment the lines below.
  #- icon: cv
  #  icon_pack: ai
  #  link: files/cv.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: ""
# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
# user_groups:
#   - Researchers
#   - Visitors
---

Je suis enseignant-chercheur au [département informatique](https://fst-informatique.univ-lyon1.fr) de l'[Université Lyon 1](https://www.univ-lyon1.fr),
membre de l'équipe [Bases de Données](https://projet.liris.cnrs.fr/bd/) du [LIRIS](https://liris.cnrs.fr/).

[Ancienne page](http://emmanuel.coquery.pages.univ-lyon1.fr)
