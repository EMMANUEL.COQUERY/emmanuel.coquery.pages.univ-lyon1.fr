---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Preuves en Coq pour la récriture de requêtes"
summary: "Écriture d'une bibliothèque de récriture de requêtes dont certaines propriétés sont prouvées en Coq"
authors: []
tags: ["bd", "coq", "pom"]
categories: ["pom"]
date: 2021-10-13T10:00:00

# Optional external URL for project (replaces project detail page).
external_link: ""

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides: ""
---

Dans ce [POM][pom], on s'intéresse à la récriture de requêtes qui consiste à reformuler une requête en utilisant d'autre relations.
Ces techniques de récriture trouvent des applications en intégration de données et en sécurité.

Concrètement, on souhaite porter une bibliothèque de récriture écrite en [OCaml][ocaml] vers [Coq][coq], puis de prouver quelques propriétés sur le code obtenu.
L'objectif étant ambitieux, on se concentrera sur certaines briques de bases: représentation des requêtes, pattern-matching et unification sur des morceaux requête.

[ocaml]: https://ocaml.org/
[coq]: https://coq.inria.fr/
[pom]: https://perso.liris.cnrs.fr/sbrandel/wiki/doku.php?id=ens:pom
