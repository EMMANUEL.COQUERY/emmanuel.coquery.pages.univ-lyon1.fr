---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Mise à jour incrémentale pour les permissions RDF"
summary: "Mise au point et implémentation d'un algorithme incrémental pour la mise à jour des permissions fines sur une base de données RDF"
authors: []
tags: ["rdf", "access control", "pom"]
categories: ["pom"]
date: 2021-10-13T10:00:00

# Optional external URL for project (replaces project detail page).
external_link: ""

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides: ""
---

Ce projet [POM][pom] s'inscrit dans le cadre d'une implémentation de contrôle d'accès fin sur un _store_ RDF (ici [Jena][jena]).
Dans sa thèse, Tarek Sayah, a mis au point un systèmede contrôle d'accès sur des données RDF permettant de prendre en compte les règles de raisonnements de type OWL-RL.
Il a produit une implémentation sur la base du moteur de requêtes et de stockage [Jena][jena].
Cette implémentation ne supporte cependant pas les mises à jour de la base (les permissions doivent être entièrement recalculées si on met à jour les données).

L'objectif de ce projet [POM][pom] consiste à concevoir et implémenter un système de calcul _incrémental_ des permissions sur les triplets.

[pom]: https://perso.liris.cnrs.fr/sbrandel/wiki/doku.php?id=ens:pom
[jena]: https://jena.apache.org/documentation/rdf/

[pom]
[jena]
