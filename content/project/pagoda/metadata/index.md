---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Métadonnées pour PAGoDA"
summary: "Élaboration d'un modèle de méta-données pour PAGoDA et implémentation prototype"
authors: []
tags: ["rdf", "pagoda", "pom"]
categories: ["pom"]
date: 2020-12-03T21:05:22+01:00

# Optional external URL for project (replaces project detail page).
external_link: ""

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides: ""
---

PAGoDA est une infrastructure calcul/stockage en développement au laboratoire [LIRIS][liris].
Dans cette infrastructure, les chercheurs vont déployer des _workflow_ de calcul qui vont utiliser et générer des données scientifiques.
Ces données étant particulièrement précieuses pour le travail des chercheurs, il est important de pouvoir les cataloguer afin de pouvoir les préserver, les retrouver et les rendre accessibles.
Cela nécessite que l'infrastructure de PAGoDA, et en particulier son serveur d'administration, soit capable de gérer des méta-données associées à ces données.

La plateforme [PerSCiDO][perscido] propose un modèle de méta-données RDF pour les jeux de données scientifiques.
Ce modèle pourrait servir de point de départ pour celui de PAGoDA qui lui ajouterait des informations internes supplémentaires comme des références vers les volumes de stockages utlisés par la plateforme ou encore des informations concernant la politique de sauvegarde à appliquer à ces données.

L'objectif de ce [POM][pom] consiste à proposer un modèle de méta-données pour PAGoDA et à prototyper un système permettant de gérer les métadonnées au sein du serveur d'administration de PAGoDA.

Technologies utilisées:

- [Blazegraph][blazegraph] pour le stockage de données RDF et [PostgreSQL][postgresql] pour le stockage de données relationnelles
- [Python/Flask][flask] pour l'implémentation du serveur d'administration de PAGoDA
- [Kubernetes][kubernetes] et [Openstack][openstack] qui sont les orchestrateurs utilisés par PAGoDA

[liris]: https://liris.cnrs.fr/
[perscido]: https://persyval-platform.imag.fr/
[blazegraph]: https://blazegraph.com/
[postgresql]: https://www.postgresql.org/
[flask]: https://flask.palletsprojects.com/en/1.1.x/
[openstack]: https://www.openstack.org/
[kubernetes]: https://kubernetes.io/
[pom]: https://perso.liris.cnrs.fr/sbrandel/wiki/doku.php?id=ens:pom
