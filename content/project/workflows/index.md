---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Workflows scientifiques pour l'expérimentation en informatique"
summary: "Créer un langage de workflow permettant de réaliser des expériences numériques"
authors: []
tags: ["workflow", "science ouverte", "reproductibilité", "pom"]
categories: ["pom"]
date: 2021-10-13T10:00:00

# Optional external URL for project (replaces project detail page).
external_link: ""

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides: ""
---

Dans ce projet [POM][pom], on s'intéresse à élaborer un langage de worflow permettant de réaliser des expériences numériques,
par exemple pour tester la précision des réponses d'un nouvel algorithme.
Un des points importants dans ce type de workflow est de fournir des éléments de traçabilité:
on souhaite par exemple garder une trace des données qui ont circulé dans le workflow.
Cela permet de reproduire les expérimentations pour en vérifier les résultats.

Dans un premier temps on identifiera les bonnes caractéristiques de ces workflows et on proposera un langage permttant de décrire des workflows d'expérimentation numérique.
Dans un deuxième temps on proposera une implméntation de ce langage en le compilant vers des workflows [Argo Workflows][argo].

[argo]: https://argoproj.github.io/argo-workflows/
[pom]: https://perso.liris.cnrs.fr/sbrandel/wiki/doku.php?id=ens:pom
